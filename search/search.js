/* This autocomplete function shows suggestion of matches based on key entries */
$(function() {
    $("input#name").autocomplete({
        source: "../search/backend-search.php",
        minLength: 2,
        select: function (event, ui) {
            var item = ui.item;
            if(item) {
                $("#email").val(item.email);
                $("#crsid").val(item.crsid);
                $("#phone").val(item.phone);
            }
         }
    });
    // Place value to the input fields
    $(document).on("click", function(){
        $('input#name').html($('#name').val());
        $('input#crsid').html($('#crsid').val());
    });
});