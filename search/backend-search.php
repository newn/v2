<?php
require_once("../includes/conf.php");
require_once("../includes/con_db.php");
// Attempt search query execution
$return_arr = array();
try{
    if(isset($_GET['term'])){
        // create prepared statement
        $sql = "SELECT * FROM user_details WHERE name LIKE :term AND inactive=0";
        $stmt = $db->prepare($sql);
        $term = '%'.$_GET['term'].'%';
        // bind parameters to statement
        $stmt->bindParam(':term', $term);
        // execute the prepared statement
        $stmt->execute();
        if($stmt->rowCount() > 0){
            while($row = $stmt->fetch()){
                //echo "<p>".$row['name']." (".$row['crsid'].")</p>";
                // Store values in a row
                $row_array['label'] = $row['name']." (".$row['crsid'].")";
                $row_array['value'] = $row['name'];
                $row_array['email'] = $row['eadd'];
                $row_array['crsid'] = $row['crsid'];
                $row_array['phone'] = $row['phone'];
                array_push( $return_arr, $row_array );

            }
            echo json_encode($return_arr),"\n";
        } else{
            echo "<p>No matches found</p>";
        }
    }  
} catch(PDOException $e){
    die("ERROR: Could not able to execute $sql. " . $e->getMessage());
}
?>