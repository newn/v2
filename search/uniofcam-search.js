/* This autocomplete function shows suggestion of matches based on key entries */
$(function() {
    $("#cam_name").autocomplete({
        source: "../search/uniofcam-backend.php",
		minLength: 2,
		select: function (event, ui) {
	        var item = ui.item;
	        if(item) {
                $("#cam_assoc").val(item.cam_assoc);
                $("#cam_email").val(item.cam_email);
                $("#cam_crsid").val(item.cam_crsid);
                $("#cam_phone").val(item.cam_phone);
                $("#cam_groups").val(item.cam_groups);
            }
         }
    })
});