<?php
// Fetch user photo using UIS Card Office API
if(isset($_GET['getphoto'])) {
    $photo = 'https://getphoto:T0adP0rch@webservices.admin.cam.ac.uk/ucws/get_photo/index.cgi/'.$_GET['crsid'].'.jpeg';
    $photo = @file_get_contents($photo);
    if ($photo !== FALSE) {
    	$photoData = base64_encode($photo);
    	echo '<img src="data:image/jpeg;base64,'.$photoData.'" width="120px">';
    } else {
    	echo "Photo unavailable";
    }
}
?>