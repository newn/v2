<?php
require_once("../includes/conf.php");

// UniofCam search using the UIS Lookup API
$q=$_GET['term'];

$return_arr = array();

$search = file_get_contents("https://".$lkup_usr.":".$lkup_pwd."@www.lookup.cam.ac.uk/api/v1/person/search?query=".$q."&fetch=all_attrs,all_groups&format=json");
$search = json_decode($search, TRUE);

foreach ($search['result']['people'] as $person) {   
    // Get the person's crsid
    $cam_crsid = $person['identifier']['value'];
    // Get the person's name
    $cam_name=""; if (!empty($person['displayName'])) { $cam_name = $person['displayName']; } else { $cam_name = $person['registeredName']; } 
    // Identify if user is staff or student
    $cam_assoc = "";
    if ($person['staff'] == "1") { $cam_assoc = "staff"; }
    if ($person['student'] == "1") { $cam_assoc = "student"; }
    // Get the person's main email
    $cam_email = "";
    $emails = array();
    foreach($person['attributes'] as $attr) {
        if($attr['scheme'] == "email") {
            $emails[] = $attr['value'];
        }
    }
    if(empty($emails)) { $cam_email = $cam_crsid . "@cam.ac.uk"; } else { $cam_email = $emails[0]; }
    // Get the person's phone
    $cam_phone = "";
    $phones = array();
    foreach($person['attributes'] as $attr) {
        if($attr['scheme'] == "universityPhone") {
            $phones[] = $attr['value'];
        }
    }
    if(!empty($phones)) { $cam_phone = $phones[0]; }
    // Get the person's group memberships
    $cam_groups = "";
    $groups = array();
    foreach($person['groups'] as $group) {
        $groups[] = $group['name'];
    }
    if (!empty($groups)) { $cam_groups = implode(",", $groups); }
    // Store values in a row
    $row_array['label'] = $cam_name." (".$cam_crsid.")";
    $row_array['value'] = $cam_name;
    $row_array['cam_assoc'] = $cam_assoc;
    $row_array['cam_email'] = $cam_email;
    $row_array['cam_crsid'] = $cam_crsid;
    $row_array['cam_phone'] = $cam_phone;
    $row_array['cam_groups'] = $cam_groups;
    array_push( $return_arr, $row_array );
}
echo json_encode($return_arr),"\n";
?>