<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner; ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- File upload css -->
<link href="../css/bootstrap-imageupload.css" rel="stylesheet">
<!-- Local css -->
<link href="local.css" rel="stylesheet">
<!-- For printing function (http://printjs.crabbly.com) -->
<script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
		<?php
     	// Show the sidebar items
     	include("../includes/sidebar-menu-items.php");
     	?>
    </ul>
</div>	
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
				<span class="sr-only">Toggle navigation</span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="#"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
        		<?php
				// Control the view of menu items for the logged person			
				// Show the menu items
				include("../includes/navbar-menu-items.php");			
				?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
        	    // Show the right menu items
        	    include("../includes/navbar-right-menu-items.php");           
        	    ?>
			</ul>
		</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php
			// Insert form values to database
			//==================================================
			if(isset($_POST['add'])) {
               	if (!empty($_POST['crsid'])) { $usr_crsid = $_POST['crsid']; } else { die("<div class='col-xs-12 text-center'><strong>Error!</strong> No recipient selected.  Please <a href='./'>Go back</a></div>"); }
				// Actually insert the values
				// Upload image if exist
				if($_FILES['fileToUpload']['tmp_name'] !== "") {
					$target_dir = "../".$upload_path;
					$img_name = "parcels-".bin2hex(openssl_random_pseudo_bytes(10))."-".time().".".substr(strrchr(basename($_FILES["fileToUpload"]["name"]), "."), 1); // rename image file
					$target_file = $target_dir . $img_name;
					move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file); // Upload the image
					// Resize the image so it won't take up so much space on the server
					require_once("../includes/php_image_magician.php");
					$magicianObj = new imageLib($target_file);
					$magicianObj -> resizeImage(500, 500, "auto", true);
					$magicianObj -> saveImage($target_file, 100);
				}
				else { $img_name = ""; }
				// Format notes for the notification
				if (!empty($_POST['notes'])) {$notesTxt = $_POST['notes']."<br>";} else {$notesTxt = "";}
				// Insert to database
				$datetime = date("Y-m-d H:i:s");
				$query = "INSERT INTO parcels (crsid,type_post,qty,location,notes,img,date_rec) VALUES (:crsid,:type_post,:qty,:location,:notes,:img,:date_rec)";
				$stmt = $db->prepare($query);
				$stmt->execute(array("crsid"=>$usr_crsid,"type_post"=>$_POST['type_post'],"qty"=>$_POST['qty'],"location"=>$_POST['location'],"notes"=>$notesTxt,"img"=>$img_name,"date_rec"=>$datetime));
				// Populate keywords and notify users
				$query = "SELECT parcels.id, parcels.crsid, name, notes, ptype, qty, plocation, eadd FROM parcels ";
				$query .= "INNER JOIN user_details ON user_details.crsid = parcels.crsid ";
				$query .= "INNER JOIN parcels_type ON parcels_type.id = parcels.type_post ";
				$query .= "INNER JOIN parcels_loc ON parcels_loc.id = parcels.location ";
				$query .= "WHERE date_rec = :date_rec LIMIT 1";
    			$stmt = $db->prepare($query);
    			$stmt->execute(array("date_rec" => $datetime));
    			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    				// Set keywords
    				$query2 = "UPDATE parcels SET keywords = :keywords WHERE id = :id";
    				$stmt2 = $db->prepare($query2);
    				$stmt2->execute(array("keywords" => $row['crsid']." ".$row['name'],"id" => $row['id']));
    				// Email notification
                	if (!empty($row['notes'])) { $msg_note = "<br><strong>".$notes.":</strong><br>".nl2br($row['notes']); } else { $msg_note = ""; }
                	$message = $notify_msg."<br><br><strong>".$type.": </strong>".$row['ptype']."<br><strong>".$quantity.": </strong>".$row['qty']."<br><strong>".$location.": </strong>".$row['plocation'].$msg_note;
            		$headers = 'MIME-Version: 1.0' . "\r\n";
            		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            		if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
            		mail($row['eadd'],$notify_subj,$message,$headers);
            		// Store some values to div for printing
            		$printL = "<div id='printL' style='padding-top: 10px; padding-left: 15px; color: #f2f2f2;'>".$row['name']." (".$row['crsid'].")<br>".$row['qty']."x ".$row['ptype']."<br>".$row['plocation']."<br>".substr($row['notes'], 0, 20)."</div>";				
    			}
				?>
				<!-- Show modal after submission -->
				<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog">
				    <div class="modal-dialog modal-sm">
				        <div class="modal-content">
				            <div class="modal-body text-center"><?php echo $form_conf ?></div>
				            <div class="modal-footer">
				                <?php
				                if ($appset_print == 1) { // If printing is enabled
				                	echo '<button id="reload" type="button" class="btn btn-default btn-sm" data-dismiss="modal" onclick="printJS({ printable: \'printL\', type: \'html\'})">Close</button>';
				                } else {
				                	echo '<button id="reload" type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>';
				                }
				                ?>
				                
				            </div>
				        </div>
				    </div>
				</div>
				<script type="text/javascript">
				$(document).ready(function () {
   					$('#confirmModal').modal('show');
   					// reload page
   					$('.modal-footer').on('click','#reload',function(){
  						window.location = window.location.pathname;
					});
				});
				</script>
			<?php
			}
			if ($parcels_admin == "true" || $parcels_manager == "true") {
			?>
				<form name="myForm" data-toggle="validator" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php $_PHP_SELF; ?>" role="form">
				<div class="form-group form-group-lg">
					<label for ="name" class="col-sm-3 col-md-4 control-label"><?php echo $form_recipient; ?>:</label>
					<div class="col-sm-6 col-md-4">
					<input type="text" name="name" id="name" class="form-control" placeholder="Search for people" autocomplete="off">
					<input type="hidden" name="crsid" id="crsid">
					</div>
				</div>
				<div class="form-group form-group-lg">
					<label for="type_post" class="col-sm-3 col-md-4 control-label"><?php echo $form_type_qty; ?>:</label>
					<div class="col-sm-6 col-md-4">
						<div class="input-group" style="width: 100%;">
							<select name="type_post" class="form-control" style="max-width: 80%" required>
							<?php
							$query = "SELECT id, ptype FROM parcels_type WHERE deleted = 0";
    						$stmt = $db->prepare($query);
    						$stmt->execute();
    						while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    							echo "<option value='".$row['id']."'>".$row['ptype']."</option>";
    						}
							?>
							</select>						
							<input type="number" name="qty" class="form-control" value="1" min="1" max="99" style="max-width: 20%">
						</div>
					</div>
				</div>
				<div class="form-group form-group-lg">
					<label for="location" class="col-sm-3 col-md-4 control-label"><?php echo $location; ?>:</label>
					<div class="col-sm-6 col-md-4">
						<select name="location" class="form-control" placeholder="" required>
						<option value="" selected>Please select...</option>
						<?php
						$query = "SELECT id, plocation FROM parcels_loc WHERE deleted = 0";
    					$stmt = $db->prepare($query);
    					$stmt->execute();
    					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    						echo "<option value='".$row['id']."'>".$row['plocation']."</option>";
    					}
						?>
						</select>
					</div>
				</div>
				<div class="form-group form-group-lg">
					<label class="col-sm-3 col-md-4 control-label"><?php echo $notes; ?>:</label>
					<div class="col-sm-6 col-md-4">
					<textarea name="notes" class="form-control" rows="2" style="height: auto;" maxlength="150"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 col-md-4 control-label"></label>
    				<div class="col-sm-6 col-md-4">
    					<div class="imgupload panel panel-default">
        					<div class="panel-heading clearfix">
        						<h3 class="panel-title pull-left">Upload image</h3>
							</div>
							<div class="file-tab panel-body">
								<div>
									<button type="button" class="btn btn-default btn-file">
										<span>Browse</span>
										<?php
										if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false)) {
											echo '<input type="file" id="fileToUpload" name="fileToUpload" accept="image/*">';
										} else { echo '<input type="file" id="fileToUpload" name="fileToUpload">'; }
										?>
									</button>
									<button type="button" class="btn btn-default">Remove</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- File upload script -->
				<script src="../js/bootstrap-imageupload.js"></script>
				<script type="text/javascript">
					$('.imgupload').imageupload({
					 	allowedFormats: [ "jpg", "jpeg", "png", "gif" ],
					 	previewWidth: 250,
					 	previewHeight: 250,
					 	maxFileSizeKb: 5120
					});
				</script>
				<div class="form-group form-group-lg">
					<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
					<div class="col-sm-6 col-md-4">
					<button type="submit" name="add" id="add" class="btn btn-primary active"><?php echo $submit; ?></button>
					</div>
				</div>
				</form>
			<?php
				if ($appset_print == 1) { // echo div with values for print
					if (isset($printL)) {echo $printL;};
				}
			}
			else {
				// If user is not authorised, this message will appear
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
			}
			?>
		</div>
	</div>
	<br /><br />       
   </div>
</body>
</html>