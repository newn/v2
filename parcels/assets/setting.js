// Update settings
$('.settingForm').on('click','#save',function(){
    var emailfrom = $("input#emailfrom").val();
    var actrowlimit = $("input#actrowlimit").val();
    var arcrowlimit = $("input#arcrowlimit").val();
    var arcviewlimit = $("input#arcviewlimit").val();
    var bulkrem = $("#bulkRem option:selected").val();
    var signopt = $("#signOpt option:selected").val();
    var printL = $("#printL option:selected").val();
    $.ajax({
        type: 'POST',
        url: '../../includes/app-settings.php?app=parcels&appset_emailfrom='+emailfrom+'&appset_actrowlimit='+actrowlimit+'&appset_arcrowlimit='+arcrowlimit+'&appset_arcviewlimit='+arcviewlimit+'&appset_bulkrem='+bulkrem+'&appset_signoption='+signopt+'&appset_print='+printL,
        success: function(data) {
          $('#saveMsg').html(data);
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
/*** Parcel type ***/
// Get values when edit parcel type modal is opened
$('#editTypeSpan').on('click','#editTypeBtn',function(){
    var typetxt = $("#ptype option:selected").text();
    var typeval = $("#ptype option:selected").val().split("|");
    $("input#typeTxt").val(typetxt);
    $("input#typeId").val(typeval[0]);
    $("#typetitle").html("Edit name:");
    $("#deltypediv").show();
    $("#deltype").prop("checked", false);
    $("#typeSaveSpan").show();
    $("#typeAddSpan").hide();
    if (typeval[1] == 1) { $("#signtype").prop("checked", true); }
        else { $("#signtype").prop("checked", false); }
});
// When add parcel type modal is opened
$('#addTypeSpan').on('click','#addTypeBtn',function(){
    $("#typetitle").html("Add type:");
    $("#deltypediv").hide();
    $("input#typeTxt").val('');
    $("#typeSaveSpan").hide();
    $("#typeAddSpan").show();
    $("#signtype").prop("checked", false);
});
// Edit parcel type
$('#ptypeModal').on('click','#typeSave',function(){
    var typetxt = $("input#typeTxt").val();
    var typeid = $("input#typeId").val();
    if ($('#signtype').is(':checked')) { var typesign = 1; } 
        else { var typesign = 0; }
    if ($('#deltype').prop('checked')) { var del = 1; }
        else { var del = 0; }
    $.ajax({
        type: 'POST',
        url: 'queries.php?edittype&id='+typeid+'&ptype='+typetxt+'&signed='+typesign+'&del='+del,
        success: function(data) { $('#saveMsg').html(data); },
        error:function(err){ alert("error"+JSON.stringify(err)); }
    });
    $('#ptypeModal').modal('hide');
});
// Add parcel type
$('#ptypeModal').on('click','#typeAdd',function(){
    var typetxt = $("input#typeTxt").val();
    if ($('#signtype').is(':checked')) { var typesign = 1; } 
        else { var typesign = 0; }
    $.ajax({
        type: 'POST',
        url: 'queries.php?addtype&ptype='+typetxt+'&signed='+typesign,
        success: function(data) { $('#saveMsg').html(data); },
        error:function(err){ alert("error"+JSON.stringify(err)); }
    });
    $('#ptypeModal').modal('hide');
});
/*** Parcel location ***/
// Get values when edit parcel location modal is opened
$('#editLocSpan').on('click','#editLocBtn',function(){
    var loctxt = $("#ploc option:selected").text();
    var locval = $("#ploc option:selected").val();
    $("input#locTxt").val(loctxt);
    $("input#locId").val(locval);
    $("#loctitle").html("Edit name:");
    $("#dellocdiv").show();
    $("#delloc").prop("checked", false);
    $("#locSaveSpan").show();
    $("#locAddSpan").hide();
});
// When add parcel location modal is opened
$('#addLocSpan').on('click','#addLocBtn',function(){
    $("#loctitle").html("Add location:");
    $("#dellocdiv").hide();
    $("input#locTxt").val('');
    $("#locSaveSpan").hide();
    $("#locAddSpan").show();
});
// Edit parcel location
$('#plocModal').on('click','#locSave',function(){
    var loctxt = $("input#locTxt").val();
    var locid = $("input#locId").val();
    if ($('#delloc').prop('checked')) { var del = 1; }
        else { var del = 0; }
    $.ajax({
        type: 'POST',
        url: 'queries.php?editloc&id='+locid+'&ploc='+loctxt+'&del='+del,
        success: function(data) { $('#saveMsg').html(data); },
        error:function(err){ alert("error"+JSON.stringify(err)); }
    });
    $('#plocModal').modal('hide');
});
// Add parcel location
$('#plocModal').on('click','#locAdd',function(){
    var loctxt = $("input#locTxt").val();
    $.ajax({
        type: 'POST',
        url: 'queries.php?addloc&ploc='+loctxt,
        success: function(data) { $('#saveMsg').html(data); },
        error:function(err){ alert("error"+JSON.stringify(err)); }
    });
    $('#plocModal').modal('hide');
});