// Function to mark item as collected or sign
function collectedFunc(id) {
    $.ajax({
        url: "collected.php?id="+id,
        type: "POST",                   
        beforeSend:function(){
            return confirm("Mark this item as collected?");
        },
        success: function(data){
            if(data == 1) { // Item needs to be signed
                // Load signature data
                $.ajax({
                    type: 'POST',
                    url: 'signature.php?id='+id,
                    success: function(msg) {
                      $('#signBody').html(msg);
                    },
                    error:function(err){
                      alert("error"+JSON.stringify(err));
                    }
                });
                $('#signModal').modal('show');
            }
            // If not require signature, just mark as collected and fade out entry in table
            if (data == 0) {
                var table = "parcels";
                $("tr[id^=" + table + "_row_" + id +"]").fadeOut('slow');
                updateRowCount(table);
            }
        }
    });
}
// Populate the details of entries to the modal
$('#resultTbl').on('click','.dtlBtn',function(){
    var id = $(this).data('id');
    $.ajax({
        type: 'POST',
        url: 'getDetails.php?id='+id,
        beforeSend: function(){
            $('.dtlBody').html("Fetching data, please wait...");
        },
        success: function(data) {
          $('.dtlBody').html(data);
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
// Set remind modal with id value
$('#resultTbl').on('click','#addBtnRow',function(){
    var id = $(this).data('id');
    $("input#id").val(id);
});
// Update entries with remind notes
$('#remSend').on('click','#submit',function(){
    var id = $("input#id").val();
    var remtxt = $("input#remNote").val();
    $.ajax({
        type: 'POST',
        url: 'remind.php?id='+id+'&remtxt='+remtxt,
        beforeSend: function(){
            $('#remBody').html("Submitting, please wait...");
            $('#remSend').html('');
        },
        success: function(data) {
            $('#remBody').html(data);
            $('#remSend').html('<button type="button" class="btn btn-default btn-sm" data-dismiss="modal" onClick="window.location.reload();">Close</button>');
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
// Bulk update entries with remind notes
$('#remBulk').on('click','#submitBulk',function(){
    var time = $("#remSel option:selected").val();
    var remtxt = $("input#remNoteBulk").val();
    $.ajax({
        type: 'POST',
        url: 'remind.php?time='+time+'&remtxt='+remtxt,
        beforeSend: function(){
            $('#remBodyBulk').html("Submitting, please wait...");
            $('#remBulk').html('');
        },
        success: function(data) {
            $('#remBodyBulk').html(data);
            $('#remBulk').html('<button type="button" class="btn btn-default btn-sm" data-dismiss="modal" onClick="window.location.reload();">Close</button>');
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
// Display user photo
function showPhoto(id,crsid) {
    jQuery(function($) {
        $.ajax({
            type: 'POST',
            url: '../../search/uniofcam-getphoto.php?getphoto&crsid='+crsid,
            beforeSend: function(){
                $('.photo-'+id).html("<img src='../../css/loading.gif' width='20px'>");
            },
            success: function(data) {
              $('.photo-'+id).html(data);
            },
            error:function(err){
              alert("error"+JSON.stringify(err));
            }
        });
    });
}