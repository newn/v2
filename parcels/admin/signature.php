<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Local css -->
<link href="../local.css" rel="stylesheet">
</head>
<body>
	<div class="row">
		<div class="col-xs-12">
			<?php
			//==================================================
			// Get query strings
            parse_str($_SERVER['QUERY_STRING']);			
			if ($parcels_admin == "true" || $parcels_manager == "true") { ?>				
				<div id="signMsg"><!-- Sign error message here --></div>
				<!-- Buttons to toggle sign or scan -->
				<div id="optionBtns">
					<button id="signBtn" type="button" class="btn btn-default btn-sm">Sign</button>&nbsp;
					<button id="scanBtn" type="button" class="btn btn-default btn-sm">Scan ID</button>
				</div>
				<?php
				// Show or hide sign/scan on page load
				if ($appset_signoption == 1) { $signStyle = ""; $scanStyle = " style='display: none;'"; }
				if ($appset_signoption == 2) { $scanStyle = ""; $signStyle = " style='display: none;'"; } 
				?>
				<div id="signfor">	
					<!-- Signature option -->
					<div id="sign" class="sigPad"<?php echo $signStyle ?>>						
						<ul class="sigNav">
            			   	<li class="drawIt"><a href="#draw-it">Sign</a></li>
            			   	<li class="clearButton"><a href="#clear">Clear</a></li>
            			</ul>
            			<div class="sig sigWrapper">
            			   	<div class="typed"></div>
            			   	<canvas class="pad" width="228" height="73"></canvas>
            			   	<input type="hidden" name="row_id" value="<?php echo $_GET['id'] ?>">
            			   	<input type="hidden" name="output" class="output">
            			</div>
            			<br />
            			<div class="pull-right">
            			   	<button type="submit" class="btn btn-default btn-sm signSub">Submit</button>&nbsp;
            			   	<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            			</div>						
					</div>
					<!-- Scan option -->
					<div id="scan" class="col-xs-12"<?php echo $scanStyle ?>>
						<?php if ($univ_id == "cam") { // Only available for cam installations ?>
							<div class="form-group form-group-lg">
								<input type="text" name="user_id" id="user_id" class="form-control" maxlength="10" autocomplete="off" placeholder="<?php echo $scan_card ?>" autofocus>
								<input type="hidden" name="row_id" value="<?php echo $_GET['id'] ?>">
							</div>
							<div style="display:none;"><button id="scanSubBtn" type="submit" class="btn btn-default btn-sm signSub">Submit</button></div>
            				<div class="pull-right"><button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button></div>
            			<?php } else { echo "<p>This is option is not available.</p>"; } ?>
					</div>
				</div>
					<!-- Go here for options www.thomasjbradley.ca/lab/signature-pad/ -->
               		<script src="../signature/jquery.signaturepad.min.js"></script>
               		<script>
               		    $(document).ready(function() {
               		        // Set the signature pad options
               		        var options = {
               		        	lineTop : 85,
               		        	drawOnly : true
               		        };
               		        $('.sigPad').signaturePad(options);
               		        // Disable button configured as default (for sign/scan option)
               		        var signopt = "<?php echo $appset_signoption ?>";
               		        if (signopt == 1) {$('#signBtn').prop('disabled', true).css('cursor', 'default');}
               		        	else {$('#scanBtn').prop('disabled', true).css('cursor', 'default');}
               		    });
               		    // For the sign/scan option
                       	$('#optionBtns').on('click','#signBtn',function(){
                            $('#sign').show();
                            $('input#user_id').val('');
                            $('#scan').hide();
                            $('#signBtn').prop('disabled', true).css('cursor', 'default');
                            $('#scanBtn').prop('disabled', false).css('cursor', 'pointer');
                            $('#signMsg').html('');
                       	});
                       	$('#optionBtns').on('click','#scanBtn',function(){
                            $('#sign').hide();
                            $('#scan').show();
                            $("input:text:visible:first").focus();
                            $('#scanBtn').prop('disabled', true).css('cursor', 'default');
                            $('#signBtn').prop('disabled', false).css('cursor', 'pointer');
                            $('#signMsg').html('');
                       	});
                       	// Process if signed with signature or scan id
                       	$('#signfor').on('click','.signSub',function(){
                       		var id = $("input[name=row_id]").val();
                       		var output = $("input[name=output]").val();
                       		var user_id = $("input[name=user_id]").val();
                       		$.ajax({
                			    type: 'POST',
                			    url: 'queries.php?sign&id='+id+'&output='+output+'&user_id='+user_id,
                			    beforeSend:function(){
        						    $('#signMsg').html("<img src='../../css/loading.gif' width='20px'><br><i>Processing, please wait...</i>");
        						},
                			    success: function(data) {
                			    	if (data == 1 || data == 2) { // For scan option, if card has no data or recipient mismatch
                			    		if (data == 1) { $("#signMsg").html("<span style='color:red;'>Oopps! Card data does not match the parcel recipient.</span>"); }
                			    		if (data == 2) { $("#signMsg").html("<span style='color:red;'>Oopps! No data found on user card.</span>"); }
                			    	} else {
                			    		$('#signModal').modal('hide'); // Hide modal
                			    		// Fade out the entry in table after signing
                			    		var table = "parcels";
                						$("tr[id^=" + table + "_row_" + id +"]").fadeOut('slow');
                						updateRowCount(table);
                			    	}
                			    },
                			    error:function(err){
                			      alert("error"+JSON.stringify(err));
                			    }
                			});
                       	});
						// Auto focus submit button and auto click upon scan of id card
                       	document.getElementById('user_id').onkeypress = function (e) { 
                       		if (e.which === 13) { 
                       			document.getElementById("scanSubBtn").focus();
    							$("#scanSubBtn").click();
                       		} 
                       	};
               		</script>
               		<script src="../signature/assets/json2.min.js"></script>
				<?php				
			}
			else {
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg.".</p><br>";
                echo "<p><a href='../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
			}
			?>	
		</div>
	</div>    
</body>
</html>