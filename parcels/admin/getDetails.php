<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<body>	
	<div class="row">
		<div class="col-xs-12">
			<?php
			//==================================================
			// Get query strings
            parse_str($_SERVER['QUERY_STRING']);			
			// Show details
			if ($parcels_admin == "true" || $parcels_manager == "true") {
				// Get parcel values
				$query = "SELECT parcels.crsid,eadd,qty,collected,date_rec,date_col,signature,img,notes,name,ptype,plocation FROM parcels ";
				$query .= "INNER JOIN user_details ON parcels.crsid = user_details.crsid ";
				$query .= "INNER JOIN parcels_type ON parcels.type_post = parcels_type.id ";
				$query .= "INNER JOIN parcels_loc ON parcels.location = parcels_loc.id WHERE parcels.id = :id LIMIT 1";
				$stmt = $db->prepare($query);
				$stmt->execute(array(":id" => $id));
				$parcel_arr = $stmt->fetchAll();
				foreach($parcel_arr as $parcel) { ?>
					<div class="table-responsive">
						<table class="table table-striped table-condensed">
							<thead><tr><th colspan="2"><b><?php echo $details ?></b></th></tr></thead>
							<tbody>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $usr_name ?>:</b></td>
									<td><?php echo $parcel['name']." (".$parcel['crsid'].")";
									if ($parcel['collected'] == 0) { echo "&nbsp;&nbsp;<a href='mailto:".$parcel['eadd']."?subject=About%20your%20parcel'><i class='fa fa-envelope'></i></a>"; } ?>
									</td>
								</tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $type ?>:</b></td><td><?php echo $parcel['ptype'] ?></td></tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $quantity ?>:</b></td><td><?php echo $parcel['qty'] ?></td></tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $location ?>:</b></td><td><?php echo $parcel['plocation'] ?></td></tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $col_date_rec ?>:</b></td><td><?php echo date('d-m-Y H:i:s', strtotime($parcel['date_rec'])) ?></td></tr>
								<?php
                    			if(!is_null($parcel['date_col'])) {
                    			    echo "<tr><td class='col-xs-2 col-sm-3 col-md-3'><b>".$collected.":</b></td><td>".date('d-m-Y H:i:s', strtotime($parcel['date_col']))."</td></tr>";
                    			}
                    			if(!is_null($parcel['signature'])) { ?>
                    			    <tr>
                    			    	<td class='col-xs-2 col-sm-3 col-md-3'><b><?php echo $col_signature ?>:</b></td>
                    			    	<?php if ($parcel['signature'] == "card") {
                    			    		echo "<td>".$detail_scantxt."</td>";
                    			    	} else { ?>
                    			    		<td>
                    			    			<div class="sigPad signed">
            								    	<div class="sigWrapper"><canvas width="228" height="73" style="border:1px solid #ddd;"></canvas></div>
            									</div>
                    			    		</td>
                    			    		<script src="../signature/jquery.signaturepad.js"></script>
            								<script>
            								    var sig = <?php echo $parcel['signature'] ?>;
            								    $(document).ready(function() {
            								       $('.sigPad').signaturePad({displayOnly:true}).regenerate(sig);
            								    });
            								</script>
            								<script src="../signature/assets/json2.min.js"></script>
            							<?php } ?>
                    				</tr>
                    			<?php
                    			}
                    			if (!empty($parcel['img'])) { ?>
                    				<tr><td class="col-xs-2 col-sm-3 col-md-3"><b>Image:</b></td><td><a href="../../<?php echo $upload_path.$parcel['img'] ?>" target="_blank"><img src="../../<?php echo $upload_path.$parcel['img'] ?>" style="max-width: 150px;"></a></td></tr> 
                    			<?php }
                    			if (!empty($parcel['notes'])) {
                    			?>
                    				<tr><td class="col-xs-2 col-sm-3 col-md-3" colspan="2"><b><?php echo $notes ?>:</b></td></tr>
									<tr><td class="col-xs-2 col-sm-3 col-md-3" colspan="2" style="word-wrap: break-word; width: 320px; white-space: normal;"><?php echo $parcel['notes'] ?></td></tr>
								<?php
								}
								$query = "SELECT * FROM parcels_reminder WHERE par_id=:par_id";
								$stmt = $db->prepare($query);
								$stmt->execute(array(":par_id" => $id));
								if ($stmt->rowCount() > 0) {
									$reminder_arr = $stmt->fetchAll(); ?>
									<tr><td class="col-xs-2 col-sm-3 col-md-3" colspan="2"><b>Reminders:</b></td></tr>
									<tr><td class="col-xs-2 col-sm-3 col-md-3" colspan="2" style="word-wrap: break-word; width: 320px; white-space: normal;">
										<?php
										foreach($reminder_arr as $rem) {
											echo "<span class='text-muted'>[".date('d-m-Y', strtotime($rem['date_ins']))."]</span> ".$rem['reminder']."<br>";
										}
										?>
									</td></tr>
								<?php } ?>
							</tbody>									
						</table>
					</div>
				<?php
				}
			}
			else {
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg.".</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
			}
			?>	
		</div>
	</div>    
</body>
</html>