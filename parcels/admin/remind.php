<?php
header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<body>
<?php
// Insert values to database
//==================================================			
// Query strings from POST
parse_str($_SERVER['QUERY_STRING']);
// For single reminder
if (isset($id)) {
    // Insert reminder to database
    $query = "INSERT INTO parcels_reminder (par_id,reminder) VALUES (:par_id,:reminder)";
    $stmt = $db->prepare($query);
    $stmt->execute(array(":par_id"=>$id,":reminder"=>$remtxt));
    // Send email reminder to user
    $query = "SELECT ptype, qty, plocation, date_rec, eadd FROM parcels ";
    $query .= "INNER JOIN parcels_type ON parcels.type_post = parcels_type.id ";
    $query .= "INNER JOIN parcels_loc ON parcels.location = parcels_loc.id ";
    $query .= "INNER JOIN user_details ON parcels.crsid = user_details.crsid WHERE parcels.id = :id LIMIT 1";
    $stmt = $db->prepare($query);
    $stmt->execute(array(":id" => $id));
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        // Send email
        if (!empty($remtxt)) { $msg_note = "<br><br><strong>".$remind_message.":</strong><br>".nl2br($remtxt); } else { $msg_note = ""; }
        $message = $remind_email_msg."<br><br><strong>".$type.": </strong>".$row['ptype']."<br><strong>".$quantity.": </strong>".$row['qty']."<br><strong>".$location.": </strong>".$row['plocation']."<br><strong>".$date_rec.": </strong>".date('d-m-Y H:i:s', strtotime($row['date_rec'])).$msg_note;
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
        mail($row['eadd'],$remind_subj,$message,$headers);       
        // HTML output
        echo "<div>".$remind_summary."</div>";
    }
}
// For bulk reminders
if (isset($time)) {
    // Select all entries where parcels are uncollected and older than $time
    $query = "SELECT parcels.id, ptype, qty, plocation, date_rec, eadd FROM parcels ";
    $query .= "INNER JOIN parcels_type ON parcels.type_post = parcels_type.id ";
    $query .= "INNER JOIN parcels_loc ON parcels.location = parcels_loc.id ";
    $query .= "INNER JOIN user_details ON parcels.crsid = user_details.crsid WHERE collected=0 AND date_rec < DATE_SUB(NOW(), INTERVAL $time DAY)";
    $stmt = $db->prepare($query);
    $stmt->execute();
    $bulkCount = $stmt->rowCount();
    if ($bulkCount > 0) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // Insert reminder to database
            $query2 = "INSERT INTO parcels_reminder (par_id,reminder) VALUES (:par_id,:reminder)";
            $stmt2 = $db->prepare($query2);
            $stmt2->execute(array(":par_id"=>$row['id'],":reminder"=>$remtxt));
            // Send reminder email to user
            if (!empty($remtxt)) { $msg_note = "<br><br><strong>".$remind_message.":</strong><br>".nl2br($remtxt); } else { $msg_note = ""; }
            $message = $remind_email_msg."<br><br><strong>".$type.": </strong>".$row['ptype']."<br><strong>".$quantity.": </strong>".$row['qty']."<br><strong>".$location.": </strong>".$row['plocation']."<br><strong>".$date_rec.": </strong>".date('d-m-Y H:i:s', strtotime($row['date_rec'])).$msg_note;
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
            mail($row['eadd'],$remind_subj,$message,$headers);            
        }
        // HTML output success
        echo "<span style='color: green;'>Success!</span><br>";
        echo "<b>".$bulkCount."</b> recipients notified.";
    }
    else {
        // HTML output fail
        echo "No entries found, try again.";
    }
}
?>
</body>
</html>