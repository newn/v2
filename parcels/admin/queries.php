<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
// Limit access to key admins only
if ($parcels_admin == "true") {
    $errmsg = "<span style='color:red;'>Something went wrong. Please try again.</span>";
    // Edit/Delete parcel type
    if (isset($_GET['edittype'])) {
        $query = "UPDATE parcels_type SET ptype=:ptype, signed=:signed, deleted=:deleted, date_modified=:date_modified WHERE id=:id";
        $stmt = $db->prepare($query);
        if ($stmt->execute(array("ptype"=>$_GET['ptype'],"signed"=>$_GET['signed'],"deleted"=>$_GET['del'],"date_modified"=>date("Y-m-d H:i:s"),"id"=>$_GET['id']))) { 
            echo "<span style='color:green;'>Parcel type edited successfully!</span> <a href='./setting.php'>Reload</a>"; 
        } else { echo $errmsg; }
        
    }
    // Add parcel type
    if (isset($_GET['addtype'])) {
        $query = "INSERT INTO parcels_type (ptype, signed) VALUES ('".$_GET['ptype']."','".$_GET['signed']."');";
        $stmt = $db->prepare($query);
        if ($stmt->execute()) { echo "<span style='color:green;'>Parcel type added successfully!</span> <a href='./setting.php'>Reload</a>"; } else { echo $errmsg; }
    }
    // Edit/Delete parcel location
    if (isset($_GET['editloc'])) {
        $query = "UPDATE parcels_loc SET plocation=:ploc, deleted=:deleted, date_modified=:date_modified WHERE id=:id";
        $stmt = $db->prepare($query);
        if ($stmt->execute(array("ploc"=>$_GET['ploc'],"deleted"=>$_GET['del'],"date_modified"=>date("Y-m-d H:i:s"),"id"=>$_GET['id']))) { 
            echo "<span style='color:green;'>Parcel location edited successfully!</span> <a href='./setting.php'>Reload</a>"; 
        } else { echo $errmsg; }
        
    }
    // Add parcel location
    if (isset($_GET['addloc'])) {
        $query = "INSERT INTO parcels_loc (plocation) VALUES ('".$_GET['ploc']."');";
        $stmt = $db->prepare($query);
        if ($stmt->execute()) { echo "<span style='color:green;'>Parcel location added successfully!</span> <a href='./setting.php'>Reload</a>"; } else { echo $errmsg; }
    }
}
if ($parcels_admin == "true" || $parcels_manager == "true") {
    // Sign option - insert values to database
    if(isset($_GET['sign'])) {
        $datetime = date('Y-m-d H:i:s'); // Set the date
        if (!empty($_GET['output'])) {
            $id = $_GET['id'];
            $output = filter_input(INPUT_GET, 'output', FILTER_UNSAFE_RAW);
            $query = "UPDATE parcels SET signature=:signature, sign_id=:sign_id, collected=:collected, date_col=:date_col WHERE id=:id";
            $stmt = $db->prepare($query);
            $stmt->execute(array("signature" => $output,"sign_id" => $_GET['id'],"collected" => 1,"date_col" => $datetime,"id" => $_GET['id']));
        }
        // Scan option - insert values to database
        if(!empty($_GET['user_id'])) {
            //*****************************************************************************************************************//
            // Get user crsid from UniofCam Card Office
            $k_crsid = file_get_contents("http://app4.admin.cam.ac.uk/ws/card_get_crsid_for_mifare_id/index.cgi/".$_GET['user_id']);
            $k_crsid = json_decode($k_crsid, TRUE);
            //*****************************************************************************************************************//
            // If crsid in university card exist or not
            if(!empty($k_crsid['crsid'])) { 
                $usr_crsid = $k_crsid['crsid']; 
                // Check if user matches the record
                $query = "SELECT id FROM parcels WHERE crsid=:crsid AND id=:id";
                $stmt = $db->prepare($query);
                $stmt->execute(array("crsid" => $usr_crsid,"id" => $_GET['id']));
                if ($stmt->rowCount() > 0) {
                    // Insert values to database
                    $id = $_GET['id'];
                    $query = "UPDATE parcels SET signature=:signature, sign_id=:sign_id, collected=:collected, date_col=:date_col WHERE id=:id";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("signature" => "card","sign_id" => $_GET['id'],"collected" => 1,"date_col" => $datetime,"id" => $_GET['id']));
                }
                else { echo "1"; } // Card data does not match the parcel recipient
            } 
            else { echo "2"; } // No data found on user card
        }
    }
}
else {
    echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
    echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
}