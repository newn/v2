<?php
header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$parcels_texts = new ajaxCRUD("Item", "parcels_texts", "field", "");
##
########################################################  
#the table fields have prefixes; i want to give the heading titles something more meaningful
$parcels_texts->displayAs("field", "Field");
$parcels_texts->displayAs("value", "Value");
#i can order my table by whatever i want
$parcels_texts->addOrderBy("ORDER BY value ASC");
#i could disallow editing for certain, individual fields
$parcels_texts->disallowEdit('field');
#set the number of rows to display (per page)
$parcels_texts->setLimit(20);
#where field to better-filter my table
$parcels_texts->addWhereClause("WHERE field NOT LIKE 'appset%'");
#set a filter box at the top of the table
#$parcels_texts->addAjaxFilterBox('value');
$parcels_texts->deleteText = "delete";
# Disallow add
$parcels_texts->disallowAdd();
# Disallow delete
$parcels_texts->disallowDelete();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css -->
<link href="../local.css" rel="stylesheet">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
	
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="../"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
    	<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
    	   		<span class="icon-bar"></span>
    	   		<span class="icon-bar"></span>
    	   		<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    	</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
    	    	<?php
				// Control the view of menu items for the logged user			
				// Show the menu items
    	    	include("../../includes/navbar-menu-items.php");			
				?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
    	    	// Show the right menu items
    	    	include("../../includes/navbar-right-menu-items.php");           
    	    	?>
			</ul>
		</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php              
            // actually show the table if user has access
			if ($parcels_admin == "true") {
				?>
				<div class='col-xs-12' style='margin-bottom: 10px;'><h5><b>App Settings:</b></h5></div>
				<div class='col-md-6 settingForm'>
					<div class="col-sm-7 input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">From email:</button></span>
      					<input id="emailfrom" type="text" class="form-control input-sm" placeholder="Leave blank for default" value="<?php echo $appset_emailfrom ?>">
      				</div>
      				<div class="col-sm-6 input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Page rows:</button></span>
                        <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><span class='glyphicon glyphicon-edit'></span></button></span>
                        <input id="actrowlimit" type="number" class="form-control input-sm" value="<?php echo $appset_actrowlimit ?>">
                        <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><i class='fas fa-archive'></i></button></span>
                        <input id="arcrowlimit" type="number" class="form-control input-sm" value="<?php echo $appset_arcrowlimit ?>">
      				</div>
      				<div class="col-sm-4 input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><?php echo $nav_archive ?> view limit:</button></span>
      					<input id="arcviewlimit" type="number" class="form-control input-sm" value="<?php echo $appset_arcviewlimit ?>">
      				</div>
      				<div class="col-sm-5 input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Bulk remind:</button></span>
      					<select id="bulkRem" class="form-control input-sm">
      						<?php if ($appset_bulkrem == 0) { 
      							echo "<option value='0' selected>Disabled</option><option value='1'>Enabled</option>"; 
      						} else { echo "<option value='0'>Disabled</option><option value='1' selected>Enabled</option>"; }
      						?>     							
      					</select>
      				</div>
                    <div class="col-sm-5 input-group">
                        <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Sign default:</button></span>
                        <select id="signOpt" class="form-control input-sm">
                            <?php if ($appset_signoption == 1) { 
                                echo "<option value='1' selected>Signature</option><option value='2'>Scan Id</option>"; 
                            } else { echo "<option value='1'>Signature</option><option value='2' selected>Scan Id</option>"; }
                            ?>                              
                        </select>
                    </div>
                    <div class="col-sm-8 input-group">
                        <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Parcel types:</button></span>
                        <select id="ptype" class="form-control input-sm">
                            <?php
                            $query = "SELECT id, ptype, signed FROM parcels_type WHERE deleted=0 ORDER BY id";
                            $stmt = $db->prepare($query);
                            $stmt->execute();
                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                echo "<option value='".$row['id']."|".$row['signed']."'>".$row['ptype']."</option>";
                            }
                            ?>                              
                        </select>
                        <span id="editTypeSpan" class="input-group-btn"><button type="button" id="editTypeBtn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#ptypeModal">Edit</button></span>
                        <span id="addTypeSpan" class="input-group-btn" style="padding-left: 4px;"><button id="addTypeBtn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#ptypeModal">Add</button></span>
                    </div>
                    <div class="col-sm-9 input-group">
                        <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Parcel Locations:</button></span>
                        <select id="ploc" class="form-control input-sm">
                            <?php
                            $query = "SELECT id, plocation FROM parcels_loc WHERE deleted=0 ORDER BY id";
                            $stmt = $db->prepare($query);
                            $stmt->execute();
                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                echo "<option value='".$row['id']."'>".$row['plocation']."</option>";
                            }
                            ?>                              
                        </select>
                        <span id="editLocSpan" class="input-group-btn"><button type="button" id="editLocBtn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#plocModal">Edit</button></span>
                        <span id="addLocSpan" class="input-group-btn" style="padding-left: 4px;"><button id="addLocBtn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#plocModal">Add</button></span>
                    </div>
                    <div class="col-sm-6 input-group">
                        <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Printing (Chrome only):</button></span>
                        <select id="printL" class="form-control input-sm">
                            <?php if ($appset_print == 0) { 
                                echo "<option value='0' selected>Disabled</option><option value='1'>Enabled</option>"; 
                            } else { echo "<option value='0'>Disabled</option><option value='1' selected>Enabled</option>"; }
                            ?>                              
                        </select>
                    </div>
					<button id="save" type="button" class="btn btn-default btn-sm">Save</button>
					<div id="saveMsg"><!-- This is where the saved success message will appear --></div>
				</div>
                <!-- Start edit parcel type modal -->
                <div id="ptypeModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-sm">
                    <!-- modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <h4>Parcel type</h4>                                                    
                            <div id="typetitle">Edit name:</div>
                            <input type="text" class="form-control input-sm" id="typeTxt" maxlength="100">
                            <input type="hidden" id="typeId">                             
                            <div id="signtypediv" class="checkbox">
                                <label><input type="checkbox" id="signtype">Requires signature?</label>
                            </div>
                            <div id="deltypediv" class="checkbox">
                                <label><input type="checkbox" id="deltype">Delete this type?</label>
                            </div>                                                     
                        </div>
                        <div class="modal-footer">
                            <span id="typeSaveSpan"><button type="submit" name="typeSave" id="typeSave" class="btn btn-default btn-sm">Save</button></span>
                            <span id="typeAddSpan"><button type="submit" name="typeAdd" id="typeAdd" class="btn btn-default btn-sm">Add</button></span>
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- End edit parcel type modal -->
                <!-- Start edit parcel location modal -->
                <div id="plocModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-sm">
                    <!-- modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <h4>Parcel location</h4>                                                    
                            <div id="loctitle">Edit name:</div>
                            <input type="text" class="form-control input-sm" id="locTxt" maxlength="100">
                            <input type="hidden" id="locId">
                            <div id="dellocdiv" class="checkbox">
                                <label><input type="checkbox" id="delloc">Delete this location?</label>
                            </div>                                                     
                        </div>
                        <div class="modal-footer">
                            <span id="locSaveSpan"><button type="submit" name="locSave" id="locSave" class="btn btn-default btn-sm">Save</button></span>
                            <span id="locAddSpan"><button type="submit" name="locAdd" id="locAdd" class="btn btn-default btn-sm">Add</button></span>
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- End edit parcel location modal -->
				<div class='col-xs-12' style='margin-top:20px;'><h5><b>Language:</b> <i>(Click on value to edit)</i></h5></div>
			<?php
			echo "<div class='col-xs-12 col-sm-12 col-md-12'>";               
            $parcels_texts->showTable();
			echo "<br /><br />";
			echo "</div>";
            ?>
            <!-- Load js file -->
            <script src="../assets/setting.js"></script>
            <?php
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
			?>	
		</div>
		
	</div>
	<br /><br />       
</div>
</body>
</html>