<?php
// Define user's identity and access
require_once("../../includes/access.php");
// get the time and date
$datetime = date('Y-m-d H:i:s');
if ($parcels_admin == "true" || $parcels_manager == "true") {
    // Identify types that require signature
	$query = "SELECT id FROM parcels_type WHERE signed=1 AND deleted=0";
    $stmt = $db->prepare($query);
    $stmt->execute();
    if ($stmt->rowCount() > 0) {
        $signed_arr = $stmt->fetchAll();
        foreach ($signed_arr as $signed) { $signedList[] = $signed['id']; }
        // Identify if this parcel need to be signed
        $query = "SELECT id FROM parcels WHERE type_post IN (".implode(',',$signedList).") AND collected=0 AND id=:id";
        $stmt = $db->prepare($query);
        $stmt->execute(array("id"=>$_GET['id']));
        if ($stmt->rowCount() > 0) { die("1"); /* This requires signature */ }
    }
    // Just mark as collected
    $query = "UPDATE parcels SET collected = :collected, date_col = :date_col WHERE id = :id";
    $stmt = $db->prepare($query);
    $stmt->execute(array("collected" => 1,"date_col" => $datetime,"id" => $_GET['id']));
    echo "0";   
}
?>