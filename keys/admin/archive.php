<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
//Specfic table in database
$query = "SELECT * FROM keys_mgmt";
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$tbl_keys_mgmt = new ajaxCRUD("Item", "keys_mgmt", "id", "");
##
########################################################
#i can define a relationship to another table
$tbl_keys_mgmt->defineRelationship("key_id", "keys_list", "key_id", "CONCAT('<div class=\'hover-container\'>',key_no,'<div class=\'hover-content\'>',key_name,'</div></div>')");
$tbl_keys_mgmt->defineRelationship("issued_by", "user_details", "crsid", "name");
$tbl_keys_mgmt->defineRelationship("returned_by", "user_details", "crsid", "name");
// Detect if mobile device, hide some column
if(!isset($_GET['screen'])) {
/* This code will be executed if screen resolution has not been detected.*/
    echo "<script language='JavaScript'>
    <!-- 
    document.location=\"$PHP_SELF?screen=done&w=\"+screen.width;
    //-->
    </script>";
}
else {    
    if(($_GET['w']<=480)) {
        $tbl_keys_mgmt->omitField("issued_by");
        $tbl_keys_mgmt->omitField("date_issued");
        $tbl_keys_mgmt->omitField("returned_by");
        $tbl_keys_mgmt->omitField("date_returned");
    }
}
#the table fields have prefixes; i want to give the heading titles something more meaningful
$tbl_keys_mgmt->displayAs("id", $header_details);
$tbl_keys_mgmt->displayAs("key_id", $header_key);
$tbl_keys_mgmt->displayAs("crsid", $details_issued_to);
$tbl_keys_mgmt->displayAs("issued_by", $details_issued_by);
$tbl_keys_mgmt->displayAs("date_issued", $header_issdate);
$tbl_keys_mgmt->displayAs("returned_by", $details_returned_by);
$tbl_keys_mgmt->displayAs("date_returned", $header_retdate);
$tbl_keys_mgmt->displayAs("keywords", $search_text);
#i could omit a field if I wanted
$tbl_keys_mgmt->omitField("ext_name");
$tbl_keys_mgmt->omitField("ext_email");
$tbl_keys_mgmt->omitField("key_stat");
$tbl_keys_mgmt->omitField("auto_rem");
$tbl_keys_mgmt->omitField("detail_notes");
$tbl_keys_mgmt->omitField("keywords");
#i could disallow editing for certain, individual fields
$tbl_keys_mgmt->disallowEdit('id');
$tbl_keys_mgmt->disallowEdit('key_id');
$tbl_keys_mgmt->disallowEdit('crsid');
$tbl_keys_mgmt->disallowEdit('issued_by');
$tbl_keys_mgmt->disallowEdit('date_issued');
$tbl_keys_mgmt->disallowEdit('returned_by');
$tbl_keys_mgmt->disallowEdit('date_returned');
#i can use a where field to better-filter my table
$tbl_keys_mgmt->addWhereClause("WHERE key_stat=0 AND date_returned >= DATE_SUB(NOW(),INTERVAL $appset_arcviewlimit MONTH)");
#i can order my table by whatever i want
$tbl_keys_mgmt->addOrderBy("ORDER BY date_returned DESC");
#i can disallow adding rows to the table
$tbl_keys_mgmt->disallowAdd();
#set the number of rows to display (per page)
$tbl_keys_mgmt->setLimit($appset_arcrowlimit);
#set a filter box at the top of the table
$tbl_keys_mgmt->addAjaxFilterBox('keywords',15);
#i can format the data in cells however I want with formatFieldWithFunction
$tbl_keys_mgmt->formatFieldWithFunction('id', 'makeLink');
$tbl_keys_mgmt->formatFieldWithFunctionAdvanced('crsid', 'substituteName');
$tbl_keys_mgmt->formatFieldWithFunction('date_issued', 'formatDate');
$tbl_keys_mgmt->formatFieldWithFunction('date_returned', 'formatDate');
$tbl_keys_mgmt->deleteText = "delete";
# Conditional features based on the person
if ($keys_admin != "true") {
   $tbl_keys_mgmt->disallowDelete();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css to this app -->
<link href="../local.css" rel="stylesheet">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <?php
            // Control the view of menu items for the logged user        
            // Show the menu items
            include("../../includes/navbar-menu-items.php");       
            ?>
        </ul>
        <!-- Show user profile and logout option -->
        <ul class="nav navbar-nav navbar-right">
            <?php           
            // Show the right menu items
            include("../../includes/navbar-right-menu-items.php");           
            ?>
        </ul>
    </div>
    </div>
</nav>
<div  class="container">    
    <div class="row">
        <div id="resultTbl" class="col-xs-12">
            <?php
            // my self-defined functions used for formatFieldWithFunction
            function makeLink($val) {
                if (empty($val)) return "";
                return "<button type='button' class='btn btn-link dtlBtn' data-id='$val' data-test='Hi' data-toggle='modal' data-target='#dtlModal'>View</button>";
            }
            function substituteName($val, $id){
                $name_query = qr("SELECT name, ext_name FROM keys_mgmt LEFT OUTER JOIN user_details ON keys_mgmt.crsid = user_details.crsid WHERE keys_mgmt.id = $id");
                $sub_name = $name_query['name'];
                $sub_extname = $name_query['ext_name'];
                if ($sub_name == ''){
                        $sub_name = $sub_extname;
                }
                return $sub_name;
            }
            function formatDate($val) { return date("d-m-Y H:i:s", strtotime($val)); }           
            // actually show the table if user has access
            if ($keys_admin == "true" || $keys_manager == "true") {               
            $tbl_keys_mgmt->showTable();
            # Output number of rows returned
            echo "<button type='button' class='btn btn-static'>Total rows: ";
            $tbl_keys_mgmt->insertRowsReturned();
            echo "</button>";
            echo "&nbsp;<button type='button' class='btn btn-static'>View: ".$appset_arcviewlimit." months</button>";
            echo "&nbsp;<a type='button' class='btn btn-default' data-toggle='modal' data-target='#exportModal'>Export to file</a>";
            echo "<br /><br />";
            // Export modal
            include ("../../export/expmodal.php");
            ?>
            <!-- Populate the details of entries to the modal -->
            <script type="text/javascript">                
                $('#resultTbl').on('click','.dtlBtn',function(){
                    var id = $(this).data('id');
                    $.ajax({
                        type: 'POST',
                        url: 'getDetails.php?id='+id,
                        success: function(data) {
                          $('.detBody').html(data);
                        },
                        error:function(err){
                          alert("error"+JSON.stringify(err));
                        }
                    });
                });
            </script>
            <!-- Details Modal -->
            <div name="dtlModal" id="dtlModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body detBody"><!-- content from details.php goes here --></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    </div>    
                </div>
                </div>
            </div>
            <?php
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
            ?>  
        </div>
    </div>
    <br /><br />       
</div>
</body>
</html>