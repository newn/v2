<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
// Limit access to key admins only
if ($keys_admin == "true") {
    $reload = " <a href='./setting.php'>Refresh page</a>";
    $errmsg = "<span style='color:red;'>Something went wrong. Please try again.</span>".$reload;
    // Add staff
    if (isset($_GET['card_no'])) {
        if (!empty($_GET['card_no']) && !empty($_GET['staffid'])) { // Make sure required fields are populated
            if (ctype_digit($_GET['card_no']) && strlen($_GET['card_no']) < 11) { // make sure fields are on correct format
                $query = "INSERT INTO keys_staff (card_id, staff_crsid) VALUES ('".$_GET['card_no']."','".$_GET['staffid']."')";
                $stmt = $db->prepare($query);
                if ($stmt->execute()) { echo "<span style='color:green;'>Staff added successfully!</span>"; } else { echo $errmsg; }
            } else {
                echo "<span style='color:red;'>Card number must be integer values and not exceed 10 digits.</span>";
            }
        }
        else {
           echo "<span style='color:red;'>All fields are required. Please try again.</span>";
        }
    }
    // Edit key group
    if (isset($_GET['editkeygrp'])) {
        if ($_GET['del'] == 1) { // If delete group is selected
            // Check if group is being used first
            $query = "SELECT key_group FROM keys_list WHERE key_group=:key_group";
            $stmt = $db->prepare($query);
            $stmt->execute(array("key_group" => $_GET['id']));
            if ($stmt->rowCount() > 0) { 
                die("<span style='color:red;'>Group still in use! Make sure all keys are not using this group.</span>");
            } else { // Delete group if safe to delete
                $query = "DELETE FROM keys_grps WHERE id=:id";
                $stmt = $db->prepare($query);
                if ($stmt->execute(array("id" => $_GET['id']))) { echo "<span style='color:green;'>Group deleted successfully!</span>".$reload; } else { echo $errmsg; }
            }
        } else { // If group is edited instead
            $query = "UPDATE keys_grps SET groups=:groups, hidden=:hidden WHERE id=:id";
            $stmt = $db->prepare($query);
            if ($stmt->execute(array("groups" => $_GET['groups'], "hidden" => $_GET['hid'], "id" => $_GET['id']))) { 
                echo "<span style='color:green;'>Group edited successfully!</span>".$reload; 
            } else { echo $errmsg; }
        }
    }
    // Add key group
    if (isset($_GET['addkeygrp'])) {
        $query = "INSERT INTO keys_grps (groups,hidden) VALUES ('".$_GET['groups']."','".$_GET['hid']."');";
        $stmt = $db->prepare($query);
        if ($stmt->execute()) { echo "<span style='color:green;'>Group added successfully!</span>".$reload; } else { echo $errmsg; }
    }
    // Set group reminder options
    if (isset($_GET['optkeygrp'])) {
        if (!empty($_GET['dur_time'])) { $dur_time = $_GET['dur_time']; } else { $dur_time = "0"; }
        // Check first if reminder exist, if yes then just update
        $query = "SELECT id FROM keys_autorem WHERE grp_id=:grp_id AND rem_id=:rem_id";
        $stmt = $db->prepare($query);
        $stmt->execute(array("grp_id"=>$_GET['grp_id'], "rem_id"=>$_GET['rem_id']));
        if ($stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $query2 = "UPDATE keys_autorem SET dur_time=:dur_time, fixed_time=:fixed_time, subject=:subject, message=:message, e_cc=:e_cc, e_bcc=:e_bcc WHERE id=:id";
                $stmt2 = $db->prepare($query2);
                $stmt2->execute(array("dur_time"=>$dur_time,"fixed_time"=>$_GET['fixed_time'],"subject"=>$_GET['subject'],"message"=>$_GET['message'],"e_cc"=>$_GET['e_cc'],"e_bcc"=>$_GET['e_bcc'],"id"=>$row['id']));
            }
        } else { // If it's a new reminder, then insert to database
            if ($_GET['rem_id'] == 0) { die("<span style='color:green;'><br>Reminder options saved!</span>".$reload); } // Do not process the reminder option template
            $query = "INSERT INTO keys_autorem (grp_id,rem_id,fixed_time,dur_time,subject,message,e_cc,e_bcc) VALUES ('".$_GET['grp_id']."','".$_GET['rem_id']."','".$_GET['fixed_time']."','".$dur_time."','".$_GET['subject']."','".$_GET['message']."','".$_GET['e_cc']."','".$_GET['e_bcc']."')";
            $stmt = $db->prepare($query);
            $stmt->execute();
        }
        echo "<span style='color:green;'><br>Reminder options saved!</span>".$reload."<br><br>";
    }
    // Fetch group hidden status
    if (isset($_GET['gethidgrp'])) {
        $query = "SELECT * FROM keys_grps WHERE id=:id AND hidden=1";
        $stmt = $db->prepare($query);
        $stmt->execute(array("id"=>$_GET['id']));
        if ($stmt->rowCount() > 0) { echo "1"; } else { echo "0"; }
    }
    // Fetch group reminder options
    if (isset($_GET['optkeygrpmod'])) {
        $query = "SELECT * FROM keys_autorem WHERE grp_id=:grp_id ORDER BY rem_id ASC";
        $stmt = $db->prepare($query);
        $stmt->execute(array("grp_id" => $_GET['grp_id']));
        if ($stmt->rowCount() > 0) {
            $rem_array = $stmt->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($rem_array);
        } else { echo "0"; }
    }
    // Delete group reminder option
    if (isset($_GET['delgrprem'])) { // Delete the reminder from table
        $query = "DELETE FROM keys_autorem WHERE rem_id=:rem_id AND grp_id=:grp_id";
        $stmt = $db->prepare($query);
        $stmt->execute(array("rem_id"=>$_GET['rem_id'], "grp_id"=>$_GET['grp_id']));
        // Update the reminder sequence
        $x = 1;
        $query = "SELECT rem_id FROM keys_autorem WHERE grp_id=:grp_id ORDER BY rem_id ASC";
        $stmt = $db->prepare($query);
        $stmt->execute(array("grp_id"=>$_GET['grp_id']));
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($x !== $row['rem_id']) {
                $query2 = "UPDATE keys_autorem SET rem_id=:x WHERE rem_id=:rem_id AND grp_id=:grp_id";
                $stmt2 = $db->prepare($query2);
                $stmt2->execute(array("x"=>$x, "rem_id"=>$row['rem_id'], "grp_id"=>$_GET['grp_id']));
            }
            $x++;
        }
        echo "<span style='color:red;'><br>Reminder deleted!</span>".$reload."<br><br>";
    }
    // Delete row
    if (isset($_GET['delrow'])) {
        $query = "DELETE FROM keys_mgmt WHERE id=:id";
        $stmt = $db->prepare($query);
        if ($stmt->execute(array("id"=>$_GET['id']))) { echo $_GET['id']; }
    }
}
else {
    echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
    echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
}