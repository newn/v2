<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
//Specfic table in database
//$query = "SELECT * FROM keys_texts";
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$tbl_keys_texts = new ajaxCRUD("Item", "keys_texts", "field", "");
##
########################################################  
#the table fields have prefixes; i want to give the heading titles something more meaningful
$tbl_keys_texts->displayAs("field", "Field");
$tbl_keys_texts->displayAs("value", "Value");
#i can order my table by whatever i want
$tbl_keys_texts->addOrderBy("ORDER BY value ASC");
#i could disallow editing for certain, individual fields
$tbl_keys_texts->disallowEdit('field');
#set the number of rows to display (per page)
$tbl_keys_texts->setLimit(20);
#where field to better-filter my table
$tbl_keys_texts->addWhereClause("WHERE field NOT LIKE 'appset%'");
#set a filter box at the top of the table
#$tbl_keys_texts->addAjaxFilterBox('value');
$tbl_keys_texts->deleteText = "delete";
# Disallow add
$tbl_keys_texts->disallowAdd();
# Disallow delete
$tbl_keys_texts->disallowDelete();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css -->
<link href="../local.css" rel="stylesheet">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="../"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
        <?php
		// Control the view of menu items for the logged user		
		// Show the menu items
        include("../../includes/navbar-menu-items.php");		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php           
            // Show the right menu items
            include("../../includes/navbar-right-menu-items.php");           
            ?>
		</ul>
	</div>
	</div>
</nav>
<div class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php              
            // actually show the table if user has access
			if ($keys_admin == "true") { ?>             
                <div class='col-xs-12' style='margin-bottom: 10px;'><h5><b>App Settings:</b></h5></div>
				<div class='col-md-6 settingForm'>
					<div class="col-sm-7 input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">From email:</button></span>
      					<input id="emailfrom" type="text" class="form-control input-sm" placeholder="Leave blank for default" value="<?php echo $appset_emailfrom ?>">
      				</div>
      				<div class="col-sm-5 input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><?php echo $nav_manage ?> rows per page:</button></span>
      					<input id="actrowlimit" type="number" class="form-control input-sm" value="<?php echo $appset_actrowlimit ?>">
      				</div>
      				<div class="col-sm-5 input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><?php echo $nav_archive ?> rows per page:</button></span>
      					<input id="arcrowlimit" type="number" class="form-control input-sm" value="<?php echo $appset_arcrowlimit ?>">
      				</div>
      				<div class="col-sm-4 input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><?php echo $nav_archive ?> view limit:</button></span>
      					<input id="arcviewlimit" type="number" class="form-control input-sm" value="<?php echo $appset_arcviewlimit ?>">
      				</div>
                    <div class="col-sm-5 input-group">
                        <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><?php echo $identify_keylist ?> rows per page:</button></span>
                        <input id="keyrowlimit" type="number" class="form-control input-sm" value="<?php echo $appset_keyrowlimit ?>">
                    </div>
                    <div class="col-sm-9 input-group">
                        <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Key groups:</button></span>
                        <select id="keygroup" class="form-control input-sm">
                            <?php
                            $query = "SELECT id, groups FROM keys_grps ORDER BY id";
                            $stmt = $db->prepare($query);
                            $stmt->execute();
                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                echo "<option value='".$row['id']."'>".$row['groups']." (".$row['id'].")</option>";
                            }
                            ?>                              
                        </select>
                        <span id="editSpan" class="input-group-btn"><button type="button" id="editGrpBtn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#keyGrpModal">Edit</button></span>
                        <span id="addSpan" class="input-group-btn" style="padding-left: 4px;"><button id="addGrpBtn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#keyGrpModal">Add</button></span>
                        <span id="optSpan" class="input-group-btn" style="padding-left: 5px;"><button id="optGrpBtn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#keyGrpOptModal">Options</button></span>
                    </div>
					<button id="save" type="button" class="btn btn-default btn-sm">Save</button>
					<div id="saveMsg"><!-- This is where the saved success message will appear --></div>
				</div>
                <!-- Start edit group modal -->
                <div id="keyGrpModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-sm">
                    <!-- modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <h4>Manage group</h4>                                                    
                            <div id="grptitle">Edit group name:</div>
                            <input type="text" class="form-control input-sm" id="grpTxt">
                            <input type="hidden" id="grpVal">
                            <div id="hidgrpdiv" class="checkbox-inline" style="margin-top: 10px;">
                                <label style="font-weight: normal;"><input type="checkbox" id="hidgrp">Masked by default</label>
                            </div><br>                
                            <div id="delgrpdiv" class="checkbox-inline" style="margin-top: 10px;">
                                <label style="font-weight: normal;"><input type="checkbox" id="delgrp"><span style="color: #d9534f;">Delete group?</span></label>
                            </div>                                                       
                        </div>
                        <div class="modal-footer">
                            <span id="editSaveSpan"><button type="submit" name="editSave" id="editSave" class="btn btn-default btn-sm">Save</button></span>
                            <span id="editAddSpan"><button type="submit" name="editAdd" id="editAdd" class="btn btn-default btn-sm">Add</button></span>
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>                                  
                        </div>
                    </div>
                    </div>
                </div>
                <!-- End edit group modal -->
                <!-- Start options group modal -->
                <div id="keyGrpOptModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <!-- modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <h4>Group options</h4>                                                    
                            <div>Set auto-reminder for group <span id="grpName"></span></div>
                            <!-- Template for reminder options -->
                            <div id="optfields-0" class="optfields">
                                <div id="opt-0">
                                    <div id="optTitle-0">Reminder 0:</div>
                                    <input type="hidden" id="remId-0" value="0">
                                    <div class="col-sm-3">
                                        <select class="form-control input-sm" id="remTime-0">
                                            <optgroup label="Time period"><option value="24:00:00">Duration</option></optgroup>
                                            <optgroup label="Fixed time">
                                                <?php // List all available times
                                                    $x=0;
                                                    while ($x < 24) {
                                                        if ($x < 10) { echo "<option value='0".$x.":00:00'>0".$x.":00</option>"; }
                                                        else { echo "<option value='".$x.":00:00'>".$x.":00</option>"; }
                                                        $x++;
                                                    }
                                                ?>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="col-sm-3"><input type="number" class="form-control input-sm" id="remDur-0" placeholder="Hours" maxlength="3"></div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-5"><input type="text" class="form-control input-sm" id="remSubj-0" placeholder="Subject"></div>
                                    <div class="col-sm-3"><input type="text" class="form-control input-sm" id="remCc-0" placeholder="Cc: Email"></div>
                                    <div class="col-sm-3"><input type="text" class="form-control input-sm" id="remBcc-0" placeholder="Bcc: Email"></div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-default btn-xs" tabindex="-1" onclick="bold()"><b>B</b></button>
                                        <button class="btn btn-default btn-xs" tabindex="-1" onclick="underline()"><u>U</u></button>
                                        <button class="btn btn-default btn-xs" tabindex="-1" onclick="link()">Link</button>
                                        <!--<button class="btn btn-default btn-xs" onclick="displayhtml()">Display HTML</button>-->
                                    </div>
                                    <div id="remMsg-0" class="editor" contenteditable="true" spellcheck="true">Type message here...</div>
                                    <div class="pull-right remDelDiv"><button id="remDel-0" type="button" class="btn btn-link btn-xs" data-id="0"><span class="text-danger">Delete</span></button></div>
                                </div>
                            </div>
                            <!-- End of template -->
                            <div id="addRemDiv"><button id="addRemBtn" data-id="0" class="btn btn-default btn-xs">Add reminder</button></div>
                            <div id="optSaveMsg"><!-- This is where the option saved success message will appear --></div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" id="optSave" class="btn btn-default btn-sm">Save</button>
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- End options group modal -->
				<div class='col-xs-12' style='margin-top:20px;'><h5><b>Language:</b> <i>(Click on value to edit)</i></h5></div>
			<?php
			echo "<div class='col-xs-12 col-sm-12 col-md-12'>";               
            $tbl_keys_texts->showTable();
			echo "<br /><br />";
			echo "</div>";
            ?>
            <!-- Load js file -->
            <script src="../../js/wysiwyg.js"></script>
            <script src="../assets/setting.js"></script>
            <?php
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
			?>	
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>