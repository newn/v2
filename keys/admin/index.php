<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
//Specfic table in database
$query = "SELECT * FROM keys_mgmt";
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$tbl_keys_mgmt = new ajaxCRUD("Item", "v_keys_mgmt", "id", "");
##
########################################################
#i can define a relationship to another table
$tbl_keys_mgmt->defineRelationship("key_id", "keys_list", "key_id", "CONCAT('<div class=\'hover-container\'>',key_no,'<div class=\'hover-content\'>',key_name,'</div></div>')");
$tbl_keys_mgmt->defineRelationship("issued_by", "user_details", "crsid", "name");
# Show or hide hidden items
if (isset($_GET['hid'])) {
    $tbl_keys_mgmt->defineRelationship("key_group", "keys_grps", "id", "groups", "groups", "0", "WHERE hidden=1");
} else {
    $tbl_keys_mgmt->defineRelationship("key_group", "keys_grps", "id", "groups", "groups", "0", "WHERE hidden=0");
}
// Detect if mobile device, hide some column
if(!isset($_GET['screen'])) {
/* This code will be executed if screen resolution has not been detected.*/
    echo "<script language='JavaScript'>
    <!-- 
    document.location=\"$PHP_SELF?screen=done&w=\"+screen.width;
    //-->
    </script>";
}
else {    
    if(($_GET['w']<=480)) {
        $tbl_keys_mgmt->omitField("issued_by");
        $tbl_keys_mgmt->omitField("date_issued");
        $tbl_keys_mgmt->omitField("groups");
    }
}
#the table fields have prefixes; i want to give the heading titles something more meaningful
$tbl_keys_mgmt->displayAs("id", $header_details);
$tbl_keys_mgmt->displayAs("key_id", $header_key);
$tbl_keys_mgmt->displayAs("groups", $header_grp);
$tbl_keys_mgmt->displayAs("crsid", $details_issued_to);
$tbl_keys_mgmt->displayAs("issued_by", $details_issued_by);
$tbl_keys_mgmt->displayAs("date_issued", $header_issdate);
$tbl_keys_mgmt->displayAs("auto_rem", $header_stat);
$tbl_keys_mgmt->displayAs("detail_notes", $details_notes);
$tbl_keys_mgmt->displayAs("keywords", $search_text);
#i could omit a field if I wanted
$tbl_keys_mgmt->omitField("ext_name");
$tbl_keys_mgmt->omitField("ext_email");
$tbl_keys_mgmt->omitField("key_stat");
if (isset($_GET['hid'])) {
    $tbl_keys_mgmt->omitField("auto_rem");
}
$tbl_keys_mgmt->omitField("returned_by");
$tbl_keys_mgmt->omitField("date_returned");
$tbl_keys_mgmt->omitField("keywords");
$tbl_keys_mgmt->omitField("key_group");
$tbl_keys_mgmt->omitField("hidden");
#i could disallow editing for certain, individual fields
$tbl_keys_mgmt->disallowEdit('id');
$tbl_keys_mgmt->disallowEdit('key_id');
$tbl_keys_mgmt->disallowEdit('key_group');
$tbl_keys_mgmt->disallowEdit('crsid');
$tbl_keys_mgmt->disallowEdit('issued_by');
$tbl_keys_mgmt->disallowEdit('date_issued');
$tbl_keys_mgmt->disallowEdit('auto_rem');
$tbl_keys_mgmt->disallowEdit('detail_notes');
$tbl_keys_mgmt->disallowEdit('groups');
#i can use a where field to better-filter my table
# Show or hide hidden items
if (isset($_GET['hid'])) { $tbl_keys_mgmt->addWhereClause("WHERE (key_stat='1' AND hidden='1')"); }
else { $tbl_keys_mgmt->addWhereClause("WHERE (key_stat='1' AND hidden='0')"); }
#i can order my table by whatever i want
$tbl_keys_mgmt->addOrderBy("ORDER BY date_issued DESC, auto_rem DESC");
#i can disallow adding rows to the table
$tbl_keys_mgmt->disallowAdd();
#i can add a button that performs some action
if ($keys_admin == "true") { $tbl_keys_mgmt->addButtonToRow($delete_btn, "", "", "delrow"); }
$remind_btn = preg_replace('/\s+/', '', $remind_btn); // Remove whitespace for this string
$tbl_keys_mgmt->addButtonToRow($remind_btn, "", "", "modal"); // Modified code in ajaxCRUD.class.php on line 1716-1726 to open modal instead of a js function when parameter value is "modal"
#set the number of rows to display (per page)
$tbl_keys_mgmt->setLimit($appset_actrowlimit);
#set a filter box at the top of the table
$tbl_keys_mgmt->addAjaxFilterBox('keywords',15);
$tbl_keys_mgmt->addAjaxFilterBox('key_group');
#i can format the data in cells however I want with formatFieldWithFunction
$tbl_keys_mgmt->formatFieldWithFunction('id', 'makeLink');
$tbl_keys_mgmt->formatFieldWithFunction('detail_notes', 'makeLink2');
$tbl_keys_mgmt->formatFieldWithFunctionAdvanced('crsid', 'substituteName');
$tbl_keys_mgmt->formatFieldWithFunction('date_issued', 'formatDate');
$tbl_keys_mgmt->formatFieldWithFunction('auto_rem', 'showIcon');
# Conditional features based on the person
$tbl_keys_mgmt->disallowDelete();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css to this app -->
<link href="../local.css" rel="stylesheet">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <?php
        // Control the view of menu items for the logged user        
        // Show the menu items
        include("../../includes/navbar-menu-items.php");        
        ?>
        </ul>
        <!-- Show user profile and logout option -->
        <ul class="nav navbar-nav navbar-right">
            <?php           
            // Show the right menu items
            include("../../includes/navbar-right-menu-items.php");           
            ?>
        </ul>
    </div>
    </div>
</nav>
<div  class="container">
    <div class="row">           
        <div id="resultTbl" class="col-xs-12">                
            <?php
            // my self-defined functions used for formatFieldWithFunction
            function makeLink($val) {
                if (empty($val)) return "";
                return "<button type='button' class='btn btn-link dtlBtn' data-id='$val' data-toggle='modal' data-target='#dtlModal'>View</button>";
            }
            function makeLink2($val) {
                if (empty($val)) return "";
                return "<div class='hover-container'>
                            <button type='button' class='btn btn-link'><span class='glyphicon glyphicon-info-sign'></span></button>
                            <div class='hover-content'>".$val."</div>
                        </div>";                  
            }
            function substituteName($val, $id){
                $name_query = qr("SELECT name, ext_name FROM keys_mgmt LEFT OUTER JOIN user_details ON keys_mgmt.crsid = user_details.crsid WHERE keys_mgmt.id = $id");
                $sub_name = $name_query['name'];
                $sub_extname = $name_query['ext_name'];
                if ($sub_name == ''){
                        $sub_name = $sub_extname;
                }
                return $sub_name;
            } 
            function formatDate($val) { return date("d-m-Y H:i:s", strtotime($val)); }               
            if ($keys_admin == "true" || $keys_manager == "true") {
            // If notes are updated
            if(isset($_POST['detail_notes'])) {
                $detail_notes = $_POST['detail_notes'];
                $id = $_POST['id'];
                // Update db table fields
                $query = "UPDATE keys_mgmt SET detail_notes= :detail_notes WHERE id= :id";
                $stmt = $db->prepare($query);
                $stmt->execute(array(":detail_notes" => $detail_notes,":id" => $id));
            } 
            function showIcon($val) {
                if ($val == 0) { return "&nbsp;"; } else { return "<span style='color:#D9534F;'><i class='fas fa-clock' aria-hidden='true' title='Due to be returned'></i></span>"; }
            }     
            // actually show the table if user has access
            $tbl_keys_mgmt->showTable();
            # Output number of rows returned
            echo "<button type='button' class='btn btn-static'>Total rows: ";
            $tbl_keys_mgmt->insertRowsReturned();
            echo "</button>";
            # Only show masked items button if there's hidden group is configured
            $query = "SELECT count(*) FROM v_keys_mgmt WHERE hidden=1";
            $stmt = $db->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                if (isset($_GET['hid'])) {
                    echo "&nbsp;<a href='./' type='button' class='btn btn-default'>All Items</a>";
                } else {
                    echo "&nbsp;<a href='javascript:window.location.search += \"&hid\";' type='button' class='btn btn-default'>Masked Items</a>";
                }
            }
            echo "<br /><br />";
            ?>
            <!-- Details Modal -->
            <div name="dtlModal" id="dtlModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body dtlBody"><!-- content from details.php goes here --></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    </div>    
                </div>
                </div>
            </div>
            <!-- Remind Modal -->
            <div name="<?php echo $remind_btn ?>" id="<?php echo $remind_btn ?>" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div id="remBody" class="modal-body">
                        <input type='hidden' id='id'>
                        <div style="text-align: left;">An email reminder will be sent</div>
                        <input type='text' id='remNote' class='form-control' placeholder='Add note (optional)' maxlength='100'>
                    </div>
                    <div id="remSend" class="modal-footer">
                        <button id="submit" type="button" class="btn btn-default btn-sm">Send</button>
                        <button id="close" type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    </div>    
                </div>
                </div>
            </div>
            <!-- Load js file -->
            <script src="../assets/admin.js"></script>
            <?php
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
            ?>  
        </div>
    </div>
    <br /><br />       
</div>
</body>
</html>