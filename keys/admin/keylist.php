<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$tbl_keys_list = new ajaxCRUD("Key", "keys_list", "id", "");
##
########################################################
#i don't want to visually show the primary key in the table
$tbl_keys_list->omitPrimaryKey();
#i can define a relationship to another table
$tbl_keys_list->defineRelationship("key_group","keys_grps","id","groups","id",0);
#the table fields have prefixes; i want to give the heading titles something more meaningful
$tbl_keys_list->displayAs("key_id", "Key ID");
$tbl_keys_list->displayAs("key_no", $details_key_no);
$tbl_keys_list->displayAs("key_name", $details_key_name);
$tbl_keys_list->displayAs("key_group", "Group");
$tbl_keys_list->displayAs("notes", "Notes");
#i can order my table by whatever i want
$tbl_keys_list->addOrderBy("ORDER BY key_no ASC");
#set the number of rows to display (per page)
$tbl_keys_list->setLimit($appset_keyrowlimit);
#set a filter box at the top of the table
$tbl_keys_list->addAjaxFilterBox('key_id');
$tbl_keys_list->addAjaxFilterBox('key_no');
$tbl_keys_list->addAjaxFilterBox('key_name');
# Disallow add
$tbl_keys_list->disallowAdd();
# define texts
$tbl_keys_list->deleteText = "delete";
$tbl_keys_list->cancelText = "Close";
# Conditional features based on the person
if ($keys_admin != "true") {
   $tbl_keys_list->disallowDelete();
   $tbl_keys_list->disallowEdit('key_no');
   $tbl_keys_list->disallowEdit('key_name');
   $tbl_keys_list->disallowEdit('key_group');
   $tbl_keys_list->disallowEdit('notes');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css -->
<link href="../local.css" rel="stylesheet">
</head>
<body>
<!-- Primary Page Layout
================================================== -->  
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <?php
        // Control the view of menu items for the logged user       
        // Show the menu items
        include("../../includes/navbar-menu-items.php");        
        ?>
        </ul>
        <!-- Show user profile and logout option -->
        <ul class="nav navbar-nav navbar-right">
            <?php           
            // Show the right menu items
            include("../../includes/navbar-right-menu-items.php");           
            ?>
        </ul>
    </div>
    </div>
</nav>
<div class="container">    
    <div class="row">
        <div class="col-xs-12">
            <?php                
            // actually show the table if user has access
            if ($keys_admin == "true" || $keys_manager == "true") {               
                $tbl_keys_list->showTable();
                # Output number of rows returned
                echo "<button type='button' class='btn btn-static'>Total rows: ";
                $tbl_keys_list->insertRowsReturned();
                echo "</button>";
                if ($keys_admin == "true") {
                    echo "&nbsp;<button type='button' class='btn btn-default' data-toggle='modal' data-target='#impModal'>Manage keys</button>";
                }
                echo "&nbsp;<a type='button' href='".$dir_path."export/?tbl=".$app."_list&path=".$app."'' class='btn btn-default'>Export to file</a>";
                echo "<br /><br />";
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }
            ?>
            <!-- Modal to display management of keys -->
            <div name="impModal" id="impModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-left">
                        <h4 class="modal-title">Manage Keys</h4><hr style="margin-bottom: 20px; margin-top: 0px;">
                        <div style="margin-bottom:10px;"><span style="color:red;"><b>NOTE:</b></span> It is highly recommended NOT to REUSE/REASSIGN key tags as it might cause the history logs to be inaccurate.</div>
                        <div id="upContent">
                            <select id="selAddOption" style="max-width:200px;" class="form-control">
                                <option value="0" selected>Quick add</option>
                                <option value="1">Import from file</option>
                                <option disabled>──────────</option>
                                <option value="2">Order tags/reader</option>
                            </select>
                            <div id="quickAdd" style="margin-top:10px;">
                                <div class="col-xs-12 col-sm-4 input-group">
                                    <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">ID: <span style="color:red;">*</span></button></span>
                                    <input id="keyid" type="number" class="form-control input-sm" placeholder="0000000000">
                                </div>
                                <div class="col-xs-12 col-sm-4 input-group">
                                    <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Number: <span style="color:red;">*</span></button></span>
                                    <input id="keyno" type="text" class="form-control input-sm" placeholder="RM001">
                                </div>
                                <div class="col-xs-12 col-sm-6 input-group">
                                    <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Name:</button></span>
                                    <input id="keyname" type="text" class="form-control input-sm" placeholder="Student Room Key 1">
                                </div>
                                <div class="col-xs-12 col-sm-6 input-group">
                                    <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Group:</button></span>
                                    <select id="keygroup" class="form-control input-sm">
                                        <?php
                                        $query = "SELECT id, groups FROM keys_grps ORDER BY id";
                                        $stmt = $db->prepare($query);
                                        $stmt->execute();
                                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<option value='".$row['id']."'>".$row['groups']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-8 input-group">
                                    <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Notes:</button></span>
                                    <input id="keynotes" type="text" class="form-control input-sm" maxlength="100">
                                </div>
                                <div id="quickAddMsg" style="margin-top:5px;"><!--Message for quick add will appear here--></div>
                            </div>
                            <div id="uploadForm" style="margin-top:10px; display:none;">
                                <input class="upRadio" type="radio" name="upRadio" value="skip" checked> Skip existing -- <i>only add new keys</i><br>
                                <input class="upRadio" type="radio" name="upRadio" value="overwrite"> Update -- <i>add new keys and update existing</i><br>
                                <input class="upRadio" type="radio" name="upRadio" value="delete"> Delete and write -- <i>Delete all key records then add keys from file</i><br>
                                <div id="warn_overwrite" style="color: red;">WARNING!<br>This option will update existing keys that matches the file (based on key id).<br>Audit logs will change to reflect the details of the updated keys.</div>
                                <div id="warn_delete" style="color: red;">WARNING!<br>This option will <b>DELETE ALL</b> existing key records.<br>Some audit log entries might show blank if the key id is missing on the new key records.</div>
                                <div style="margin-top: 10px;">Select csv file to upload: <i>(Download sample file <a href="key-sample.csv">here</a>)</i><br><input type="file" id="fileToUpload" name="fileToUpload"></div>
                            </div>
                            <div id="orderForm" style="margin-top:10px; display:none;">
                                <h5>Mifare Classic 1K Key Fobs (Blue) - <i>pack of 100</i></h5>
                                <div class="input-group">
                                    <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Key Fobs:</button></span>
                                    <select id="selOrdOption" style="max-width:100px;" class="form-control input-sm">
                                        <option value="100" selected>100</option>
                                        <option value="200">200</option>
                                        <option value="300">300</option>
                                        <option value="400">400</option>
                                        <option value="500">500</option>
                                        <option value="1000">1000</option>
                                    </select>
                                </div>
                                <h5>Mifare IC Card Reader</h5>
                                <div class="input-group">
                                    <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Mifare Reader:</button></span>
                                    <input id="mifareReader" type="number" class="form-control input-sm" value="0" style="max-width:60px;">
                                </div>
                                <h5>Contact email</h5>
                                <div class="input-group">
                                    <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Email:</button></span>
                                    <input id="orderEmail" type="text" class="form-control input-sm" style="max-width:200px;" maxlength="100">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span id="subBtnSpan"><button type="button" id="subBtn" class="btn btn-sm btn-default">Submit</button></span>
                        <span id="upBtnSpan" style="display:none;"><button type="button" name="upFile" id="upBtn" class="btn btn-sm btn-default">Upload</button></span>
                        <span id="orderBtnSpan" style="display:none;"><button type="button" id="orderBtn" class="btn btn-sm btn-default">Submit Order</button></span>
                        <span id="closeBtn"><button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button></span>
                    </div>  
                </div>
                </div>
            </div>
            <!-- Script to check file and insert data -->
            <script type="text/javascript">                
                // Show details based on option chosen
                $(document).ready(function () {
                    $('#warn_overwrite').hide();
                    $('#warn_delete').hide();
                    $(".upRadio").change(function () { //use change event
                        if (this.value == "skip") { $('#warn_overwrite').hide(); $('#warn_delete').hide(); } // If skip is chosen
                        if (this.value == "overwrite") { $('#warn_overwrite').show(); $('#warn_delete').hide(); } // If overwite is chosen
                        if (this.value == "delete") { $('#warn_delete').show(); $('#warn_overwrite').hide(); } // If delete is chosen
                    });
                    // Toggle between quick add and upload from file
                    $('#selAddOption').on('change', function() {
                        if ($(this).val() == 0){
                            $('#quickAdd').show();
                            $('#subBtnSpan').show();
                            $('#uploadForm').hide();
                            $('#upBtnSpan').hide();
                            $('#orderForm').hide();
                            $('#orderBtnSpan').hide();
                        }
                        if ($(this).val() == 1){
                            $('#quickAdd').hide();
                            $('#subBtnSpan').hide();
                            $('#uploadForm').show();
                            $('#upBtnSpan').show();
                            $('#orderForm').hide();
                            $('#orderBtnSpan').hide();
                        }
                        if ($(this).val() == 2){
                            $('#quickAdd').hide();
                            $('#subBtnSpan').hide();
                            $('#uploadForm').hide();
                            $('#upBtnSpan').hide();
                            $('#orderForm').show();
                            $('#orderBtnSpan').show();
                        }
                    });
                });
                // Use ajax to quick add keys
                $('.modal-footer').on('click','#subBtn',function(){
                    var keyid = $("input#keyid").val();
                    var keyno = $("input#keyno").val();  
                    var keyname = $("input#keyname").val();
                    var keygroup = $("#keygroup option:selected").val();
                    var keynotes = $("input#keynotes").val();
                    $.ajax({
                        type: 'POST',
                        url: 'import.php?key_id='+keyid+'&key_no='+keyno+'&key_name='+keyname+'&key_group='+keygroup+'&notes='+keynotes,
                        success: function(data) {
                            $('#quickAdd').find("input").val("");
                            $('#quickAdd').find("input#keygroup").val("0");
                            $('#quickAddMsg').html(data);
                            $('#closeBtn').html('<a href="./keylist.php"><button type="button" class="btn btn-sm btn-default">Reload</button></a>');
                        },
                        error:function(err){
                            alert("error"+JSON.stringify(err));
                        }
                    });
                });
                // Use ajax to upload file to process keys
                $('.modal-footer').on('click','#upBtn',function(){
                    var up_opt = $("#upContent input[type='radio']:checked").val();
                    var file_data = $('#fileToUpload').prop('files')[0];   
                    var form_data = new FormData();                  
                    form_data.append('file', file_data);
                    $.ajax({
                        type: 'POST',
                        url: 'import.php?up_opt='+up_opt,
                        data: form_data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function(){
                            $('#upContent').html("<img src='loading.gif' width='25px'><br><i>Processing, please wait...</i>");
                        },
                        success: function(data) {
                            $('#upContent').html(data);
                            $('.modal-footer').html('<a href="./keylist.php"><button type="button" class="btn btn-sm btn-default">Reload</button></a>');
                        },
                        error:function(err){
                            alert("error"+JSON.stringify(err));
                        }
                    });
                });
                // Process ordering of tags/reader
                $('.modal-footer').on('click','#orderBtn',function(){
                    var tags = $("#selOrdOption option:selected").val();
                    var mreader = $("input#mifareReader").val();  
                    var ordmail = $("input#orderEmail").val();
                    $.ajax({
                        type: 'POST',
                        url: 'ordmail.php?tags='+tags+'&mreader='+mreader+'&ordmail='+ordmail,
                        beforeSend: function(){
                            $('#upContent').html("<img src='loading.gif' width='25px'><br><i>Submitting, please wait...</i>");
                        },
                        success: function(data) {
                            $('#upContent').html(data);
                            $('.modal-footer').html('<a href="./keylist.php"><button type="button" class="btn btn-sm btn-default">Reload</button></a>');
                        },
                        error:function(err){
                            alert("error"+JSON.stringify(err));
                        }
                    });
                });
            </script> 
        </div>
    </div>
    <br /><br />       
</div>
</body>
</html>