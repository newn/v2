<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<body>	
	<div class="row">
		<div class="col-xs-12">
			<?php
			//==================================================
			// Get query strings from url
            parse_str($_SERVER['QUERY_STRING']);			
			// Show details
			if ($keys_admin == "true" || $keys_manager == "true") {
				$query = "SELECT key_stat from keys_mgmt where id = :id";
				$stmt = $db->prepare($query);
				$stmt->execute(array("id" => $id));
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					if ($row['key_stat'] == 0) {
						$query = "SELECT keys_mgmt.crsid, keys_mgmt.key_stat, keys_list.key_no, keys_list.key_name, i_to.name as issued_to, i_to_eadd.eadd as eadd, keys_mgmt.auto_rem, i_by.name as issued_by, keys_mgmt.date_issued, r_by.name as returned_by, keys_mgmt.ext_name, keys_mgmt.ext_email, keys_mgmt.detail_notes, keys_mgmt.date_returned FROM keys_mgmt 
						INNER JOIN keys_list ON keys_mgmt.key_id = keys_list.key_id 
						LEFT OUTER JOIN user_details i_to ON keys_mgmt.crsid = i_to.crsid 
						LEFT OUTER JOIN user_details i_by ON keys_mgmt.issued_by = i_by.crsid 
						LEFT OUTER JOIN user_details r_by ON keys_mgmt.returned_by = r_by.crsid 
						LEFT OUTER JOIN user_details i_to_eadd ON keys_mgmt.crsid = i_to_eadd.crsid 
						WHERE keys_mgmt.id = :id LIMIT 1";
					}
					else {
						$query = "SELECT keys_mgmt.crsid, keys_mgmt.key_stat, keys_list.key_no, keys_list.key_name, i_to.name as issued_to, i_to_eadd.eadd as eadd, keys_mgmt.auto_rem, i_by.name as issued_by, keys_mgmt.date_issued, keys_mgmt.ext_name, keys_mgmt.ext_email, keys_mgmt.detail_notes FROM keys_mgmt 
						INNER JOIN keys_list ON keys_mgmt.key_id = keys_list.key_id 
						LEFT OUTER JOIN user_details i_to ON keys_mgmt.crsid = i_to.crsid 
						LEFT OUTER JOIN user_details i_by ON keys_mgmt.issued_by = i_by.crsid 
						LEFT OUTER JOIN user_details i_to_eadd ON keys_mgmt.crsid = i_to_eadd.crsid 
						WHERE keys_mgmt.id = :id LIMIT 1";
					}
				}
				$stmt = $db->prepare($query);
				$stmt->execute(array("id" => $id));
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					?>
					<div class="table-responsive">
						<table class="table table-striped table-condensed">
							<thead><tr><th colspan="2"><b><?php echo $details_header ?></b></th></tr></thead>
							<tbody>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_key_no ?>:</b></td><td><?php echo $row['key_no'] ?></td></tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_key_name ?>:</b></td><td><?php echo $row['key_name'] ?></td></tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php if (!empty($row['issued_to'])) { echo $details_issued_to; } else { echo $details_issued_to." (Ext)"; } ?>:</b></td>
									<td>
										<?php 
										if (!empty($row['issued_to']) && $row['key_stat'] == 1) { 
											echo $row['issued_to']." (".$row['crsid'].")&nbsp;&nbsp;<a href='mailto:".$row['eadd']."?subject=Unreturned%20key'><i class='fa fa-envelope'></i></a>"; 
										} elseif (!empty($row['issued_to']) && $row['key_stat'] == 0) {
											echo $row['issued_to']." (".$row['crsid'].")";
										} elseif (!empty($row['ext_email']) && $row['key_stat'] == 1) { 
											echo $row['ext_name']."&nbsp;&nbsp;<a href='mailto:".$row['ext_email']."?subject=Unreturned%20key'><i class='fa fa-envelope'></i></a>"; 
										} else {
											echo $row['ext_name'];
										}
										?>
									</td>
								</tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_issued_by ?>:</b></td><td><?php echo $row['issued_by'] ?></td></tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_date_issued ?>:</b></td><td><?php echo date("d-m-Y H:i:s", strtotime($row['date_issued'])); ?></td></tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_returned_by ?>:</b></td><td><?php if (!empty($row['returned_by'])) { echo $row['returned_by']; } else { echo "--"; } ?></td></tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_date_returned ?>:</b></td><td><?php if (!empty($row['date_returned'])) { echo date("d-m-Y H:i:s", strtotime($row['date_returned'])); } else { echo "--"; } ?></td></tr>
								<?php
								if (empty($row['date_returned'])) {
									?>
									<tr><td colspan="2" class="col-xs-12"><b><?php echo "Notes" ?>:</b></td></tr>
									<tr>
										<td colspan="2" class="col-xs-12">
											<form name="notesForm" data-toggle="validator" class="form-horizontal" method="post" action="./" role="form">
												<textarea class="form-control" name="detail_notes" style="min-width: 50%; height: 60px;"><?php echo $row['detail_notes'] ?></textarea>
												<input type="hidden" name="id" value="<?php echo $id ?>">
												<button type="submit" class="btn btn-default btn-xs pull-right" style="margin-top: 10px;">Submit</button>
											</form>
										</td>
									</tr>
								<?php
								}
								else {
									if (!empty($row['detail_notes'])) {
										echo "<tr><td class='col-xs-12' colspan='2'><b>".$details_notes.":</b></td></tr>";
										echo "<tr><td class='col-xs-12' colspan='2' style='word-wrap: break-word; width: 320px; white-space: normal;'>".$row['detail_notes']."</td></tr>";
									}
								}
								$query2 = "SELECT * FROM keys_reminder WHERE kr_id=:kr_id";
								$stmt2 = $db->prepare($query2);
								$stmt2->execute(array(":kr_id" => $id));
								if ($stmt2->rowCount() > 0) {
									$reminder_arr = $stmt2->fetchAll(); ?>
									<tr><td class="col-xs-12" colspan="2"><b>Reminders:</b></td></tr>
									<tr><td class="col-xs-12" colspan="2" style="word-wrap: break-word; width: 320px; white-space: normal;">
										<?php
										foreach($reminder_arr as $rem) {
											echo "<span class='text-muted'>[".date('d-m-Y', strtotime($rem['date_ins']))."]</span> ".$rem['reminder']."<br>";
										}
										?>
									</td></tr>
								<?php } ?>
							</tbody>									
						</table>
					</div>
				<?php
				}
			}
			else {
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg.".</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
			}
			?>	
		</div>
	</div>    
</body>
</html>