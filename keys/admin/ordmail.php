<?php
include("../../settings/ver.php");
// Set the timezone
date_default_timezone_set("Europe/London");
// Send order request
if (filter_var($_GET['ordmail'], FILTER_VALIDATE_EMAIL)) {
	$message = "<b>Key tags/reader order enquiry</b><br>Tags: ".$_GET['tags']."<br>Mifare Reader: ".$_GET['mreader']."<br><br><i>This email was generated from within OFMS software</i>";
	$headers = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From:" . $_GET['ordmail'] . "\r\n";
	if(mail($supEmail,"Keys order enquiry",$message,$headers)) {
		echo "<span>Your order request has been submitted!<br><br>We will be in touch shortly.</span>";
	} else {
		echo "<span style='color:red;'>There was an error submitting your request, please try again.";
	}
} else {
	echo "<span style='color:red;'>The email is not valid, please try again.";
}

?>