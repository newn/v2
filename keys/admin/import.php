<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
// Limit access to key admins only
if ($keys_admin == "true") {
    // Quick add key
    if (isset($_GET['key_id'])) {
        if (!empty($_GET['key_id']) && !empty($_GET['key_no'])) { // Make sure required fields are populated
            if (ctype_digit($_GET['key_id']) && strlen($_GET['key_id']) < 11 && ctype_digit($_GET['key_group'])) { // make sure fields are on correct format
                $query = "INSERT INTO keys_list (key_id, key_no, key_name, key_group, notes) VALUES ('".$_GET['key_id']."','".$_GET['key_no']."','".$_GET['key_name']."','".$_GET['key_group']."','".$_GET['notes']."')";
                $stmt = $db->prepare($query);
                if ($stmt->execute()) { echo "<span style='color:green;'>Key added successfully!</span>"; } else { echo "<span style='color:red;'>Something went wrong. Please try again.</span>"; }
            } else {
                echo "<span style='color:red;'>ID/Group must be integer values. ID must not exceed 10 digits.</span>";
            }
        }
        else {
           echo "<span style='color:red;'>Some fields are required. Please try again.</span>";
        }
    }
    // Upload from file file 
    if (isset($_GET['up_opt'])) {
        //validate whether uploaded file is a csv file
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', '   application/excel', 'application/vnd.msexcel', 'text/plain');
        if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
            if(is_uploaded_file($_FILES['file']['tmp_name'])){               
                // Delete key_list table first if delete option is chosen
                if ($_GET['up_opt'] == "delete") {
                    $query = "DELETE FROM keys_list";
                    $stmt = $db->prepare($query);
                    $stmt->execute();
                }
                //open uploaded csv file with read only mode
                $csvFile = fopen($_FILES['file']['tmp_name'], 'r');                
                //skip first line
                fgetcsv($csvFile);
                // Count new keys and duplicates
                $new_cnt = 0; // New entries
                $dup_cnt = 0; // Duplicate entries that will be processed
                $nodup_cnt = 0; // Duplicate entries that will not be processed
                //parse data from csv file line by line
                while(($line = fgetcsv($csvFile)) !== FALSE){
                    if (strlen($line[0]) == 9) { $key_id = "0".$line[0]; } else { $key_id = $line[0]; } // Make sure key id has 10 digits
                    //check whether key already exists in database with same id
                    $query = "SELECT id FROM keys_list WHERE key_id = :key_id";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("key_id" => $key_id));
                    if (empty($line[3])) { $grp = 0; } else { $grp = $line[3]; }
                    if ($stmt->rowCount() > 0) {
                        if ($_GET['up_opt'] == "overwrite") {
                            //update key data
                            $db->query("UPDATE keys_list SET key_no = '".$line[1]."', key_name = '".$line[2]."', key_group = '".$grp."', notes = '".$line[4]."' WHERE key_id = ".$key_id);
                            $dup_cnt = $dup_cnt + 1;
                        }
                        else { $nodup_cnt = $nodup_cnt + 1; }
                    }
                    else {
                        //insert key data into database
                        $db->query("INSERT INTO keys_list (key_id, key_no, key_name, key_group, notes) VALUES ('".$key_id."','".$line[1]."','".$line[2]."','".$grp."','".$line[4]."')");
                        $new_cnt = $new_cnt + 1;
                    }
                }                
                //close opened csv file
                fclose($csvFile);   
                // Show summary of processed keys
                echo "Success!<br>";
                if ($new_cnt > 0) { echo "<b>".$new_cnt."</b> new entries were processed.<br>"; }
                if ($dup_cnt > 0) { echo "<b>".$dup_cnt."</b> duplicates were processed.<br>"; }
                if ($nodup_cnt > 0) { echo "<b>".$nodup_cnt."</b> duplicates skipped."; }
            }
            else {
                // Show error
                echo "There was an error while uploading the file. Please check the file and try again.";
            }
        }
        else {
            // Show if file is invalid
            echo "The file is invalid! Please try again.";
        }
    }
}
else {
    echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
    echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
}