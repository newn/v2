<?php
header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<body>
<?php
// Insert values to database
//==================================================			
// Query strings from POST
parse_str($_SERVER['QUERY_STRING']);
// For single reminder
if (isset($id)) {
    // Insert reminder to database
    $query = "INSERT INTO keys_reminder (kr_id,reminder) VALUES (:kr_id,:reminder)";
    $stmt = $db->prepare($query);
    $stmt->execute(array(":kr_id"=>$id,":reminder"=>$remtxt));
    // Send email reminder to user
    $query = "SELECT key_name, date_issued, eadd FROM keys_mgmt ";
    $query .= "INNER JOIN keys_list ON keys_mgmt.key_id = keys_list.key_id ";
    $query .= "INNER JOIN user_details ON keys_mgmt.crsid = user_details.crsid WHERE keys_mgmt.id = :id LIMIT 1";
    $stmt = $db->prepare($query);
    $stmt->execute(array(":id" => $id));
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        // Send email
        if (!empty($remtxt)) { $msg_note = "<br><br><strong>".$remind_message.":</strong><br>".nl2br($remtxt); } else { $msg_note = ""; }
        $message = $remind_email_msg."<br><br><strong>".$key_nm_title.": </strong>".$row['key_name']."<br><strong>".$date_iss.": </strong>".date("d-m-Y H:i:s", strtotime($row['date_issued'])).$msg_note;
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
        mail($row['eadd'],$remind_subj,$message,$headers);       
        // HTML output
        echo "<div>".$remind_summary."</div>";
    }
}
?>
</body>
</html>