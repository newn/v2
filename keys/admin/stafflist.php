<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$tbl_keys_staff = new ajaxCRUD("Staff", "keys_staff", "id", "");
##
########################################################
#i can define a relationship to another table
$tbl_keys_staff->defineRelationship("staff_crsid", "user_details", "crsid", "name");
#i don't want to visually show the primary key in the table
$tbl_keys_staff->omitPrimaryKey();
#the table fields have prefixes; i want to give the heading titles something more meaningful
$tbl_keys_staff->displayAs("card_id", $card_id);
$tbl_keys_staff->displayAs("staff_crsid", $staff_name);
#i can order my table by whatever i want
$tbl_keys_staff->addOrderBy("ORDER BY staff_crsid ASC");
#set the number of rows to display (per page)
$tbl_keys_staff->setLimit(50);
# Disallow add
$tbl_keys_staff->disallowAdd();
# Disallow edit
$tbl_keys_staff->disallowEdit('staff_crsid');
# define texts
$tbl_keys_staff->deleteText = "delete";
$tbl_keys_staff->cancelText = "Close";
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css -->
<link href="../local.css" rel="stylesheet">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <?php
        // Control the view of menu items for the logged user       
        // Show the menu items
        include("../../includes/navbar-menu-items.php");       
        ?>
        </ul>
        <!-- Show user profile and logout option -->
        <ul class="nav navbar-nav navbar-right">
            <?php           
            // Show the right menu items
            include("../../includes/navbar-right-menu-items.php");           
            ?>
        </ul>
    </div>
    </div>
</nav>
<div  class="container" style="max-width: 720px;">    
    <div class="row">
        <div class="col-xs-12">
            <?php                
            // actually show the table if user has access
            if ($keys_admin == "true") {     
                $tbl_keys_staff->showTable();
                # Output number of rows returned
                echo "<button type='button' class='btn btn-static'>Total rows: ";
                $tbl_keys_staff->insertRowsReturned();
                echo "</button>";
                echo "&nbsp;<button type='button' class='btn btn-default' data-toggle='modal' data-target='#addStaffModal'>Add staff</button>";
                echo "<br /><br />";
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
            ?>  
        </div>
        <!-- Modal to display management of keys -->
        <div name="addStaffModal" id="addStaffModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body text-left">
                    <h4 class="modal-title">Add Staff</h4><hr style="margin-bottom: 20px; margin-top: 0px;">
                    <div id="addContent">
                        <div id="addStaffForm" style="margin-top:10px;">
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><?php echo $card_id ?>:</button></span>
                                <input id="card_no" type="number" class="form-control input-sm" placeholder="0000000000" style="max-width:150px;">
                            </div>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><?php echo $staff_crsid ?>:</button></span>
                                <input id="staffid" type="text" class="form-control input-sm" style="max-width:100px;" placeholder="abw23" maxlength="50">
                            </div>
                            <div id="addStaffMsg" style="margin-top:5px;"><!--Message for add staff will appear here--></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span id="addBtnSpan"><button type="button" id="addBtn" class="btn btn-sm btn-default">Add</button></span>
                    <span id="closeBtn"><button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button></span>
                </div>  
            </div>
            </div>
        </div>
        <script type="text/javascript">
        // Use ajax to quick add keys
        $('.modal-footer').on('click','#addBtn',function(){
            var card_no = $("input#card_no").val();
            var staffid = $("input#staffid").val();
            $.ajax({
                type: 'POST',
                url: 'queries.php?card_no='+card_no+'&staffid='+staffid,
                success: function(data) {
                    $('#addStaffForm').find("input").val("");
                    $('#addStaffMsg').html(data);
                    $('#closeBtn').html('<a href="./stafflist.php"><button type="button" class="btn btn-sm btn-default">Reload</button></a>');
                },
                error:function(err){
                    alert("error"+JSON.stringify(err));
                }
            });
        });
        </script>
    </div>
    <br /><br />       
</div>
</body>
</html>