<!-- Return key form -->
<form name="returnForm" data-toggle="validator" class="form-horizontal" method="post" action="." role="form">
	<input type="hidden" name="returned_by" id="returned_by" value="<?php echo $staff_crsid ?>">
	<div class="form-group">
		<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
		<div class="col-sm-6 col-md-4"> 							
  		<div class="btn-group btn-group" role="group" aria-label="Key return">
  			<button id="ret-btn1" type="button" class="btn btn-primary active"><?php echo $return_label ?></button>
  			<button id="ret-btn2" type="button" class="btn btn-default">Multi</button>
		</div>
		</div>
	</div>
	<div class="form-group form-group-lg ret-input">
		<label for ="key_id" class="col-sm-3 col-md-4 control-label">&nbsp;</label>
		<div class="col-sm-6 col-md-4">
		<input type="text" name="key_id[]" id="retkey_id" class="form-control" maxlength="10" placeholder="<?php echo $return_key_id_ph ?>" autocomplete="off" autofocus>
		</div>
	</div>
	<div class="form-group form-group-lg multiret-input">
		<label for ="multikey_id" class="col-sm-3 col-md-4 control-label">&nbsp;</label>
		<div class="col-sm-6 col-md-4">
		<input type="text" name="key_id[]" id="retmultikey_id" class="form-control" placeholder="Scan key tags" autocomplete="off" autofocus>
		</div>
	</div>
	<div class="submit">
		<div class="form-group form-group-lg">
			<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
			<div class="col-sm-6 col-md-4">
				<button type="submit" name="return" id="return" class="btn btn-default btn-sm pull-right" onclick="return confirm('Click OK to return the key or Cancel to go back')"><?php echo $submit ?></button>
			</div>
		</div>
	</div>
</form>