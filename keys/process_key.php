<?php
// get the time and date
$datetime = date('Y-m-d H:i:s');
// Process when key is issued
if(isset($_POST['issue'])) {
    // If key is issued by scanning the user id card
    if(!empty($_POST['user_id'])) {   	
        //*****************************************************************************************************************//
        // Get user crsid from UniofCam Card Office
    	$k_crsid = file_get_contents("http://app4.admin.cam.ac.uk/ws/card_get_crsid_for_mifare_id/index.cgi/".$_POST['user_id']);
		$k_crsid = json_decode($k_crsid, TRUE);
        //*****************************************************************************************************************//
		// If crsid in university card returns empty then die
		if(!empty($k_crsid['crsid'])) { 
            $usr_crsid = $k_crsid['crsid']; 
            // Check if user is on the database and active
            $query = "SELECT crsid FROM user_details WHERE crsid=:crsid AND inactive=0 LIMIT 1";
            $stmt = $db->prepare($query);
            $stmt->execute(array("crsid" => $k_crsid['crsid']));
            if ($stmt->rowCount() > 0) { $usr_crsid = $k_crsid['crsid'];  } else { die ($identify_user_error.'<br /><a href="./"><b>Go back</b></a>'); }
        } else { die ($identify_user_error.'<br /><a href="./"><b>Go back</b></a>'); }
	}
    // Get the value of crsid if internal search is selected
    elseif (!empty($_POST['crsid'])) {
        $usr_crsid = $_POST['crsid'];
    }
    else {
        // If external is selected
        if ($_POST['ext_name'] == "") { die ($identify_user_error.'<br /><a href="./"><b>Go back</b></a>'); }
        $usr_crsid = "";
    }
	// Store the keys in an array
    if (!empty($_POST['key_id'][0])) { $keyIds = explode(",", rtrim($_POST['key_id'][0],',')); }
    if (!empty($_POST['key_id'][1])) { $keyIds = explode(",", rtrim($_POST['key_id'][1],',')); }
    // Remove duplicates
    $keyIds = array_unique($keyIds);                
    // Define array variables
    $keysToIssue = array();
    $keysNotInDb = array();
    $keysStillOut = array();
    foreach ($keyIds as $keyId) {                    
        // Check to see if the key is in database                       
        $query = "SELECT key_id FROM keys_list WHERE key_id = :key_id LIMIT 1";
        $stmt = $db->prepare($query);
        $stmt->execute(array("key_id" => $keyId));
        if ($stmt->rowCount() < 1) {
            $keysNotInDb[] = $keyId; // If key is not in DB then store it in the array
        }
        // Check to see if the key is still out
        $query = "SELECT key_id FROM keys_mgmt WHERE key_stat = :key_stat AND key_id = :key_id LIMIT 1";
        $stmt = $db->prepare($query);
        $stmt->execute(array("key_stat" => 1, "key_id" => $keyId));
        if ($stmt->rowCount() > 0) {
            $keysStillOut[] = $keyId; // If the key is still out then store it in the array
        }
        else { $keysToIssue[] = $keyId; } // If the key has not been issued yet then store it in the array
    }
    // We need to remove the keys not in DB from the $keysToIssue array
    $keysToInsert = array_diff($keysToIssue, $keysNotInDb);
    foreach ($keysToInsert as $keyToInsert) {                   
        // Insert values to database
        $query = "INSERT INTO keys_mgmt (key_id,crsid,ext_name,ext_email,key_stat,issued_by,date_issued) VALUES (:key_id,:crsid,:ext_name,:ext_email,:key_stat,:issued_by,:date_issued)";
        $stmt = $db->prepare($query);
        $stmt->execute(array("key_id"=>$keyToInsert,"crsid"=>$usr_crsid,"ext_name"=>$_POST['ext_name'],"ext_email"=>$_POST['ext_email'],"key_stat"=>1,"issued_by"=>$_POST['issued_by'],"date_issued"=>$datetime));
        // Populate keywords column for search function
        $query = "SELECT crsid, ext_name, key_no, key_name FROM keys_mgmt INNER JOIN keys_list ON keys_mgmt.key_id = keys_list.key_id WHERE keys_mgmt.key_id = :key_id AND date_issued = :date_issued";
        $stmt = $db->prepare($query);
        $stmt->execute(array("key_id" => $keyToInsert, "date_issued" => $datetime));
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($row['ext_name'] == "") {
                $query2 ="SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
                $stmt2 = $db->prepare($query2);
                $stmt2->execute(array("crsid" => $row['crsid']));
                while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                    $query3 = "UPDATE keys_mgmt SET keywords = :keywords WHERE key_id = :key_id AND date_issued = :date_issued";
                    $stmt3 = $db->prepare($query3);
                    $stmt3->execute(array("keywords" => $row['key_no']." ".$row['key_name']." ".$row2['name'],"key_id" => $keyToInsert, "date_issued" => $datetime));
                }
            }
            else {
                $query2 = "UPDATE keys_mgmt SET keywords = :keywords WHERE key_id = :key_id AND date_issued = :date_issued";
                $stmt2 = $db->prepare($query2);
                $stmt2->execute(array("keywords" => $row['key_no']." ".$row['key_name']." ".$row['ext_name'],"key_id" => $keyToInsert, "date_issued" => $datetime));
            }
        }
    }
    // Print a warning message for keys not in database or not yet returned
    if (!empty($keysNotInDb) || !empty($keysStillOut)) {
        echo "<div class='text-center' style='margin-bottom: 20px;'>";
        echo "<font color='red'><b>WARNING!</b></font> These keys have not been issued:<br>";
        foreach ($keysNotInDb as $keyNotInDb) {
            echo "<b>".$keyNotInDb."</b> - <i>Not in database</i><br>";
        }
        foreach ($keysStillOut as $keyStillOut) {
            $query = "SELECT key_no FROM keys_list WHERE key_id = :key_id LIMIT 1";
            $stmt = $db->prepare($query);
            $stmt->execute(array("key_id" => $keyStillOut));
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo "<b>".$row['key_no']."</b> - <i>Still out</i><br>";
            }
        }
        echo "</div>";
    }												
}			
// Update database if key is returned
if(isset($_POST['return'])) {				
	// Store the keys in an array
    if (!empty($_POST['key_id'][0])) { $keyIds = explode(",", rtrim($_POST['key_id'][0],',')); }
    if (!empty($_POST['key_id'][1])) { $keyIds = explode(",", rtrim($_POST['key_id'][1],',')); }
    // Remove duplicates
    $keyIds = array_unique($keyIds);               
    // Define array variables
    $keysToReturn = array();
    $keysNotIssued = array();
    foreach ($keyIds as $keyId) {
        // Check to see if key was not issued
        $query = "SELECT key_id FROM keys_mgmt WHERE key_stat = :key_stat AND key_id = :key_id LIMIT 1";
        $stmt = $db->prepare($query);
        $stmt->execute(array("key_stat" => 1, "key_id" => $keyId));
        if ($stmt->rowCount() > 0) { $keysToReturn[] = $keyId; } // If the key has been issued then store it in the array
            else { $keysNotIssued[] = $keyId; } // If the key was not issued then store it in the array
    }                
    foreach ($keysToReturn as $keyToReturn) {
        // Update the database when key is returned
        $query = "UPDATE keys_mgmt SET key_stat = :key_stat, returned_by = :returned_by, date_returned = :date_returned WHERE key_id = :key_id AND key_stat = 1";
        $stmt = $db->prepare($query);
        $stmt->execute(array("key_stat"=>0,"returned_by"=>$_POST['returned_by'],"date_returned"=>$datetime,"key_id"=>$keyToReturn));
    }
    if (!empty($keysNotIssued)) {
        echo "<div class='text-center'>";
        echo "<font color='red'><b>WARNING!</b></font> These keys have not been returned:<br>";
        foreach ($keysNotIssued as $keyNotIssued) {
            $query ="SELECT key_no FROM keys_list WHERE key_id = :key_id LIMIT 1";
            $stmt = $db->prepare($query);
            $stmt->execute(array("key_id" => $keyNotIssued));            
            if ($stmt->rowCount() > 0) { 
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    echo "<b>".$row['key_no']."</b> - <i>Not issued yet</i><br>"; // Not issued
                }
            }
            else { echo "<b>".$keyNotIssued."</b> - <i>Not in database</i><br>"; } // Not in database
        }
        echo "<br></div>";
    }
}
?>