<!-- Issue key form -->
<form name="issueForm" id="issueForm" data-toggle="validator" class="form-horizontal" method="post" action="." role="form">
	<input type="hidden" name="issued_by" id="issued_by" value="<?php echo $staff_crsid ?>">
	<div class="form-group">
		<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
		<div class="col-sm-6 col-md-4">
  		<div class="btn-group btn-group" role="group" aria-label="Key issue">
  			<button id="iss-btn1" type="button" class="btn btn-primary active"><?php echo $issue_label ?></button>
  			<button id="iss-btn2" type="button" class="btn btn-default">Multi</button>
		</div>
		</div>
	</div>						
	<div class="form-group form-group-lg iss-input">
		<label for ="key_id" class="col-sm-3 col-md-4 control-label">&nbsp;</label>
		<div class="col-sm-6 col-md-4">
		<input type="text" name="key_id[]" id="isskey_id" class="form-control" maxlength="10" placeholder="<?php echo $issue_key_id_ph ?>" autocomplete="off" autofocus>
		</div>
		
	</div>
	<div class="form-group form-group-lg multiiss-input">
		<label for ="key_id" class="col-sm-3 col-md-4 control-label">&nbsp;</label>
		<div class="col-sm-6 col-md-4">
		<input type="text" name="key_id[]" id="issmultikey_id" class="form-control" placeholder="Scan key tags" autocomplete="off" autofocus>
		</div>
	</div>					
	<!-- This is where the key numbers will be stored to display on confirm window -->
	<div class="showkeynodiv" style="display:none"></div>
	<div class="form-group form-group-lg">
		<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
		<div class="col-sm-6 col-md-4"> 							
  			<div class="radio">
  				<label><big><input type="radio" name="search_radio" id="search_radio" value="scan" checked><?php echo $issue_scan ?>&nbsp;&nbsp;</big></label>
				<label><big><input type="radio" name="search_radio" id="search_radio" value="inst"><?php echo $issue_search ?>&nbsp;&nbsp;</big></label>
				<label><big><input type="radio" name="search_radio" id="search_radio" value="ext"><?php echo $issue_external ?></big></label>
			</div>
		</div>
	</div>
	<div class="scan">
		<div class="form-group form-group-lg">
			<label for ="name" class="col-sm-3 col-md-4 control-label">&nbsp;</label>
			<div class="col-sm-6 col-md-4">
			<input type="text" name="user_id" id="user_id" class="form-control" maxlength="10" placeholder="<?php echo $issue_user_id_ph ?>" autocomplete="off">
			</div>
		</div>
	</div>
	<div class="inst">
		<div class="form-group form-group-lg">
			<label for ="name" class="col-sm-3 col-md-4 control-label">&nbsp;</label>
			<div class="col-sm-6 col-md-4">
			<input type="text" id="name" class="form-control" maxlength="50" placeholder="<?php echo $issue_name_ph1 ?>">
			<input type="hidden" name="crsid" id="crsid">
			</div>
		</div>
	</div>
	<div class="ext">
		<div class="form-group form-group-lg">
			<label for ="ext_name" class="col-sm-3 col-md-4 control-label">&nbsp;</label>
			<div class="col-sm-6 col-md-4">
			<input type="text" name="ext_name" id="e_name" class="form-control" maxlength="50" placeholder="<?php echo $issue_name_ph2 ?>">
			</div>
		</div>
		<div class="form-group form-group-lg">
			<label for ="ext_email" class="col-sm-3 col-md-4 control-label">&nbsp;</label>
			<div class="col-sm-6 col-md-4">
			<input type="text" name="ext_email" id="e_email" class="form-control" maxlength="50" placeholder="<?php echo $issue_email_ph ?>">
			</div>
		</div>
	</div>
	<div class="submit">
		<div class="form-group form-group-lg">
			<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
			<div class="col-sm-6 col-md-4">
			<button type="submit" name="issue" id="issue" class="btn btn-default btn-sm pull-right" onclick="return confirm_issue()"><?php echo $submit ?></button>
			</div>
		</div>
	</div>
</form>