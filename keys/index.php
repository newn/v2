<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Hide submit button
================================================== -->  
<script type="text/javascript">
	$(function () {
		$(".submit").hide();
	})
</script>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
  	<ul class="nav navmenu-nav">
		<?php
  		// Show the sidebar items
  		include("../includes/sidebar-menu-items.php");
  		?>
  	</ul>
</div>	
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
    </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
        <?php
		// Control the view of menu items for the logged person		
		// Show the menu items
		include("../includes/navbar-menu-items.php");
		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php           
            // Show the right menu items
            include("../includes/navbar-right-menu-items.php");           
            ?>
		</ul>
	</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php
			// Process the key being issued/returned
            require_once("process_key.php");
            // Check if user is authorised
			if ($keys_admin == "true" || $keys_manager == "true") {
			?>
				<form name="IdentifyForm" data-toggle="validator" class="form-horizontal" method="post" action="key.php" role="form">
				<div class="form-group">
					<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
					<div class="col-sm-6 col-md-4">
  					<label><big><?php echo $identify_title ?></big></label>
					</div>
				</div>
				<div class="form-group form-group-lg">
					<label for ="person_id" class="col-sm-3 col-md-4 control-label"><?php echo $identify_person_id ?></label>
					<div class="col-sm-6 col-md-4">
					<input type="text" name="person_id" class="form-control" maxlength="10" placeholder="<?php echo $identify_card_ph ?>" autocomplete="off" autofocus>
					<!-- Help -->
					<p><br /><br /><font color="#2e8bcc"><b>Help</b></font> <a href="#" tabindex="0" data-toggle="popover" title="" data-trigger="focus" data-html="true" data-content='Scan your University card using an rfid/mifare reader'><span class="glyphicon glyphicon-question-sign"></span></a></p>
					</div>
				</div>
				<div class="submit">
					<div class="form-group form-group-lg">
						<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
						<div class="col-sm-6 col-md-4">
						<button type="submit" name="identify" id="identify" class="btn btn-primary active"><?php echo $submit ?></button>
						</div>
					</div>
				</div>
				</form>
			<?php
			}
			else {
				// If user is not authorised, this message will appear
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
            	echo "<p><a href='../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
			}
			?>
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>