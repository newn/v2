<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Let's hide some form fields -->  
<script type="text/javascript" src="assets/frmfields.js"></script>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
		<?php
     	// Show the sidebar items
     	include("../includes/sidebar-menu-items.php");
     	?>
    </ul>
</div>	
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
        	   <span class="icon-bar"></span>
        	   <span class="icon-bar"></span>
        	   <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
    	       <?php
			// Control the view of menu items for the logged user
			// Show the menu items
			include("../includes/navbar-menu-items.php");
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
    	        // Show the right menu items
    	        include("../includes/navbar-right-menu-items.php");           
    	        ?>
			</ul>
		</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php
			// Determine the staff logging in
			if (!empty($_POST['person_id'])) {	
				$query = "SELECT staff_crsid, name FROM keys_staff INNER JOIN user_details ON keys_staff.staff_crsid = user_details.crsid WHERE card_id = :card_id";
                $stmt = $db->prepare($query);
                $stmt->execute(array("card_id" => $_POST['person_id']));
                if ($stmt->rowCount() < 1) { 
                	die ($identify_staff_error.'<br /><a href="./"><button type="button" class="btn btn-default btn-sm">Back</button></a>&nbsp;<a href="admin/stafflist.php"><button type="button" class="btn btn-default btn-sm">Add staff</button></a>'); 
                }
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                	$staff_crsid = $row['staff_crsid'];
                	$staff_active_name = $row['name'];
                }
				// Show the form if user has access
				if ($keys_admin == "true" || $keys_manager == "true") {
					// Show issue form first
					if ($appset_issretord == 1) { include("key-iss.php"); include("key-ret.php"); }
					// Show return form first
					if ($appset_issretord == 2) { include("key-ret.php"); include("key-iss.php"); }
					?>					
					<!-- JQuery functions
					====================================================================================== -->
					<script type="text/javascript">
						// Show the key_no value from the key_id
						$('#isskey_id,#issmultikey_id').change(function(){
   							if ($('#isskey_id').val() !== '') { var keyid=$('#isskey_id').val(); }
   							if ($('#issmultikey_id').val() !== '') { var keyid=$('#issmultikey_id').val(); }
   							$.ajax({url:"showkeyno.php?keyid="+keyid,cache:false,success:function(result){
       							$(".showkeynodiv").html(result);
   							}});
						});
						// Issue popup with key number details
						function confirm_issue() {
  							var msg_keyno = $(".showkeynodiv").text();
  							return confirm(msg_keyno);
						}
					</script>
					<!-- We need to change the behaviour the enter key to be tab key for some input fields
					====================================================================================== -->
					<script type="text/javascript">
						document.getElementById('isskey_id').onkeypress = function (e) { if (e.which === 13) { document.getElementById("user_id").focus(); return false; } };
						document.getElementById('issmultikey_id').onkeypress = function (e) { if (e.which === 13) { document.getElementById('issmultikey_id').value += ','; return false; } };
						document.getElementById('retmultikey_id').onkeypress = function (e) { if (e.which === 13) { document.getElementById('retmultikey_id').value += ','; return false; } };
					</script>					
					<div class="form-group">
						<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
						<div class="col-sm-6 col-md-4"><b><?php echo $staff_active ?>:</b> <i><?php echo $staff_active_name ?><i/>
							<!-- Help -->
							<p><br /><font color="#2e8bcc"><b>Help</b><font> <a href="#" tabindex="0" data-toggle="popover" title="" data-trigger="focus" data-html="true" data-content='<b>Return key</b><br><u>Return</u>: Scan card/key to return<br><u>Multi</u>: Scan multiple keys to return<br><br><b>Issue key</b><br /><u>Issue</u>: Scan card/key to issue<br><u>Multi</u>: Scan multiple keys to issue<br><u>Scan</u>: Scan user id card to identify<br /><u>Search</u>: Search user from database<br /><u>External</u>: Manually type user details'><span class="glyphicon glyphicon-question-sign"></span></a></p>
						</div>
					</div>
				<?php
				}
				else {
					// If person is not authorised, this message will appear
					echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
               	   	echo "<p><a href='../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
				}
			}
			else {
				// If person id has no value then return to identification page
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$identify_error."</h4><br><p>".$identify_staff_error."</p><br>";
               	echo "<p><a href='./' class='btn btn-primary active' role='button'>".$identify_button."</a></p>";
			}
			?>
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>