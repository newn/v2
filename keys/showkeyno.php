<?php
// Define user's identity and access
require_once("../includes/access.php");
// Store the keys in an array
$keyIds = explode(",", rtrim($_GET['keyid'],','));
// Remove duplicates
$keyIds = array_unique($keyIds);
echo "The following keys will be issued:\n";
foreach ($keyIds as $keyId) {
	// Find the key number on the database from the key id
	$query = "SELECT key_no FROM keys_list WHERE key_id = :key_id LIMIT 1";
	$stmt = $db->prepare($query);
	$stmt->execute(array("key_id" => $keyId));
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		if(!empty($row['key_no'])) { echo $row['key_no']."\n"; }
	}
}
echo "\nClick OK to issue this key or Cancel to go back";
?>