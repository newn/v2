// Update settings
$('.settingForm').on('click','#save',function(){
    var emailfrom = $("input#emailfrom").val();
    var actrowlimit = $("input#actrowlimit").val();
    var arcrowlimit = $("input#arcrowlimit").val();
    var arcviewlimit = $("input#arcviewlimit").val();
    var keyrowlimit = $("input#keyrowlimit").val();
    $.ajax({
        type: 'POST',
        url: '../../includes/app-settings.php?app=keys&appset_emailfrom='+emailfrom+'&appset_actrowlimit='+actrowlimit+'&appset_arcrowlimit='+arcrowlimit+'&appset_arcviewlimit='+arcviewlimit+'&appset_keyrowlimit='+keyrowlimit,
        success: function(data) {
          $('#saveMsg').html(data);
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
// Get values when edit key group modal is opened
$('#editSpan').on('click','#editGrpBtn',function(){
    var grptxt = $("#keygroup option:selected").text();
    var grptxt = grptxt.replace(/ *\([^)]*\) */g, ""); // Omit the id number from group name
    var grpval = $("#keygroup option:selected").val();
    $("input#grpTxt").val(grptxt);
    $("input#grpVal").val(grpval);
    $("#grptitle").html("Edit group name:");
    $.ajax({
        type: 'POST',
        url: 'queries.php?gethidgrp&id='+grpval,
        success: function(data) {
            if (data == 1) { $('#hidgrp').prop('checked', true); }
            else { $('#hidgrp').prop('checked', false); }
        },
        error:function(err){
            alert("error"+JSON.stringify(err));
        }
    });
    $("#delgrpdiv").show();
    $("#editSaveSpan").show();
    $("#editAddSpan").hide();
});
// When add key group modal is opened
$('#addSpan').on('click','#addGrpBtn',function(){
    $("#grptitle").html("Add group:");
    $("#delgrpdiv").hide();
    $("input#grpTxt").val('');
    $("#editSaveSpan").hide();
    $("#editAddSpan").show();
});
// When options for key group modal is opened
$('#optSpan').on('click','#optGrpBtn',function(){
    var grpval = $("#keygroup option:selected").val();
    var grpname = $("#keygroup option:selected").text();
    var grpname = grpname.replace(/ *\([^)]*\) */g, ""); // Omit the id number from group name 
    $("#grpName").html("<b>"+grpname+"</b>");
    $.ajax({
        type: 'POST',
        url: 'queries.php?optkeygrpmod&grp_id='+grpval,
        success: function(response) {
            if (response == "0") { // If no reminders are set, clone the reminder options template
                if($("#opt-1").length == 0) { // Avoid displaying the form on a loop (happens when button is clicked multiple times without refreshing the page)
                    var clone = $("#optfields-0").clone();
                    clone.find("[id='opt-0']").attr("id","opt-1");
                    clone.find("[id='opt-1']").show();
                    clone.find("#optTitle-0").html("Reminder 1:<br>");
                    clone.find("[id='optTitle-0']").attr("id","optTitle-1");
                    clone.find("[id='remId-0']").val("1");
                    clone.find("[id='remId-0']").attr("id","remId-1");
                    clone.find("[id='remTime-0']").val("24:00:00");
                    clone.find("[id='remTime-0']").attr("id","remTime-1");
                    clone.find("[id='remDur-0']").val("");
                    clone.find("[id='remDur-0']").attr("id","remDur-1");
                    clone.find("[id='remSubj-0']").val("");
                    clone.find("[id='remSubj-0']").attr("id","remSubj-1");
                    clone.find("[id='remCc-0']").val("");
                    clone.find("[id='remCc-0']").attr("id","remCc-1");
                    clone.find("[id='remBcc-0']").val("");
                    clone.find("[id='remBcc-0']").attr("id","remBcc-1");
                    clone.find("[id='remMsg-0']").html("Type message here...");
                    clone.find("[id='remMsg-0']").attr("id","remMsg-1");
                    clone.insertAfter("div.optfields:last");
                    $("#addRemDiv").find("[id='addRemBtn']").attr("data-id","2");
                    $(".remDelDiv").hide();
                }
            } else { // If reminders are set, fetch the data and populate the form
                var i=1;
                var data = jQuery.parseJSON(response);
                $.each(data, function(key,value) {
                    if($("#opt-"+i).length == 0) { // Avoid displaying the form on a loop (happens when button is clicked multiple times without refreshing the page)
                        var clone = $("#optfields-0").clone();
                        clone.find("[id='opt-0']").attr("id","opt-"+i);
                        clone.find("[id='opt-"+i+"']").show();
                        clone.find("#optTitle-0").html("Reminder "+i+":<br>");
                        clone.find("[id='optTitle-0']").attr("id","optTitle-"+i);
                        clone.find("[id='remId-0']").val(i);
                        clone.find("[id='remId-0']").attr("id","remId-"+i);
                        clone.find("[id='remTime-0']").val(value.fixed_time);
                        clone.find("[id='remTime-0']").attr("id","remTime-"+i);
                        if (value.dur_time == "0") { 
                            clone.find("[id='remDur-0']").val("");
                            clone.find("[id='remDur-0']").prop('disabled', true);
                        }
                        else { clone.find("[id='remDur-0']").val(value.dur_time); }
                        clone.find("[id='remDur-0']").attr("id","remDur-"+i);
                        clone.find("[id='remSubj-0']").val(value.subject);
                        clone.find("[id='remSubj-0']").attr("id","remSubj-"+i);
                        clone.find("[id='remCc-0']").val(value.e_cc);
                        clone.find("[id='remCc-0']").attr("id","remCc-"+i);
                        clone.find("[id='remBcc-0']").val(value.e_bcc);
                        clone.find("[id='remBcc-0']").attr("id","remBcc-"+i);
                        clone.find("[id='remMsg-0']").html(value.message);
                        clone.find("[id='remMsg-0']").attr("id","remMsg-"+i);
                        clone.find("[id='remDel-0']").attr("data-id",i);
                        clone.find("[id='remDel-0']").attr("id","remDel-"+i);
                        clone.insertAfter("div.optfields:last");
                    }
                    i++;
                });
                $("#addRemDiv").find("[id='addRemBtn']").attr("data-id", i);
            }
            $("[id^=remTime-]").change(function(){ // Toggle betwwen fixed and duration time
                if ($(this).val() == "24:00:00") {
                    $(this).parent().next().find("[id^=remDur-]").prop('disabled', false);
                } else {
                    $(this).parent().next().find("[id^=remDur-]").val('');
                    $(this).parent().next().find("[id^=remDur-]").prop('disabled', true);
                }
            });
            $("[id^=remDel-]").click( function(){ // Delete key group reminder
                var i = $(this).attr("data-id");
                var grpval = $("#keygroup option:selected").val();
                $.ajax({
                    type: 'POST',
                    url: 'queries.php?delgrprem&rem_id='+i+'&grp_id='+grpval,
                    beforeSend: function(){
                        return confirm("Are you sure you want to delete this reminder?");
                    },
                    success: function(data) {
                        $("#opt-"+i).hide();
                        $(".modal-footer").hide();
                        $("#addRemDiv").hide();
                        $('#optSaveMsg').html(data);
                    },
                    error:function(err){
                        alert("error"+JSON.stringify(err));
                    }
                });
            });
        },
        error:function(err){
            alert("error"+JSON.stringify(err));
        }
    });
});
// Edit key group
$('#keyGrpModal').on('click','#editSave',function(){
    var grptxt = $("input#grpTxt").val();
    var grpval = $("input#grpVal").val();
    var del = 0;
    var hid = 0;
    if($('#delgrp').prop('checked')) { var del = 1; }
    if($('#hidgrp').prop('checked')) { var hid = 1; }
    $.ajax({
        type: 'POST',
        url: 'queries.php?editkeygrp&id='+grpval+'&groups='+grptxt+'&del='+del+'&hid='+hid,
        success: function(data) {
            $('#saveMsg').html(data);
        },
        error:function(err){
            alert("error"+JSON.stringify(err));
        }
    });
    $('#keyGrpModal').modal('hide');
});
// Add key group
$('#keyGrpModal').on('click','#editAdd',function(){
    var grptxt = $("input#grpTxt").val();
    var hid = 0;
    if($('#hidgrp').prop('checked')) { var hid = 1; }
    $.ajax({
        type: 'POST',
        url: 'queries.php?addkeygrp&groups='+grptxt+'&hid='+hid,
        success: function(data) {
            $('#saveMsg').html(data);
        },
        error:function(err){
            alert("error"+JSON.stringify(err));
        }
    });
    $('#keyGrpModal').modal('hide');
});
// Insert/update key group reminder options
$('#keyGrpOptModal').on('click','#optSave',function(){
    var i=0;
    $(".optfields").each(function(){
        var grpval = $("#keygroup option:selected").val();
        var remid = $("#remId-"+i).val();
        var remtime = $("#remTime-"+i+"  option:selected").val();
        var remdur = $("#remDur-"+i).val();
        var remcc = $("#remCc-"+i).val();
        var rembcc = $("#remBcc-"+i).val();
        var remsubj = $("#remSubj-"+i).val();
        var remmsg = $("#remMsg-"+i).html();
        $.ajax({
            type: 'POST',
            url: 'queries.php?optkeygrp&grp_id='+grpval+'&rem_id='+remid+'&fixed_time='+remtime+'&dur_time='+remdur+'&e_cc='+remcc+'&e_bcc='+rembcc+'&message='+remmsg+'&subject='+remsubj,
            success: function(data) {
                $('#optSaveMsg').html(data);
                $("#addRemDiv").hide();
                $(".modal-footer").hide();
            },
            error:function(err){
                alert("error"+JSON.stringify(err));
            }
        });
        i++;
    });
});
// Add key option form
$('#addRemBtn').click( function(){
    var i = $(this).attr('data-id');
    var clone = $("#optfields-0").clone();
    clone.find("[id='opt-0']").attr("id","opt-"+i);
    clone.find("[id='opt-"+i+"']").show();
    clone.find("#optTitle-0").html("Reminder "+i+":<br>");
    clone.find("[id='optTitle-0']").attr("id","optTitle-"+i);
    clone.find("[id='remId-0']").val(i);
    clone.find("[id='remId-0']").attr("id","remId-"+i);
    clone.find("[id='remTime-0']").val("24:00:00");
    clone.find("[id='remTime-0']").attr("id","remTime-"+i);
    clone.find("[id='remDur-0']").val("");
    clone.find("[id='remDur-0']").attr("id","remDur-"+i);
    clone.find("[id='remSubj-0']").val("");
    clone.find("[id='remSubj-0']").attr("id","remSubj-"+i);
    clone.find("[id='remCc-0']").val("");
    clone.find("[id='remCc-0']").attr("id","remCc-"+i);
    clone.find("[id='remBcc-0']").val("");
    clone.find("[id='remBcc-0']").attr("id","remBcc-"+i);
    clone.find("[id='remMsg-0']").html("Type message here...");
    clone.find("[id='remMsg-0']").attr("id","remMsg-"+i);
    clone.find("[id='remDel-0']").attr("data-id",i);
    clone.find("[id='remDel-0']").attr("id","remDel-"+i);
    clone.insertAfter("div.optfields:last");
    $("#addRemDiv").hide();
    $(".remDelDiv:last").hide();
    $("[id^=remTime-]").change(function(){ // Toggle between fixed and duration time
        if ($(this).val() == "24:00:00") {
            $(this).parent().next().find("[id^=remDur-]").prop('disabled', false);
        } else {
            $(this).parent().next().find("[id^=remDur-]").val('');
            $(this).parent().next().find("[id^=remDur-]").prop('disabled', true);
        }
    });
});
// Refresh page on modal hide
$('#keyGrpOptModal').on('hidden.bs.modal', function () {
    window.location.href=window.location.href;
});

