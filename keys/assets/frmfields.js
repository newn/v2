// Show or hide form fields

$(document).ready(function(){
    //$(".submit").hide();
    $(".inst").hide();
    $(".ext").hide();
    $(".scan").show();
    $(".multiret-input").hide();
    $(".multiiss-input").hide();
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="scan"){
            $(".inst").hide();
            $("input[id='name']").val("")
            $("input[id='crsid']").val("");
            $(".ext").hide();
            $("input[id='e_name']").val("");
            $("input[id='e_email']").val("");
            $(".scan").show();
        }
        if($(this).attr("value")=="inst"){
            $(".scan").hide();
            $("input[id='user_id']").val("");
            $(".ext").hide();
            $("input[id='e_name']").val("");
            $("input[id='e_email']").val("");
            $(".inst").show();
        }
        if($(this).attr("value")=="ext"){
            $(".scan").hide();
            $("input[id='user_id']").val("");
            $(".inst").hide();
            $("input[id='name']").val("")
            $("input[id='crsid']").val("");
            $(".ext").show();
        }
    });
    $('#ret-btn2').click(function() {
        if ($('#ret-btn2').hasClass('btn-default')) { 
            $(this).addClass('btn-success').removeClass('btn-default '); 
            $('.ret-input').hide();
            $("input[id='retkey_id']").val("");
            $('.multiret-input').show();
            $("input[id='retmultikey_id']").focus();
        }
        else { 
            $(this).addClass('btn-default').removeClass('btn-success ');
            $("input[id='retmultikey_id']").val(""); 
            $('.ret-input').show();
            $('.multiret-input').hide();
            $("input[id='retkey_id']").focus();
        }
    });
    $('#iss-btn2').click(function() {
        if ($('#iss-btn2').hasClass('btn-default')) { 
            $(this).addClass('btn-success').removeClass('btn-default '); 
            $('.iss-input').hide();
            $("input[id='isskey_id']").val("");
            $('.multiiss-input').show();
            $("input[id='issmultikey_id']").focus();
        }
        else { 
            $(this).addClass('btn-default').removeClass('btn-success ');
            $('.iss-input').show();
            $('.multiiss-input').hide();
            $("input[id='issmultikey_id']").val("");
            $("input[id='isskey_id']").focus(); 
        }
    });
});