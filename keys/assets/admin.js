// Populate the details of entries to the modal
$('#resultTbl').on('click','.dtlBtn',function(){
    var id = $(this).data('id');
    $.ajax({
        type: 'POST',
        url: 'getDetails.php?id='+id,
        success: function(data) {
          $('.dtlBody').html(data);
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
// Set remind modal with id value
$('#resultTbl').on('click','#addBtnRow',function(){
    var id = $(this).data('id');
    $("input#id").val(id);
});
// Update entries with remind notes
$('#remSend').on('click','#submit',function(){
    var id = $("input#id").val();
    var remtxt = $("input#remNote").val();
    $.ajax({
        type: 'POST',
        url: 'remind.php?id='+id+'&remtxt='+remtxt,
        beforeSend: function(){
            $('#remBody').html("Submitting, please wait...");
            $('#remSend').html('');
        },
        success: function(data) {
            $('#remBody').html(data);
            $('#remSend').html('<button type="button" class="btn btn-default btn-sm" data-dismiss="modal" onClick="window.location.reload();">Close</button>');
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
// Delete row
function delrow(id){
    $.ajax({
        type: 'POST',
        url: 'queries.php?delrow&id='+id,
        beforeSend: function(){ return confirm("Are you sure you want to delete this? This cannot be undone."); },
        success: function(data) {
            var table = "v_keys_mgmt";
            $("tr[id^=" + table + "_row_" + data +"]").fadeOut('slow');
            updateRowCount(table);
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
}