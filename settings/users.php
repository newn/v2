 <?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");

require_once("../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$tbl_user_details = new ajaxCRUD("User", "user_details", "id", "");
##
########################################################  
$tbl_user_details->omitPrimaryKey();
#the table fields have prefixes; i want to give the heading titles something more meaningful
$tbl_user_details->displayAs("crsid", "User ID");
$tbl_user_details->displayAs("name", "Name");
$tbl_user_details->displayAs("eadd", "Email");
$tbl_user_details->displayAs("assoc", "Association");
$tbl_user_details->displayAs("phone", "Phone");
$tbl_user_details->displayAs("groups", "Groups");
$tbl_user_details->displayAs("inactive", "Inactive");
$tbl_user_details->displayAs("lkup_sync", "Sync");
#i could omit a field if I wanted
if (isset($_GET['inact'])) { $tbl_user_details->omitField("lkup_sync"); }
else { $tbl_user_details->omitField("inactive"); }
$tbl_user_details->omitField("added_on");
$tbl_user_details->omitField("modified_on");
#i could disallow editing for certain, individual fields
$tbl_user_details->disallowEdit('crsid');
#set the number of rows to display (per page)
$tbl_user_details->setLimit(50);
#i can order my table by whatever i want
$tbl_user_details->addOrderBy("ORDER BY crsid ASC");
# Show or hide hidden items
if (isset($_GET['inact'])) { $tbl_user_details->addWhereClause("WHERE (inactive='1')"); }
else { $tbl_user_details->addWhereClause("WHERE (inactive='0')"); }
#set a filter box at the top of the table
$tbl_user_details->addAjaxFilterBox('name');
#disallow deleting/adding of rows
$tbl_user_details->disallowAdd();
# Define checkboxes
if (isset($_GET['inact'])) { $tbl_user_details->defineCheckbox('inactive','1','0'); }
else { $tbl_user_details->defineCheckbox('lkup_sync','1','0'); }
#define allowable values for assoc
$assoc_values = array("staff", "student");
$tbl_user_details->defineAllowableValues("assoc", $assoc_values);
# set initial value for groups
$tbl_user_details->setInitialAddFieldValue ("groups", "default");
# define texts
$tbl_user_details->cancelText="Close";
# Conditional features based on user
if ($inst_admin != "true") { 
    $tbl_user_details->disallowDelete();
    $tbl_user_details->disallowEdit('assoc');
    $tbl_user_details->disallowEdit('groups');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title>Users Management</title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
// UniofCam Search
if ($univ_id == "cam") { echo "<script type='text/javascript' src='../search/uniofcam-search.js'></script>"; }
?>
<style type="text/css">
<?php
if ($univ_id == "cam") {echo "#manualAdd { display: none; }";}
?>
#manualAdd .input-group { padding-bottom: 5px; }
</style>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="#">Users Management</a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="../">Users Management</a>
    </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
        <?php
		// Control the view of menu items for the logged user		
		// Show the menu items
        include("../includes/navbar-menu-items.php");		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php
            // Show users management if user has privilege
            if ($inst_admin == "true" || $users_manager == "true") {
                ?>
                <li class='dropdown'>
                    <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-cogs'></i>&nbsp;&nbsp;Admin <span class='caret'></span></a>
                    <ul class='dropdown-menu' role='menu'>
                        <?php
                        if ($inst_admin == "true") { ?>
                            <li><a href="./"><i class='fa fa-cog'></i>&nbsp;&nbsp;Settings</a></li>
                            <li class='divider'></li>
                        <?php } ?>
                        <li><a href="./users.php"><i class='fa fa-users'></i>&nbsp;&nbsp;Users</a></li>
                    </ul>
                </li>
            <?php
            }
            ?>
            <li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?php echo $crsid ?></a></li>
            <li><a href="<?php echo $dir_path.'logout.php'; ?>"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Logout</a></li>
		</ul>
	</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php
            if ($inst_admin == "true" || $users_manager == "true") {
                if (isset($_POST['add'])) {
                    if (!empty($_POST['cam_crsid']) && $univ_id == "cam") { // Add/update user using UIS Lookup search
                        // Set email, if blank use default - crsid@cam.ac.uk
                        if (!empty($_POST['cam_email'])) { $cam_email = $_POST['cam_email']; } else { $cam_email = $_POST['cam_crsid']."@cam.ac.uk"; }
                        // Check if user exist
                        $query = "SELECT crsid FROM user_details WHERE crsid = :crsid";
                        $stmt = $db->prepare($query);
                        $stmt->execute(array("crsid"=>$_POST['cam_crsid']));
                        if ($stmt->rowCount() > 0) {
                            // If user exist just update
                            $query = "UPDATE user_details SET name=:name, eadd=:eadd, assoc=:assoc, phone=:phone,groups=:groups,modified_on=:modified_on WHERE crsid=:crsid";
                            $stmt = $db->prepare($query);
                            $stmt->execute(array("name"=>$_POST['cam_name'],"eadd"=>$cam_email,"assoc"=>$_POST['cam_assoc'],"phone"=>$_POST['cam_phone'],"groups"=>$_POST['cam_groups'],"modified_on"=>date('Y-m-d H:i:s'),"crsid"=>$_POST['cam_crsid']));
                            echo "<div class='text-center'><span style='color: green;'><b>".$_POST['cam_name']."</b> has now been updated!</span></div>";
                        }
                        else {
                            // Add new user
                            $query = "INSERT INTO user_details (crsid,name,eadd,assoc,phone,groups) VALUES (:crsid,:name,:eadd,:assoc,:phone,:groups)";
                            $stmt = $db->prepare($query);
                            $stmt->execute(array("crsid"=>$_POST['cam_crsid'],"name"=>$_POST['cam_name'],"eadd"=>$cam_email,"assoc"=>$_POST['cam_assoc'],"phone"=>$_POST['cam_phone'],"groups"=>$_POST['cam_groups']));
                            echo "<div class='text-center'><span style='color: green;'><b>".$_POST['cam_name']."</b> has now been added!</span></div>";
                        }
                    }
                    elseif (!empty($_POST['man_crsid']) && !empty($_POST['man_name']) && !empty($_POST['man_email'])) { // Add/update user using manual input
                        // Check if user exist
                        $query = "SELECT crsid FROM user_details WHERE crsid = :crsid";
                        $stmt = $db->prepare($query);
                        $stmt->execute(array("crsid"=>$_POST['man_crsid']));
                        if ($stmt->rowCount() > 0) {
                            // If user exist just update
                            $query = "UPDATE user_details SET name=:name, eadd=:eadd, assoc=:assoc, phone=:phone,groups=:groups,modified_on=:modified_on WHERE crsid=:crsid";
                            $stmt = $db->prepare($query);
                            $stmt->execute(array("name"=>$_POST['man_name'],"eadd"=>$_POST['man_email'],"assoc"=>$_POST['man_assoc'],"phone"=>$_POST['man_phone'],"groups"=>$_POST['man_groups'],"modified_on"=>date('Y-m-d H:i:s'),"crsid"=>$_POST['man_crsid']));
                            echo "<div class='text-center'><span style='color: green;'><b>".$_POST['man_name']."</b> has now been updated!</span></div>";
                        }
                        else {
                            // Add new user
                            $query = "INSERT INTO user_details (crsid,name,eadd,assoc,phone,groups) VALUES (:crsid,:name,:eadd,:assoc,:phone,:groups)";
                            $stmt = $db->prepare($query);
                            $stmt->execute(array("crsid"=>$_POST['man_crsid'],"name"=>$_POST['man_name'],"eadd"=>$_POST['man_email'],"assoc"=>$_POST['man_assoc'],"phone"=>$_POST['man_phone'],"groups"=>$_POST['man_groups']));
                            echo "<div class='text-center'><span style='color: green;'><b>".$_POST['man_name']."</b> has now been added!</span></div>";
                        }
                    }
                    else { echo "<div class='text-center'><span style='color: red;'><b>Oops! Something went wrong, please try again.</b></span></div>"; } // Show error if failed
                }                
                // Show group settings table 
                $tbl_user_details->showTable();
                if (isset($_GET['inact'])) { echo "<a href='./users.php' class='btn btn-default'>Back</a><br>"; }
                    else { 
                        echo "<button type='button' class='btn btn-default addBtn' data-toggle='modal' data-target='#addModal'>Add User</button>&nbsp;";
                        echo "<button type='button' class='btn btn-default addBtn' data-toggle='modal' data-target='#updateModal'>Update Users</button>&nbsp;";
                        echo "<a href='./users.php?inact' class='btn btn-default'>Inactive</a><br>";
                    }
                # Output number of rows returned
                echo "<br>Total users: ";
                $tbl_user_details->insertRowsReturned();
                echo "<br><br>";
            }
            else {
                    echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;Unauthorised access!</h4><br><p>Sorry, you are not authorised to view this page.</p><br>";
                    echo "<p><a href='./' class='btn btn-primary active' role='button'>Dashboard</a></p>";
                }   
			?>	
		</div>
	</div>
    <!-- Start add modal -->
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Add modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add user</h4>
                </div>
                <div class="modal-body">
                <form class="form-inline" method="post" action="<?php $_PHP_SELF; ?>" role="form">                
                    <?php
                    if ($univ_id == "cam") { // Only visible for UniofCam. Use Lookup for people search. ?>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                            <input type="text" class="form-control" name="cam_name" id="cam_name" placeholder="Search UIS Lookup">
                            <input type="hidden" name="cam_crsid" id="cam_crsid">
                            <input type="hidden" name="cam_assoc" id="cam_assoc">
                            <input type="hidden" name="cam_email" id="cam_email">
                            <input type="hidden" name="cam_phone" id="cam_phone">
                            <input type="hidden" name="cam_groups" id="cam_groups">
                        </div>
                        <div><br><a id="manualLink" href='#'>Manually add user</a></div>
                    <?php } ?>
                    <div id="manualAdd">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-id-badge"></i></div>
                            <input type="text" class="form-control" name="man_crsid" id="man_crsid" placeholder="Enter user id" maxlength="8">
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                            <input type="text" class="form-control" name="man_name" id="man_name" placeholder="Enter fullname" maxlength="100">
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                            <input type="text" class="form-control" name="man_email" id="man_email" placeholder="Enter email address" maxlength="100">
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                            <select name="man_assoc" class="form-control" id="man_assoc">
                                <option value="student" selected>Student</option>
                                <option value="staff">Staff</option>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-phone-square"></i></div>
                            <input type="text" class="form-control" name="man_phone" id="man_phone" placeholder="Enter phone number">
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-id-card-o"></i></div>
                            <select name="man_groups" class="form-control" id="man_groups">
                                <?php
                                foreach ($default_inst_grp as $grp) {
                                    echo "<option value='".$grp."'>".$grp."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>                                                     
                </div>
                <div class="modal-footer">
                    <button type="submit" name="add" id="add" class="btn btn-primary btn-sm">Add</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End add modal -->
    <!-- Start update modal -->
    <div id="updateModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Update modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update users</h4>
                </div>
                <div class="modal-body">               
                    <input type="hidden" name="instid" id="instid" value="<?php echo $cam_instids ?>">
                    <div><p>This will update the users database from the University Lookup.</p></div>
                    <div id="updDiv"><button type="submit" name="update" id="update" class="btn btn-primary btn-sm">Update</button></div>
                </div>                                                     
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End update modal -->
    <script type="text/javascript"> 
        // Display manual add user form
        $(function() {
            // This makes sure the search results display on top of the modal
            $("#cam_name").autocomplete( "option", "appendTo", ".modal" );
            $('#manualLink').click(function() {
                $('#manualAdd').show();
                $('#cam_crsid').val("");
                $('#cam_name').val("");
                return false;
            });        
        });
        // Update user table (Cambridge only)
        $('#updDiv').on('click','#update',function(){
            var instid = $("input#instid").val();
            $.ajax({
                type: 'POST',
                url: 'populate_usr.php?instid='+instid,
                beforeSend: function(){
                    $('#updDiv').html("<img src='loading.gif' width='25px'><br><i>Processing, please wait...</i>");
                },
                success: function(data) {
                  $('#updDiv').html(data);
                },
                error:function(err){
                  alert("error"+JSON.stringify(err));
                }
            });
        });
    </script>
	<br /><br />       
</div>
</body>
</html>