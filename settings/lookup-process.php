<?php
// This uses the UniOfCam Lookup API to get user details
$inst = @file_get_contents("https://".$lkup_usr.":".$lkup_pwd."@www.lookup.cam.ac.uk/api/v1/inst/".trim($instid)."/members?fetch=all_attrs,all_groups&format=json");
if (strpos($http_response_header[0], "200")) { 
    $inst = json_decode($inst, TRUE);
    // Loop through all entries
    foreach ($inst['result']['people'] as $person) {
        // Get crsid of user
        $usr_crsid = $person['identifier']['value'];  
        // Get the person's name
        $displayName=""; if (!empty($person['displayName'])) { $displayName = $person['displayName']; }
        $registeredName=""; if (!empty($person['registeredName'])) { $registeredName = $person['registeredName']; }
        // Identify if user is staff or student
        if ($person['staff'] == "1") { $staff = "true"; } else { $staff = ""; }
        if ($person['student'] == "1") {$student = "true";} else { $student = ""; }
        // Get the person's email
        $emails = array();
        foreach($person['attributes'] as $attr) {
            if($attr['scheme'] == "email") {
                $emails[] = $attr['value'];
            }
        }
        // Get the person's phone
        $phones = array();
        foreach($person['attributes'] as $attr) {
            if($attr['scheme'] == "universityPhone") {
                $phones[] = $attr['value'];
            }
        }
        // Get the person's group memberships
        $groups = array();
        foreach($person['groups'] as $group) {
            $groups[] = $group['name'];
        }
        // Set variables
        if (!empty($displayName)) { $personName = $displayName; } elseif (!empty($registeredName)) { $personName = $registeredName; } else { $personName = ""; } // Set user name
        if (!empty($emails[0])) { $email = $emails[0]; } else { $email = $usr_crsid."@cam.ac.uk"; } // Set user email
        if ($staff == "true") {$assoc = "staff";} elseif ($student == "true") {$assoc="student";} else {$assoc="";} // Set user association
        if (!empty($phones[0])) { $phone = $phones[0]; } else { $phone = ""; } // Set user phone
        if (!empty($groups)) {$groups = implode(",", $groups);} else {$groups="";} // Set user's group memberships
        // Insert or update user details
        $query = "SELECT crsid, lkup_sync FROM user_details WHERE crsid = :crsid LIMIT 1";
        $stmt = $db->prepare($query);
        $stmt->execute(array("crsid" => $usr_crsid));
        if ($stmt->rowCount() < 1) {    
            // Insert user into table
            $query = "INSERT INTO user_details (crsid,name,eadd,assoc,phone,groups) VALUES (:crsid,:name,:eadd,:assoc,:phone,:groups)";
            $stmt = $db->prepare($query);
            $stmt->execute(array("crsid"=>$usr_crsid,"name"=>$personName,"eadd"=>$email,"assoc"=>$assoc,"phone"=>$phone,"groups"=>$groups));
        }
        else {
            $rows = $stmt->fetchAll();
            foreach($rows as $row) {
                if ($row['lkup_sync'] == 1) { // Check if syncing is enabled for user
                    // Update user entry in the table
                    $query = "UPDATE user_details SET name=:name,eadd=:eadd,assoc=:assoc,phone=:phone,groups=:groups,modified_on=:modified_on WHERE crsid=:crsid";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("name"=>$personName,"eadd"=>$email,"assoc"=>$assoc,"phone"=>$phone,"groups"=>$groups,"modified_on"=>date('Y-m-d H:i:s'),"crsid"=>$usr_crsid));
                }
            }
        }
    }
    $entries = $entries + count($inst['result']['people']);
}
// Echo Partial success, some error
else { $error = "WARNING: Some entries were not processed."; }
?>