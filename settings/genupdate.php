<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
?>
<!DOCTYPE html>
<html lang="en">
<body>
<?php
// Insert values to database
//==================================================
if (isset($_POST['set'])) {
	if ($_POST['set'] == "grp") { // If updating groups
		foreach ($_POST as $grp => $value) {
			if ($value !== "grp") {
				$query = "UPDATE access_grps SET user_grp=:user_grp WHERE app_grp=:app_grp";
	    		$stmt = $db->prepare($query);
	    		$stmt->execute(array(":user_grp" => $value, ":app_grp" => $grp));
			}
		}
	echo '<span style="color: green;"><b>Your changes have been saved!</b></span>';
	}
	if ($_POST['set'] == "gen") { // If updating general settings
		foreach ($_POST as $gen => $value) {
			if ($value !== "gen") {
				$query = "UPDATE gen_settings SET value=:value WHERE setting=:setting";
	    		$stmt = $db->prepare($query);
	    		$stmt->execute(array(":value" => $value, ":setting" => $gen));
			}
			// Empty dev_id setting value once the default institution group is defined (this disables the demo mode)
			if ($gen == "default_inst_grp" && $value !== "default") {
				$query = "UPDATE gen_settings SET value=:value WHERE setting=:setting";
	    		$stmt = $db->prepare($query);
	    		$stmt->execute(array(":value" => "", ":setting" => "dev_id"));
			}
		}
	echo '<span style="color: green;"><b>Your changes have been saved!</b></span>';
	}
}
if (isset($_GET['toemail']) && isset($_GET['fremail'])) {
	if (!empty($_GET['toemail']) && !empty($_GET['fremail'])) {
		$message = "Test email successfully sent!<br><br><i>Generated from OFMS software</i>";
    	$headers = 'MIME-Version: 1.0' . "\r\n";
    	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    	$headers .= "From:" . $_GET['fremail'] . "\r\n";
    	mail($_GET['toemail'],"OFMS Test Email",$message,$headers);
    	echo '<span style="color: green;">Test email sent! Please check your inbox.</span>';
	} else {
		echo '<span style="color: red;">Sender and recipient email addresses must be valid.</span>';
	}
}
?>
</body>
</html>