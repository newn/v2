// Update groups
$('#grpForm').on('click','#save',function(){
    var grpdata = $("#grpForm").serializeArray();
    grpdata.push({name: 'set', value: 'grp'});
    $.ajax({
        type: "POST",
        url: "genupdate.php",
        data: grpdata,
        success: function(str) {
            $('#saveGrpMsg').html(str);
        },
        error:function(err){
            alert("error"+JSON.stringify(err));
        }
    });
});
// Update general settings
$('#genForm').on('click','#save',function(){
    var grpdata = $("#genForm").serializeArray();
    grpdata.push({name: 'set', value: 'gen'});
    $.ajax({
        type: "POST",
        url: "genupdate.php",
        data: grpdata,
        success: function(str) {
            $('#saveGenMsg').html(str);
        },
        error:function(err){
            alert("error"+JSON.stringify(err));
        }
    });
});
// Send test email
$('#otherSettings').on('click','#testEmail',function(){
    var fremail = $("input#fremail").val();
    var toemail = $("input#toemail").val();
    $.ajax({
        type: "POST",
        url: "genupdate.php?fremail="+fremail+"&toemail="+toemail,
        success: function(data) {
            $('#otherSettingsMsg').html(data);
        },
        error:function(err){
            alert("error"+JSON.stringify(err));
        }
    });
});
// Populate or update user table (Cambridge only)
$('#divBtn').on('click','#goBtn',function(){
    var instid = $("input#instid").val();
    $.ajax({
        type: 'POST',
        url: 'populate_usr.php?instid='+instid,
        beforeSend: function(){
            $('#divBtn').html("<img src='loading.gif' width='25px'><br><i>Processing, please wait...</i>");
        },
        success: function(data) {
          $('#divBtn').html(data);
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});
// Populate or update user table (Oxford only)
$('#upload_csv').on("submit", function(e){  
    e.preventDefault(); //form will not submitted  
    // Show confirm first before processing
    if (confirm('This will PERMANENTLY DELETE the records on the selected app within the date range you have chosen!\n\nAre you sure you want to continue?')) {
        $.ajax({  
            url:"populate_usr.php",  
            method:"POST",  
            data:new FormData(this),  
            contentType:false,          // The content type used when sending data to the server.  
            cache:false,                // To unable request pages to be cached  
            processData:false,          // To send DOMDocument or non processed data file it is set to false  
            beforeSend: function(){
                $('#uploadDiv').html("<img src='loading.gif' width='25px'><br><i>Processing, please wait...</i>");
            },
            success: function(data){  
                if(data == "Error1") { alert("The file is not in csv format!"); }  
                else if(data == "Error2") { alert("Please select a file!"); }  
                else  { $('#uploadDiv').html(data); }  
            },
            error:function(err){
              alert("error"+JSON.stringify(err));
            }  
        })
    }  
});
// Purge history records
$('#clnBtn').click(function (event) {
    // Show confirm first before processing
    if (confirm('This will PERMANENTLY DELETE the records on the selected app within the date range you have chosen!\n\nAre you sure you want to continue?')) {
        var cln_apps = $("select#cln_apps").val();
        var daterange = $("input#daterange").val();
        $.ajax({
            type: 'POST',
            url: 'maint.php?cln_apps='+cln_apps+'&daterange='+daterange,
            beforeSend: function(){
                $('#clnDivBtn').html("<img src='loading.gif' width='25px'><br><i>Processing, please wait...</i>");
            },
            success: function(data) {
              $('#clnDivBtn').html(data);
            },
            error:function(err){
              alert("error"+JSON.stringify(err));
            }
        });
    }
});