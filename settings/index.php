<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
require_once("../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include("../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$tbl_access_grps = new ajaxCRUD("Item", "access_grps", "id", "");
##
########################################################  
$tbl_access_grps->omitPrimaryKey();
#the table fields have prefixes; i want to give the heading titles something more meaningful
$tbl_access_grps->displayAs("app_grp", "App Group");
$tbl_access_grps->displayAs("user_grp", "User Group");
#i could disallow editing for certain, individual fields
$tbl_access_grps->disallowEdit('app_grp');
#set the number of rows to display (per page)
$tbl_access_grps->setLimit(100);
#disallow adding of rows
$tbl_access_grps->disallowAdd();
#disallow deleting of rows
$tbl_access_grps->disallowDelete();

?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title>Settings</title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- File upload css -->
<link href="../css/bootstrap-imageupload.css" rel="stylesheet">
<!-- Local css -->
<link href="local.css" rel="stylesheet">
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="#">Settings</a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
        <?php
		// Control the view of menu items for the logged user		
		// Show the menu items
        include("../includes/navbar-menu-items.php");		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php
            // Show group management if user has privilege
            if ($inst_admin == "true" || $users_manager == "true") {
                ?>
                <li class='dropdown'>
                    <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-cogs'></i>&nbsp;&nbsp;Admin <span class='caret'></span></a>
                    <ul class='dropdown-menu' role='menu'>
                        <?php
                        if ($inst_admin == "true") { ?>
                            <li><a href="./"><i class='fa fa-cog'></i>&nbsp;&nbsp;Settings</a></li>
                            <li class='divider'></li>
                        <?php } ?>
                        <li><a href="./users.php"><i class='fa fa-users'></i>&nbsp;&nbsp;Users</a></li>
                    </ul>
                </li>
            <?php
            }
            ?>
            <li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?php echo $crsid ?></a></li>
            <li><a href="<?php echo $dir_path.'logout.php'; ?>"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Logout</a></li>
		</ul>
	</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
            <?php
            // If submit enquiry support
            if (isset($_POST['subBtn'])) {
                $message = "<b>Name:</b> ".$_POST['tsName']."<br><b>Email:</b> ".$_POST['tsEmail']."<br><b>Institution:</b> ".$banner."<br><b>Version:</b> ".$ofms_ver."<br><b>Message:</b><br>".nl2br($_POST['tsMsg']);
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= "From:" . $_POST['tsEmail'] . "\r\n";
                mail($supEmail,"ofms enquiry (".$banner.")",$message,$headers);
                // Display confirm message
                echo "<div class='text-center'><span style='color: green;'><h4>Thank you for submitting a support query.</h4>We will get back to you shortly.</span></div>";  
            }
            // Upload image if exist
            if (isset($_POST['logoSub'])) {
                if($_FILES['fileToUpload']['tmp_name'] !== "") {
                    // Delete existing file
                    if (file_exists("../".$upload_path."pdf-logo.png")) { unlink("../".$upload_path."pdf-logo.png"); }
                    $target_dir = "../".$upload_path;
                    //$img_name = "pdf-logo.".substr(strrchr(basename($_FILES["fileToUpload"]["name"]), "."), 1); // rename image file
                    $img_name = "pdf-logo.png"; // rename image file
                    $target_file = $target_dir . $img_name;
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) { // Upload the image ?>
                        <script type="text/javascript">
                        $(function() {
                            $("#otherSettingsMsg").html('<span style="color: green;"><b>Image uploaded!</b></span>');
                        });
                        </script>
                    <?php
                    }
                }
            }
            ?>
            <!-- Groups Settings -->
            <?php
            if ($inst_admin == "true") { ?>
                <!-- General Settings -->
                <div class="col-md-6">
                    <div style='margin-bottom: 10px;'><h5><b>General:</b></h5></div>
                    <form id='genForm'>
                        <div class="input-group">
                            <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">University:</button></span>
                            <?php
                            if ($univ_id == "cam") { echo '<input type="text" class="form-control input-sm" value="Cambridge" readonly>'; }
                            if ($univ_id == "ox") { echo '<input type="text" class="form-control input-sm" value="Oxford" readonly>'; }
                            ?>
                        </div>
                        <div class="input-group">
                            <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Institution:</button></span>
                            <input name="banner" type="text" class="form-control input-sm" value="<?php echo $banner ?>">
                        </div>
                        <div class="input-group">
                            <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Institution Admins:</button></span>
                            <input name="inst_admins" type="text" class="form-control input-sm" value="<?php echo $inst_admins ?>">
                        </div>
                        <div class="input-group">
                            <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Users Managers:</button></span>
                            <input name="users_managers" type="text" class="form-control input-sm" value="<?php echo $users_managers ?>">
                        </div>
                        <div class="input-group">
                            <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Member Groups:</button></span>
                            <input name="default_inst_grp" type="text" class="form-control input-sm" value="<?php echo implode(",", $default_inst_grp) ?>">
                        </div>
                        <div class="input-group">
                            <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Default Sender Email:</button></span>
                            <input id="fremail" name="default_fr_email" type="text" class="form-control input-sm" value="<?php echo $default_fr_email ?>">
                        </div>
                        <div class="input-group">
                            <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Default Apps Notification Recipient:</button></span>
                            <input id="toemail" name="default_to_email" type="text" class="form-control input-sm" value="<?php echo $default_to_email ?>">
                        </div>
                        <button id="save" type="button" class="btn btn-default btn-sm">Save</button>
                        <div id="saveGenMsg"><!-- This is where the saved success message will appear --></div>
                    </form>
                    <div id="otherSettings" style="margin-top: 20px;">
                        <h5>Other settings:</h5>
                        <button id="testEmail" class="btn btn-default btn-sm">Send test email</button>
                        <a href="#" type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#upLogo">PDF export logo</a><br>
                        <div id="otherSettingsMsg"><!-- This is where the saved success message will appear --></div>
                    </div>
                    <!-- Start upload logo modal -->
                    <div name="upLogo" id="upLogo" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="." role="form">
                            <div class="modal-body">
                                <div class="imgupload panel panel-default">
                                    <div class="panel-heading clearfix">
                                        <h3 class="panel-title pull-left">Upload image</h3>
                                    </div>
                                    <div class="file-tab panel-body">
                                        <div>
                                            <button type="button" class="btn btn-default btn-file">
                                                <span>Browse</span>
                                                <input type="file" id="fileToUpload" name="fileToUpload">
                                            </button>
                                            <button type="button" class="btn btn-default">Remove</button>
                                        </div>
                                    </div>
                                </div>
                                <div>* PNG image format only<br>* Max files size 500kB</div>
                                <!-- File upload script -->
                                <script src="../js/bootstrap-imageupload.js"></script>
                                <script type="text/javascript">
                                    $('.imgupload').imageupload({
                                        allowedFormats: ["png"],
                                        previewWidth: 100,
                                        previewHeight: 100,
                                        maxFileSizeKb: 500
                                    });
                                </script>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="logoSub" class="btn btn-default btn-sm">Upload</button>
                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                            </div>
                            </form>    
                        </div>
                        </div>
                    </div>
                    <!-- End upload logo modal -->
                </div>
                <?php
                // Fetch the groups values
                $query = "SELECT app_grp, user_grp FROM access_grps";
                $stmt = $db->prepare($query);
                $stmt->execute();
                $grp_arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($grp_arr as $grp) {
                    ${$grp['app_grp']} = $grp['user_grp'];
                }
                ?>
                <div class="col-md-6">
                    <div style='margin-bottom: 10px;'><h5><b>Groups:</b><span style="font-size: 0.85em;"><i> (Separate multiple groups with commas)</i></span></h5></div>
                    <form id='grpForm'>
                        <?php
                        if (in_array("keys", $modules)) { ?>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Keys Admins:</button></span>
                                <input name="keys_admins" type="text" class="form-control input-sm" value="<?php echo $keys_admins ?>">
                            </div>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Keys Managers:</button></span>
                                <input name="keys_managers" type="text" class="form-control input-sm" value="<?php echo $keys_managers ?>">
                            </div>
                        <?php 
                        }
                        if (in_array("parcels", $modules)) { ?>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Parcels Admins:</button></span>
                                <input name="parcels_admins" type="text" class="form-control input-sm" value="<?php echo $parcels_admins ?>">
                            </div>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Parcels Managers:</button></span>
                                <input name="parcels_managers" type="text" class="form-control input-sm" value="<?php echo $parcels_managers ?>">
                            </div>
                        <?php 
                        }
                        if (in_array("incident", $modules)) { ?>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Incident Admins:</button></span>
                                <input name="incident_admins" type="text" class="form-control input-sm" value="<?php echo $incident_admins ?>">
                            </div>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Incident Managers:</button></span>
                                <input name="incident_managers" type="text" class="form-control input-sm" value="<?php echo $incident_managers ?>">
                            </div>
                        <?php 
                        }
                        if (in_array("parking", $modules)) { ?>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Parking Admins:</button></span>
                                <input name="parking_admins" type="text" class="form-control input-sm" value="<?php echo $parking_admins ?>">
                            </div>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Parking Managers:</button></span>
                                <input name="parking_managers" type="text" class="form-control input-sm" value="<?php echo $parking_managers ?>">
                            </div>
                        <?php 
                        }
                        if (in_array("bike", $modules)) { ?>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Bike Admins:</button></span>
                                <input name="bike_admins" type="text" class="form-control input-sm" value="<?php echo $bike_admins ?>">
                            </div>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Bike Managers:</button></span>
                                <input name="bike_managers" type="text" class="form-control input-sm" value="<?php echo $bike_managers ?>">
                            </div>
                        <?php 
                        }
                        if (in_array("rbs", $modules)) { ?>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Room Booking Admins:</button></span>
                                <input name="rbs_admins" type="text" class="form-control input-sm" value="<?php echo $rbs_admins ?>">
                            </div>
                            <div class="input-group">
                                <span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">Room Booking Managers:</button></span>
                                <input name="rbs_managers" type="text" class="form-control input-sm" value="<?php echo $rbs_managers ?>">
                            </div>
                        <?php } ?>
                        <button id="save" type="button" class="btn btn-default btn-sm">Save</button>
                        <div id="saveGrpMsg"><!-- This is where the saved success message will appear --></div>
                    </form>
                </div>
                <?php
                // Update user table (Cambridge University only)
                if ($univ_id == "cam") {
                    ?>
                    <div class='col-xs-12 boxRow text-center' style="margin-top: 20px;">
                        <h4><u>Update Users</u></h4>
                        <div id="updateUsr" class="form-group">
                            <h5>Enter your institution ids:</h5>
                            <div class="col-md-offset-4 col-md-4"><input type="text" name="instid" id="instid" class="form-control" placeholder="i.e. TRIN,TRINUG,TRINPG" value="<?php echo $cam_instids ?>"></div>
                            <div class="col-md-offset-4 col-md-4"><p><i><b>Note:</b> Separate ids with commas. Click <a href="https://www.lookup.cam.ac.uk/inst" target="_blank">here</a> to see your institution.</i></p></div>
                            <div class="col-md-offset-4 col-md-4" id="divBtn" style="margin-bottom: 10px;"><button type="submit" name="goBtn" id="goBtn" class="btn btn-primary">Go</button></div>
                        </div>
                    </div>
                <?php
                }
                if ($univ_id == "ox") {
                    ?>
                    <div class='col-xs-12 boxRow text-center'>
                        <h4><u>Update Users</u></h4>
                        <form id="upload_csv" method="post" enctype="multipart/form-data">  
                            <h5>Upload csv file with user data</h5> 
                            <div class="col-md-offset-4 col-md-4"><input type="file" name="usersFile" class="form-control"></div>  
                            <div class="col-md-offset-4 col-md-4"><p><b><span style="color: red;">Note:</span></b> Make sure the columns in the file are in correct sequence.<br>Click <a href="../docs/import-users.csv">here</a> to download sample file.</p></div>
                            <div  class="col-md-offset-4 col-md-4" id="uploadDiv" style="margin-bottom: 10px;"><button type="submit" name="upload" id="upload" class="btn btn-primary">Upload</button></div>  
                        </form>
                    </div>
                <?php    
                }
                ?>
                <div class='col-xs-12 boxRow text-center'>
                    <h4><u>Maintenance</u></h4>
                    <div id="clnHist" class="form-group">
                        <label>Database Cleanup</label>
                        <p>Select the app and choose the history date range to purge.</p>
                        <div class='col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6'>
                            <div class="col-xs-12 col-sm-6 col-md-6" style="margin-bottom: 10px;">
                                <?php
                                echo '<select name="cln_apps" id="cln_apps" class="form-control">';
                                echo '<option value="" selected>Choose app...</option>';
                                foreach ($modules as $mod) {
                                    if ($mod =="parcels") { echo '<option value="'.htmlspecialchars($mod).'">'. htmlspecialchars($mod) . "</option>\n"; }
                                    if ($mod =="keys") { echo '<option value="'.htmlspecialchars($mod).'">'. htmlspecialchars($mod) . "</option>\n"; }
                                    if ($mod =="incident") { echo '<option value="'.htmlspecialchars($mod).'">'. htmlspecialchars($mod) . "</option>\n"; }
                                    if ($mod =="bike") { echo '<option value="'.htmlspecialchars($mod).'">'. htmlspecialchars($mod) . "</option>\n"; }
                                    if ($mod =="parking") { echo '<option value="'.htmlspecialchars($mod).'">'. htmlspecialchars($mod) . "</option>\n"; }
                                }
                                echo '</select>';
                                ?>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6" style="margin-bottom: 10px;">
                                <input type="text" name="daterange" id="daterange" class="form-control" style="cursor: default; background-color: #fff" readonly>
                                <script type="text/javascript">
                                $(function() {
                                    $('input[name="daterange"]').daterangepicker({
                                        showDropdowns: true,
                                        locale: {
                                            format: 'YYYY-MM-DD'
                                        }
                                    });
                                });
                                </script>
                            </div>
                        </div>
                        <div id="clnDivBtn" class='col-xs-12 col-sm-12 col-md-12 text-center' style="margin-bottom: 10px;"><button type="submit" name="clnBtn" id="clnBtn" class="btn btn-primary">Cleanup</button></div>
                    </div>
                </div>
            <?php
            }
            else {
                    echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;Unauthorised access!</h4><br><p>Sorry, you are not authorised to view this page.</p><br>";
                    echo "<p><a href='./' class='btn btn-primary active' role='button'>Dashboard</a></p>";
                }   
			?>	
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>