<?php
require_once("../includes/access.php");
//*******************************//
// For Cambridge University only //
//*******************************//
if ($univ_id == "cam") {
    if (!empty($_GET['instid'])) {
        $entries = 0;
        $instid_array = explode(",", $_GET['instid']);
        foreach ($instid_array as $instid) {
            // Include the process script
            include("lookup-process.php");
        }
        if (!empty($error)) { 
            $error = "<span style='color: #efad4d;'>".$error."</span><br>";
            echo $error; 
        }
        // Insert inst id values to database
        $query = "UPDATE gen_settings SET value=:cam_instids WHERE setting=:setting";
        $stmt = $db->prepare($query);
        $stmt->execute(array("cam_instids"=>$_GET['instid'],"setting"=>"cam_instids"));
        // Echo success processing
        echo "<span style='color: green;'><b>".$entries."</b> entries processed.</span><br><a href='users.php'>View users</a>";
    }
    else {
        // Echo error processing
        echo "<span style='color: red;'>There was an error, please try again.</span> <a href='./'>Reload</a>";
    }
}
//****************************//
// For Oxford University only //
//****************************//
if ($univ_id == "ox") {
    if(!empty($_FILES["usersFile"]["name"])) {   
        // Count how many rows are processed
        $user_count = 0;
        // Define accepted file type
        $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
        if(in_array($_FILES['usersFile']['type'],$mimes)) { 
            $file_data = fopen($_FILES["usersFile"]["tmp_name"], 'r');  
            fgetcsv($file_data);  
            while($row = fgetcsv($file_data)) {  
                if($row == 1){ $row++; continue; } // Ignore first row
                // Check if user is existing or new
                $query = "SELECT crsid FROM user_details WHERE crsid = :crsid LIMIT 1";
                $stmt = $db->prepare($query);
                $stmt->execute(array("crsid" => $row[0]));
                if ($stmt->rowCount() < 1) {
                    // Insert new user
                    $query = "INSERT INTO user_details (crsid,name,eadd,assoc,phone,groups) VALUES (:crsid,:name,:eadd,:assoc,:phone,:groups)";  
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("crsid"=>$row[0],"name"=>$row[1],"eadd"=>$row[2],"assoc"=>$row[3],"phone"=>$row[4],"groups"=>$row[5]));
                }
                else {
                    // Update existing user
                    $query = "UPDATE user_details SET name=:name,eadd=:eadd,assoc=:assoc,phone=:phone,groups=:groups,modified_on=:modified_on WHERE crsid = :crsid";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("name"=>$row[1],"eadd"=>$row[2],"assoc"=>$row[3],"phone"=>$row[4],"groups"=>$row[5],"modified_on"=>date('Y-m-d H:i:s'),"crsid"=>$row[0]));
                }
                $user_count = $user_count + 1;
            }
            echo "<span style='color: green;'>Success!<br><b>".$user_count."</b> rows inserted/updated.</span>";
        }  
        else { echo 'Error1'; }  
    }  
    else { echo "Error2"; }
}
?>