<?php
require_once("../includes/access.php");
$cln_app = $_GET['cln_apps'];
// First check the string if date is valid
$dates = explode(" ", $_GET['daterange']);
// Function to validate date
function validateDate($date, $format = 'Y-m-d H:i:s') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
// Purge history process
if (!empty($cln_app)) {
	if (validateDate($dates[0], 'Y-m-d') !== false && validateDate($dates[2], 'Y-m-d') !== false) { // Validate the dates first
		$fromDate = $dates[0]." 00:00:00";
		$upToDate = date('Y-m-d', strtotime( "$dates[2] + 1 day" ))." 00:00:00"; // Add 1 day to end date to include the actual selected end date for purging
		// Set the variable for field value
		if ($cln_app == "parcels") { $cln_field = "date_col"; }
		if ($cln_app == "parking" || $cln_app == "bike") { $cln_field = "date_added"; }
		if ($cln_app == "keys") { $cln_field = "date_returned"; }
		if ($cln_app == "incident") { $cln_field = "created"; }
		// Count the records to delete
		$query = "SELECT id FROM $cln_app WHERE $cln_field >= '$fromDate' AND $cln_field < '$upToDate'"; 
		$stmt = $db->prepare($query); 
		$stmt->execute(); 
		$rowsToDelete = $stmt->rowCount();
		if ($rowsToDelete > 0) { // Process delete if records are found
			// Delete images first in uploads folder for certain apps
			if ($cln_app == "parcels" || $cln_app == "parking" || $cln_app == "bike") {
				$query = "SELECT img FROM $cln_app WHERE $cln_field >= '$fromDate' AND $cln_field < '$upToDate'"; 
				$stmt = $db->prepare($query); 
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					unlink("../".$upload_path.$row['img']);
				}
			}
			// Actually delete records in database
			$query = "DELETE FROM $cln_app WHERE $cln_field >= '$fromDate' AND $cln_field < '$upToDate'";
			$stmt = $db->prepare($query);
	    	$stmt->execute();
	    	echo "<span style='color: green;'>Success! ".$rowsToDelete." records deleted.  <a href='./'>Reload</a></span>"; // Show success if records were deleted
		}
		else {
			echo "<span style='color: red;'>No records were found within that date range. <a href='./'>Reload</a></span>";// Show if no records were found
		}
	}
	else {
		echo "<span style='color: red;'>There was error with the date range.<br>Please try again.</span>"; // Show if there's an issue with the dates
	}
}
else {
	echo "<span style='color: red;'>You must select an app to continue. <a href='./'>Reload</a></span>"; // Show if no app is chosen from the list
}

?>