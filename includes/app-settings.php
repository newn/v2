<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("access.php");
?>
<!DOCTYPE html>
<html lang="en">
<body>
<?php
// Insert values to database
//==================================================
// Insert note to database
$app = $_GET['app'];
foreach($_GET as $field => $value) {
    // Check first if email/s entered is valid
    if ($field == "appset_emailfrom" || $field == "appset_emailstaff") {
    	if (!empty($value)) { 
    		$val_arr = explode(",", $value);
            foreach ($val_arr as $value) {
                if (!filter_var($value, FILTER_VALIDATE_EMAIL)) { 
                    die('<span style="color: red;"><b>Error! Please check email address.</b></span>'); 
                }
            }
            $value = implode(",", $val_arr);
    	}
    }
    $query = "UPDATE ".$app."_texts SET value=:value WHERE field=:field";
    $stmt = $db->prepare($query);
    $stmt->execute(array(":value" => $value, ":field" => $field));
    // For incident app, if adding / deleting a category
    if ($field == "catadd" && !empty($value)) {
    	$query = "INSERT INTO ".$app."_categories (category) VALUES (:value)";
    	$stmt = $db->prepare($query);
    	$stmt->execute(array(":value" => $value));
    }
    if ($field == "catdel" && !empty($value)) {
    	$query = "UPDATE ".$app."_categories SET deleted=1 WHERE id=:value";
    	$stmt = $db->prepare($query);
    	$stmt->execute(array(":value" => $value));
    }
}
echo '<span style="color: green;"><b>Your changes have been saved!</b></span>';
?>
</body>
</html>