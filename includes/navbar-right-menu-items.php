<?php
// Define right menu links
if (${$app."_admin"} == "true" || ${$app."_manager"} == "true") {
echo "<li class='dropdown'>";
	// Options menu link
    echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><span class='glyphicon glyphicon-menu-hamburger'></span>&nbsp;&nbsp;Options <span class='caret'></span></a>";
	echo "<ul class='dropdown-menu' role='menu'>";
    // Manage link
    if (in_array($app, array('parking','bike','incident'), true ) ) {
        echo "<li><a href='".$dir_path.$app."/admin'><i class='far fa-list-alt'></i>&nbsp;&nbsp;".$nav_manage."</a></li>";
    } else {
        echo "<li><a href='".$dir_path.$app."/admin'><i class='fas fa-edit'></i>&nbsp;&nbsp;".$nav_manage."</a></li>";
    }
    // Archive and Export links for keys and parcels
    if ($app == "parcels" || $app == "keys") {                        
        echo "<li class='divider'></li>";
        echo "<li><a href='".$dir_path.$app."/admin/archive.php'><i class='fas fa-archive'></i>&nbsp;&nbsp;".$nav_archive."</a></li>";                      
    }                
    // Links for keys
    if ($app == "keys") {                        
        echo "<li class='divider'></li>";
        echo "<li><a href='".$dir_path.$app."/admin/keylist.php'><i class='far fa-list-alt'></i>&nbsp;&nbsp;".$identify_keylist."</a></li>";
        if ($keys_admin == "true") {
            echo "<li class='divider'></li>";
            echo "<li><a href='".$dir_path.$app."/admin/stafflist.php'><i class='fa fa-users'></i>&nbsp;&nbsp;".$identify_stafflist."</a></li>"; 
        }                      
    }
    // Approve link for parking and bike
    if ($app == "parking" || $app == "bike") {
        if ($parking_admin == "true" || $bike_admin == "true") {
            echo "<li class='divider'></li>";
            echo "<li><a href='".$dir_path.$app."/approve.php'><i class='fas fa-check-circle'></i>&nbsp;&nbsp;".$nav_appr."</a></li>";
        }                        
    }
    // Settings link
    if (${$app."_admin"} == "true") {                       
        echo "<li class='divider'></li>";
        echo "<li><a href='".$dir_path.$app."/admin/setting.php'><i class='fas fa-sliders-h'></i>&nbsp;&nbsp;Settings</a></li>";                                   
    }
	echo "</ul>";
echo "</li>";
}
?>
<li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?php echo $crsid ?></a></li>
<li><a href="<?php echo $dir_path.'logout.php'; ?>"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Logout</a></li>