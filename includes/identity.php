<?php
// For demo purpose only
if ($dev_id !== "") { $crsid = $dev_id; }
// Get userid from session (works with either Shibboleth or WeAuth)
if (!isset($crsid)) {
	if (strstr($_SERVER['REMOTE_USER'], "@")) {
		$crsid = strstr($_SERVER['REMOTE_USER'], '@', true);
	} else {
		$crsid = $_SERVER['REMOTE_USER'];
	}
}
// Check if user has privilege to access the app
$query = "SELECT * FROM user_details WHERE crsid = :crsid AND inactive=0 LIMIT 1";
$stmt = $db->prepare($query);
$stmt->execute(array("crsid"=>$crsid));
if ($stmt->rowCount() > 0) {
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    $logged_usr_name = $row['name'];
	    $logged_usr_email = $row['eadd'];
	    $logged_usr_phone = $row['phone'];
	    $logged_usr_assoc = $row['assoc'];
	    $logged_usr_groups = explode(",", $row['groups']); 
	    $default_inst_grp = explode(",", $default_inst_grp);
	    $valid_grp = array_intersect($default_inst_grp, $logged_usr_groups);
	    if (empty($valid_grp)) {
	    	die("<div style='padding-top: 20px; text-align: center; color: #686868; font-weight: bold; font-size: 1.1em;'><h2>Oops!</h2><p>You don't seem to have access to this web app.</p><p>Please contact the Porter's Lodge.</p></div>");
	    }
	}
}
else { 
	die("<div style='padding-top: 20px; text-align: center; color: #686868; font-weight: bold; font-size: 1.1em;'><h2>Oops!</h2><p>You don't seem to have access to this web app.</p><p>Please contact the Porter's Lodge.</p></div>");
}
?>