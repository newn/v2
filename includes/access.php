<?php
require_once("conf.php"); // Configuration file
require_once("con_db.php"); // DB connection
require_once("identity.php"); // Identify user
// Get the available apps
$fullPath = rtrim($_SERVER['DOCUMENT_ROOT'],'/\\').$dir_path;
$directories = glob($fullPath . '*' , GLOB_ONLYDIR);
foreach ($directories as $dir) {
    $paths = explode("/", $dir);
    $modules[] = end($paths);
}
// Identify special users
$inst_admin = "";
if (!empty($inst_admins)) {
    $inst_admins_arr = explode(",", $inst_admins);
    if (array_intersect($logged_usr_groups, $inst_admins_arr)) { $inst_admin = "true"; } //Identify if user is an institution admin
}
$users_manager = "";
if (!empty($users_managers)) {
    $users_managers_arr = explode(",", $users_managers);
    if (array_intersect($logged_usr_groups, $users_managers_arr)) { $users_manager = "true"; } //Identify if user is users manager
}
// Access rights definition for each app
$query = "SELECT app_grp, user_grp FROM access_grps";
$stmt = $db->prepare($query);
$stmt->execute();
$grp_results = $stmt->fetchAll(PDO::FETCH_ASSOC);
// Match the user's lookup membership to the database table
$parcels_manager = "";
$parcels_admin = "";
if (in_array("parcels", $modules)) {
    foreach($grp_results as $grp) {
        if($grp['app_grp'] == "parcels_managers") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $parcels_manager = "true"; }
            }
        }
        if($grp['app_grp'] == "parcels_admins") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $parcels_admin = "true"; }
                if (strpos($inst_admins, $logged_usr_group) !== false) { $parcels_admin = "true"; }
            }
        }
    }
}
$keys_manager = "";
    $keys_admin = "";
if (in_array("keys", $modules)) {
    foreach($grp_results as $grp) {
        if($grp['app_grp'] == "keys_managers") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $keys_manager = "true"; }
            }
        }
        if($grp['app_grp'] == "keys_admins") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $keys_admin = "true"; }
                if (strpos($inst_admins, $logged_usr_group) !== false) { $keys_admin = "true"; }
            }
        }
    }
}
$bike_manager = "";
$bike_admin = "";
if (in_array("bike", $modules)) {
    foreach($grp_results as $grp) {
        if($grp['app_grp'] == "bike_managers") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $bike_manager = "true"; }
            }
        }
        if($grp['app_grp'] == "bike_admins") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $bike_admin = "true"; }
                if (strpos($inst_admins, $logged_usr_group) !== false) { $bike_admin = "true"; }
            }
        }
    }
}
$parking_manager = "";
$parking_admin = "";
if (in_array("parking", $modules)) {
    foreach($grp_results as $grp) {
        if($grp['app_grp'] == "parking_managers") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $parking_manager = "true"; }
            }
        }
        if($grp['app_grp'] == "parking_admins") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $parking_admin = "true"; }
                if (strpos($inst_admins, $logged_usr_group) !== false) { $parking_admin = "true"; }
            }
        }
    }
}
$incident_manager = "";
$incident_admin = "";
if (in_array("incident", $modules)) {
    foreach($grp_results as $grp) {
        if($grp['app_grp'] == "incident_managers") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $incident_manager = "true"; }
            }
        }
        if($grp['app_grp'] == "incident_admins") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $incident_admin = "true"; }
                if (strpos($inst_admins, $logged_usr_group) !== false) { $incident_admin = "true"; }
            }
        }
    }
}
$rbs_manager = "";
$rbs_admin = "";
if (in_array("rbs", $modules)) {
    foreach($grp_results as $grp) {
        if($grp['app_grp'] == "rbs_managers") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $rbs_manager = "true"; }
            }
        }
        if($grp['app_grp'] == "rbs_admins") {
            foreach ($logged_usr_groups as $logged_usr_group) {
                if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $rbs_admin = "true"; }
                if (strpos($inst_admins, $logged_usr_group) !== false) { $rbs_admin = "true"; }
            }
        }
    }
}
// This is specific to Newnham College
$other_manager = "";
$other_admin = "";
foreach($grp_results as $grp) {
    if($grp['app_grp'] == "other_managers") {
        foreach ($logged_usr_groups as $logged_usr_group) {
            if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $other_manager = "true"; }
        }
    }
    if($grp['app_grp'] == "other_admins") {
        foreach ($logged_usr_groups as $logged_usr_group) {
            if (strpos($grp['user_grp'], $logged_usr_group) !== false) { $other_admin = "true"; }
            if (strpos($inst_admins, $logged_usr_group) !== false) { $other_admin = "true"; }
        }
    }
}
?>