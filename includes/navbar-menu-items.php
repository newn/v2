<!-- Navbar menu items -->
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-th-large"></span>&nbsp;&nbsp;Your Apps <span class="caret"></span></a>
	<ul class="dropdown-menu" role="menu">                   
    	<?php
    	echo "<li><a href='".$dir_path."'><span class='glyphicon glyphicon-dashboard'></span>&nbsp;&nbsp;Dashboard</a></li>"; 
		// Available links depending on person
		if (in_array("keys", $modules)) {
			if ($keys_admin == "true" || $keys_manager == "true") {
				echo "<li class='divider'></li>";
				echo "<li><a href='".$dir_path."keys'><i class='fa fa-key'></i>&nbsp;&nbsp;Keys Management</a></li>";										
			}
		}
		if (in_array("parcels", $modules)) {
			if ($parcels_admin == "true" || $parcels_manager == "true") {
				echo "<li class='divider'></li>";
				echo "<li><a href='".$dir_path."parcels'><i class='fas fa-box'></i>&nbsp;&nbsp;Parcels Management</a></li>";										
			}
		}
		if (in_array("bike", $modules)) {
			echo "<li class='divider'></li>";
			echo "<li><a href='".$dir_path."bike'><i class='fa fa-bicycle'></i>&nbsp;&nbsp;Bike Registration</a></li>";
		}
		if (in_array("parking", $modules)) {									
			if ($logged_usr_assoc == "staff") {
				echo "<li class='divider'></li>";
				echo "<li><a href='".$dir_path."parking'><i class='fa fa-car'></i>&nbsp;&nbsp;Parking Registration</a></li>";									
			}
		}
		if (in_array("rbs", $modules)) {
			echo "<li class='divider'></li>";
			echo "<li><a href='".$dir_path."rbs'><i class='fas fa-calendar-alt'></i>&nbsp;&nbsp;Room Booking</a></li>";
		}
		if (in_array("incident", $modules)) {
			if ($incident_manager == "true" || $incident_admin == "true") { // For manager/admin users
				echo "<li class='divider'></li>";
				echo "<li><a href='".$dir_path."incident'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;Incident Reports</a></li>";										
			} elseif ($incident_part_rows > 0) { // For user included on a report
				echo "<li class='divider'></li>";
				echo "<li><a href='".$dir_path."incident/view-rep.php'><i class='fa fa-exclamation-triangle'></i>&nbsp;&nbsp;Incident Reports</a></li>";
			}
		}
		// Other Apps
		if ($other_admin == "true" || $other_manager == "true") {
			echo "<li class='divider'></li>";
			echo "<li><a href='/ofms/shifts' target='_blank'><i class='fa fa-calendar'></i>&nbsp;&nbsp;Porters Shift Schedule</a></li>";										
			echo "<li class='divider'></li>";
			echo "<li><a href='/ofms/callouts'><i class='fa fa-phone'></i>&nbsp;&nbsp;Callout Reports</a></li>";										
		}
		if ($other_admin == "true") {
			echo "<li class='divider'></li>";
			echo "<li><a href='http://itservices.newn.cam.ac.uk/formal-hall' target='_blank'><i class='fa fa-desktop'></i>&nbsp;&nbsp;Digital Signage</a></li>";										
		}
		if ($logged_usr_assoc == "student" || $other_admin == "true" || $other_manager == "true") {
			echo "<li class='divider'></li>";
			echo "<li><a href='/ofms/srb' target='_blank'><i class='fa fa-calendar-check-o'></i>&nbsp;&nbsp;Student Rooms Booking</a></li>";										
		}
		if ($logged_usr_assoc == "staff") {
			echo "<li class='divider'></li>";
			echo "<li><a href='/smfd'><i class='fa fa-cutlery'></i>&nbsp;&nbsp;SenMem Friday Dinners</a></li>";										
		}
		if ($logged_usr_assoc == "student" || $other_admin == "true" || $other_manager == "true") {
			echo "<li class='divider'></li>";
			echo "<li><a href='/ofms/gym'><i class='fa fa-link'></i>&nbsp;&nbsp;Gym Users Checklist</a></li>";										
		}
		if ($other_admin == "true" || $other_manager == "true") {
			echo "<li class='divider'></li>";
			echo "<li><a href='/ofms/accident'><i class='fa fa-medkit'></i>&nbsp;&nbsp;Accident Reports</a></li>";										
		}
		if ($logged_usr_assoc == "staff") {
			echo "<li class='divider'></li>";
			echo "<li><a href='/ofms/parking/book'><i class='fa fa-product-hunt'></i>&nbsp;&nbsp;Carpark Booking</a></li>";										
		}
		?>
	</ul>
</li>