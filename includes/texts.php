<?php
// Determine app name from url
$app_url = explode($dir_path, $_SERVER['REQUEST_URI']);
if (strpos($app_url[1], '/') !== false) { // Check if string has '/' (need to be removed)
	$app = strstr($app_url[1], "/", true);
} else { $app = $app_url[1]; }
// Fetch table field and values and return filed as variable
$query = "SELECT field, value FROM ".$app."_texts";
$stmt = $db->prepare($query);
$stmt->execute();
$txt_arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($txt_arr as $txt) {
	${$txt['field']} = $txt['value'];
}
?>