<?php
//************ NO NEED TO EDIT BELOW ************//
//***********************************************//
// DB connection using PDO (this should be the preferred method)
$dsn = "mysql:host=".$MYSQL_HOST.";dbname=".$MYSQL_DB;
try {
    $db = new PDO($dsn, $MYSQL_LOGIN, $MYSQL_PASS);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    echo "Connection failed: ".$e->getMessage();
}

// Set the general settings variables
$query = "SELECT setting, value FROM gen_settings";
$stmt = $db->prepare($query);
$stmt->execute();
$genset_arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($genset_arr as $genset) {
	${$genset['setting']} = $genset['value'];
}
// Set the timezone
date_default_timezone_set('Europe/London');
?>