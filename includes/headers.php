<!-- IE Compatibility Mode -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Specific Metas
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- FONT
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<link href="https://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
<!-- CSS
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Custom CSS -->
<?php 
// Load color theme
if($site_theme == "aquablue") { echo "<link rel='stylesheet' href='".$dir_path."css/aquablue.css'>"; }
if($site_theme == "uniofcam") { echo "<link rel='stylesheet' href='".$dir_path."css/uniofcam.css'>"; }
// General customization for bootstrap
echo "<link rel='stylesheet' href='".$dir_path."css/custom.css'>"; 
?>
<!-- For the off-canvas sidebar -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
<?php echo "<link rel='stylesheet' href='".$dir_path."css/offcanvas_custom.css'>"; ?>
<!-- Theme for the people search results -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/flick/jquery-ui.css">
<!-- Font awesome for more icons and fonts -->
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous"> -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<!-- favicon and home screen -->
<?php echo "<link rel='icon' type='image/x-icon' href='".$dir_path."includes/favicon-".$univ_id.".ico'>";
echo '<link href="'.$dir_path.'icons/apple-touch-icon.png" rel="apple-touch-icon">';
echo '<link href="'.$dir_path.'icons/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152">';
echo '<link href="'.$dir_path.'icons/apple-touch-icon-167x167.png" rel="apple-touch-icon" sizes="167x167">';
echo '<link href="'.$dir_path.'icons/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180">';
echo '<link href="'.$dir_path.'icons/icon-hires.png" rel="icon" sizes="192x192">';
echo '<link href="'.$dir_path.'icons/icon-normal.png" rel="icon" sizes="128x128">'; ?>
<!-- JavaScript files
================================================== -->
<!-- Jquery core -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Bootstrap core -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- For daterange picker -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="https://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
<!-- Validates form for required fields -->
<?php echo "<script type='text/javascript' src='".$dir_path."js/validator.js'></script>"; ?>
<!-- Functions for ajaxcrud -->
<?php if (isset($ajaxcrud_is_used)) { 
	echo "<script type='text/javascript' src='".$dir_path."ajaxcrud/js/javascript_functions.js'></script>";
	echo "<script type='text/javascript' src='".$dir_path."ajaxcrud/js/validation.js'></script>";
	echo "<script type='text/javascript' src='".$dir_path."ajaxcrud/js/jquery.maskedinput.js'></script>";
	echo "<script type='text/javascript' src='".$dir_path."ajaxcrud/js/jquery.validate.min.js'></script>"; 
} ?>
<!-- For the off-canvas sidebar -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<!-- For people search -->
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
<?php if(!isset($ajaxcrud_is_used)) { echo "<script type='text/javascript' src='".$dir_path."search/search.js'></script>"; } ?>
<!-- Fix shifting fixed navbar to the right -->
<?php echo "<script type='text/javascript' src='".$dir_path."js/navfix.js'></script>"; ?>
<!-- Initialize popovers
================================================== -->  
<?php if(!isset($ajaxcrud_is_used)) { ?>
<script type="text/javascript">
	$(function () {
		$('[data-toggle="popover"]').popover();
	})
</script>
<?php } ?>