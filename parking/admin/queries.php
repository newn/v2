<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
// Limit access to key admins only
if ($parcels_admin == "true") {
	$errmsg = "<span style='color:red;'>Something went wrong. Please try again.</span>";
	$fieldempty = "<span style='color:red;'>Filed must not be empty!</span>";
    // Process Academic Membership, Department, Location names
    if ($_GET['action'] == "edit") {
      	if ($_GET['del'] == 1) {
      		$query = "UPDATE parking_".$_GET['tbl']." SET deleted=1, date_modified=NULL WHERE id=:id";
      		$stmt = $db->prepare($query);
      		if ($stmt->execute(array("id"=>$_GET['id']))) {
      			echo "<span style='color:green;'>Changes saved successfully!</span> <a href='./setting.php'>Reload</a>"; 
      		} else { echo $errmsg; }
      	}
      	else {
      		if (!empty($_GET['inputtext'])) {
      			$query = "UPDATE parking_".$_GET['tbl']." SET name=:name, date_modified=NULL WHERE id=:id";
      			$stmt = $db->prepare($query);
      			if ($stmt->execute(array("name"=>$_GET['inputtext'],"id"=>$_GET['id']))) { 
      	    		echo "<span style='color:green;'>Changes saved successfully!</span> <a href='./setting.php'>Reload</a>"; 
      			} else { echo $errmsg; }
      		}
      		else { echo $fieldempty; }
      	}
    }
   	if ($_GET['action'] == "add") {
   		if (!empty($_GET['inputtext'])) {
   			$query = "INSERT INTO parking_".$_GET['tbl']." (name, date_modified) VALUES (:name,NULL)";
   			$stmt = $db->prepare($query);
   			if ($stmt->execute(array("name"=>$_GET['inputtext']))) {
      			echo "<span style='color:green;'>Name added successfully!</span> <a href='./setting.php'>Reload</a>"; 
      		} else { echo $errmsg; }
   		}
   		else { echo $fieldempty; }
    }  
}
?>