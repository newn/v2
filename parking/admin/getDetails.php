<?php
header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<body>	
	<div class="row">
		<div class="col-xs-12">
			<?php
			//==================================================
			// Get query strings
            parse_str($_SERVER['QUERY_STRING']);
			// Show details
			if ($parking_admin == "true" || $parking_manager == "true") {
				// Fetch data from parking table
				$query = "SELECT v_num, ext_name, parking.crsid, eadd, user_details.name, parking_loc.name AS locname, col_stat, admin_dept, acad_mem, registration, make_model, colour, img, date_added FROM parking ";
				$query .= "LEFT OUTER JOIN user_details ON parking.crsid = user_details.crsid ";
				$query .= "INNER JOIN parking_loc ON parking.location = parking_loc.id ";
				$query .= "WHERE parking.id = :id LIMIT 1";
    			$stmt = $db->prepare($query);
    			$stmt->execute(array("id" => $id));
    			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					?>
					<div class="table-responsive">
						<table class="table table-striped table-condensed">
							<thead><tr><th colspan="2">&nbsp;</th></tr></thead>
							<tbody>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $permit ?>:</b></td><td><?php echo $row['v_num'] ?></td></tr>
								<?php
								if (empty($row['ext_name'])) {
									?>
									<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $usr_name ?>:</b></td><td><?php echo $row['name']." (".$row['crsid'].")&nbsp;&nbsp;<a href='mailto:".$row['eadd']."?subject=Your%20Parking%20Registration'><i class='fa fa-envelope'></i></a>"; ?></td></tr>
									<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $col_stat ?>:</b></td><td>
										<?php 
										if ($row['admin_dept']!=0 || $row['acad_mem']!=0) { 
											if ($row['admin_dept']!=0) {$query2 = "SELECT name FROM parking_dept WHERE id=".$row['admin_dept'];}
											if ($row['acad_mem']!=0) {$query2 = "SELECT name FROM parking_acadmem WHERE id=".$row['acad_mem'];}
											$stmt2 = $db->prepare($query2);
											$stmt2->execute();
											while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) { echo $row2['name']." "; }
										}
										if ($row['col_stat'] == 1) {echo "(".$details_col_stat_1.")";} else {echo "(".$details_col_stat_2.")";} 
										?>
									</td></tr>
								<?php
								}
								else {
									echo "<tr><td class='col-xs-2 col-sm-3 col-md-3'><b>".$usr_name."</b>:</td><td>".$row['ext_name']."</td></tr>";
									echo "<tr><td class='col-xs-2 col-sm-3 col-md-3'><b>".$col_stat."</b>:</td><td>".$external."</td></tr>";
								}
								?>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $vehicle ?>:</b></td><td style="word-wrap: break-word; width: 320px; white-space: normal;"><?php echo $row['make_model']." (".$row['colour'].")"; ?></td></tr>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $regis ?>:</b></td><td><?php echo $row['registration'] ?></td></tr>	
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $carpark ?>:</b></td><td><?php echo $row['locname'] ?></td></tr>
								<?php if (!empty($row['img'])) { ?><tr><td class="col-xs-2 col-sm-3 col-md-3"><b>Image:</b></td><td><a href="../../<?php echo $upload_path.$row['img'] ?>" target="_blank"><img src="../../<?php echo $upload_path.$row['img'] ?>" style="max-width: 150px;"></a></td></tr> <?php } ?>
								<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_date_added ?>:</b></td><td><?php echo date('d-m-Y H:i:s', strtotime($row['date_added'])); ?></td></tr>
							</tbody>									
						</table>
					</div>
				<?php
				}
			}
			else {
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg.".</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
			}
			?>	
		</div>
	</div>    
</body>
</html>