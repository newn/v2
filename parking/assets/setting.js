// Academic membership add modal values
$('.settingForm').on('click','#addAcadMemBtn',function(){
   var acadmem = $('button#acadmemtitletxt').text();
   var inputtbl = "acadmem";
   var inputaction = "add";
   $('#modHeader').html('<h4>'+acadmem+'</h4>');
   $('#modTitle').html('Add name:');
   $('#inputTbl').val(inputtbl);
   $('#inputAction').val(inputaction);
   $('#delname').prop('checked', false);
   $("#deldiv").hide();
});
// Department add modal values
$('.settingForm').on('click','#addDeptBtn',function(){
   var dept = $('button#depttitletxt').text();
   var inputtbl = "dept";
   var inputaction = "add";
   $('#modHeader').html('<h4>'+dept+'</h4>');
   $('#modTitle').html('Add name:');
   $('#inputTbl').val(inputtbl);
   $('#inputAction').val(inputaction);
   $('#delname').prop('checked', false);
   $("#deldiv").hide();
});
// Location add modal values
$('.settingForm').on('click','#addLocBtn',function(){
   var loc = $('button#loctitletxt').text();
   var inputtbl = "loc";
   var inputaction = "add";
   $('#modHeader').html('<h4>'+loc+'</h4>');
   $('#modTitle').html('Add name:');
   $('#inputTbl').val(inputtbl);
   $('#inputAction').val(inputaction);
   $('#delname').prop('checked', false);
   $("#deldiv").hide();
});
// Academic membership edit modal values
$('.settingForm').on('click','#editAcadMemBtn',function(){
   var acadmem = $('button#acadmemtitletxt').text();
   var inputtext = $("#acadmemsel option:selected").text();
   var inputid = $("#acadmemsel option:selected").val();
   var inputtbl = "acadmem";
   var inputaction = "edit";
   $('#modHeader').html('<h4>'+acadmem+'</h4>');
   $('#modTitle').html('Edit name:');
   $('#inputText').val(inputtext);
   $('#inputId').val(inputid);
   $('#inputTbl').val(inputtbl);
   $('#inputAction').val(inputaction);
   $('#delname').prop('checked', false);
});
// Department edit modal values
$('.settingForm').on('click','#editDeptBtn',function(){
   var dept = $('button#depttitletxt').text();
   var inputtext = $("#deptsel option:selected").text();
   var inputid = $("#deptsel option:selected").val();
   var inputtbl = "dept";
   var inputaction = "edit";
   $('#modHeader').html('<h4>'+dept+'</h4>');
   $('#modTitle').html('Edit name:');
   $('#inputText').val(inputtext);
   $('#inputId').val(inputid);
   $('#inputTbl').val(inputtbl);
   $('#inputAction').val(inputaction);
   $('#delname').prop('checked', false);
});
// Location edit modal values
$('.settingForm').on('click','#editLocBtn',function(){
   var loc = $('button#loctitletxt').text();
   var inputtext = $("#locsel option:selected").text();
   var inputid = $("#locsel option:selected").val();
   var inputtbl = "loc";
   var inputaction = "edit";
   $('#modHeader').html('<h4>'+loc+'</h4>');
   $('#modTitle').html('Edit name:');
   $('#inputText').val(inputtext);
   $('#inputId').val(inputid);
   $('#inputTbl').val(inputtbl);
   $('#inputAction').val(inputaction);
   $('#delname').prop('checked', false);
});
// Process setting modal
$('#setModal').on('click','#saveBtn',function(){
    var inputtext = $("#inputText").val();
    var inputid = $("#inputId").val();
    var inputtbl = $("#inputTbl").val();
    var inputaction = $("#inputAction").val();
    if ($('#delname').prop('checked')) { var del = 1; }
        else { var del = 0; }
    $.ajax({
        type: 'POST',
        url: 'queries.php?tbl='+inputtbl+'&action='+inputaction+'&id='+inputid+'&inputtext='+inputtext+'&del='+del,
        success: function(data) { $('#saveMsg').html(data); },
        error:function(err){ alert("error"+JSON.stringify(err)); }
    });
    $('#setModal').modal('hide');
});
// Update settings
$('.settingForm').on('click','#save',function(){
    var emailfrom = $("input#emailfrom").val();
    var emailstaff = $("input#emailstaff").val();
    var actrowlimit = $("input#actrowlimit").val();
    $.ajax({
        type: 'POST',
        url: '../../includes/app-settings.php?app=parking&appset_emailfrom='+emailfrom+'&appset_emailstaff='+emailstaff+'&appset_actrowlimit='+actrowlimit,
        success: function(data) {
          $('#saveMsg').html(data);
        },
        error:function(err){
          alert("error"+JSON.stringify(err));
        }
    });
});