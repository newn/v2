// Auto hide fields
$(document).ready(function(){
    $(".onbehalf").hide();
    $(".ext").hide();
    $(".acad").hide();
    $(".admin").show();
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="0"){ // External
            $(".ext").show();
            $(".acad").hide();
            $("select[id='sel_acad']").val("0");
            $(".admin").hide();
            $("select[id='sel_admin']").val("0");
            $(".onbehalf").hide();
            $("input[id='name']").val("");
            $("input[id='crsid']").val("");
        }
        if($(this).attr("value")=="1"){ // Admin
            $(".ext").hide();
            $("input[id='ext_name']").val("");
            $(".acad").hide();
            $("select[id='sel_acad']").val("0");
            $(".admin").show();
            $(".onbehalf").hide();
            $("input[id='name']").val("");
            $("input[id='crsid']").val("");
        }
        if($(this).attr("value")=="2"){ // Academic
            $(".ext").hide();
            $("input[id='ext_name']").val("");
            $(".admin").hide();
            $("select[id='sel_admin']").val("0");
            $(".acad").show();
            $(".onbehalf").hide();
            $("input[id='name']").val("");
            $("input[id='crsid']").val("");
        }
        if($(this).attr("value")=="3"){ // On Behalf
            $(".ext").hide();
            $("input[id='ext_name']").val("");
            $(".admin").hide();
            $("select[id='sel_admin']").val("0");
            $(".acad").hide();
            $("select[id='sel_acad']").val("0");
            $(".onbehalf").show();
        }
    });
});
//
$("#obhStat").on('change',function(){
    if($(this).val() == 1) { 
    	$('#lblHeader').text('Department');
    	$('#obhDeptSel').show();
    	$('#obhAcadMemSel').hide();
    }
    if($(this).val() == 2) { 
    	$('#lblHeader').text('Membership');
    	$('#obhAcadMemSel').show();
    	$('#obhDeptSel').hide();
    }
});