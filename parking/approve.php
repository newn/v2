<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner; ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Load customization for bootstrap -->
<link rel="stylesheet" href="local.css">
</head>
<body>
<!-- Primary Page Layout
================================================== -->  
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <?php
        // Control the view of menu items for the logged user        
        // Show the menu items
        include("../includes/navbar-menu-items.php");       
        ?>
        </ul>
        <!-- Show user profile and logout option -->
        <ul class="nav navbar-nav navbar-right">
            <?php           
            // Show the right menu items
            include("../includes/navbar-right-menu-items.php");           
            ?>
        </ul>
    </div>
    </div>
</nav>
<div  class="container" style="max-width: 768px;">	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<?php
			// Upload/delete entry on database
			//==================================================
			if(isset($_POST['submit'])) {
                if (!empty($_POST['v_num'])) {
                    $query = "UPDATE parking SET v_num=:v_num,location=:location,keywords=CONCAT(keywords, ' ', :keyword) WHERE id=:id";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("v_num"=>$_POST['v_num'],"location"=>$_POST['location'],"keyword"=>$_POST['v_num'],"id"=>$_POST['id']));
                    // Show message approved successfully
                    echo "<h4>".$msg_appr_ty."</h4><p>".$msg_appr_success."</p>";
                    // Notify user for approved registration
                    $query = "SELECT v_num, parking_loc.name, eadd FROM parking ";
                    $query .= "INNER JOIN user_details ON parking.crsid = user_details.crsid ";
                    $query .= "INNER JOIN parking_loc ON parking_loc.id = parking.location ";
                    $query .= "WHERE parking.id = :id";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("id" => $_POST['id']));
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $message = $appr_e_msg."<br><br><b>".$permit.": </b>".$row['v_num']."<br><b>".$carpark.": </b>".$row['name']."<br><br>".$appr_e_msg2;
                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
                        mail($row['eadd'],$appr_e_subj,$message,$headers);
                    }
                }
                else {
                    // Notify user for deleted registration
                    $query = "SELECT eadd FROM parking INNER JOIN user_details ON parking.crsid = user_details.crsid WHERE parking.id = :id";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("id" => $_POST['id']));
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        if (!empty($row['eadd'])) {
                            if (!empty($_POST['del_msg'])) { $reason = "<b>Reason:</b><br><i>".$_POST['del_msg']."</i><br><br>"; } else { $reason = ""; }
                            $message = $del_e_msg."<br><br>".$reason.$del_e_msg2;
                            $headers = 'MIME-Version: 1.0' . "\r\n";
                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                            if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
                            mail($row['eadd'],$del_e_subj,$message,$headers);
                        }
                    }
                    // Delete registration
                    // First delete the image file if exist
                    $query = "SELECT img FROM parking WHERE id = :id";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("id" => $_POST['id']));
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        unlink("../".$upload_path.$row['img']);
                    }
                    // Delete the database entry
                    $query = "DELETE FROM parking WHERE id = :id";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("id" => $_POST['id']));
                    // Show message for deleted registration
                    echo "<p style='color: red;'>".$msg_appr_delete."</p>";
                }
			}
            if ($parking_admin == "true") {				
			?>
            <table id="apprTbl" class="table table-hover">
                <thead><tr><th><?php echo $usr_name ?></th><th class="tblHiRes"><?php echo $vehicle ?></th><th><?php echo $regis ?></th><th>Image</th><th>Action</th></tr></thead>
                <tbody>
                    <?php
                    // Show results of entries
                    $query = 'SELECT * FROM parking WHERE v_num IS NULL';
                    $stmt = $db->prepare($query);
                    $stmt->execute();
                    if ($stmt->rowCount() > 0) {
                        echo "<tr>";
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                            if (!empty($row['crsid'])) {
                                $query2 = "SELECT name FROM user_details WHERE crsid = :crsid";
                                $stmt2 = $db->prepare($query2);
                                $stmt2->execute(array("crsid" => $row['crsid']));
                                while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                    echo "<td>".$row2['name']."</td>";
                                }
                            }
                            else {
                                echo "<td>".$row['ext_name']."</td>";
                            }                            
                            echo "<td class='tblHiRes'>".$row['make_model']."</td><td>".$row['registration']."</td>";
                            if (!empty($row['img'])) {
                                echo "<td><a class='imgModal' href='#' data-toggle='modal' data-target='#imgModal' data-id='".$row['img']."'><img style='max-width: 80px; max-height: 50px;' src='../".$upload_path.$row['img']."'></a></td>";
                            }
                            else { echo "<td>&nbsp;</td>"; }
                            echo "<td><button type='button' class='btn btn-default apprBtn' data-id='".$row['id']."' data-crsid='".$row['crsid']."' data-toggle='modal' data-target='#apprModal'>Approve</button></td>";
                            echo "</tr>";
                        }
                    }
                    else { echo "<tr><td>No data</td></tr>"; }
                    ?>
                </tbody>
            </table>
            <!-- Image Modal -->
            <script type="text/javascript">
            $(document).on("click", ".imgModal", function () {
                var img_id = $(this).data('id');
                var upload_path = "<?php echo $upload_path ?>";
                $(".mb-img").html("<div class='text-center'><img src='../"+upload_path+img_id+"' style='max-width: 100%; max-height: 500px;'></div>");
            }); 
            </script>
            <div name="imgModal" id="imgModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body mb-img"><!-- Where the image will be inserted --></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    </div>    
                </div>
                </div>
            </div>
            <!-- Script to load data -->
            <script type="text/javascript">                
                $('#apprTbl').on('click','.apprBtn',function(){
                    $('#apprModal').validator();
                    var id = $(this).data('id');
                    var crsid = $(this).data('crsid');
                    $.ajax({
                        type: 'POST',
                        url: 'getRow.php?id='+id+'&crsid='+crsid,
                        success: function(data) {
                          $('.mb-appr').html(data);
                        },
                        error:function(err){
                          alert("error"+JSON.stringify(err));
                        }
                    });
                });
            </script>
            <!-- Modal to display approve window -->
            <div name="apprModal" id="apprModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body mb-appr"><!-- content from getRow.php goes here --></div>  
                </div>
                </div>
            </div>
            <?php
            }
            else {
                // If user is not authorised, this message will appear
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }
            ?>				
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>