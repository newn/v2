<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");	
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- File upload css -->
<link href="../css/bootstrap-imageupload.css" rel="stylesheet">
<!-- Load customization for bootstrap -->
<link rel="stylesheet" href="local.css">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
	<?php
    // Show the sidebar items
    include("../includes/sidebar-menu-items.php");
    ?>
    </ul>
</div>		
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
        <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
           <?php
			// Show the menu items
			include("../includes/navbar-menu-items.php");		
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
			<?php           
               // Show the right menu items
               include("../includes/navbar-right-menu-items.php");           
               ?>
			</ul>
		</div>
	</div>
</nav>
<div class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php
			// Upload form values to database
			//==================================================
			if(isset($_POST['add'])) {
            	// If self-service registration
            	$deptNum = $_POST['admin_dept'];
            	$acadMemNum = $_POST['acad_mem'];
            	// If on Behalf registration
            	if (!empty($_POST['crsid'])) { $crsid = $_POST['crsid']; } 
            	// If external registration
            	if (!empty($_POST['ext_name'])) { $ext_name = $_POST['ext_name']; $crsid = ""; } 
            	else { $ext_name = ""; }
            	// Set college status
            	$col_stat_num = $_POST['col_stat'];
            	if ($_POST['col_stat'] == "3") { 
            		if ($_POST['obhStat'] == 1) {$deptNum = $_POST['obhDept']; } else { $deptNum = 0; }
            		if ($_POST['obhStat'] == 2) {$acadMemNum = $_POST['obhAcadMem']; } else { $acadMemNum = 0; }
            		$col_stat_num = $_POST['obhStat']; 
            	}
            	// Set vehicle reg number
            	$registration = str_replace(' ', '', $_POST['registration']);
            	$registration = strtoupper($registration);
            	// Set date
            	$datetime = date("Y-m-d H:i:s");
            	// Upload image if exist
				if($_FILES['fileToUpload']['tmp_name'] !== "") {
					$target_dir = "../".$upload_path;
					$img_name = "parking-".bin2hex(openssl_random_pseudo_bytes(10))."-".time().".".substr(strrchr(basename($_FILES["fileToUpload"]["name"]), "."), 1); // rename image file
					$target_file = $target_dir . $img_name;
					move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file); // Upload the image
					// Resize the image so it won't take up so much space on the server
					require_once("../includes/php_image_magician.php");
					$magicianObj = new imageLib($target_file);
					$magicianObj -> resizeImage(500, 500, "auto", true);
					$magicianObj -> saveImage($target_file, 100);
				}
				else { $img_name = ""; }
				// Insert values to database
				$query = "INSERT INTO parking (crsid,col_stat,admin_dept,acad_mem,ext_name,make_model,colour,registration,location,img,date_added) VALUES (:crsid,:col_stat,:admin_dept,:acad_mem,:ext_name,:make_model,:colour,:registration,:location,:img,:date_added)";
				$stmt = $db->prepare($query);
				$stmt->execute(array("crsid"=>$crsid,"col_stat"=>$col_stat_num,"admin_dept"=>$deptNum,"acad_mem"=>$acadMemNum,"ext_name"=>$ext_name,"make_model"=>$_POST['make_model'],"colour"=>$_POST['colour'],"registration"=>$registration,"location"=>$_POST['location'],"img"=>$img_name,"date_added"=>$datetime));
				// Fetch id for email notification and to populate keywords for search
				$query = "SELECT parking.id, name, make_model, registration, colour FROM parking INNER JOIN user_details ON parking.crsid = user_details.crsid WHERE date_added = :date_added LIMIT 1";
    			$stmt = $db->prepare($query);
    			$stmt->execute(array(":date_added" => $datetime));
    			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    				if (empty($ext_name)) {
    					// Email the user registering
						$message = $reg_e_msg."<br><br>".$reg_e_msg2;
						$headers = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						// Email "From:" definition
						if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
						mail($logged_usr_email,$reg_e_subj,$message,$headers);
						// Email "To:" definition
						if (empty($appset_emailstaff)) { $to = $default_to_email; } else { $to = $appset_emailstaff; }
						// Email staff for approval
						$message = $reg_e_staff_msg."<br><br><b>".$usr_name.":</b> ".$row['name']."<br><b>".$vehicle.":</b> ".$row['make_model']."<br><b>".$regis.":</b> ".$row['registration']."<br><br>Click <a href='http://".$_SERVER['HTTP_HOST'].$dir_path."parking/approve.php'>here</a> to approve.";
						mail($to,$reg_e_staff_subj,$message,$headers);
						// Populate keywords field in parking table
    					$query2 = "UPDATE parking SET keywords = :keywords WHERE id = :id";
    					$stmt2 = $db->prepare($query2);
    					$stmt2->execute(array(":keywords" => $row['name']." ".$row['make_model']." ".$row['registration']." ".$row['colour'],"id" => $row['id']));    					
    				}
    				else {
    					// Email "To:" definition
						if (empty($appset_emailstaff)) { $to = $default_to_email; } else { $to = $appset_emailstaff; }
						// Email staff for approval (ext)
						$message = $reg_e_staff_msg."<br><br><b>".$usr_name." (Ext):</b> ".$ext_name."<br><b>".$vehicle.":</b> ".$row['make_model']."<br><b>".$regis.":</b> ".$row['registration']."<br><br>Click <a href='http://".$_SERVER['HTTP_HOST'].$dir_path."parking/approve.php'>here</a> to approve.";
						mail($to,$reg_e_staff_subj,$message,$headers);
    					// Populate keywords field in parking table (ext)
    					$query2 = "UPDATE parking SET keywords = :keywords WHERE id = :id";
    					$stmt2 = $db->prepare($query2);
    					$stmt2->execute(array(":keywords" => $ext_name." ".$row['make_model']." ".$row['registration']." ".$row['colour'],"id" => $row['id']));
    				}
    			}
				?>
				<!-- Show modal after submission -->
				<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-body">
				                <p><b><?php echo $reg_summary ?></b></p>
				                <p><?php echo $reg_summary2 ?></p>
				            </div>
				            <div class="modal-footer">
				                <button id="reload" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				            </div>
				        </div>
				    </div>
				</div>
				<script type="text/javascript">
				$(document).ready(function () {
    				$('#confirmModal').modal('show');
    				// reload page
    				$('.modal-footer').on('click','#reload',function(){
  						window.location = window.location.pathname;
					})
				});
				</script>
				<?php
				}
				else {
					// Show the form if user is a member of staff
					if ($logged_usr_assoc == "staff") {
						if (empty($_GET['new'])) { 
							$query = "SELECT v_num, make_model, registration, name, img FROM parking ";
							$query .= "INNER JOIN parking_loc ON parking_loc.id = parking.location ";
							$query .= "WHERE crsid = :crsid";
    						$stmt = $db->prepare($query);
    						$stmt->execute(array("crsid" => $crsid));
    						if ($stmt->rowCount() > 0) {
    							$regCount = 1;
    							?>
    							<div class="container" style="max-width: 720px;">
    								<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12">
    										<table id="apprTbl" class="table table-hover">
                								<thead><tr><th><?php echo $permit ?></th><th><?php echo $vehicle ?></th><th class="tblHiRes"><?php echo $regis ?></th><th class="tblHiRes"><?php echo $carpark ?></th><th>Image</th><th>Status</th></tr></thead>
                								<tbody>
                									<?php
                									while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                										echo "<tr>";
                										if (!empty($row['v_num'])) { echo "<td>".$row['v_num']."</td>"; } else { echo "<td>--</td>"; }
                										echo "<td>".$row['make_model']."</td><td class='tblHiRes'>".$row['registration']."</td><td class='tblHiRes'>".$row['name']."</td>";
                										if (!empty($row['img'])) {
                											echo "<td><a class='imgModal' href='#' data-toggle='modal' data-target='#imgModal' data-id='".$row['img']."'><img style='max-width: 80px; max-height: 50px;' src='../".$upload_path.$row['img']."'></a></td>";
                										}
                										else { echo "<td>&nbsp;</td>"; }
                										if (empty($row['v_num'])) { echo "<td><span style='color: red;'>Pending</span></td>"; } else { echo "<td><span style='color: green;'>Approved</span></td>"; }
                										echo "</tr>";
                									}
                									?>
                								</tbody>
                							</table>
                							<div class="text-center"><a href="./?new=1" class="btn btn-primary">Submit New Registration</a></div>
                						</div>
                					</div>
                				</div>
                				<!-- Image Modal -->
                				<script type="text/javascript">
                				$(document).on("click", ".imgModal", function () {
									var img_id = $(this).data('id');
									var upload_path = "<?php echo $upload_path ?>";
									$(".modal-body").html("<div class='text-center'><img src='../"+upload_path+img_id+"' style='max-width: 100%; max-height: 500px;'></div>");
								}); 
                				</script>
        						<div name="imgModal" id="imgModal" class="modal fade" role="dialog">
        						    <div class="modal-dialog">
        						    <div class="modal-content">
        						        <div class="modal-body"><!-- Where the image will be inserted --></div>
        						        <div class="modal-footer">
        						            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        						        </div>    
        						    </div>
        						    </div>
        						</div>
    						<?php
    						}
    					}
    					if (empty($regCount) || !empty($_GET['new'])) {
						?>
							<form name="myForm" data-toggle="validator" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php $_PHP_SELF; ?>" role="form">
								<div class="form-group form-group-lg">
									<label class="col-sm-3 col-md-4 control-label"></label>
									<div class="col-sm-6 col-md-4 radio">
  										<label><input type="radio" name="col_stat" id="col_stat" value="1" checked><?php echo $reg_staff_1 ?>&nbsp;&nbsp;</label>
										<label><input type="radio" name="col_stat" id="col_stat" value="2"><?php echo $reg_staff_2 ?>&nbsp;&nbsp;</label>
									<?php 
									if ($parking_admin == "true") {
										echo "<label><input type='radio' name='col_stat' id='col_stat' value='3'>".$onbehalf."&nbsp;&nbsp;</label>";
										echo "<label><input type='radio' name='col_stat' id='col_stat' value='0'>".$external."</label>";
									}
									?>
									</div>
								</div>
								<div class="admin">
									<div class="form-group form-group-lg">
										<label class="col-sm-3 col-md-4 control-label"><?php echo $reg_dept ?></label>
										<div class="col-sm-6 col-md-4">
										<select id="sel_admin" name="admin_dept" class="form-control">
										<option value="0" selected>Please select...</option>
										<?php
                                		$query = "SELECT id, name FROM parking_dept WHERE deleted=0 ORDER BY name";
                                		$stmt = $db->prepare($query);
                                		$stmt->execute();
                                		$dept_arr = $stmt->fetchAll();
                                		foreach ($dept_arr as $row) {
                                		    echo "<option value='".$row['id']."'>".$row['name']."</option>";
                                		}
                                		?>
										</select>
										</div>
									</div>
								</div>
								<div class="acad">
									<div class="form-group form-group-lg">
										<label class="col-sm-3 col-md-4 control-label"><?php echo $reg_category ?></label>
										<div class="col-sm-6 col-md-4">
										<select id="sel_acad" name="acad_mem" class="form-control">
										<option value="0" selected>Please select...</option>
										<?php
                                		$query = "SELECT id, name FROM parking_acadmem WHERE deleted=0 ORDER BY name";
                                		$stmt = $db->prepare($query);
                                		$stmt->execute();
                                		$acadmem_arr = $stmt->fetchAll();
                                		foreach ($acadmem_arr as $row) {
                                		    echo "<option value='".$row['id']."'>".$row['name']."</option>";
                                		}
                                		?>
										</select>
										</div>
									</div>
								</div>
								<div class="onbehalf">
									<div class="form-group form-group-lg">
										<label class="col-sm-3 col-md-4 control-label">Name</label>
										<div class="col-sm-6 col-md-4">
											<input type="text" id="name" name="name" class="form-control" placeholder="Search for people...">
											<input type="hidden" id="crsid" name="crsid">
										</div>
									</div>
									<div class="form-group form-group-lg">
										<label class="col-sm-3 col-md-4 control-label">Status</label>
										<div class="col-sm-6 col-md-4">
											<select id="obhStat" name="obhStat" class="form-control">
												<option value="1" selected>Admin Staff</option>
												<option value="2">Academic Staff</option>
											</select>
										</div>
									</div>
									<div class="form-group form-group-lg">
										<label id="lblHeader" class="col-sm-3 col-md-4 control-label"><?php echo $dept ?></label>
										<div id="obhDeptSel" class="col-sm-6 col-md-4">
											<select name="obhDept" class="form-control">
												<option value="0" selected>Please select...</option>
												<?php
												foreach ($dept_arr as $row) {
                                				    echo "<option value='".$row['id']."'>".$row['name']."</option>";
                                				}		
												?>
											</select>
										</div>
										<div id="obhAcadMemSel" class="col-sm-6 col-md-4" style="display:none;">
											<select name="obhAcadMem" class="form-control">
												<option value="0" selected>Please select...</option>
												<?php
												foreach ($acadmem_arr as $row) {
                                				    echo "<option value='".$row['id']."'>".$row['name']."</option>";
                                				}		
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="ext">
									<div class="form-group form-group-lg">
										<label class="col-sm-3 col-md-4 control-label">Name</label>
										<div class="col-sm-6 col-md-4">
										<input type="text" id="ext_name" name="ext_name" class="form-control" placeholder="Enter name..." maxlength="50">
										</div>
									</div>
								</div>
								<div class="form-group form-group-lg">
									<label for ="make_model" class="col-sm-3 col-md-4 control-label"><?php echo $vehicle ?><span style="color: red;"> * </span></label>
									<div class="col-sm-6 col-md-4">
									<input type="text" name="make_model" class="form-control" placeholder="<?php echo $reg_vehicle_ph ?>" maxlength="50" required>
									</div>
								</div>
								<div class="form-group form-group-lg">
									<label  for="registration" class="col-sm-3 col-md-4 control-label"><?php echo $reg_regis ?><span style="color: red;"> * </span></label>
									<div class="col-sm-6 col-md-4">
									<input type="text" name="registration" class="form-control" placeholder="<?php echo $reg_regis_ph ?>" maxlength="20" required>
									</div>
								</div>
								<div class="form-group form-group-lg">
									<label for="colour" class="col-xs-12 col-sm-3 col-md-4 control-label"><?php echo $colour ?><span style="color: red;"> * </span></label>
									<div class="col-xs-4 col-sm-2 col-md-2">
									<input type="text" name="colour" class="form-control" placeholder="<?php echo $reg_colour_ph ?>" maxlength="20" required>
									</div>
								</div>
								<div class="form-group form-group-lg">
									<label for="location" class="col-sm-3 col-md-4 control-label"><?php echo $reg_pref_car ?><span style="color: red;"> * </span></label>
									<div class="col-sm-6 col-md-4">
										<select name="location" class="form-control" required>
											<option value="" selected>Please select...</option>
											<?php
                                			$query = "SELECT id, name FROM parking_loc WHERE deleted=0 ORDER BY name";
                                			$stmt = $db->prepare($query);
                                			$stmt->execute();
                                			$loc_arr = $stmt->fetchAll();
                                			foreach ($loc_arr as $row) {
                                		    	echo "<option value='".$row['id']."'>".$row['name']."</option>";
                                			}
                                			?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-4 control-label"></label>
    								<div class="col-sm-6 col-md-4">
    									<div class="imgupload panel panel-default">
        									<div class="panel-heading clearfix">
        										<h3 class="panel-title pull-left">Upload image</h3>
											</div>
											<div class="file-tab panel-body">
												<div>
													<button type="button" class="btn btn-default btn-file">
														<span>Browse</span>
														<?php
														if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false)) {
															echo '<input type="file" id="fileToUpload" name="fileToUpload" accept="image/*">';
														} else { echo '<input type="file" id="fileToUpload" name="fileToUpload">'; }
														?>
													</button>
													<button type="button" class="btn btn-default">Remove</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- File upload script -->
								<script src="../js/bootstrap-imageupload.js"></script>
								<script type="text/javascript">
									$('.imgupload').imageupload({
									 	allowedFormats: [ "jpg", "jpeg", "png", "gif" ],
									 	previewWidth: 250,
									 	previewHeight: 250,
									 	maxFileSizeKb: 5120
									});
								</script>
								<div class="form-group form-group-lg">
									<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
									<div class="col-sm-6 col-md-4">
									<button type="submit" name="add" id="add" class="btn btn-primary active"><?php echo $reg_submit ?></button>
									</div>
								</div>
							</form>
							<!-- form scripts -->
							<script src="assets/form.js"></script>
						<?php
						}
					}
					else {
						// If user is not authorised, this message will appear
						echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                    	echo "<p><a href='../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
					}
				}
				?>
			</div>
		</div>
		<br /><br />       
    </div>
</body>
</html>