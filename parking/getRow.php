<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<script type="text/javascript">
    $(function () {
        $("#del_tick").click(function () {
            if ($(this).is(":checked")) {
                $("#apprLabel").html('<label class="control-label">Reject message (optional):</label>');
                $("#apprInput").html('<input type="text" id="del_msg" name="del_msg" class="form-control" maxlength="100" placeholder="Enter message here...">');
                $("#subBut").html('<button id="submit" name="submit" type="submit" class="btn btn-danger btn-sm">Reject</button>');
                $("#cpSel").hide();
            } else {
            	$("#apprLabel").html('<label class="control-label">Enter permit number:</label>');
                $("#apprInput").html('<input type="text" id="v_num" name="v_num" class="form-control" maxlength="10" required>');
                $("#subBut").html('<button id="submit" name="submit" type="submit" class="btn btn-success btn-sm">Approve</button>');
                $("#cpSel").show();
            }
        });
    });
</script>
</head>
<body>
<?php
if ($parking_admin == "true") {
	// First, get the user's name and existing registrations
	$query = "SELECT v_num, name FROM parking INNER JOIN user_details ON parking.crsid = user_details.crsid WHERE parking.crsid = :crsid AND v_num IS NOT NULL";
   	$stmt = $db->prepare($query);
   	$stmt->execute(array("crsid" => $_GET['crsid']));
   	if ($stmt->rowCount() > 0) {
   		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $v_name = $row['name']; }
   		echo "<p><b>Name:</b> ".$v_name."<br>";
   		echo "<b>Registered vehicles:</b> ".$stmt->rowCount()."</p>";
   	}
	// Show approve options
	$query = "SELECT location, name FROM parking ";
	$query .= "INNER JOIN parking_loc ON parking_loc.id = parking.location ";
	$query .= "WHERE parking.id = :id LIMIT 1";
	$stmt = $db->prepare($query);
	$stmt->execute(array("id" => $_GET['id']));
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		?>
		<form name="apprForm" data-toggle="validator" method="post" action="<?php $_PHP_SELF; ?>" role="form">
		<input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
		<div class="form-group">
			<div id="apprLabel"><label class="control-label">Enter permit number:</label></div>	
			<div id="apprInput"><input type="text" id="v_num" name="v_num" class="form-control" maxlength="10" required></div>		
		</div>
		<div id="cpSel" class="form-group">
			<label class="control-label">Change <?php echo strtolower($carpark) ?>:</label>
			<select name="location" class="form-control">
			<?php
			$query2 = "SELECT id, name FROM parking_loc";
			$stmt2 = $db->prepare($query2);
			$stmt2->execute();
			while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
				if ($row['location'] == $row2['id']) {
					echo '<option value="'.$row2['id'].'" selected>'.$row2['name']."</option>";
				} else { echo '<option value="'.$row2['id'].'">'.$row2['name']."</option>"; }
			}
			?>
			</select>	
		</div>
		<div class="form-group">
			<label class="control-label"><span style="color: #d9534f;">Delete this registration?</span></label>&nbsp;
			<input type="checkbox" id="del_tick">	
		</div>
		<div class="form-group" style="text-align: right;">
			<div id="subBut"><button id="submit" name="submit" type="submit" class="btn btn-success btn-sm">Approve</button></div>
		</div>
		</form>	
	<?php
	}
}
?>
</body>
</html>