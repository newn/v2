<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("includes/access.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title>Dashboard</title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("includes/headers.php");
// Load color theme
if($site_theme == "aquablue") { echo '<link rel="stylesheet" href="css/aquablue.css">'; }
if($site_theme == "uniofcam") { echo '<link rel="stylesheet" href="css/uniofcam.css">'; }
?>
<!-- Load customization for bootstrap -->
<link rel="stylesheet" href="css/custom.css">
<!-- <script type="text/javascript" src="search/name_search.js"></script> -->
<script type="text/javascript">
/* This autocomplete function shows suggestion of matches based on key entries */
$(function() {
    $("#name").autocomplete({
        source: "search/backend-search.php",
        minLength: 2,
        select: function (event, ui) {
            var item = ui.item;
            if(item) {
                $("#email").val(item.email);
                $("#crsid").val(item.crsid);
                $("#phone").val(item.phone);
            }
         }
    })
});
</script>
<!-- Populate data from people search
================================================== -->  
<script type="text/javascript">
    $(document).on("click", function(){
        $('#out_name').html($('#name').val());
        $('#out_crsid').html($('#crsid').val());
        // We only need the primary email here so omit everything after comma
        $('#out_email').html($('#email').val().split(",")[0]);
        $('#out_mailto').attr('href','mailto:'+$('#email').val().split(",")[0]);
        $('#out_phone').html($('#phone').val());
    });
</script>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is the navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
      	<div class="navbar-header">
		    <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		    <span class="sr-only">Toggle navigation</span>
          	<span class="icon-bar"></span>
          	<span class="icon-bar"></span>
          	<span class="icon-bar"></span>
		    </button>
		<a class="navbar-brand navbar-center" href="#"><?php echo $banner ?></a>
      	</div>
	    <div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
        <?php
		// Control the view of menu items for the logged user		
		// Show the menu items
		include("includes/navbar-menu-items.php");
		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
            <?php
            // Show group management if user has privilege
            if ($inst_admin == "true" || $users_manager == "true") {
                ?>
                <li class='dropdown'>
                    <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-cogs'></i>&nbsp;&nbsp;Admin <span class='caret'></span></a>
                    <ul class='dropdown-menu' role='menu'>
                        <?php
                        if ($inst_admin == "true") { ?>
                            <li><a href="settings/"><i class='fa fa-cog'></i>&nbsp;&nbsp;Settings</a></li>
                            <li class='divider'></li>
                        <?php } ?>
                        <li><a href="settings/users.php"><i class='fa fa-users'></i>&nbsp;&nbsp;Users</a></li>
                    </ul>
                </li>
                <?php
            }
            ?>
	  		<li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?php echo $crsid ?></a></li>
	  		<li><a href="<?php echo $dir_path.'logout.php'; ?>"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Logout</a></li>
		</ul>
	    </div>
	</div>
</nav>
<div class="container" style="max-width: 1024px;">
    <div class="row dashboard">
    	<div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group">
                <div class="tile blue">
                    <form class="form">                
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Search for people" autocomplete="off">
                            <input type="hidden" name="crsid" id="crsid" class="form-control">
                            <input type="hidden" name="email" id="email" class="form-control">
                            <input type="hidden" name="phone" id="phone" class="form-control">     
                        </div>               
                    </form>
                    <p>&nbsp;</p>
                    <p style="font-size: 18px"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<span id="out_name"><?php echo $logged_usr_name ?></span></p>
                    <p style="font-size: 17px"><span class="glyphicon glyphicon-tag"></span>&nbsp;&nbsp;<span id="out_crsid"><?php echo $crsid ?></span></p>
                    <div style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis"><p style="font-size: 17px"><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;<a href="mailto:<?php echo $logged_usr_email ?>" id="out_mailto"><span id="out_email"><?php echo $logged_usr_email ?></span></a></p></div>
                    <p style="font-size: 17px"><span class="glyphicon glyphicon-phone"></span>&nbsp;&nbsp;<span id="out_phone"><?php echo $logged_usr_phone ?></span></p>
                </div>
                <?php
                // ***** For demo purpose only ***** //
                if (isset($_GET['demo']) && $dev_id !== "") { ?>
                    <form id="demoForm">
                        <span style="color: #404040;">For demo purpose only:</span><br>
                        <div class="input-group">
                            <select name="dev_id" class="form-control input-sm">
                                <option value="" selected>Choose one...</option>
                                <option value="usradm">Site Admin</option>
                                <option value="usrmgr">Users Manager</option> 
                                <option value="usrstaff">Staff User</option>
                                <option value="usrstud">Student User</option>                              
                            </select>
                            <span class="input-group-btn">
                                <button id="demoBtn" class="btn btn-default btn-sm" type="button">Submit</button>
                            </span>
                        </div>
                        <div id="demoMsg"><!--Message will show up here--></div>
                    </form>
                    <script type="text/javascript">
                    // Change logged in user (demo only)
                    $('#demoForm').on('click','#demoBtn',function(){
                        var demodata = $("#demoForm").serializeArray();
                        demodata.push({name: 'set', value: 'gen'});
                        $.ajax({
                            type: "POST",
                            url: "settings/genupdate.php",
                            data: demodata,
                            success: function(str) {
                                $('#demoMsg').html(str+' <a href="./" style="color:red;">Reload</a>');
                            },
                            error:function(err){
                                alert("error"+JSON.stringify(err));
                            }
                        });
                    });
                    </script>
                <?php } // ***** End of demo ***** // ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8">
            <?php
            if (in_array("keys", $modules)) {
                if($keys_manager == "true" || $keys_admin == "true") { ?>
                <div class="col-xs-12 col-sm-6 col-md-6 block">
                    <div class="tile app">
                        <div class="col-xs-3 col-sm-3 col-md-3 icon"><a href="keys/">
                            <?php
                            if($site_theme == "aquablue") { echo '<i class="fa fa-key"></i>'; }
                            if($site_theme == "uniofcam") { echo '<img src="css/img/icon-keys.jpg" width="80px" height="80px">'; }
                            ?>
                        </a></div>
                        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="keys/">Keys</a>
                            <?php
                            $query = "SELECT count(*) FROM v_keys_mgmt WHERE key_stat=1 AND hidden=0";
                            $stmt = $db->prepare($query);
                            $stmt->execute();
                            $num_rows = $stmt->fetchColumn();
                            ?>
                            <div class="app_info"><span class="big_num"><?php echo $num_rows ?> </span> still out
                            <?php 
                            if ($keys_manager == "true" || $keys_admin == "true") {
                                echo "| <a href='keys/admin' style='color: #3c8dbc;'>Manage</a>";
                            }
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
            }
            if (in_array("parcels", $modules)) { 
                if($parcels_manager == "true" || $parcels_admin == "true") { ?>
                <div class="col-xs-12 col-sm-6 col-md-6 block">
                    <div class="tile app">
                        <div class="col-xs-3 col-sm-3 col-md-3 icon"><a href="parcels/">
                            <?php
                            if($site_theme == "aquablue") { echo '<i class="fas fa-box"></i>'; }
                            if($site_theme == "uniofcam") { echo '<img src="css/img/icon-parcel.jpg" width="80px" height="80px">'; }
                            ?>
                        </a></div>
                        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="parcels/">Parcels</a>
                            <?php
                            $query = "SELECT count(*) FROM parcels WHERE collected=0";
                            $stmt = $db->prepare($query);
                            $stmt->execute();
                            $num_rows = $stmt->fetchColumn();
                            ?>
                            <div class="app_info"><span class="big_num"><?php echo $num_rows ?> </span> uncollected
                            <?php 
                            if ($parcels_manager == "true" || $parcels_admin == "true") {
                                echo "| <a href='parcels/admin' style='color: #3c8dbc;'>Manage</a>";
                            }
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
            }
            if (in_array("bike", $modules)) {
            ?>
            <div class="col-xs-12 col-sm-6 col-md-6 block">
                <div class="tile app">
                    <div class="col-xs-3 col-sm-3 col-md-3 icon"><a href="bike/">
                        <?php
                        if($site_theme == "aquablue") { echo '<i class="fa fa-bicycle"></i>'; }
                        if($site_theme == "uniofcam") { echo '<img src="css/img/icon-bike.jpg" width="80px" height="80px">'; }
                        ?>
                    </a></div>
                    <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="bike/">Bike</a>
                        <?php
                        if ($bike_manager == "true" || $bike_admin == "true") { $query = "SELECT count(*) FROM bike WHERE b_num IS NULL"; } else { $query = "SELECT count(*) FROM bike WHERE b_num IS NOT NULL AND crsid = '".$crsid."'"; }
                        $stmt = $db->prepare($query);
                        $stmt->execute();
                        $num_rows = $stmt->fetchColumn();
                        ?>
                        <div class="app_info"><span class="big_num"><?php echo $num_rows ?> </span>
                        <?php 
                        if ($bike_manager == "true" || $bike_admin == "true") {
                            echo "pending";
                            if ($num_rows > 0) { echo " | <a href='bike/approve.php' style='color: #3c8dbc;'>Approve</a>"; } else { echo " | <a href='bike/admin' style='color: #3c8dbc;'>Manage</a>"; }
                        } else { echo "registered"; }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php }
            if (in_array("parking", $modules)) {
                if($logged_usr_assoc == "staff") { ?>
                <div class="col-xs-12 col-sm-6 col-md-6 block">
                    <div class="tile app">
                        <div class="col-xs-3 col-sm-3 col-md-3 icon"><a href="parking/">
                            <?php
                            if($site_theme == "aquablue") { echo '<i class="fa fa-car"></i>'; }
                            if($site_theme == "uniofcam") { echo '<img src="css/img/icon-cars.jpg" width="80px" height="80px">'; }
                            ?>                         
                        </a></div>
                        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="parking/">Parking</a>
                            <?php
                            if ($parking_manager == "true" || $parking_admin == "true") { $query = "SELECT count(*) FROM parking WHERE v_num IS NULL"; } else { $query = "SELECT count(*) FROM parking WHERE v_num IS NOT NULL AND crsid = '".$crsid."'"; }
                            $stmt = $db->prepare($query);
                            $stmt->execute();
                            $num_rows = $stmt->fetchColumn();
                            ?>
                            <div class="app_info"><span class="big_num"><?php echo $num_rows ?> </span>
                            <?php 
                            if ($parking_manager == "true" || $parking_admin == "true") {
                                echo "pending";
                                if ($num_rows > 0) { echo " | <a href='parking/approve.php' style='color: #3c8dbc;'>Approve</a>"; } else { echo " | <a href='parking/admin' style='color: #3c8dbc;'>Manage</a>"; }
                            } else { echo "registered"; }
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
            }
            if (in_array("rbs", $modules)) {
            // Show rbs module if user is admin/manager or room owner/user
            $query = "SELECT DISTINCT restriction FROM rbs_rooms";
            $stmt = $db->prepare($query);
            $stmt->execute();
            $rbs_grp_restr = $stmt->fetchAll();
            foreach ($rbs_grp_restr as $grpr) { $grpr_arr[] = $grpr['restriction']; }
            $grpr_str = implode(",", $grpr_arr);
            $grpr_str = implode(",", array_unique(explode(',', $grpr_str))); 
            $grpr_arr = explode(",", $grpr_str);
            if (array_intersect($grpr_arr, $logged_usr_groups) || $rbs_manager == "true" || $rbs_admin == "true") {
            ?>
            <div class="col-xs-12 col-sm-6 col-md-6 block">
                <div class="tile app">
                    <div class="col-xs-3 col-sm-3 col-md-3 icon"><a href="rbs/">
                        <?php
                        if($site_theme == "aquablue") { echo '<i class="far fa-calendar-alt"></i>'; }
                        if($site_theme == "uniofcam") { echo '<img src="css/img/icon-room.jpg" width="80px" height="80px">'; }
                        ?>
                    </a></div>
                    <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="rbs/">Room Booking</a>
                        <?php
                        // Identify if user owns a room
                        $query = "SELECT owner,id FROM rbs_rooms WHERE LOCATE('".$crsid."',owner) > 0";
                        $stmt = $db->prepare($query);
                        $stmt->execute();
                        $rbs_own_rows = $stmt->fetchAll();
                        // List all booking awaiting approval if user is admin
                        if ($rbs_manager == "true" || $rbs_admin == "true") { 
                            $query = "SELECT count(*) FROM rbs_events WHERE approved=0 AND (start > NOW() OR recur=1 AND end > NOW())"; 
                            $rmNumTxt = "pending approval";
                        }
                        // List all booking awaiting approval for room owner/s
                        elseif ($stmt->rowCount() > 0) {
                            foreach ($rbs_own_rows as $rm) {
                                $rm_arr[] = $rm['id'];
                            }
                            $rm_arr = implode(",", $rm_arr);
                            $query = "SELECT count(*) FROM rbs_events WHERE approved=0 AND (start > NOW() OR recur=1 AND end > NOW()) AND rm_id IN (".$rm_arr.")";
                            $rmNumTxt = "requires your approval";
                        }
                        // List booking of normal user awaiting approval
                        else { 
                            $query = "SELECT count(*) FROM rbs_events WHERE approved=0 AND (start > NOW() OR recur=1 AND end > NOW()) AND crsid = '".$crsid."'";
                            $rmNumTxt = "awaiting approval"; 
                        }
                        $stmt = $db->prepare($query);
                        $stmt->execute();
                        $num_rows = $stmt->fetchColumn();
                        ?>
                        <div class="app_info"><span class="big_num"><?php echo $num_rows ?> </span><?php echo $rmNumTxt ?></div>
                    </div>
                </div>
            </div>
            <?php
            } 
            }
            if (in_array("incident", $modules)) {
                if($incident_manager == "true" || $incident_admin == "true") { ?>
                <div class="col-xs-12 col-sm-6 col-md-6 block">
                    <div class="tile app">
                        <div class="col-xs-3 col-sm-3 col-md-3 icon"><a href="incident/">
                            <?php
                            if($site_theme == "aquablue") { echo '<i class="fa fa-exclamation-triangle"></i>'; }
                            if($site_theme == "uniofcam") { echo '<img src="css/img/icon-incident.jpg" width="80px" height="80px">'; }
                            ?>
                        </a></div>
                        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="incident/">Incident</a>
                            <?php
                            $query = "SELECT count(*) FROM incident WHERE created like '%".date('Y-m-d')."%'";
                            $stmt = $db->prepare($query);
                            $stmt->execute();
                            $num_rows = $stmt->fetchColumn();
                            ?>
                            <div class="app_info"><span class="big_num"><?php echo $num_rows ?> </span> new report
                            <?php 
                            if ($incident_manager == "true" || $incident_admin == "true") {
                                echo "| <a href='incident/admin' style='color: #3c8dbc;'>Manage</a>";
                            }
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                elseif ($incident_part_rows > 0) { // variable from sidebar-menu-items.php ?>
                    <div class="col-xs-12 col-sm-6 col-md-6 block">
                        <div class="tile app">
                            <div class="col-xs-3 col-sm-3 col-md-3 icon"><a href="incident/view-rep.php">
                                <?php
                                if($site_theme == "aquablue") { echo '<i class="fa fa-exclamation-triangle"></i>'; }
                                if($site_theme == "uniofcam") { echo '<img src="css/img/icon-incident.jpg" width="80px" height="80px">'; }
                                ?>
                            </a></div>
                            <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="incident/view-rep.php">Incident</a>
                                <?php
                                // Show if there's any new report with the user included
                                $query = "SELECT COUNT(*) FROM incident WHERE LOCATE('".$crsid."',hod_crsid) > 0 AND created LIKE '%".date('Y-m-d')."%'";
                                $stmt = $db->prepare($query);
                                $stmt->execute();
                                $num_rows = $stmt->fetchColumn();
                                if ($num_rows < 1) { $num_rows = 0; }
                                ?>
                                <div class="app_info"><span class="big_num"><?php echo $num_rows ?> </span> new | <a href="incident/view-rep.php" style="color: #3c8dbc;">View reports</a></div>
                            </div>
                        </div>
                    </div>
                <?php }               
            }
            //
            include("extindex.php");
            ?>
        </div>
  	</div>
</div>
</body>
</html>