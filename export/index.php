<?php
// Define person's access rights
require_once("../includes/access.php");
// Extract strings from url
parse_str($_SERVER['QUERY_STRING']);
// Restrict access to this feature
if (${$path."_admin"} == "true" || ${$path."_manager"} == "true") {   
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment;filename=".$tbl."_tbl_export.csv");    
    // Define range operator
    if (isset($range) && $range == 0) { $oprsign = "<"; $range = 60; } else { $oprsign = ">="; }        
    // If bike table is exported
    if ($tbl == "bike") {
        $query = "SELECT b_num AS 'Bike Number',bike.crsid AS Crsid,name AS Name,make_model AS 'Make/Model',frame_num AS 'Frame Number',colour AS Colour,gear AS Gear,wheel AS 'Wheel Size',postcode AS Postcode,features AS Features,date_added AS 'Date Added' FROM ".$tbl." ";
        $query .= "LEFT JOIN user_details ON user_details.crsid = bike.crsid ";
        $query .= "WHERE date_added ".$oprsign." DATE_SUB(NOW(), INTERVAL ".$range." MONTH)";
    }
    // If parking table is exported
    if ($tbl == "parking") {
        $query = "SELECT v_num AS 'Permit Number',parking.crsid AS Crsid,user_details.name AS Name,parking_dept.name AS 'Department',parking_acadmem.name AS 'Membership',ext_name AS 'Name (External)',make_model AS 'Make/Model',registration AS 'Registration',colour AS Colour,parking_loc.name AS Carpark,date_added AS 'Date Added' FROM ".$tbl." ";
        $query .= "LEFT JOIN user_details ON user_details.crsid = parking.crsid ";
        $query .= "LEFT JOIN parking_loc ON parking_loc.id = parking.location ";
        $query .= "LEFT JOIN parking_dept ON parking_dept.id = parking.admin_dept ";
        $query .= "LEFT JOIN parking_acadmem ON parking_acadmem.id = parking.acad_mem ";
        $query .= "WHERE date_added ".$oprsign." DATE_SUB(NOW(), INTERVAL ".$range." MONTH)";
    }
    // If parcels table is exported
    if ($tbl == "parcels") {
        $query = "SELECT parcels.crsid AS Crsid,name AS Name,ptype AS Type,qty AS Quantity,plocation AS Location,notes AS Notes,date_rec AS 'Date Received',date_col AS 'Date Collected' FROM ".$tbl." ";
        $query .= "LEFT JOIN user_details ON user_details.crsid = parcels.crsid ";
        $query .= "LEFT JOIN parcels_type ON parcels_type.id = parcels.type_post ";
        $query .= "LEFT JOIN parcels_loc ON parcels_loc.id = parcels.location ";
        $query .= "WHERE date_rec ".$oprsign." DATE_SUB(NOW(), INTERVAL ".$range." MONTH)";
    }
    // If keys table is exported
    if ($tbl == "keys_mgmt") {
        $query = "SELECT keys_mgmt.key_id AS 'Key Id',key_no AS 'Key Number',keys_mgmt.crsid AS Crsid,N1.name AS Name,ext_name AS 'Name (External)',ext_email AS 'Email (External)',N2.name AS 'Issued By',date_issued AS 'Date Issued',detail_notes AS Notes,N3.name AS 'Returned By',date_returned AS 'Date Returned' FROM ".$tbl." ";
        $query .= "LEFT JOIN user_details AS N1 ON N1.crsid = keys_mgmt.crsid ";
        $query .= "LEFT JOIN user_details AS N2 ON N2.crsid = keys_mgmt.issued_by ";
        $query .= "LEFT JOIN user_details AS N3 ON N3.crsid = keys_mgmt.returned_by ";
        $query .= "LEFT JOIN keys_list ON keys_list.key_id = keys_mgmt.key_id ";
        $query .= "WHERE date_issued ".$oprsign." DATE_SUB(NOW(), INTERVAL ".$range." MONTH)";
    }
    // If keys list table is exported
    if ($tbl == "keys_list") {
        $query = "SELECT key_id AS 'Key Id',key_no AS 'Key Number',key_name AS 'Key Name',groups AS 'Key Group',notes AS 'Notes' FROM ".$tbl." ";
        $query .= "LEFT JOIN keys_grps ON keys_grps.id = keys_list.key_group";
    }
    // If incident is exported
    if ($tbl == "incident") {
        $query = "SELECT category AS Category,N1.name AS Name,subject AS Subject,per_crsid AS 'Person Invloved (Crsid)',N2.name AS 'Person Involved (Name)',place AS Place,stud_yr AS Year,contact AS Contact,details AS Details,hp_comment AS 'Head Porters Comment',hod_crsid AS 'Submited To',created AS 'Date Created' FROM ".$tbl." ";
        $query .= "LEFT JOIN incident_categories ON incident_categories.id = incident.cat_id ";
        $query .= "LEFT JOIN user_details AS N1 ON N1.crsid = incident.rep_crsid ";
        $query .= "LEFT JOIN user_details AS N2 ON N2.crsid = incident.per_crsid ";
        $query .= "WHERE created ".$oprsign." DATE_SUB(NOW(), INTERVAL ".$range." MONTH)";
    }
    // If room booking is exported
    if ($tbl == "rbs_events") {
        $query = "SELECT rbs_events.crsid AS Crsid,N1.name AS Name,Rm.room AS Room,Cat.category AS Category,title AS Title,description AS Description,approved AS Approved,recur AS Recurring,dow AS 'Days Of The Week',start AS 'Start Date',end AS 'End Date',tel AS Contact,N2.name AS 'Last Modified By',rbs_events.date_modified AS 'Date Last Modified' FROM ".$tbl." ";
        $query .= "LEFT JOIN user_details AS N1 ON N1.crsid = rbs_events.crsid ";
        $query .= "LEFT JOIN rbs_rooms AS Rm ON Rm.id = rbs_events.rm_id ";
        $query .= "LEFT JOIN rbs_categories AS Cat ON Cat.id = rbs_events.cat_id ";
        $query .= "LEFT JOIN user_details AS N2 ON N2.crsid = rbs_events.modified_by ";
        $query .= "WHERE end ".$oprsign." DATE_SUB(NOW(), INTERVAL ".$range." MONTH)";
    }
    //Prepare Query, Bind Parameters, Excute Query
    $stmt = $db->prepare($query);
    $stmt->execute();    
    //Export to .CSV
    $fp = fopen('php://output', 'w');    
    if ($stmt->rowCount() > 0) {
        // first set
        $first_row = $stmt->fetch(PDO::FETCH_ASSOC);
        $headers = array_keys($first_row);
        fputcsv($fp, $headers); // put the headers
        fputcsv($fp, array_values($first_row)); // put the first row   
        while ($row = $stmt->fetch(PDO::FETCH_NUM))  {
        fputcsv($fp,$row); // push the rest
        }
    } else {
        fputcsv($fp, array("No data returned!")); // No data
    }
    fclose($fp);
}
else {
    die("You do not have access to use this feature.");
}
?>