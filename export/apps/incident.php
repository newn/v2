<?php
// Define user's identity and access
require_once("../../includes/access.php");
// FPDF class
include_once('../fpdf/fpdf.php');
class PDF extends FPDF {
// Page header
function Header() {
    global $upload_path; // Make this variable accessible outside the function
    // Logo
    $this->Image('../../'.$upload_path.'pdf-logo.png',10,10,50);
    $this->SetFont('Arial','',12);
    // Move to the right
    $this->Cell(130);
    // Title
    $this->Cell(60,10,'Incident No. '.$_GET['id'],1,0,'C');
    // Line break
    $this->Ln(30);
}
// Page footer
function Footer() {
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}
// Fetch report values
$query = "SELECT subject,category,name,details,hp_comment,hod_crsid,created FROM incident ";
$query .= "INNER JOIN incident_categories ON incident_categories.id = incident.cat_id ";
$query .= "INNER JOIN user_details ON user_details.crsid = incident.rep_crsid ";
$query .= "WHERE incident.id=".$_GET['id']." LIMIT 1";
$stmt = $db->prepare($query);
$stmt->execute();
$report = $stmt->fetchAll();
// Fetch participants
$participants = "";
foreach($report as $row) {
    if (!empty($row['hod_crsid'])) {
        $participants = explode(",", $row['hod_crsid']);
        foreach ($participants as $participant) {
            $query = "SELECT name FROM user_details WHERE crsid=:crsid";
            $stmt = $db->prepare($query);
            $stmt->execute(array("crsid"=>$participant));
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $partNames[] = $row['name']; }
        }
        $participants = implode(", ", $partNames);
    }
}
// Create the PDF
$pdf = new PDF();
//header
$pdf->AddPage();
//footer page
$pdf->AliasNbPages();
$pdf->SetFont('Arial','',11);
// Display report
foreach($report as $row) {
// Date reported
$pdf->SetFont('Arial','B',11);
$pdf->Cell(30,7,'Date:',1);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,7,date('d M Y H:i:s', strtotime($row['created'])),1);
$pdf->Ln();
$pdf->Cell(0,2,'',0);
$pdf->Ln();
// Subject
$pdf->SetFont('Arial','B',11);
$pdf->Cell(30,7,'Subject:',1);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,7,$row['subject'],1);
$pdf->Ln();
$pdf->Cell(0,2,'',0);
$pdf->Ln();
// Category
$pdf->SetFont('Arial','B',11);
$pdf->Cell(30,7,'Category:',1);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,7,$row['category'],1);
$pdf->Ln();
$pdf->Cell(0,2,'',0);
$pdf->Ln();
// Reported by
$pdf->SetFont('Arial','B',11);
$pdf->Cell(30,7,'Reported by:',1);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,7,$row['name'],1);
$pdf->Ln();
$pdf->Cell(0,2,'',0);
$pdf->Ln();
// Participants
if (!empty($participants)) {
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(30,7,'Participants:',1);
    $pdf->SetFont('Arial','',11);
    $pdf->MultiCell(160,7,$participants,1);
    $pdf->Ln(5);
}
// Details
$pdf->SetFont('Arial','B',11);
$pdf->Cell(0,7,'Details:','LTR');
$pdf->Ln(5);
$pdf->SetFont('Arial','',11);
$pdf->MultiCell(190,5,$row['details'],'LBR');
$pdf->Ln();
// HP comment
if (!empty($row['hp_comment'])) {
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(0,7,"Head Porter's Comment:",'LTR');
    $pdf->Ln(5);
    $pdf->SetFont('Arial','',11);
    $pdf->MultiCell(190,5,$row['hp_comment'],'LBR');
}
}
// Get comment values
$query = "SELECT name,com_text,date_ins,hour_ins FROM incident_comments ";
$query .= "INNER JOIN user_details ON user_details.crsid = incident_comments.crsid ";
$query .= "WHERE incident_comments.page_id=".$_GET['id']." ORDER BY date_ins ASC";
$stmt = $db->prepare($query);
$stmt->execute();
$comments = $stmt->fetchAll();
// Display the comments
if (!empty($comments)) {
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B',11);
$pdf->Cell(30,8,'Conversation Log:',0);
$pdf->Ln();
foreach ($comments as $comment) {
    $pdf->SetFont('Arial','I',10);
    $pdf->Cell(37,5,"(".$comment['date_ins']." ".$comment['hour_ins'].")",0);
    $pdf->SetFont('Arial','I',10);
    $pdf->Cell(100,5,$comment['name'],0);
    $pdf->Ln(5);
    $pdf->SetFont('Arial','',10);
    $pdf->MultiCell(190,5,$comment['com_text'],0);
    $pdf->Ln(6);
}
}
// Output PDF
$fileName = 'incident-report-'.$_GET['id'].'.pdf';
$pdf->Output($fileName, 'I');
?>