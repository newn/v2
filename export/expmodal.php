<!-- Export Modal -->
<div class="modal fade" id="exportModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content text-left">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Export records to file</h4>
            </div>
            <div class="modal-body">
                <p>Choose date range:</p>
                <select id="datersel" class="form-control">
                    <option value="1" selected>1 month</option>
                    <option value="6">6 months</option>
                    <option value="12">1 year</option>
                    <option value="24">2 years</option>
                    <option value="36">3 years</option>
                    <option value="48">4 years</option>
                    <option value="60">5 years</option>
                    <option value="0">All older records</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <span id="expLnk">
                <?php 
                    if ($app == "keys") { echo "<a type='button' href='".$dir_path."export/?tbl=".$app."_mgmt&path=".$app."&range=1' class='btn btn-primary expbtn'>Export</a>"; }
                    elseif ($app == "rbs") { echo "<a type='button' href='".$dir_path."export/?tbl=".$app."_events&path=".$app."&range=1' class='btn btn-primary expbtn'>Export</a>"; }
                    else { echo "<a type='button' href='".$dir_path."export/?tbl=".$app."&path=".$app."&range=1' class='btn btn-primary expbtn'>Export</a>"; }
                ?>
                </span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // Change export query strings based on selected date range
    $('#datersel').change(function() {
        var month = $(this).find(":selected").val();
        // This replaces the range value to the chosen date
        $('#expLnk > a').attr("href",
            $("#expLnk > a").attr('href').replace(/\&range=[^&]+/, '&range='+month)
        );
    });
    $('.expbtn').on('click',function() {
        $('#exportModal').modal('hide');
    });
</script>