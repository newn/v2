<?php
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
// Insert data to database
if (isset($_POST['title']) && isset($_POST['start']) && isset($_POST['end']) && isset($_POST['cat_id']) && isset($_POST['tel'])) {
	if (isset($_POST['recur'])) { // If the event is recurring
		$dow = implode(",", $_POST['chkDow']);
		$end = explode(" ", $_POST['end']);
		if ($end[1] == "00:00") { $end = "23:59"; } else { $end = $end[1]; } // To fix a bug if recurring is all-day
		$end = $_POST['endDate']." ".$end; // Update this variable with the end date of the recurring event
		$end = date('Y-m-d H:i:s', strtotime($end));
		$query = "INSERT INTO rbs_events(crsid, title, description, start, end, recur, dow, cat_id, rm_id, tel) values (:crsid, :title, :description, :start, :end, :recur, :dow, :cat_id, :rm_id, :tel)";
	} else { $query = "INSERT INTO rbs_events(crsid, title, description, start, end, cat_id, rm_id, tel) values (:crsid, :title, :description, :start, :end, :cat_id, :rm_id, :tel)"; }
	$stmt = $db->prepare($query);
	if ($stmt == false) { 
		print_r($db->errorInfo()); 
		die ("Prepare error!"); 
	}
	if (isset($_POST['recur'])) { // If the event is recurring
		$exec = $stmt->execute(array("crsid"=>$crsid,"title"=>$_POST['title'],"description"=>$_POST['txtDesc'],"start"=>date('Y-m-d H:i:s', strtotime($_POST['start'])),"end"=>$end,"recur"=>$_POST['recur'],"dow"=>$dow,"cat_id"=>$_POST['cat_id'],"rm_id"=>$_POST['rm_id'],"tel"=>$_POST['tel']));
	} else {
		$end = date('Y-m-d H:i:s', strtotime($_POST['end']));
		$exec = $stmt->execute(array("crsid"=>$crsid,"title"=>$_POST['title'],"description"=>$_POST['txtDesc'],"start"=>date('Y-m-d H:i:s', strtotime($_POST['start'])),"end"=>$end,"cat_id"=>$_POST['cat_id'],"rm_id"=>$_POST['rm_id'],"tel"=>$_POST['tel']));
	}
	if ($exec == false) { 
		print_r($stmt->errorInfo()); 
		die ("Execute error!"); 
	}
	else { // Send notifications
		// Get room details
		$query = "SELECT room, owner FROM rbs_rooms WHERE id = :id";
		$stmt = $db->prepare($query);
		$stmt->execute(array("id"=>$_POST['rm_id']));
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
			$rm = $row['room'];
			$own = $row['owner'];
		}
		// Get email of room owner/s
		$own_arr = explode(",", $own);
		foreach($own_arr as $own) {
			$query = "SELECT eadd FROM user_details WHERE crsid=:crsid";
			$stmt = $db->prepare($query);
			$stmt->execute(array("crsid"=>$own));
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$own_eadd[] = $row['eadd'];
			}
		}
		// Get name of user booking the room
		$query = "SELECT name FROM user_details WHERE crsid = :crsid";
		$stmt = $db->prepare($query);
		$stmt->execute(array("crsid"=>$crsid));
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $nm = $row['name']; }
		// Define if recurring or not
		if (isset($_POST['recur'])) { 
			$recur = "<br><b>Recurring:</b> Yes";
			foreach ($_POST['chkDow'] as $ddow) {
				if ($ddow == 0) {$ddow = "Sun";}
				if ($ddow == 1) {$ddow = "Mon";}
				if ($ddow == 2) {$ddow = "Tues";}
				if ($ddow == 3) {$ddow = "Wed";}
				if ($ddow == 4) {$ddow = "Thurs";}
				if ($ddow == 5) {$ddow = "Fri";}
				if ($ddow == 6) {$ddow = "Sat";}
				$dows[] = $ddow; // List of dow as day words
			}
			$dows = "<br><b>DaysoftheWeek:</b> ".implode(",", $dows);
		} else { 
			$recur = "";
			$dows = "";
		}
		// Set headers
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }	// Set the From email
		// Notify owner/admin for approval
		if (empty($appset_emailstaff)) { $to = $default_to_email; } else { $to = $appset_emailfrom; } // Set the To email
		if(!empty($own_eadd)) {$to = $to.",".implode(",", $own_eadd);} // Add the room owner/s if exist
		$message = $notify_msg_admin."<br><br>View <a href='http://".$_SERVER['HTTP_HOST'].$dir_path."rbs'>Room Booking</a> to approve or reject.<br><br>";
		$message .= "<b>Name:</b> ".$nm."<br><b>Contact:</b> ".$_POST['tel']."<br><b>Room:</b> ".$rm."<br>";
		$message .= "<b>Start:</b> ".date('d M Y H:i',strtotime($_POST['start']))."<br><b>End:</b> ".date('d M Y H:i',strtotime($end));
		$message .= $recur.$dows;
		mail($to,$notify_subj_admin,$message,$headers);
		// Notify user booking the room
		if (!empty($own_eadd)) {$to = implode(",", $own_eadd);} else {$to = $to;}
		$message = $notify_msg_pending."<br><br><b>Room:</b> ".$rm."<br>";
		$message .= "<b>Start:</b> ".date('d M Y H:i',strtotime($_POST['start']))."<br><b>End:</b> ".date('d M Y H:i',strtotime($end));
		$message .= $recur.$dows;
		$message .= "<br><br>".$notify_msg_query."<br>".$to;
		mail($logged_usr_email,$notify_subj_pending,$message,$headers);
	}
}
header('Location: '.$_SERVER['HTTP_REFERER']);
?>
