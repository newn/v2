<?php
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
// Delete event
if (isset($_POST['delete']) && isset($_POST['id'])){
	// Send email notification to user
	$query = "SELECT room,approved,recur,dow,start,end,eadd,owner FROM rbs_events INNER JOIN rbs_rooms ON rbs_events.rm_id = rbs_rooms.id INNER JOIN user_details ON rbs_events.crsid = user_details.crsid WHERE rbs_events.id = :id LIMIT 1";
	$stmt = $db->prepare($query);
	$stmt->execute(array("id"=>$_POST['id']));
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
		if ($row['recur'] == 1) {
			$start = $row['start'];
			$recurStat = $row['recur'];
			$endArr = explode(" ", $row['end']);
			$end = date('Y-m-d', strtotime($date. ' + 1 days'))." ".$endArr[1];
			$recur = "<br><b>Recurring:</b> Yes";
			$ddows = explode(",", $row['dow']);
			foreach ($ddows as $ddow) {
				if ($ddow == 0) {$ddow = "Sun";}
				if ($ddow == 1) {$ddow = "Mon";}
				if ($ddow == 2) {$ddow = "Tues";}
				if ($ddow == 3) {$ddow = "Wed";}
				if ($ddow == 4) {$ddow = "Thurs";}
				if ($ddow == 5) {$ddow = "Fri";}
				if ($ddow == 6) {$ddow = "Sat";}
				$dows[] = $ddow; // List dow in day words
			}
			$dows = "<br><b>DaysoftheWeek:</b> ".implode(",", $dows); 
		} else { $recur = ""; $dows = ""; }
		// Get email of room owner/s
		$own_arr = explode(",", $row['owner']);
		foreach($own_arr as $own) {
			$query2 = "SELECT eadd FROM user_details WHERE crsid=:crsid";
			$stmt2 = $db->prepare($query2);
			$stmt2->execute(array("crsid"=>$own));
			while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
				$own_eadd[] = $row2['eadd'];
			}
		}
		// Set headers
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		// Set From email
		if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
		// Set email for query
		if (empty($appset_emailstaff)) { $to = $default_to_email; } else { $to = $appset_emailstaff; }
		// If room has owner, then use this email for query
		if(!empty($own_eadd)) {$to = implode(",", $own_eadd);}
		// Email notification for user
		if ($row['approved'] == 1) { $subj_del = $notify_subj_appr_del; $msg_del = $notify_msg_appr_del; } else { $subj_del = $notify_subj_event_del; $msg_del = $notify_msg_event_del; }
		$message = $msg_del."<br><br><b>Room:</b> ".$row['room']."<br><b>Start:</b> ".date('d M Y H:i:s',strtotime($row['start']))."<br><b>End:</b> ".date('d M Y H:i:s',strtotime($row['end']));
		$message .= $recur.$dows;
		if (!empty($_POST['delTxt'])) { $message .= "<br><br><b>Reason:</b><br>".$_POST['delTxt']; }
		$message .= "<br><br>".$notify_msg_query."<br>".$to;
		mail($row['eadd'],$subj_del,$message,$headers);
	}
	// Update if event is recurring and the start date is older than now
	if ($recurStat == 1 && $start < date('Y-m-d H:i:s')) {
		$query = "UPDATE rbs_events SET end=:end WHERE id=:id";
		$stmt = $db->prepare($query);
		if ($stmt == false) {
			print_r($db->errorInfo());
			die ('Prepare error!');
		}
		$exec = $stmt->execute(array("end"=>$end,"id"=>$_POST['id']));
		if ($exec == false) {
			print_r($stmt->errorInfo());
			die ('Execute error!');
		}
	}
	else { // Delete event entry including recurring events which start date is later than now
		$query = "DELETE FROM rbs_events WHERE id = :id";
		$stmt = $db->prepare($query);
		if ($stmt == false) {
			print_r($db->errorInfo());
			die ('Prepare error!');
		}
		$exec = $stmt->execute(array("id"=>$_POST['id']));
		if ($exec == false) {
			print_r($stmt->errorInfo());
			die ('Execute error!');
		}
	}
}
// Update event
elseif (isset($_POST['title']) && isset($_POST['cat_id']) && isset($_POST['id'])) {	
	$id = $_POST['id'];
	$title = $_POST['title'];
	$cat_id = $_POST['cat_id'];
	// Check if event is approved or recurring
	$appr_e_sent = "";
	$query = "SELECT approved, recur, end FROM rbs_events WHERE id = :id";
	$stmt = $db->prepare($query);
	$stmt->execute(array("id"=>$id));
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
		if ($row['approved'] == 1 && $row['recur'] == 0) {$appr_e_sent = "true"; } // Do not send email if already approved and not recurring
		$recur = $row['recur'];
		$end = $row['end'];
	}
	if(isset($_POST['approved'])) { $approved = 1; } else { $approved = 0; } // Set approve status
	if ($recur == 1) {
		$dow = implode(",", $_POST['chkDow']);
		$end = explode(" ", $end);
		$end = $_POST['endDateEdit']." ".$end[1];
		$query = "UPDATE rbs_events SET title=:title,description=:description,cat_id=:cat_id,approved=:approved,dow=:dow,end=:end,modified_by=:modified_by,date_modified=:date_modified WHERE id=:id";
	} else {
		$query = "UPDATE rbs_events SET title=:title,description=:description,cat_id=:cat_id,approved=:approved,modified_by=:modified_by,date_modified=:date_modified WHERE id=:id";	
	}
	$stmt = $db->prepare($query);
	if ($stmt == false) {
		print_r($db->errorInfo());
		die ('Prepare error!');
	}
	if ($recur == 1) {
		$exec = $stmt->execute(array("title"=>$title,"description"=>$_POST['txtDescEdit'],"cat_id"=>$cat_id,"approved"=>$approved,"dow"=>$dow,"end"=>date('Y-m-d H:i:s', strtotime($end)),"modified_by"=>$crsid,"date_modified"=>date('Y-m-d H:i:s'),"id"=>$id));
	} else {
		$exec = $stmt->execute(array("title"=>$title,"description"=>$_POST['txtDescEdit'],"cat_id"=>$cat_id,"approved"=>$approved,"modified_by"=>$crsid,"date_modified"=>date('Y-m-d H:i:s'),"id"=>$id));
	}
	if ($exec == false) {
		print_r($stmt->errorInfo());
		die ('Execute error!');
	}
	// Send email notification to user
	if ($appr_e_sent !== "true") {
		if ($recur == 1) { $query = "SELECT title,room,start,end,eadd,dow,owner FROM rbs_events INNER JOIN rbs_rooms ON rbs_events.rm_id = rbs_rooms.id INNER JOIN user_details ON rbs_events.crsid = user_details.crsid WHERE rbs_events.id = :id LIMIT 1"; }
		else { $query = "SELECT title,room,start,end,eadd,owner FROM rbs_events INNER JOIN rbs_rooms ON rbs_events.rm_id = rbs_rooms.id INNER JOIN user_details ON rbs_events.crsid = user_details.crsid WHERE rbs_events.id = :id LIMIT 1"; }
		$stmt = $db->prepare($query);
		$stmt->execute(array("id"=>$id));
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
			// Get email of room owner/s
			$own_arr = explode(",", $row['owner']);
			foreach($own_arr as $own) {
				$query2 = "SELECT eadd FROM user_details WHERE crsid=:crsid";
				$stmt2 = $db->prepare($query2);
				$stmt2->execute(array("crsid"=>$own));
				while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
					$own_eadd[] = $row2['eadd'];
				}
			}
			// Set headers
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			// Set From email
			if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
			// Set email for query
			if (empty($appset_emailstaff)) { $to = $default_to_email; } else { $to = $appset_emailstaff; }
			// If room has owner/s, then use this email for query
			if(!empty($own_eadd)) {$to = implode(",", $own_eadd);}	
			// Email notification for user
			if ($recur == 1) {
				$ddows = explode(",", $row['dow']);
				foreach ($ddows as $ddow) {
					if ($ddow == 0) {$ddow = "Sun";}
					if ($ddow == 1) {$ddow = "Mon";}
					if ($ddow == 2) {$ddow = "Tues";}
					if ($ddow == 3) {$ddow = "Wed";}
					if ($ddow == 4) {$ddow = "Thurs";}
					if ($ddow == 5) {$ddow = "Fri";}
					if ($ddow == 6) {$ddow = "Sat";}
					$dows[] = $ddow; // List dow in day words
				}
				$message = $notify_msg_change_recur.":<br><br><b>Purpose:</b> ".$row['title']."<br><b>Room:</b> ".$row['room'];
				$message .= "<br><b>Start:</b> ".date('d M Y H:i:s',strtotime($row['start']))."<br><b>End:</b> ".date('d M Y H:i:s',strtotime($row['end']));
				$message .= "<br><b>Recurring:</b> Yes<br><b>DaysoftheWeek:</b> ".implode(",", $dows);
				$message .= "<br><br>".$notify_msg_query."<br>".$to;
				mail($row['eadd'],$notify_subj_change_recur,$message,$headers);
			} else {
				$message = $notify_msg_approved."<br><br><b>Purpose:</b> ".$row['title']."<br><b>Room:</b> ".$row['room']."<br><b>Start:</b> ".date('d M Y H:i:s',strtotime($row['start']))."<br><b>End:</b> ".date('d M Y H:i:s',strtotime($row['end']));
				$message .= "<br><br>".$notify_msg_query."<br>".$to;
				mail($row['eadd'],$notify_subj_approved,$message,$headers);
			}
		}
	}
}
header('Location: '.$_SERVER['HTTP_REFERER']);	
?>
