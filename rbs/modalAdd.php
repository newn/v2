<!-- Modal Add -->
	<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form id="addForm" class="form-horizontal" method="POST" action="addEvent.php">		
				  	<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Book Room</h4>
				  	</div>
				  	<div class="modal-body">					
					  	<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Purpose:</label>
							<div class="col-sm-7">
						  		<input type="text" name="title" class="form-control" id="title" placeholder="Limit to 35 characters..." maxlength="35" required>
							</div>
					  	</div>
					  	<div class="form-group"> 
							<div class="col-sm-offset-2 col-sm-9">
								<div id="desc"><button id="btnDesc" type="button" class="btn btn-link">Add information</button></div>
							</div>
						</div>
					  	<input type="hidden" name="rm_id" value="<?php echo $rm_id ?>">
					  	<div class="form-group">
							<label for="category" class="col-sm-2 control-label">Category:</label>
							<div class="col-sm-8">
							  	<select name="cat_id" class="form-control" id="cat_id" required>
									<option value="">Choose</option>
									<?php
									$query = "SELECT * FROM rbs_categories WHERE deleted = 0";
									$stmt = $db->prepare($query);
									$stmt->execute();
									while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
										echo "<option value='".$row['id']."'>".$row['category']."</option>";
									}
									?>
								</select>
							</div>
					  	</div>
					  	<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Contact:</label>
							<div class="col-sm-5">
						  		<?php if($appset_contactreq == 0) {
						  			echo '<input type="number" name="tel" class="form-control" id="tel" placeholder="Enter phone number...">';
						  		} else {
						  			echo '<input type="number" name="tel" class="form-control" id="tel" placeholder="Enter phone number..." required>';
						  		} ?>
							</div>
					  	</div>
					  	<div class="form-group"> 
							<div class="col-sm-offset-2 col-sm-10">
								<div class="checkbox">
									<label><input type="checkbox" name="recur" id="recur" value="1"> Make this recurring?</label>
								</div>
							</div>
						</div>
						<div id="recurDiv">
							<div class="form-group"> 
								<div class="col-sm-offset-2 col-sm-10">
									<div class="col-xs-6 col-sm-4">Days of the week:
										<div class="checkbox">
											<label>
												<input type="checkbox" name="chkDow[]" id="chkDow" value="0"> Sunday<br>
												<input type="checkbox" name="chkDow[]" id="chkDow" value="1"> Monday<br>
												<input type="checkbox" name="chkDow[]" id="chkDow" value="2"> Tuesday<br>
												<input type="checkbox" name="chkDow[]" id="chkDow" value="3"> Wednesday<br>
												<input type="checkbox" name="chkDow[]" id="chkDow" value="4"> Thursday<br>
												<input type="checkbox" name="chkDow[]" id="chkDow" value="5"> Friday<br>
												<input type="checkbox" name="chkDow[]" id="chkDow" value="6"> Saturday
											</label>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6">End date:<br>
										<div id="endDate" style="padding-top: 10px;">
											<input type="text" name="endDate" class="form-control" style="cursor: default; background-color: #fff" readonly>
										</div>
									</div>
								</div>
							</div>
						</div>
					  	<div id="dateDiv">
					  		<div class="form-group">
								<label for="start" class="col-sm-2 control-label">Start:</label>
								<div class="col-sm-6">
									<input type="text" name="start" class="form-control" id="start" readonly>
								</div>
					  		</div>
					  		<div class="form-group">
								<label for="end" class="col-sm-2 control-label">End:</label>
								<div class="col-sm-6">
									<input type="text" name="end" class="form-control" id="end" readonly>
								</div>
					  		</div>
					  	</div>
					  	<?php if ($rbs_admin != "true") { // Only show Approve/Delete if user is admin ?>
					  	<div class="form-group"> 
							<div class="col-sm-offset-2 col-sm-10">
								<label>* Once you submit this request you can no longer edit it.</label>
							</div>
						</div>
						<?php } ?>			
				  	</div>
				  	<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit</button>
				  	</div>
				</form>
				<script type="text/javascript">
				// Check if days of week checkboxes are checked
				$('#addForm').on('submit', function (e) {
  					if ($('#recur').is(':checked')) { // If recurring is checked
  						if ($("input[id=chkDow]:checked").length === 0) { // At least one dow should be selected to continue
      						e.preventDefault();
      						alert('You must choose days of the week.');
      						return false;
  						}
  					}
				});
				// Insert description textarea
				$('#desc').on('click', '.btn-link', function() {
					$('#desc').html('<textarea id="txtDesc" name="txtDesc" class="form-control" placeholder="Enter information..."></textarea>');
				});
				</script>
			</div>
	  	</div>
	</div>