<!-- Modal Edit -->
	<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="editForm" class="form-horizontal" method="POST" action="editEventDetails.php">
			  	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Edit Booking</h4>
			  	</div>
			  	<div class="modal-body">				
				  	<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Purpose</label>
						<div class="col-sm-9">
						  	<input type="text" name="title" class="form-control" id="title" maxlength="50" required>
						</div>
				  	</div>
				  	<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-9">
							<div id="divDescEdit"><button id="btnDescEdit" type="button" class="btn btn-link">Show/Add information</button></div>
							<div id="descEdit" style="display: none;"><textarea id="txtDescEdit" name="txtDescEdit" class="form-control" placeholder="Enter information..."></textarea></div>
						</div>
					</div>
				  	<div class="form-group">
						<label for="room" class="col-sm-2 control-label">Room</label>
						<div class="col-sm-8">
						  	<input type="text" name="room" class="form-control" id="room" readonly>
						</div>
				  	</div>
				  	<div class="form-group">
						<label for="category" class="col-sm-2 control-label">Category</label>
						<div class="col-sm-8">
						  	<select name="cat_id" class="form-control" id="cat_id">
							  	<option value="">Choose</option>
							  	<?php
								$query = "SELECT * FROM rbs_categories WHERE deleted = 0";
								$stmt = $db->prepare($query);
								$stmt->execute();
								while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
									echo "<option value='".$row['id']."'>".$row['category']."</option>";
								}
								?>						  
							</select>
						</div>
				  	</div>
				  	<div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10">
							<div style="margin-bottom: 5px;"><b>Name:</b> <span id="uname"></span></div>
							<div style="margin-bottom: 5px;"><b>Contact:</b> <span id="tel"></span></div>
							<div><b>Email:</b> <span id="eadd"></span></div>
						</div>
					</div>
					<div id="recurDiv">
						<div class="form-group"> 
							<div class="col-sm-offset-2 col-sm-10">
								<input type="hidden" id="recurEdit">
								<label><b>Recurrence:</b></label><br>
								<div class="col-xs-6 col-sm-4">Days of the week:
									<div class="checkbox">
										<label>
											<input type="checkbox" name="chkDow[]" id="chkDow" value="0"> Sunday<br>
											<input type="checkbox" name="chkDow[]" id="chkDow" value="1"> Monday<br>
											<input type="checkbox" name="chkDow[]" id="chkDow" value="2"> Tuesday<br>
											<input type="checkbox" name="chkDow[]" id="chkDow" value="3"> Wednesday<br>
											<input type="checkbox" name="chkDow[]" id="chkDow" value="4"> Thursday<br>
											<input type="checkbox" name="chkDow[]" id="chkDow" value="5"> Friday<br>
											<input type="checkbox" name="chkDow[]" id="chkDow" value="6"> Saturday
										</label>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6">End date:<br>
									<div id="endDateEdit" style="padding-top: 10px;">
										<input type="text" name="endDateEdit" class="form-control" style="cursor: default; background-color: #fff" value="" readonly>
									</div>
								</div>
							</div>
						</div>
					</div>
				    <div class="form-group apprDiv"> 
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label><input type="checkbox" name="approved" id="approved" value="1"> Approve booking</label>
							</div>
						</div>
					</div>
				    <div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10 col-md-8">
						  	<div class="checkbox">
								<label class="text-danger"><input type="checkbox" id="delete" name="delete"> Delete booking</label>
						  	</div>
						  	<div id="delTxt" style="display: none; margin-top: 10px;"><textarea class="form-control" name="delTxt" placeholder="(Optional) Enter reason..."></textarea></div>
						</div>
					</div>				  
				  	<input type="hidden" name="id" class="form-control" id="id">				
			  	</div>
			  	<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
			  	</div>
			</form>
			<script type="text/javascript">
			// Check if days of week checkboxes are checked
			$('#editForm').on('submit', function (e) {
  				if ($('#recurEdit').val() == 1) { // If event is recurring
  					if ($("input[id=chkDow]:checked").length === 0) { // At least one dow should be selected to continue
      					e.preventDefault();
      					alert('You must choose days of the week.');
      					return false;
  					}
  				}
			});
			$('#divDescEdit').on('click', '.btn-link', function() { // Insert description textarea
				$('#descEdit').show();
			});
			$("#delete").change(function() { // Show delete text area
    			if(this.checked) { $('#delTxt').show(); }
			});
			</script>
		</div>
	  	</div>
	</div>