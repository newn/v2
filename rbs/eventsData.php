<?php
// Config from https://github.com/jamelbaz/FullCalendar-BS3-PHP-MySQL
// Fetch room events
if ($rm_id != 0) {
	$query = "SELECT name, eadd, rbs_events.id, rm_id, title, description, approved, start, end, recur, dow, room, cat_id, tel, color, category, restriction FROM rbs_events ";
	$query .= "INNER JOIN rbs_categories ON rbs_events.cat_id = rbs_categories.id ";
	$query .= "INNER JOIN rbs_rooms ON rbs_events.rm_id = rbs_rooms.id ";
	$query .= "INNER JOIN user_details ON rbs_events.crsid = user_details.crsid ";
	$query .= "WHERE rm_id = ".$rm_id." AND start >= (NOW() - interval ".$appset_eventview." month)";
}
else {
	$query = "SELECT name, eadd, rbs_events.id, rm_id, title, description, approved, start, end, recur, dow, room, cat_id, tel, color, category, restriction FROM rbs_events ";
	$query .= "INNER JOIN rbs_categories ON rbs_events.cat_id = rbs_categories.id ";
	$query .= "INNER JOIN user_details ON rbs_events.crsid = user_details.crsid ";
	$query .= "INNER JOIN rbs_rooms ON rbs_events.rm_id = rbs_rooms.id ";
	$query .= "WHERE  start >= (NOW() - interval ".$appset_eventview." month)";
} 
// Fetch events for all or specific room
$stmt = $db->prepare($query);
$stmt->execute();
$events = $stmt->fetchAll();
// Identify if user is admin
if ($rbs_admin == "true") { $usrAdm = 1; } else { $usrAdm = 0; }
?>
<script>
$(document).ready(function() {
		// Check if device is mobile so we can adjust calendar properties
		window.mobilecheck = function() {
  			var check = false;
  			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  			return check;
		};
		// Get current time
		var d = new Date();
		var curtime = d.getHours()+":00:00";
		var curView = '', curDay = d.getDay(), dowNum=0;
		// Define user privilege
		var usrAdm = "<?php echo $usrAdm ?>";
		var rm_id = "<?php echo $rm_id ?>";
		// Calendar properties
		$('#calendar').fullCalendar({
			timeFormat: 'HH:mm',
			slotLabelFormat:"HH:mm",
			firstDay: moment().day(), // Set the first day on the calendar to today
			aspectRatio: window.mobilecheck() ? 0.8 : 1.35,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: window.mobilecheck() ? 'agendaWeek,agendaDay,listWeek' : 'month,agendaWeek,agendaDay,listWeek'
			},
			scrollTime :  curtime,
			defaultView: window.mobilecheck() ? "agendaDay" : "agendaWeek",
			editable: false, // Disable dragging/resizing by default
			eventLimit: true, // allow "more" link when too many events
        	eventConstraint: { // This prevents changing event to a past date
            	start: moment().format('YYYY-MM-DD HH:mm:ss'),
            	end: '2100-01-01 00:00:00' // hard coded goodness unfortunately
        	},
        	eventOverlap: false,
			selectable: true,
			selectHelper: true,
			select: function(start, end) {
				if (rm_id != 0) { // Make sure room is chosen first before creating event
					if(start.isBefore(moment())) { // Check if selected date/time is in the past
        			alert('The date/time you selected is in the past!');
    				} else {
						$('#ModalAdd #start').val(moment(start).format('DD-MM-YYYY HH:mm'));
						$('#ModalAdd #end').val(moment(end).format('DD-MM-YYYY HH:mm'));
						$('#ModalAdd #recur').prop('checked', false); // Make sure rucurring is unchecked by default
						$('#ModalAdd #recurDiv').hide(); // Make sure rucurring option is hidden by default
						$('#ModalAdd #dateDiv').show(); // Make sure dates are showing by default
						$('#ModalAdd').modal('show');
						// Initialize the DateRangePicker for event add modal
						$('input[name="endDate"]').daterangepicker({
                	        singleDatePicker: true,
                	        showDropdowns: true,
                	        locale: { format: "DD-MM-YYYY" },
                	        startDate: "<?php echo date('d-m-Y', strtotime('+2 days')); ?>",
                	        minDate: "<?php echo date('d-m-Y', strtotime('+2 days')); ?>",
                	        parentEl: "#endDate"
                	    });
					}
				} else { alert('Please select a room to book first!'); }
			},
			viewRender: function(view) { // This will change the "firstDay" to 0 (Sunday) on month view and today for week view
				if (view && view.name !== curView) {
      				if (view.name === 'agendaWeek') {
        				dowNum = curDay;
      				} else { dowNum = 0; }
      				setTimeout(function() {
        				$("#calendar").fullCalendar('option', 'firstDay', dowNum);
      				}, 10);
    			}
    			if (view) {
        			curView = view.name;
    			} 
			},
			eventRender: function(event, element) {
				var cur30min = d.getTime() + (30 * 60 * 1000); // Set current time plus 30 minutes
				if (event.clickable == 1 && event.start > cur30min) { // Event can only be clicked-to-edit if user is admin/owner or event is not in the past
					element.bind('click', function() {
						$('#ModalEdit #id').val(event.id);
						$('#ModalEdit #title').val(event.title);
						$('#ModalEdit #txtDescEdit').val(event.description.replace(/<br>/g,"\n")); // Replace <br> tag with "\n" to display properly in textarea
						$('#ModalEdit #room').val(event.room);
						$('#ModalEdit #cat_id').val(event.cat_id);
						$('#ModalEdit #tel').html(event.tel);
						$('#ModalEdit #uname').html(event.uname);
						$('#ModalEdit #eadd').html("<a href='mailto:"+event.eadd+"?Subject=Your%20Room%20Booking%20Request'>"+event.eadd+"</a>");
						if (event.recur == 1) { 
							$.each(event.dow, function() { // Tick appropriate checkboxes from event for editing
      							$('input[value="'+(this)+'"]').prop('checked', true);
							});
							$('input[name="endDateEdit"]').val(moment(event.dowend).format('DD-MM-YYYY')); // Set input value with end date from event for editing	
							$('#ModalEdit #recurEdit').val(event.recur); // For checking if recur
							$('#ModalEdit #recurDiv').show(); // Show recurring details	
						} 
						else {
							$('#ModalEdit #recurDiv').hide(); // Hide recurring section if event doesn't repeat	
							$('#ModalEdit #recurEdit').val('0'); // For checking if recur
						}
						if (event.approved == 1) { // Hide approve checkbox if already approved
							$('#ModalEdit #approved').prop('checked', true);
							$('#ModalEdit .apprDiv').hide(); 
						} else { 
							$('#ModalEdit #approved').prop('checked', false); 
							$('#ModalEdit .apprDiv').show(); 
						}						
						$('#ModalEdit').modal('show'); // Show edit modal when event is clicked
						// Initialize the DateRangePicker for event edit modal
						$('input[name="endDateEdit"]').daterangepicker({
                            singleDatePicker: true,
        					showDropdowns: true,
                            locale: { format: "DD-MM-YYYY" },
                            startDate: moment(event.dowend).format('DD-MM-YYYY'),
                            minDate: "<?php echo date('d-m-Y', strtotime('+2 days')); ?>",
                            parentEl: "#endDateEdit"
                        });
					});
				}
				//var trTitle = truncate(event.title,20); // Shorten title on calendar view
				//element.find('.fc-title').html(trTitle);
				// Append recurring icon and room color id if event is recurring and not approved
				if (event.recur == 1 && event.approved == 0) { 
					element.find('.fc-time').append("&nbsp;&nbsp;<i class='fa fa-circle' style='color: "+event.colorid+"'></i>&nbsp;&nbsp;<span class='glyphicon glyphicon-refresh'></span>"); 
				}
				// Append recurring icon only if approved
				if (event.recur == 1 && event.approved == 1) { 
					element.find('.fc-time').append("&nbsp;&nbsp;<span class='glyphicon glyphicon-refresh'></span>"); 
				}
				// Append room color id if not recurring and not approved
				if (event.recur == 0 && event.approved == 0) { 
					element.find('.fc-time').append("&nbsp;&nbsp;<i class='fa fa-circle' style='color: "+event.colorid+"'></i>"); 
				} 
				element.find('.fc-title').append("<br><i>"+event.category+"</i>"); // Append category name
				// Define start/end of recurring events
				var theDate = new Date(event.start);
        		var endDate = event.dowend;
				var startDate = event.dowstart;       		
        		if (theDate >= endDate) { return false; } // Don't show if current date is past the recurring end date
        		if (theDate <= startDate) { return false; } // Don't show if current date is before the recurring start date
			},
			eventAfterRender: function(event, element, view) {
			    if (event.clickable == 1) { // Show Qtip, only if admin/owner
			    	if (event.description) { 
			    		var eDesc = "<b>"+event.title+"</b><br>"+event.description+"<br><br><b>by:</b><i> "+event.uname+"</i>" 
			    	} else { var eDesc = "<b>"+event.title+"</b><br><br><b>by:</b><i> "+event.uname+"</i>"; }
			    	$(element).qtip({
			    	    content: window.mobilecheck() ? "" : eDesc,
			    	    position: {
			    	        target: 'mouse', // Track the mouse as the positioning target
			    	        adjust: { x: 15, y: 5 } // Offset it slightly from under the mouse
			    	    },
			    	    style: 'qtip-light'
			    	});
				}
			},
			eventDrop: function(event, delta, revertFunc) { // Change position (Dragging)
        		if (!confirm("You are trying to change this schedule to\nStart: "+event.start.format('DD-MM-YYYY HH:mm')+"\nEnd: "+event.end.format('DD-MM-YYYY HH:mm')+"\n\nAre you sure about this change?")) {
            		revertFunc(); // Revert to original schedule if cancelled
        		} else {
        			edit(event); // Change schedule upon confirm
        		}
			},
			eventResize: function(event, delta, revertFunc) { // Change of length (Resizing)
				if (!confirm("You are trying to change this schedule to\nStart: "+event.start.format('DD-MM-YYYY HH:mm')+"\nEnd: "+event.end.format('DD-MM-YYYY HH:mm')+"\n\nAre you sure about this change?")) {
            		revertFunc(); // Revert to original schedule if cancelled
        		} else {
        			edit(event); // Change schedule upon confirm
        		}
			},
			events: [
			<?php 
			foreach($events as $event): 			
				// Identify if user is admin for each room
				if ($rbs_admin == "true") { $usrAdm = 1; } else { $usrAdm = 0; } 
				// Identify if user is a room owner
				$query = "SELECT owner FROM rbs_rooms WHERE id=:id LIMIT 1";
				$stmt = $db->prepare($query);
				$stmt->execute(array('id' => $event['rm_id']));
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { if (strpos($row['owner'], $crsid) !== false) { $usrAdm = 1; } }
				// If event is recurring, modify dates
				if ($event['recur'] == 1) { 
					$start = explode(" ", $event['start']);
					$dowstart = $start[0];
					$start = $start[1];
					$end = explode(" ", $event['end']);
					$dowend = $end[0];
					$end = $end[1];
				}
				else { // If event is not recurring
					$start = $event['start'];
					$end = $event['end'];
				}
				// Define color for approved and pending
				if ($event['approved'] == 1) { $color = $event['color']; } else { $color = "#aeaeae"; } 
				// Define if event is draggable/resizeable and clickable
				if ($usrAdm == 1 && $event['start'] > date('Y-m-d H:i:s')) { $editable = "true"; $clickable = 1; } 
					else { $editable = "false"; $clickable = 0; } 
				// Do not make recurring event draggable/resizeable by default
				if ($event['recur'] == 1) { $editable = "false"; } 
				// Make recurring clickable if end date is still in the future
				if ($usrAdm == 1 && $event['recur'] == 1 && $event['end'] > date('Y-m-d H:i:s')) { $clickable = 1; }
				// Define the event information
				$event_dtls = "{";
				$event_dtls .= "id: '".$event['id']."',";
				$event_dtls .= "title: '".addslashes($event['title'])."',";
				// Replace line breaks from textarea in mysql data with <br> tag
				$event_dtls .= "description: '".strtr(addslashes($event['description']),array("\r\n"=>'<br>', "\r"=>'<br>', "\n"=>'<br>'))."',";
				$event_dtls .= "start: '".$start."',";
				$event_dtls .= "end: '".$end."',";
				if ($event['recur'] == 1) {
					$event_dtls .= "dow: [".$event['dow']."],";
					$event_dtls .= "dowstart: new Date('".$event['start']."'),";
					$event_dtls .= "dowend: new Date('".$event['end']."'),";
					$event_dtls .= "recur: 1,";
				} else { $event_dtls .= "recur: 0,"; }
				$event_dtls .= "room: '".$event['room']."',";
				$event_dtls .= "cat_id: '".$event['cat_id']."',";
				$event_dtls .= "tel: '".$event['tel']."',";
				$event_dtls .= "uname: '".$event['name']."',";
				$event_dtls .= "eadd: '".$event['eadd']."',";
				$event_dtls .= "color: '".$color."',";
				$event_dtls .= "colorid: '".$event['color']."',";
				$event_dtls .= "approved: '".$event['approved']."',";
				$event_dtls .= "category: '".$event['category']."',";
				$event_dtls .= "clickable: ".$clickable.",";
				$event_dtls .= "editable: ".$editable;
				$event_dtls .= "},";
				// Show events on depending on user
				$rm_grp_arr = explode(",", $event['restriction']); // Room group restriction
				if (array_intersect($rm_grp_arr,$logged_usr_groups) || $rbs_admin == "true" || $rbs_manager == "true") { echo $event_dtls; }
			endforeach; 
			?>
			]
		});		
		function edit(event){
			start = event.start.format('DD-MM-YYYY HH:mm:ss');
			if(event.end){
				end = event.end.format('DD-MM-YYYY HH:mm:ss');
			}else{
				end = start;
			}			
			id =  event.id;			
			Event = [];
			Event[0] = id;
			Event[1] = start;
			Event[2] = end;			
			$.ajax({
			 url: 'editEventDate.php',
			 type: "POST",
			 data: {Event:Event},
			 success: function(rep) {
					if(rep == 'OK'){ alert('Changes saved!\n\nAn email has been sent to notify the user.'); }
					else if (rep == 'EmailErr') { alert('Changes saved!\n\nEmail not sent, check settings.'); }
					else{ alert('Could not be saved. try again.'); }
				}
			});
		}		
	});
</script>