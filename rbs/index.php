<?php
// Get query strings
parse_str($_SERVER['QUERY_STRING']);
if (!isset($rm_id)) { $rm_id = "0"; } // Define default room id
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
// Get all room details
$query = "SELECT * FROM rbs_rooms WHERE deleted = 0 AND id <> :id ORDER BY room"; // List all rooms
$stmt = $db->prepare($query);
$stmt->execute(array("id"=>$rm_id));
$rooms = $stmt->fetchAll();
$rmCount = $stmt->rowCount(); // Determine how many rooms in the database
// Get the active room
$queryAct = "SELECT * FROM rbs_rooms WHERE deleted = 0 AND id = :id"; // For active room
$stmtAct = $db->prepare($queryAct);
$stmtAct->execute(array("id"=>$rm_id));
$roomAct = $stmtAct->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- FullCalendar CSS -->
<link href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.3.1/fullcalendar.css" rel="stylesheet">
<link href="fullcalendar-custom.css" rel="stylesheet">
<!-- FullCalendar Scripts -->
<script src='../js/moment.min.js'></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.3.1/fullcalendar.min.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<!-- Load Qtip (http://qtip2.com/) -->
<script type="text/javascript" src="//cdn.jsdelivr.net/qtip2/3.0.3/basic/jquery.qtip.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/qtip2/3.0.3/basic/jquery.qtip.min.css">
<!-- Custom scripts -->
<script type="text/javascript">
$(document).ready(function(){
	// Auto hide fields
	$("#recurDiv").hide();
	$("#recur").click(function () {
    	if ($(this).is(":checked")) {
    	    $("#recurDiv").show();
    	    $("#dateDiv").hide();
    	} else {
    		$("#recurDiv").hide();
    	    $("#dateDiv").show();
    	}
    });
    // Load more function
    size_div = $("#roomsBox .boxCell").size();
    x = "<?php echo $appset_roomcount ?>";
    $('#roomsBox .boxCell:lt('+x+')').show();
    $('#loadMore').click(function () {
        x= (x+3 <= size_div) ? x+3 : size_div;
        $('#roomsBox .boxCell:lt('+x+')').slideDown();
    });
});
</script>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
	<?php
    // Show the sidebar items
    include("../includes/sidebar-menu-items.php");
    ?>
    </ul>
</div>		
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
        <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
           <?php
			// Show the menu items
			include("../includes/navbar-menu-items.php");		
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
			<?php           
               // Show the right menu items
               include("../includes/navbar-right-menu-items.php");           
               ?>
			</ul>
		</div>
	</div>
</nav>
<!-- Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!-- Mobile device view only -->
            <div class="hidden-md hidden-lg">
            	<div class="form-group">
            		<select id="myDropdown" class="form-control" onChange="window.location.href=this.value">
            			<?php
            			if ($rm_id == 0) { echo "<option value='./?rm_id=0' selected>All rooms</option>"; }
            			    else { 
                                foreach ($roomAct as $room) {
                                    if (array_intersect($logged_usr_groups,explode(",", $room['restriction'])) ==! false || $rbs_admin == "true" || $rbs_manager == "true") {
                                        echo "<option value='./?rm_id=".$room['id']."' selected>".$room['room']."</option>"; 
                                        echo "<option value='./?rm_id=0'>All rooms</option>";
                                    }
                                }
                            }
            			foreach ($rooms as $room) { 
                            if (array_intersect($logged_usr_groups,explode(",", $room['restriction'])) ==! false || $rbs_admin == "true" || $rbs_manager == "true") {
                                echo "<option value='./?rm_id=".$room['id']."'>".$room['room']."</option>";
                            } 
                        }
            			?>
            		</select>
            	</div>
            	<?php if ($rm_id == 0) { echo "<div class='text-center selRmTxt'>Select room from above</div>"; }
            	if ($rm_id != 0) {
            		echo "<div class='col-xs-12 itemBox'>";
                        foreach ($roomAct as $room) { // Display the active room
            				if (array_intersect($logged_usr_groups,explode(",", $room['restriction'])) ==! false || $rbs_admin == "true" || $rbs_manager == "true") {
            				    if ($room['id'] == $rm_id) {
            				    	echo "<a style='display:block' href='./?rm_id=".$room['id']."'>";
                                    echo "<div class='col-xs-3 col-sm-2'><img class='img-responsive' src='../".$upload_path.$room['img']."'></div>";
            				    	echo "<div class='col-xs-9 col-sm-10' style='margin-bottom:5px;'>".$room['room']."</div>";
            				    	echo "<div class='itemTxtsm'><i class='fa fa-circle' style='color: ".$room['color']."'></i>";
            				    	echo "&nbsp;&nbsp;|&nbsp;&nbsp;<i class='fa fa-user-plus'></i> ".$room['capacity'];
            				    	echo "</div></a>";
            				    }
                            }
            			}               			
            		echo "</div>";
            	} ?>
            </div>
            <!-- Desktop and tablet device view only -->
            <div class="hidden-xs hidden-sm col-md-3 col-lg-offset-1 col-lg-2 scrollit">
            	<?php 
            	if ($rm_id == 0) { 
            		$item_box = "itemBoxActive"; // Highlight selected room 
            		$selRmTxt = "<div class='text-center selRmTxt'>Select room from below:</div>"; // Show select room text
            	} else { 
            		$item_box = "itemBox";
            		$selRmTxt = ""; 
            	}
            	?>
            	<div id="allRmsCell" class="col-md-12 <?php echo $item_box; ?>"><div class="text-center"><a href="./?rm_id=0">All Rooms</a></div></div>
                <?php
                echo $selRmTxt;
                echo "<div id='roomsBox'>";
                // Display active room
                $stmtAct = $db->prepare($queryAct);
                $stmtAct->execute(array("id"=>$rm_id));
                while ($row = $stmtAct->fetch(PDO::FETCH_ASSOC)) {
                    if (array_intersect($logged_usr_groups,explode(",", $room['restriction'])) ==! false || $rbs_admin == "true" || $rbs_manager == "true") {
                        echo "<a style='display:block' href='./?rm_id=".$row['id']."'>";
                        if ($row['id'] == $rm_id) { $item_box = "itemBoxActive"; } else { $item_box = "itemBox"; } // Highlight selected room
                        echo "<div class='col-md-12 boxCell ".$item_box."' style='display:none'>";
                        echo "<div class='col-md-12' style='background: url(../".$upload_path.$row['img'].") 30% no-repeat; background-size:270px; height:100px;'></div>";
                        echo "<div class='col-md-12' style='padding-top:10px;'><b>".$row['room']."</b><br>";
                        echo "<div class='itemTxtsm'><i class='fa fa-circle' style='color: ".$row['color']."'></i>";
                        echo "&nbsp;&nbsp;|&nbsp;&nbsp;<i class='fa fa-user-plus'></i> ".$row['capacity']."</div></div>";
                        echo "</div></a>";
                    }
                }
                foreach ($rooms as $room) { // Display the rest of the rooms
                    if (array_intersect($logged_usr_groups,explode(",", $room['restriction'])) ==! false || $rbs_admin == "true" || $rbs_manager == "true") {
                        echo "<a style='display:block' href='./?rm_id=".$room['id']."'>";
                        echo "<div class='col-md-12 boxCell itemBox' style='display:none'>";
                        echo "<div class='col-md-5'><img class='img-responsive' src='../".$upload_path.$room['img']."'></div>";
                        echo "<div class='col-md-7'>";
                        echo "<div style='margin-bottom:5px;'>".$room['room']."</div>";
                        echo "<div class='itemTxtsm'><i class='fa fa-circle' style='color: ".$room['color']."'></i>";
                        echo "&nbsp;&nbsp;|&nbsp;&nbsp;<i class='fa fa-user-plus'></i> ".$room['capacity']."</div></div>";
                        echo "</div></a>";
                    }
                }
                echo "</div>";
                if ($rmCount >= $appset_roomcount) { // Show load more button
                    echo "<div><button class='btn btn-default btn-sm' id='loadMore' style='width: 100%'>Load more</button></div>";
                }
                ?>
            </div>
            <!-- Display the Calendar -->         
            <div class="col-xs-12 col-md-9 col-lg-7" id="calendar"></div>
        </div>			
    </div>
    <!-- /.row -->		
	<?php 
	require_once("modalAdd.php"); // Add modal
	require_once("modalEdit.php"); // Edit modal
	?>		
	
</div>
<!-- /.container -->
<?php require_once("eventsData.php"); // Functions to load FullCalendar data ?>
</body>
</html>
