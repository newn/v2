<?php
// Process when room is created, edited or deleted
if (isset($_POST['submit'])) {
	if (!empty($_POST['rm_id'])) {
		// Identify file name of existing image
		$query = "SELECT img FROM rbs_rooms WHERE id = :id LIMIT 1";
		$stmt = $db->prepare($query);
		$stmt->execute(array("id"=>$_POST['rm_id']));
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $cur_img = $row['img']; }
	}
	if (isset($_POST['delRm'])) {
		// Mark room as deleted (but don't delete for historical purpose)
		$query = "UPDATE rbs_rooms SET deleted = 1 WHERE id = :id";
		$stmt = $db->prepare($query);
		$stmt->execute(array("id"=>$_POST['rm_id']));
	}
	else {
		// Check if image is uploaded
		if($_FILES['fileToUpload']['tmp_name'] !== "") {
			$target_dir = "../../".$upload_path;
			$img_name = "rbs-".bin2hex(openssl_random_pseudo_bytes(10))."-".time().".".substr(strrchr(basename($_FILES["fileToUpload"]["name"]), "."), 1); // rename image file
			$target_file = $target_dir . $img_name;
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))  { // Upload new image
				if (isset($cur_img)) { unlink("../../".$upload_path.$cur_img); } // Delete old image
				$cur_img = $img_name; // Set new name of image
			}
			// Resize the image so it won't take up so much space on the server
			require_once("../../includes/php_image_magician.php");
			$magicianObj = new imageLib($target_file);
			$magicianObj -> resizeImage(800, 500, "exact", true);
			$magicianObj -> saveImage($target_file, 100);
		}
		if (isset($_POST['rm_id'])) {
			// Update table
        	$query = "UPDATE rbs_rooms SET room=:room, restriction=:restriction, owner=:owner, capacity=:capacity, color=:color, img=:img, date_modified=:date_modified WHERE id=:id";
        	$stmt = $db->prepare($query);
        	$stmt->execute(array("room"=>$_POST['room'],"restriction"=>$_POST['rm_grp'],"owner"=>preg_replace('/\s+/', '', rtrim($_POST['rm_owner'],',')),"capacity"=>$_POST['capacity'],"color"=>$_POST['color'],"img"=>$cur_img,"date_modified"=>date('Y-m-d H:i:s'),"id"=>$_POST['rm_id']));
		}
		else {
			// Insert data to table
			$query = "INSERT INTO rbs_rooms (room,restriction,owner,capacity,color,img,date_modified) VALUES (:room,:restriction,:owner,:capacity,:color,:img,:date_modified)";
			$stmt = $db->prepare($query);
			$stmt->execute(array("room"=>$_POST['room'],"restriction"=>$_POST['rm_grp'],"owner"=>preg_replace('/\s+/', '', rtrim($_POST['rm_owner'],',')),"capacity"=>$_POST['capacity'],"color"=>$_POST['color'],"img"=>$cur_img,"date_modified"=>date('Y-m-d H:i:s')));
		}
	}
}
// Process if category is added
if (isset($_POST['addCat'])) {
	$query = "INSERT INTO rbs_categories (category) VALUES (:category)";
	$stmt = $db->prepare($query);
	$stmt->execute(array("category"=>$_POST['cat']));
}
// Process if category is deleted
if (isset($_POST['delCat'])) {
	$query = "UPDATE rbs_categories SET deleted = 1 WHERE id = :id";
	$stmt = $db->prepare($query);
	$stmt->execute(array("id"=>$_POST['selCat']));
}
?>