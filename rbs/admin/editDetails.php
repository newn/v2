<?php
header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
?>
<!DOCTYPE html>
<html lang="en">
<body>	
	<div class="row">
		<div class="col-xs-12">
			<?php
			//==================================================
			// Show details
			if ($rbs_admin == "true" || $rbs_manager == "true") {
				// Fetch data from bike table
				$query = "SELECT * FROM rbs_rooms WHERE id = :id LIMIT 1";
    			$stmt = $db->prepare($query);
    			$stmt->execute(array("id" => $_GET['id']));
    			$rm_dtls = $stmt->fetchAll();
    			foreach ($rm_dtls as $rm_dtl) {
    				$room = $rm_dtl['room'];
                    $rm_owner = $rm_dtl['owner'];
                    $rm_grp = $rm_dtl['restriction'];
    				$capacity = $rm_dtl['capacity'];
    				$color = $rm_dtl['color'];
    			}
    			if (!empty($rm_dtls)) { echo "<input type='hidden' name='rm_id' value=".$_GET['id'].">"; }
    			?>
				<div class="form-group">
                    <label for="room" class="col-sm-2 control-label">Room <?php if (empty($rm_dtls)) { echo "<span style='color: red;'>*</span>"; } ?> :</label>
                    <div class="col-sm-7">
                        <input type="text" name="room" class="form-control" id="room" value="<?php if (!empty($room)) { echo $room; } ?>" placeholder="<?php if (empty($room)) { echo 'Enter room name'; } ?>" maxlength="50" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="rm_owner" class="col-sm-2 control-label">Owner:</label>
                    <div class="col-sm-5">
                        <input type="text" name="rm_owner" class="form-control" id="rm_owner" value="<?php if (!empty($rm_owner)) { echo $rm_owner; } ?>" placeholder="<?php if (empty($rm_owner)) { echo 'Enter user id'; } ?>" maxlength="50">
                        <span style="font-size: 0.8em">If multiple, use commas to separate id's</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="rm_grp" class="col-sm-2 control-label">Group <?php if (empty($rm_dtls)) { echo "<span style='color: red;'>*</span>"; } ?> :</label>
                    <div class="col-sm-5">
                        <input type="text" name="rm_grp" class="form-control" id="rm_grp" value="<?php if (!empty($rm_grp)) { echo $rm_grp; } ?>" placeholder="<?php if (empty($rm_owner)) { echo 'Enter access group'; } ?>" maxlength="100" required>
                        <span style="font-size: 0.8em">If multiple, use commas to separate groups</span>
                    </div>
                </div>
                <div class="form-group">
                   	<label for="capacity" class="col-sm-2 control-label">Capacity:</label>
                   	<div class="col-sm-2">
                    	<input type="number" name="capacity" class="form-control" id="capacity" value="<?php if (!empty($capacity)) { echo $capacity; } else { echo "1"; } ?>">
                   	</div>
                </div>
                <div class="form-group">
                    <label for="color" class="col-sm-2 control-label">Color:</label>
                    <div id="cpicker" class="col-xs-9 col-sm-4 input-group colorpicker-component" style="padding-left: 16px;">
                    	<input type="text" name="color" id="color" class="form-control" value="<?php if (!empty($color)) { echo $color; } else { echo "#000000"; } ?>">
                    	<span class="input-group-addon"><i></i></span>
                	</div>
                </div>
				<div class="form-group">
					<label for="fileToUpload" class="col-sm-2 control-label">Image <?php if (empty($rm_dtls)) { echo "<span style='color: red;'>*</span>"; } ?> :</label>
    				<div class="col-sm-10">
    					<div class="imgupload panel panel-default">
        					<div class="panel-heading clearfix">
        						<h3 class="panel-title pull-left"><?php if (!empty($rm_dtls)) { echo "Change image"; } else { echo "Upload image"; } ?></h3>
							</div>
							<div class="file-tab panel-body">
								<div>
									<button type="button" class="btn btn-default btn-file">
										<span>Browse</span>
										<input type="file" id="fileToUpload" name="fileToUpload" <?php if (empty($rm_dtls)) { echo "required"; } ?>>
									</button>
									<button type="button" class="btn btn-default">Remove</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				if (!empty($rm_dtls)) { ?>
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-6">
							<div class="checkbox-inline"><input type="checkbox" id="delRm" name="delRm">&nbsp;<span style="color: #d9534f; font-size: 1.1em">Delete this room?</span></div>
						</div>	
					</div>
				<?php
				}
			}
			else {
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg.".</p><br>";
                echo "<p><a href='/$paths[1]' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
			}
			?>	
		</div>
	</div>    
</body>
</html>