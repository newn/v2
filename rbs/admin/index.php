<?php
header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
// To process data when room is edited / added
require_once("processData.php");
require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
$rbs_rooms = new ajaxCRUD("Item", "rbs_rooms", "id", "");
######################################################## 
// Detect if mobile device, hide some column
if(!isset($_GET['screen'])) {
/* This code will be executed if screen resolution has not been detected.*/
    echo "<script language='JavaScript'>
    <!-- 
    document.location=\"$PHP_SELF?screen=done&w=\"+screen.width;
    //-->
    </script>";
}
else {    
    if(($_GET['w']<=480)) {
        $rbs_rooms->omitField("restriction");
        $rbs_rooms->omitField("capacity");
        $rbs_rooms->omitField("color");
        $rbs_rooms->omitField("img");
    }
}
#the table fields have prefixes; i want to give the heading titles something more meaningful
$rbs_rooms->displayAs("id", "&nbsp;");
$rbs_rooms->displayAs("room", $header_room);
$rbs_rooms->displayAs("restriction", $header_grpres);
$rbs_rooms->displayAs("owner", $header_owner);
$rbs_rooms->displayAs("capacity", $header_capacity);
$rbs_rooms->displayAs("color", $header_color);
$rbs_rooms->displayAs("img", "Image");
#i could disallow editing for certain, individual fields
$rbs_rooms->disallowEdit('room');
$rbs_rooms->disallowEdit('restriction');
$rbs_rooms->disallowEdit('owner');
$rbs_rooms->disallowEdit('capacity');
$rbs_rooms->disallowEdit('color');
$rbs_rooms->disallowEdit('img');
#hide field
$rbs_rooms->omitfield("deleted");
$rbs_rooms->omitfield("date_modified");
#i can order my table by whatever i want
$rbs_rooms->addOrderBy("ORDER BY room ASC");
#where clause
$rbs_rooms->addWhereClause("WHERE deleted = 0");
#i can disallow adding/deleting rows to the table
$rbs_rooms->disallowAdd();
$rbs_rooms->disallowDelete();
#set the number of rows to display (per page)
$rbs_rooms->setLimit("10");
#i can format the data in cells however I want with formatFieldWithFunction
#this is arguably one of the most important (visual) functions
$rbs_rooms->formatFieldWithFunction('id', 'editLink');
$rbs_rooms->formatFieldWithFunction('img', 'showImg');
$rbs_rooms->formatFieldWithFunction('color', 'showColor');
$rbs_rooms->deleteText = "delete";
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- BS Image Upload -->
<link href="../../css/bootstrap-imageupload.css" rel="stylesheet">
<script src="../../js/bootstrap-imageupload.js"></script>
<!-- Bootstrap-Colopicker (https://farbelous.github.io/bootstrap-colorpicker) -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.min.css.map">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js"></script>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="../"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
        <?php
		// Control the view of menu items for the logged user
		
		// Show the menu items
        include("../../includes/navbar-menu-items.php");
		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php           
            // Show the right menu items
            include("../../includes/navbar-right-menu-items.php");           
            ?>
		</ul>
	</div>
	</div>
</nav>
<div class="container">	
	<div class="row">
		<div class="col-xs-12">
			<div id="roomsTbl" class='col-xs-12 col-sm-12 col-md-12'>
                <?php          
                // my self-defined functions used for formatFieldWithFunction
                function showImg($val) {
                    $up_path = qr("SELECT value FROM gen_settings WHERE setting='upload_path'");
                    if (empty($val)) return "";
                    return "<img src='../../".$up_path['value'].$val."' width='60px' height='45px'>";
                }
                function editLink($val) {
                    if (empty($val)) return "";
                    return "<button type='button' class='btn btn-link editBtn' data-id='$val' data-toggle='modal' data-target='#editModal'>Edit</button>";
                }
                function showColor($val) {
                    if (empty($val)) return "";
                    return "<div style='font-size: 1.2em;'><span class='glyphicon glyphicon-stop' aria-hidden='true' style='color: ".$val."'></span></div>";
                }
                // actually show the table if user has access
			    if ($rbs_admin == "true" || $rbs_manager == "true") {
                echo "<div class='col-xs-12 col-sm-12 col-md-12 text-center' style='margin-bottom: 10px;'><h4><u>Rooms</u></h4></div>";
                // Show the actual table  
                $rbs_rooms->showTable();
                ?>
                <button type='button' class='btn btn-default addBtn' data-toggle='modal' data-target='#editModal'>Add Room</button>
            </div>
            <div class='col-xs-12 col-sm-12 col-md-12 text-center' style='margin-top: 40px; padding-bottom: 15px; background-color: #f7f7f7;'>
                <h4><u>Category</u></h4>
                <form name="catForm" data-toggle="validator" class="form-inline" method="post" action="./" role="form">
                    <div class="form-group">
                        <select name="selCat" class="form-control">
                            <option value="">Choose category...</option>
                            <?php
                            $query = "SELECT * from rbs_categories WHERE deleted = 0";
                            $stmt = $db->prepare($query);
                            $stmt->execute();
                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                echo "<option value='".$row['id']."'>".$row['category']."</option>";
                            }
                            ?>
                        </select>
                        <button type='submit' name="delCat" class='btn btn-default' onclick="return confirm('Are you sure you want to delete this category?')">Delete</button>
                        &nbsp;&nbsp;&nbsp;<button type='button' class='btn btn-default' data-toggle='modal' data-target='#addCatModal'>Add Category</button>
                    </div>
                </form>
            </div> 
            <!-- Display export to file button -->
            <div class='col-xs-12 col-sm-12 col-md-12 text-center' style='margin-top: 20px; padding-top: 15px; padding-bottom: 15px; background-color: #f7f7f7;'>
                <button type='button' class='btn btn-default' data-toggle='modal' data-target='#exportModal'>Export to file</button>
            </div>
            <!-- Edit Room Modal -->
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content text-left">
                        <form name="myForm" data-toggle="validator" class="form-horizontal" method="post" enctype="multipart/form-data" action="./" role="form">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit Room</h4>
                            </div>
                            <div id="editMbody" class="modal-body"><!-- Contents will show here --></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" id="subBtn" name="submit" class="btn btn-primary"><!-- Text based on chosen option --></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Add Category Modal -->
            <div class="modal fade" id="addCatModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content text-left">
                        <form name="addCatForm" data-toggle="validator" class="form-horizontal" method="post" action="./" role="form">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Add Category</h4>
                            </div>
                            <div class="modal-body"><input type="text" name="cat" class="form-control" placeholder="Enter category name..."></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" id="subBtn" name="addCat" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                // Populate the details of entries to the modal
                $('#roomsTbl').on('click','.editBtn,.addBtn',function(){
                    $('#editModal').validator();
                    var id = $(this).data('id');
                    if (id > 0) { $("#subBtn").html('Save Changes'); } else { $("#subBtn").html('Add Room'); }
                    $.ajax({
                        type: 'POST',
                        url: 'editDetails.php?id='+id,
                        success: function(data) {
                            $('#editMbody').html(data);
                            $('#cpicker').colorpicker(); //Initialise color picker
                            $('.imgupload').imageupload({ // Initialise image upload
                                allowedFormats: [ "jpg", "jpeg", "png", "gif" ],
                                previewWidth: 150,
                                previewHeight: 150,
                                maxFileSizeKb: 5120
                            });
                        },
                        error:function(err){
                            alert("error"+JSON.stringify(err));
                        }
                    });                   
                });
            </script>          
            <?php
            // Export modal
            include ("../../export/expmodal.php");
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
			?>	
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>