<?php
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
// Update table if booking time is changed
if (isset($_POST['Event'][0]) && isset($_POST['Event'][1]) && isset($_POST['Event'][2])) {	
	$id = $_POST['Event'][0]; // Event id
	$start = $_POST['Event'][1]; // Event start time
	$end = $_POST['Event'][2]; // Event end time
	$query = "UPDATE rbs_events SET  start=:start, end=:end, modified_by=:modified_by, date_modified=:date_modified WHERE id=:id";	
	$stmt = $db->prepare($query);
	if ($stmt == false) {
		print_r($db->errorInfo());
		die ('Prepare error!');
	}
	$exec = $stmt->execute(array("start"=>date('Y-m-d H:i:s', strtotime($start)),"end"=>date('Y-m-d H:i:s', strtotime($end)),"modified_by"=>$crsid,"date_modified"=>date('Y-m-d H:i:s'),"id"=>$id));
	if ($exec == false) {
		print_r($stmt->errorInfo());
		die ('Execute error!');
	}
	else { 
		// Send email notification to user
		$query = "SELECT approved,room,eadd,owner FROM rbs_events INNER JOIN rbs_rooms ON rbs_events.rm_id = rbs_rooms.id INNER JOIN user_details ON rbs_events.crsid = user_details.crsid WHERE rbs_events.id = :id LIMIT 1";
		$stmt = $db->prepare($query);
		$stmt->execute(array("id"=>$id));
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
			if ($row['approved'] == 1) {
				// Get email of room owner/s
				$own_arr = explode(",", $row['owner']);
				foreach($own_arr as $own) {
					$query2 = "SELECT eadd FROM user_details WHERE crsid=:crsid";
					$stmt2 = $db->prepare($query2);
					$stmt2->execute(array("crsid"=>$own));
					while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
						$own_eadd[] = $row2['eadd'];
					}
				}
				// Set headers
				$headers = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				// Set From email
				if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
				// Set email for query
				if (empty($appset_emailstaff)) { $to = $default_to_email; } else { $to = $appset_emailstaff; }
				// If room has owner/s, then use this email for query
				if (!empty($own_eadd)) {$to = implode(",", $own_eadd);} else {$to = $to;}	
				// Email notification for user
				$message = $notify_msg_change_date."<br><br><b>Room:</b> ".$row['room']."<br><b>Start:</b> ".date('d M Y H:i:s',strtotime($start))."<br><b>End:</b> ".date('d M Y H:i:s',strtotime($end));
				$message .= "<br><br>".$notify_msg_query."<br>".$to;
				if (@mail($row['eadd'],$notify_subj_change_date,$message,$headers)) {
					die('OK'); // Return ok if everything is processed
				} else { die('EmailErr'); } // Return ok but email not sent
			}
		}
	}
}	
?>
