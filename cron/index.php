<?php
require_once("../includes/conf.php");
require_once("../includes/con_db.php");

//*******************************//
// For Cambridge University only //
//*******************************//
if ($univ_id == "cam") {
    // ***** For updating user database from UIS Lookup ***** //
    $curtime = strtotime('now');
    if ($curtime > strtotime('01:10am') && $curtime < strtotime('01:20am')) { // Only run at specific time
        if (!empty($cam_instids)) {
            $entries = 0;
            $instid_array = explode(",", $cam_instids);
            foreach ($instid_array as $instid) {
                // Include the process script
                include("../settings/lookup-process.php");
            }
            if (!empty($error)) { 
                $error = "[".date('Y-m-d H:i:s')."] ".$error;
                echo $error; 
            }
            // Echo success
            echo "[".date('Y-m-d H:i:s')."] SUCCESS: ".$entries." entries processed.";
        }
        else {
            // Echo error processing
            echo "[".date('Y-m-d H:i:s')."] ERROR: Institution IDs not present.";
        }
        // Check local database users' status in Lookup and mark appropriately. Skip users that do not sync.
        $query = "SELECT crsid,inactive FROM user_details WHERE lkup_sync=1";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $inactCount = 0;
        $reactCount = 0;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $person = @file_get_contents("https://".$lkup_usr.":".$lkup_pwd."@www.lookup.cam.ac.uk/api/v1/person/crsid/".$row['crsid']."?format=json");
            if (strpos($http_response_header[0], "200")) {
                $person = json_decode($person, TRUE);
                if(isset($person['result']['person'])) { // Check if crsid is valid, if not then skip
                    if ($person['result']['person']['cancelled'] == 1) { // Check if user is inactive
                        // Mark user as inactive if not already
                        if ($row['inactive'] == 0) {
                            $query2 = "UPDATE user_details SET inactive=1 WHERE crsid=:crsid";
                            $stmt2 = $db->prepare($query2);
                            $stmt2->execute(array("crsid"=>$row['crsid']));
                            $inactCount++;
                        }
                    } else { // if user is active, check if user needs to be reactivated again
                        if ($row['inactive'] == 1) {
                            $query2 = "UPDATE user_details SET inactive=0 WHERE crsid=:crsid";
                            $stmt2 = $db->prepare($query2);
                            $stmt2->execute(array("crsid"=>$row['crsid']));
                            $reactCount++;
                        }
                    }
                }
            }
        }
        echo "\n[".date('Y-m-d H:i:s')."] ".$inactCount." users were marked as inactive.";
        echo "\n[".date('Y-m-d H:i:s')."] ".$reactCount." users were marked as active again.";
    }
}
//*******************************//
// Auto Reminders                //
//*******************************//
// Send email notification to users for signed out keys
// Check if From: email in defined in keys app
$query = "SELECT value FROM keys_texts WHERE field='appset_emailfrom' LIMIT 1";
$stmt = $db->prepare($query);
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    if ($row['value'] != '') { $default_fr_email = $row['value']; }
}
// Fetch all signed out keys
$query = "SELECT keys_mgmt.id,eadd,ext_email,auto_rem,keys_mgmt.key_id,key_no,key_name,key_group,date_issued FROM keys_mgmt ";
$query .= "LEFT JOIN keys_list ON keys_mgmt.key_id = keys_list.key_id ";
$query .= "LEFT JOIN user_details ON keys_mgmt.crsid = user_details.crsid WHERE key_stat=1";
$stmt = $db->prepare($query);
$stmt->execute();
$signedOutKeys = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($signedOutKeys as $sok) {
    // Loop through each signed out key to check if notification needs to be triggered
    $query = "SELECT * FROM keys_autorem WHERE grp_id=:grp_id AND rem_id > :rem_id ORDER BY rem_id ASC LIMIT 1";
    $stmt = $db->prepare($query);
    $stmt->execute(array("grp_id"=>$sok['key_group'], "rem_id"=>$sok['auto_rem']));
    if ($stmt->rowCount() > 0) { // If row is found, process further
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // Set the email address
            if(!empty($sok['eadd'])) { $to = $sok['eadd']; }
            elseif(!empty($sok['ext_email'])) { $to = $sok['ext_email']; }
            else { $to = 0; }
            //if ($to !== 0) { // Only trigger notification if email value is present on the user details
                if($row['dur_time'] == 0) { // If reminder option is fixed time
                    if(date("H:i:s") > $row['fixed_time']) { // If key is still out past the set time in reminder
                        // Email the user
                        // Set email headers
                        $headers = 'MIME-Version: 1.0'."\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
                        $headers .= "From:".$default_fr_email."\r\n";
                        // Set any Cc and/or Bcc if any
                        if (!empty($row['e_cc'])) { $headers .= "Cc:".$row['e_cc']."\r\n"; }
                        if (!empty($row['e_bcc'])) { $headers .= "Bcc:".$row['e_bcc']."\r\n"; }
                        // Set message
                        $e_msg = "<b>Key: </b>".$sok['key_no']."<br>";
                        if(!empty($sok['key_name'])) { $e_msg .= "<b>Description: </b>".$sok['key_name']."<br>"; }
                        $e_msg .= "<br>".$row['message'];
                        // Only trigger notification if email value is present on the user details
                        if ($to !== 0) { mail($to,$row['subject'],$e_msg,$headers); }
                        // Update signed out key log to reflect triggered reminder
                        $query2 = "UPDATE keys_mgmt SET auto_rem=:auto_rem WHERE id=:id";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("auto_rem"=>$row['rem_id'], "id"=>$sok['id']));
                        // Add log to the keys_reminder table
                        $query2 = "INSERT INTO keys_reminder (kr_id,reminder) VALUES (:kr_id,:reminder)";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("kr_id"=>$sok['id'], "reminder"=>"[AUTO] ".$row['subject']));
                    }
                } else { // if reminder option is duration time
                    if(strtotime($sok['date_issued']." +".$row['dur_time']." hours") < time()) { // If key is still out past the time duration set in reminder
                        // Email the user
                        // Set email headers
                        $headers = 'MIME-Version: 1.0'."\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
                        $headers .= "From:".$default_fr_email."\r\n";
                        // Set any Cc and/or Bcc if any
                        if (!empty($row['e_cc'])) { $headers .= "Cc:".$row['e_cc']."\r\n"; }
                        if (!empty($row['e_bcc'])) { $headers .= "Bcc:".$row['e_bcc']."\r\n"; }
                        // Set message
                        $e_msg = "<b>Key: </b>".$sok['key_no']."<br>";
                        if(!empty($sok['key_name'])) { $e_msg .= "<b>Desc: </b>".$sok['key_name']."<br>"; }
                        $e_msg .= "<br>".$row['message'];
                        // Only trigger notification if email value is present on the user details
                        if ($to !== 0) { mail($to,$row['subject'],$e_msg,$headers); }
                        // Update signed out key log to reflect triggered reminder
                        $query2 = "UPDATE keys_mgmt SET auto_rem=:auto_rem WHERE id=:id";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("auto_rem"=>$row['rem_id'], "id"=>$sok['id']));
                        // Add log to the keys_reminder table
                        $query2 = "INSERT INTO keys_reminder (kr_id,reminder) VALUES (:kr_id,:reminder)";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("kr_id"=>$sok['id'], "reminder"=>"[AUTO] ".$row['subject']));
                    }
                }
            //}
        }
    }
}
//*******************************//
// For executing custom scripts  //
//*******************************//
include ("custom/index.php");
?>