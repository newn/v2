<?php
header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$bike = new ajaxCRUD("Item", "bike", "id", "");
##
######################################################## 
// Detect if mobile device, hide some column
if(!isset($_GET['screen'])) {
/* This code will be executed if screen resolution has not been detected.*/
    echo "<script language='JavaScript'>
    <!-- 
    document.location=\"$PHP_SELF?screen=done&w=\"+screen.width;
    //-->
    </script>";
}
else {    
    if(($_GET['w']<=480)) {
        $bike->omitField("crsid");
        $bike->omitField("frame_num");
    }
} 
#i can define a relationship to another table
$bike->defineRelationship("crsid", "user_details", "crsid", "name");
#the table fields have prefixes; i want to give the heading titles something more meaningful
$bike->displayAs("id", $header_details);
$bike->displayAs("b_num", $permit);
$bike->displayAs("crsid", $details_name);
$bike->displayAs("make_model", $make_model);
$bike->displayAs("frame_num", $frame_num);
$bike->displayAs("keywords", $search_text);
#i could omit a field if I wanted
$bike->omitField("colour");
$bike->omitField("gear");
$bike->omitField("wheel");
$bike->omitField("postcode");
$bike->omitField("features");
$bike->omitField("img");
$bike->omitField("date_added");
$bike->omitField("keywords");
#i could disallow editing for certain, individual fields
$bike->disallowEdit('id');
$bike->disallowEdit('crsid');
$bike->disallowEdit('b_num');
#i can order my table by whatever i want
$bike->addOrderBy("ORDER BY id DESC");
#i can use a where field to better-filter my table
$bike->addWhereClause("WHERE (b_num IS NOT NULL)");
#i can disallow adding rows to the table
$bike->disallowAdd();
#set the number of rows to display (per page)
$bike->setLimit($appset_actrowlimit);
#set a filter box at the top of the table
$bike->addAjaxFilterBox('keywords',15);
#i can format the data in cells however I want with formatFieldWithFunction
#this is arguably one of the most important (visual) functions
$bike->formatFieldWithFunction('id', 'makeLink');
$bike->deleteText = "delete";
# Conditional features based on user
if ($bike_admin != "true") {
    $bike->disallowEdit('make_model');
    $bike->disallowEdit('frame_num');
    $bike->disallowDelete();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="../"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
        <?php
		// Control the view of menu items for the logged user
		
		// Show the menu items
        include("../../includes/navbar-menu-items.php");
		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php           
            // Show the right menu items
            include("../../includes/navbar-right-menu-items.php");           
            ?>
		</ul>
	</div>
	</div>
</nav>
<div class="container">	
	<div class="row">
		<div id="resultTbl" class="col-xs-12">
			<?php
			// my self-defined functions used for formatFieldWithFunction
            function makeLink($val) {
                if (empty($val)) return "";
                    //return "<a href='details.php?id=$val'>$val</a>";
                    return "<button type='button' class='btn btn-link dtlBtn' data-id='$val' data-toggle='modal' data-target='#dtlModal'>View</button>";
            }            
            // actually show the table if user has access
			if ($bike_admin == "true" || $bike_manager == "true") {               
                $bike->showTable();
			    # Output number of rows returned
                echo "<button type='button' class='btn btn-static'>Total rows: ";
                $bike->insertRowsReturned();
                echo "</button>";
                echo "&nbsp;<a type='button' class='btn btn-default' data-toggle='modal' data-target='#exportModal'>Export to file</a>";
			    echo "<br /><br />";
                // Export modal
                include ("../../export/expmodal.php");
                ?>
                <!-- Populate the details of entries to the modal -->
                <script type="text/javascript">                
                    $('#resultTbl').on('click','.dtlBtn',function(){
                        var id = $(this).data('id');
                        $.ajax({
                            type: 'POST',
                            url: 'getDetails.php?id='+id,
                            success: function(data) {
                              $('.detBody').html(data);
                            },
                            error:function(err){
                              alert("error"+JSON.stringify(err));
                            }
                        });
                    });
                </script>
                <!-- Details Modal -->
                <div name="dtlModal" id="dtlModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body detBody"><!-- content from details.php goes here --></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        </div>    
                    </div>
                    </div>
                </div>
            <?php
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
			?>	
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>