<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$bike_texts = new ajaxCRUD("Item", "bike_texts", "field", "");
##
########################################################  
#the table fields have prefixes; i want to give the heading titles something more meaningful
$bike_texts->displayAs("field", "Field");
$bike_texts->displayAs("value", "Value");
#i could disallow editing for certain, individual fields
$bike_texts->disallowEdit('field');
#i can order my table by whatever i want
$bike_texts->addOrderBy("ORDER BY value ASC");
#set the number of rows to display (per page)
$bike_texts->setLimit(20);
#where field to better-filter my table
$bike_texts->addWhereClause("WHERE field NOT LIKE 'appset%'");
#set a filter box at the top of the table
#$bike_texts->addAjaxFilterBox('value');
$bike_texts->deleteText = "delete";
# Disallow add
$bike_texts->disallowAdd();
# Disallow delete
$bike_texts->disallowDelete();

?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
<!-- Local css -->
<link href="../local.css" rel="stylesheet">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="../"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
        	   <span class="icon-bar"></span>
        	   <span class="icon-bar"></span>
        	   <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
        	   <?php
				// Control the view of menu items for the logged user			
				// Show the menu items
        		include("../../includes/navbar-menu-items.php");			
				?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
        	    // Show the right menu items
        	    include("../../includes/navbar-right-menu-items.php");           
        	    ?>
			</ul>
		</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php              
            // actually show the table if user has access
			if ($bike_admin == "true") { ?>                 
            	<div class='col-xs-12' style='margin-bottom: 10px;'><h5><b>App Settings:</b></h5></div>
				<div class='col-md-6 settingForm'>
					<div class="input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">From email:</button></span>
      					<input id="emailfrom" type="text" class="form-control input-sm" placeholder="Leave blank for default" style="max-width:250px;" value="<?php echo $appset_emailfrom ?>">
      				</div>
      				<div class="input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1">To email:</button></span>
      					<input id="emailstaff" type="text" class="form-control input-sm" placeholder="Leave blank for default" style="max-width:250px;" value="<?php echo $appset_emailstaff ?>">
      				</div>
      				<div class="input-group">
      					<span class="input-group-btn"><button class="btn btn-default btn-sm btn-static" tabindex="-1"><?php echo $nav_manage ?> rows per page:</button></span>
      					<input id="actrowlimit" type="number" class="form-control input-sm" value="<?php echo $appset_actrowlimit ?>" style="max-width:70px;">
      				</div>
					<button id="save" type="button" class="btn btn-default btn-sm">Save</button>
					<div id="saveMsg"><!-- This is where the saved success message will appear --></div>
				</div>
				<div class='col-xs-12' style='margin-top:20px;'><h5><b>Language:</b> <i>(Click on value to edit)</i></h5></div>
			<?php
			echo "<div class='col-xs-12 col-sm-12 col-md-12'>";               
            $bike_texts->showTable();
			echo "<br /><br />";
			echo "</div>";
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
			?>	
		</div>
		<script type="text/javascript">
		    // Update settings
            $('.settingForm').on('click','#save',function(){
                var emailfrom = $("input#emailfrom").val();
                var emailstaff = $("input#emailstaff").val();
                var actrowlimit = $("input#actrowlimit").val();
                $.ajax({
                    type: 'POST',
                    url: '../../includes/app-settings.php?app=bike&appset_emailfrom='+emailfrom+'&appset_emailstaff='+emailstaff+'&appset_actrowlimit='+actrowlimit,
                    success: function(data) {
                      $('#saveMsg').html(data);
                    },
                    error:function(err){
                      alert("error"+JSON.stringify(err));
                    }
                });
            });
		</script>
	</div>
	<br /><br />       
</div>
</body>
</html>