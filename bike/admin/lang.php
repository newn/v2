<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output
#this one line of code is how you implement the class
########################################################
##
$bike_texts = new ajaxCRUD("Item", "bike_texts", "field", "");
##
########################################################  
#the table fields have prefixes; i want to give the heading titles something more meaningful
$bike_texts->displayAs("field", "Field");
$bike_texts->displayAs("value", "Value");
#i could disallow editing for certain, individual fields
$bike_texts->disallowEdit('field');
#i can order my table by whatever i want
$bike_texts->addOrderBy("ORDER BY value ASC");
#set the number of rows to display (per page)
$bike_texts->setLimit(50);
#set a filter box at the top of the table
$bike_texts->addAjaxFilterBox('value');
$bike_texts->deleteText = "delete";
# Disallow add
$bike_texts->disallowAdd();
# Disallow delete
$bike_texts->disallowDelete();

?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="../"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
        	   <span class="icon-bar"></span>
        	   <span class="icon-bar"></span>
        	   <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
        	   <?php
				// Control the view of menu items for the logged user			
				// Show the menu items
        		include("../../includes/navbar-menu-items.php");			
				?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
        	    // Show the right menu items
        	    include("../../includes/navbar-right-menu-items.php");           
        	    ?>
			</ul>
		</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php              
               // actually show the table if user has access
			if ($bike_admin == "true") {                  
            	$bike_texts->showTable();
				# Output number of rows returned
				echo "Total returned rows: ";
				$bike_texts->insertRowsReturned();
				echo "<br /><br />";
            }
            else {
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
			?>	
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>