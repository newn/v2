<?php
header ( "Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
?>
<!DOCTYPE html>
<html lang="en">
<body>	
	<div class="row">
		<div class="col-xs-12">
			<?php
			//==================================================
			// Get query strings
            parse_str($_SERVER['QUERY_STRING']);
			// Show details
			if ($bike_admin == "true" || $bike_manager == "true") {
				// Fetch data from bike table
				$query = "SELECT * FROM bike WHERE id = :id LIMIT 1";
    			$stmt = $db->prepare($query);
    			$stmt->execute(array(":id" => $id));
    			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					// Fetch data from user_details table
					$query2 = "SELECT * FROM user_details WHERE crsid = :crsid LIMIT 1";
    				$stmt2 = $db->prepare($query2);
    				$stmt2->execute(array(":crsid" => $row['crsid']));
    				while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
						?>
						<div class="table-responsive">
							<table class="table table-striped table-condensed">
								<thead><tr><th colspan="2">&nbsp;</th></tr></thead>
								<tbody>
									<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $permit ?>:</b></td><td><?php echo $row['b_num'] ?></td></tr>
									<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_name ?>:</b></td><td><?php echo $row2['name']." (".$row['crsid'].")&nbsp;&nbsp;<a href='mailto:".$row2['eadd']."?subject=Your%20Bike%20Registration'><i class='fa fa-envelope'></i></a>"; ?></td></tr>
									<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $make_model ?>:</b></td><td style="word-wrap: break-word; width: 320px; white-space: normal;"><?php echo $row['make_model'] ?></td></tr>
									<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $frame_num ?>:</b></td><td><?php echo $row['frame_num'] ?></td></tr>
									<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_colour ?>:</b></td><td><?php echo $row['colour'] ?></td></tr>
									<?php if (!empty($row['gear'])) { ?><tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_gear ?>:</b></td><td><?php echo $row['gear'] ?></td></tr> <?php } ?>
									<?php if (!empty($row['wheel']) && $row['wheel'] != 0) { ?><tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_wheel ?>:</b></td><td><?php echo $row['wheel'] ?></td></tr> <?php } ?>
									<?php if (!empty($row['postcode'])) { ?><tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $postcode_stamp ?>:</b></td><td><?php echo $row['postcode'] ?></td></tr> <?php } ?>
									<?php if (!empty($row['features'])) { ?><tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $sp_features ?>:</b></td><td style="word-wrap: break-word; width: 320px; white-space: normal;"><?php echo $row['features'] ?></td></tr> <?php } ?>
									<?php if (!empty($row['img'])) { ?><tr><td class="col-xs-2 col-sm-3 col-md-3"><b>Image:</b></td><td><a href="../../<?php echo $upload_path.$row['img'] ?>" target="_blank"><img src="../../<?php echo $upload_path.$row['img'] ?>" style="max-width: 150px;"></a></td></tr> <?php } ?>
									<tr><td class="col-xs-2 col-sm-3 col-md-3"><b><?php echo $details_date_added ?>:</b></td><td><?php echo date('d-m-Y H:i:s', strtotime($row['date_added'])); ?></td></tr>
								</tbody>									
							</table>
						</div>
					<?php
					}
				}
			}
			else {
				echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg.".</p><br>";
                echo "<p><a href='/$paths[1]' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
			}
			?>	
		</div>
	</div>    
</body>
</html>