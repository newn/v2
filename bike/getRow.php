<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<script type="text/javascript">
    $(function () {
        $("#del_tick").click(function () {
            if ($(this).is(":checked")) {
                $("#apprLabel").html('<label class="control-label">Reject message (optional):</label>');
                $("#manInput").html('');
                $("#apprInput").html('<input type="text" id="del_msg" name="del_msg" class="form-control" maxlength="100" placeholder="Enter message here...">');
                $("#rejDiv").html('');
                $("#subBut").html('<button class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>&nbsp;<button id="submit" name="submit" type="submit" class="btn btn-danger btn-sm">Reject</button>');
            //} else {
            //	$("#apprLabel").html('<label class="control-label">Bike number:</label>');
            //    $("#apprInput").html('<label><input type="radio" name="b_radio" id="auto_gen" value="1" checked>Auto-generate&nbsp;&nbsp;</label><label><input type="radio" name="b_radio" id="man_gen" value="2">Manual entry&nbsp;&nbsp;</label>');
            //    $("#subBut").html('<button id="submit" name="submit" type="submit" class="btn btn-success btn-sm">Approve</button>');
            }
        });
        $("#man_gen").click(function () {
            $("#manInput").html('<input type="text" id="b_num" name="b_num" class="form-control" placeholder="Enter bike number..." maxlength="10" required>');
        });
        $("#auto_gen").click(function () {
            $("#manInput").html('');
        });
    });
</script>
</head>
<body>
<?php
if ($bike_admin == "true") {
	$query = "SELECT b_num, name FROM bike INNER JOIN user_details ON bike.crsid = user_details.crsid WHERE bike.crsid = :crsid AND b_num IS NOT NULL";
   	$stmt = $db->prepare($query);
   	$stmt->execute(array("crsid" => $_GET['crsid']));
   	if ($stmt->rowCount() > 0) {
   		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $b_name = $row['name']; }
   		echo "<p><b>Name:</b> ".$b_name."<br>";
   		echo "<b>Registered bikes:</b> ".$stmt->rowCount()."</p>";
   	}
   	?>
	<!-- Form to display details and create bike number -->
	<form name="apprForm" data-toggle="validator" method="post" action="<?php $_PHP_SELF; ?>" role="form">
		<input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
		<div class="form-group">
			<div id="apprLabel"><label class="control-label">Bike number:</label></div>
			<div id="apprInput" class="radio">
  				<label><input type="radio" name="b_radio" id="auto_gen" value="1" checked>Auto-generate&nbsp;&nbsp;</label>
				<label><input type="radio" name="b_radio" id="man_gen" value="2">Manual entry&nbsp;&nbsp;</label>
			</div>
			<div id="manInput" class="form-group"></div>
			<!--<div id="apprLabel"><label class="control-label">Enter bike number:</label></div>	
			<div id="apprInput"><input type="text" id="b_num" name="b_num" class="form-control" maxlength="10" required></div>-->		
		</div>
		<div id="rejDiv" class="form-group">
			<label class="control-label"><span style="color: #d9534f;">Reject this registration?</span></label>&nbsp;
			<input type="checkbox" id="del_tick">		
		</div>
		<div class="form-group" style="text-align: right;">
			<div id="subBut"><button id="submit" name="submit" type="submit" class="btn btn-success btn-sm">Approve</button></div>
		</div>
	</form>
<?php
}
?>
</body>
</html>