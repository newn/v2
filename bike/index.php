<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- File upload css -->
<link href="../css/bootstrap-imageupload.css" rel="stylesheet">
<!-- Load customization for bootstrap -->
<link rel="stylesheet" href="local.css">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
		<?php
     	// Show the sidebar items
     	include("../includes/sidebar-menu-items.php");
     	?>
    </ul>
</div>		
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
        <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
			<span class="sr-only">Toggle navigation</span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
        </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
        	   <?php
			// Control the view of menu items for the logged person		
			// Show the menu items
			include("../includes/navbar-menu-items.php");		
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
        	    	// Show the right menu items
        	    	include("../includes/navbar-right-menu-items.php");           
        	    ?>
			</ul>
		</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php
			// Upload form values to database
			//==================================================
			// Insert values to database
			if(isset($_POST['add'])) {
				$datetime = date("Y-m-d H:i:s");
				// Upload image if exist
				if($_FILES['fileToUpload']['tmp_name'] !== "") {
					$target_dir = "../".$upload_path;
					$img_name = "bike-".bin2hex(openssl_random_pseudo_bytes(10))."-".time().".".substr(strrchr(basename($_FILES["fileToUpload"]["name"]), "."), 1); // rename image file
					$target_file = $target_dir . $img_name;
					move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file); // Upload the image
					// Resize the image so it won't take up so much space on the server
					require_once("../includes/php_image_magician.php");
					$magicianObj = new imageLib($target_file);
					$magicianObj -> resizeImage(500, 500, "auto", true);
					$magicianObj -> saveImage($target_file, 100);
				}
				else { $img_name = ""; }
				// Insert values to database
				$query = "INSERT INTO bike (crsid,make_model,frame_num,colour,gear,wheel,postcode,features,img,date_added) VALUES (:crsid,:make_model,:frame_num,:colour,:gear,:wheel,:postcode,:features,:img,:date_added)";
				$stmt = $db->prepare($query);
				$stmt->execute(array("crsid"=>$crsid,"make_model"=>$_POST['make'],"frame_num"=>$_POST['frame'],"colour"=>$_POST['colour'],"gear"=>$_POST['gear'],"wheel"=>$_POST['wheel'],"postcode"=>$_POST['postcode'],"features"=>$_POST['features'],"img"=>$img_name,"date_added"=>$datetime));
				// Fetch id (unique bike number) for email notification and to populate keywords for search
				$query = "SELECT bike.id, name, make_model, frame_num FROM bike INNER JOIN user_details ON bike.crsid = user_details.crsid WHERE date_added = :date_added LIMIT 1";
   				$stmt = $db->prepare($query);
   				$stmt->execute(array("date_added" => $datetime));
   				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
   					// Email notification for user
					$message = $reg_e_msg."<br><br>".$reg_e_msg2;
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
					mail($logged_usr_email,$reg_e_subj,$message,$headers);
					// Email staff for approval
					if (empty($appset_emailstaff)) { $to = $default_to_email; } else { $to = $appset_emailstaff; }
					$message = $reg_e_staff_msg."<br><br><b>".$details_name.":</b> ".$row['name']."<br>";
					$message .= "<b>".$make_model.":</b> ".$row['make_model']."<br><b>".$frame_num.":</b> ".$row['frame_num']."<br>";
					$message .= "<br>Click <a href='http://".$_SERVER['HTTP_HOST'].$dir_path."bike/approve.php'>here</a> to approve.";
					mail($to,$reg_e_staff_subj,$message,$headers);
					// Populate keywords field in bike table
   					$query2 = "UPDATE bike SET keywords = :keywords WHERE id = :id";
   					$stmt2 = $db->prepare($query2);
   					$stmt2->execute(array(":keywords" => $crsid." ".$row['name']." ".$row['make_model']." ".$row['frame_num'],"id" => $row['id']));
   				}
				?>
				<!-- Show modal after submission -->
				<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-body">
				                <p><b><?php echo $reg_summary ?></b></p>
				                <p><?php echo $reg_summary2 ?></p>
				            </div>
				            <div class="modal-footer">
				                <button id="reload" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				            </div>
				        </div>
				    </div>
				</div>
				<script type="text/javascript">
				$(document).ready(function () {
   					$('#confirmModal').modal('show');
   					// reload page
   					$('.modal-footer').on('click','#reload',function(){
  						window.location = window.location.pathname;
					})
				});
				</script>
			<?php
			}
			else {
				if (empty($_GET['new'])) { 
					$query = "SELECT * FROM bike WHERE crsid = :crsid";
    				$stmt = $db->prepare($query);
    				$stmt->execute(array("crsid" => $crsid));
    				if ($stmt->rowCount() > 0) {
    					$regCount = 1;
    					?>
    					<div class="container" style="max-width: 720px;">
    						<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
    								<table id="apprTbl" class="table table-hover">
                						<thead><tr><th><?php echo $permit ?></th><th><?php echo $make_model ?></th><th class="tblHiRes"><?php echo $frame_num ?></th><th>Image</th><th>Status</th></tr></thead>
                						<tbody>
                							<?php
                							while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                								echo "<tr>";
                								if (!empty($row['b_num'])) { echo "<td>".$row['b_num']."</td>"; } else { echo "<td>--</td>"; }
                								echo "<td>".$row['make_model']."</td><td class='tblHiRes'>".$row['frame_num']."</td>";
                								if (!empty($row['img'])) {
                									echo "<td><a class='imgModal' href='#' data-toggle='modal' data-target='#imgModal' data-id='".$row['img']."'><img style='max-width: 80px; max-height: 50px;' src='../".$upload_path.$row['img']."'></a></td>";
                								}
                								else { echo "<td>&nbsp;</td>"; }
                								if (empty($row['b_num'])) { echo "<td><span style='color: red;'>Pending</span></td>"; } else { echo "<td><span style='color: green;'>Approved</span></td>"; }
                								echo "</tr>";
                							}
                							?>
                						</tbody>
                					</table>
                					<div class="text-center"><a href="./?new=1" class="btn btn-primary">Submit New Registration</a></div>
                				</div>
                			</div>
                		</div>
                		<!-- Image Modal -->
                		<script type="text/javascript">
                		$(document).on("click", ".imgModal", function () {
							var img_id = $(this).data('id');
							var upload_path = "<?php echo $upload_path ?>";
							$(".modal-body").html("<div class='text-center'><img src='../"+upload_path+img_id+"' style='max-width: 100%; max-height: 500px;'></div>");
						}); 
                		</script>
        				<div name="imgModal" id="imgModal" class="modal fade" role="dialog">
        				    <div class="modal-dialog">
        				    <div class="modal-content">
        				        <div class="modal-body"><!-- Where the image will be inserted --></div>
        				        <div class="modal-footer">
        				            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        				        </div>    
        				    </div>
        				    </div>
        				</div>
    				<?php
    				}
    			}
    			if (empty($regCount) || !empty($_GET['new'])) {
				?>
					<form name="myForm" data-toggle="validator" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php $_PHP_SELF; ?>" role="form">
					<div class="form-group form-group-lg">
						<label class="col-sm-3 col-md-4 control-label"><?php echo $make_model ?><span style="color: red;"> * </span>:</label>
						<div class="col-sm-6 col-md-4">
						<input type="text" name="make" class="form-control" placeholder="<?php echo $reg_make_model_ph ?>" maxlength="50" required>
						</div>
					</div>
					<div class="form-group form-group-lg">
						<label class="col-sm-3 col-md-4 control-label"><?php echo $frame_num ?><span style="color: red;"> * </span>:<a href="#" tabindex="0" data-toggle="popover" title="<?php echo $reg_frame_pop_tit ?>" data-trigger="focus" data-content="<?php echo $reg_frame_pop_con ?>"><span class="glyphicon glyphicon-question-sign"></span></a></label>
						<div class="col-sm-6 col-md-4">
						<input type="text" name="frame" class="form-control" placeholder="<?php echo $reg_frame_ph ?>" maxlength="50" required>
						</div>
					</div>
					<div class="form-group form-group-lg">
						<label class="col-sm-3 col-md-4 control-label"><?php echo $reg_frame_colour ?><span style="color: red;"> * </span>:</label>
						<div class="col-sm-6 col-md-4">
						<input type="text" name="colour" class="form-control" placeholder="<?php echo $reg_frame_colour_ph ?>" maxlength="50" required>
						</div>
					</div>
					<div class="form-group form-group-lg form-inline">
						<label class="col-sm-3 col-md-4 control-label"></label>
						<div class="col-sm-6 col-md-4">
						<?php
						$gear_array = explode(',', $reg_gear_sel);
						echo '<select name="gear" class="form-control">';
						echo '<option value="" selected>Gear...</option>';
						foreach ($gear_array as $gear) {
							echo '<option value="'.htmlspecialchars($gear).'">'. htmlspecialchars($gear) . "</option>\n";
						}
						echo '</select>&nbsp;&nbsp;';					
						$wheel_array = explode(',', $reg_wheel_sel);
						echo '<select name="wheel" class="form-control">';
						echo '<option value="0" selected>Wheel...</option>';
						foreach ($wheel_array as $wheel) {
							echo '<option value="'.htmlspecialchars($wheel).'">'. htmlspecialchars($wheel) . "</option>\n";
						}
						echo '</select>';
						?>
						
						</div>
					</div>
					<div class="form-group form-group-lg">
						<label class="col-sm-3 col-md-4 control-label"><?php echo $reg_postcode ?>:<a href="#" tabindex="0" data-toggle="popover" title="<?php echo $postcode_stamp ?>" data-trigger="focus" data-content="<?php echo $reg_postcode_pop_con ?>"><span class="glyphicon glyphicon-question-sign"></span></a></label>
						<div class="col-sm-6 col-md-4">
						<input type="text" name="postcode" class="form-control" placeholder="Postcode stamp" maxlength="10">
						</div>
					</div>
					<div class="form-group form-group-lg">
						<label class="col-sm-3 col-md-4 control-label"><?php echo $sp_features ?>:<a href="#" tabindex="0" data-toggle="popover" title="<?php echo $sp_features ?>" data-trigger="focus" data-content="<?php echo $reg_features_pop_con ?>"><span class="glyphicon glyphicon-question-sign"></span></a></label>
						<div class="col-sm-6 col-md-4">
						<textarea name="features" class="form-control" rows="2" style="height: auto;" maxlength="250"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-md-4 control-label"></label>
    					<div class="col-sm-6 col-md-4">
    						<div class="imgupload panel panel-default">
        						<div class="panel-heading clearfix">
        							<h3 class="panel-title pull-left">Upload image</h3>
								</div>
								<div class="file-tab panel-body">
									<div>
										<button type="button" class="btn btn-default btn-file">
											<span>Browse</span>
											<?php
											if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false)) {
												echo '<input type="file" id="fileToUpload" name="fileToUpload" accept="image/*">';
											} else { echo '<input type="file" id="fileToUpload" name="fileToUpload">'; }
											?>
										</button>
										<button type="button" class="btn btn-default">Remove</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- File upload script -->
					<script src="../js/bootstrap-imageupload.js"></script>
					<script type="text/javascript">
						$('.imgupload').imageupload({
						 	allowedFormats: [ "jpg", "jpeg", "png", "gif" ],
						 	previewWidth: 250,
						 	previewHeight: 250,
						 	maxFileSizeKb: 8192
						});
					</script>
					<div class="form-group form-group-lg">
						<label class="col-sm-3 col-md-4 control-label">&nbsp;</label>
						<div class="col-sm-6 col-md-4">
						<button type="submit" name="add" id="add" class="btn btn-primary active"><?php echo $reg_submit ?></button>
						</div>
					</div>
					</form>
				<?php
				}
			}
			?>
		</div>
	</div>
	<br /><br />   
</div>
</body>
</html>