<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
 
if($_POST) {
    // Insert values to table
    $query = "INSERT INTO incident_comments (crsid,com_text,page_id,date_ins,hour_ins) VALUES ( :crsid, :com_text, :page_id, :date_ins, :hour_ins)";
    $stmt = $db->prepare($query);
    $stmt->execute(array("crsid" => $_POST['crsid'],"com_text" => strip_tags($_POST['com_text']),"page_id" => $_POST['page_id'],"date_ins" => date("Y-m-d"),"hour_ins" => date("H:i:s")));   
    // Retrieve email address from database
    $query = "SELECT * FROM incident WHERE id= :page_id";
    $stmt = $db->prepare($query);
    $stmt->execute(array("page_id" => $_POST['page_id']));
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        // Fetch email of reporter
        $query2 ="SELECT eadd FROM user_details WHERE crsid = :crsid LIMIT 1";
        $stmt2 = $db->prepare($query2);
        $stmt2->execute(array("crsid" => $row['rep_crsid']));
        while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
            $rep_email = $row2['eadd'];
        }
        // Fetch email of hoD's
        if (!empty($row['hod_crsid'])) {
            if (strpos($row['hod_crsid'], ",") !== false) {
                // Split the crsid of HoDs to an array
                $hod_crsid_arr = preg_split("/[\s,]+/", $row['hod_crsid']);
                foreach ($hod_crsid_arr as $hod_crsid) {
                    $query2 ="SELECT eadd FROM user_details WHERE crsid = :crsid LIMIT 1";
                    $stmt2 = $db->prepare($query2);
                    $stmt2->execute(array("crsid" => $hod_crsid));
                    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                        $hod_emails_arr[] = $row2['eadd'];
                    }
                }
                $hod_emails = implode(",",$hod_emails_arr);
            }
            else {
                $query2 ="SELECT eadd FROM user_details WHERE crsid = :crsid LIMIT 1";
                $stmt2 = $db->prepare($query2);
                $stmt2->execute(array("crsid" => $row['hod_crsid']));
                while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                    $hod_emails = $row2['eadd'];
                }
            }
        }
        // Notify by email for new comment post
        $query2 ="SELECT * FROM user_details WHERE crsid = :crsid LIMIT 1";
        $stmt2 = $db->prepare($query2);
        $stmt2->execute(array("crsid" => $_POST['crsid']));
        while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
            if (!empty($hod_emails)) { $to = $hod_emails.",".$rep_email; } else { $to = $rep_email; }
            $subj = $post_not_subj." - ".$row['subject'];
            $message = "<b>".$row2['name']."</b> ".$post_not_msg." -- ".$row['subject']."<br><br><i>".substr($_POST['com_text'],0,50)."...<a href='http://".$_SERVER['HTTP_HOST'].$dir_path."incident/report.php?id=".$_POST['page_id']."'>read more</a></i>";
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
            if (empty($appset_emailstaff)) { $headers .= "Cc:" . $default_to_email . "\r\n"; } else { $headers .= "Cc:" . $appset_emailstaff . "\r\n"; }
            mail($to,$subj,$message,$headers);
            // return the new comment
            echo "<div class='well well-sm'>";
            echo "<b>".$row2['name']."</b> <small class='muted'>posted just now</small>";
            echo "<br />";
            echo nl2br($_POST['com_text']);
            echo '</div>';
        }
    }
}
?>