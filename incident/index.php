<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Local css to this app -->
<link rel="stylesheet" href="local.css">
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
		<?php
     	// Show the sidebar items
     	include("../includes/sidebar-menu-items.php");
     	?>
    </ul>
</div>	
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
       <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
				<span class="sr-only">Toggle navigation</span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
       </div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
    	    <?php
			// Control the view of menu items for the logged person
			// Show the menu items
			include("../includes/navbar-menu-items.php");			
			?>
			</ul>
			<!-- Show user profile and logout option -->
			<ul class="nav navbar-nav navbar-right">
				<?php           
    	        // Show the right menu items
    	        include("../includes/navbar-right-menu-items.php");           
    	        ?>
			</ul>
		</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<?php
			// Upload form values to database
			//==================================================
			if(isset($_POST['add'])) {
               	if(isset($_FILES["fileToUpload"]) && $_FILES['fileToUpload']['tmp_name'][0] != '') {
               		$attachment = array();
               		$upload_err = 0;
               		$target_dir = "../".$upload_path;
					$fileCount = count($_FILES["fileToUpload"]["name"]);
	    			for($i=0; $i < $fileCount; $i++) {
	  					// rename attachment file, removing special characters in the file name
	  					$img_name = "incident-".time()."-".preg_replace("/[^\w\-\.]/", '', basename($_FILES["fileToUpload"]["name"][$i]));
 						$target_file = $target_dir . $img_name;
 						if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i],$target_file)) { $attachment[] = $img_name; } 
 							else { $upload_err = 1; }	  					
	  				} $attachment = implode(",", $attachment);
               	} else { $attachment = ""; }
               	$datetime = date("Y-m-d H:i:s");
               	$query = "INSERT INTO incident (cat_id,rep_crsid,subject,per_crsid,place,stud_yr,contact,details,attachment,created) VALUES (:cat_id,:rep_crsid,:subject,:per_crsid,:place,:stud_yr,:contact,:details,:attachment,:created)";
				$stmt = $db->prepare($query);
				$stmt->execute(array("cat_id"=>$_POST['cat_id'],"rep_crsid"=>$crsid,"subject"=>$_POST['subject'],"per_crsid"=>$_POST['crsid'],"place"=>$_POST['place'],"stud_yr"=>$_POST['stud_yr'],"contact"=>$_POST['contact'],"details"=>$_POST['details'],"attachment"=>$attachment,"created"=>$datetime));
   				// Get id for email notification
				$query = "SELECT incident.id, subject, user_details.name FROM incident ";
				$query .= "INNER JOIN user_details ON incident.rep_crsid = user_details.crsid ";
				$query .= "WHERE created = :created limit 1";
    			$stmt = $db->prepare($query);
    			$stmt->execute(array("created" => $datetime));
    			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    				// Email notification
    				$subj = $incident_subj.": ".$row['subject'];
    				$message = $incident_no." <b>".$row['id']."</b><br><br><b>".$row['name']."</b> ".$incident_msg." - <i>".$row['subject']."</i><br><br>Click <a href='http://".$_SERVER['HTTP_HOST'].$dir_path."incident/report.php?id=".$row['id']."'>here</a> to view and comment.";
    				$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
					if (empty($appset_emailstaff)) { $to = $default_to_email; } else { $to = $appset_emailstaff; }
					mail($to,$subj,$message,$headers);
    			}
				?>
				<!-- Show modal after submission -->
				<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-body">
				                <p><b>Report submitted successfully!</b></p>
				                <p><?php echo $incident_summary ?></p>
				                <?php if ($upload_err == 1 && !empty($attachment)) { echo "<p><span style='color=red'>There was an error uploading the file/s.</span></p>"; } ?>
				            </div>
				            <div class="modal-footer">
				                <button id="reload" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				            </div>
				        </div>
				    </div>
				</div>
				<script type="text/javascript">
				$(document).ready(function () {
   					$('#confirmModal').modal('show');
   					// reload page
   					$('.modal-footer').on('click','#reload',function(){
  						window.location = window.location.pathname;
					})
				});
				</script>
			<?php	
			}
			else {
				// Show the form if user is a member of staff
				if ($incident_manager == "true" || $incident_admin == "true") {
				?>
					<form name="myForm" data-toggle="validator" class="form-horizontal" method="post" action="<?php $_PHP_SELF; ?>" enctype="multipart/form-data" role="form">					
					<div class="form-group">
						<label class="col-xs-12 col-sm-2 col-md-offset-1 col-md-2 control-label"><?php echo $category ?><span style="color: red;"> * </span>:</label>
						<div class=" col-xs-12 col-sm-7 col-md-4">
						<?php
						echo '<select name="cat_id" class="form-control" required>';
						echo '<option value="" selected>Choose one...</option>';
						$query = "SELECT id, category FROM incident_categories WHERE deleted = 0 ORDER BY category";
    					$stmt = $db->prepare($query);
    					$stmt->execute();
    					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    						echo '<option value="'.htmlspecialchars($row['id']).'">'. htmlspecialchars($row['category']) . "</option>\n";
    					}
						echo '</select>';
						?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-12 col-sm-2 col-md-offset-1 col-md-2 control-label"><?php echo $subject ?><span style="color: red;"> * </span>:</label>
						<div class=" col-xs-12 col-sm-9 col-md-6">
						<input type="text" name="subject" class="form-control" placeholder="<?php echo $subject ?>" maxlength="100" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-12 col-sm-2 col-md-offset-1 col-md-2 control-label"><?php echo $form_details ?><span style="color: red;"> * </span>:</label>
						<div class=" col-xs-12 col-sm-9 col-md-6">
						<textarea name="details" class="form-control" rows="8" style="height: auto;" placeholder="<?php echo $form_pl_details ?>" required></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-12 col-sm-2 col-md-offset-1 col-md-2 control-label">
							<?php echo $form_info ?>:<a href="#" tabindex="0" data-toggle="popover" title="Information" data-trigger="focus" data-content="Use this section to include person involved, place or contact details"><span class="glyphicon glyphicon-question-sign"></span></a>
						</label>
						<div class="col-xs-12 col-sm-4 col-md-3">
						<input type="text" name="name" id="name" class="form-control" placeholder="<?php echo $form_pl_search ?>">
						<input type="hidden" name="crsid" id="crsid">
						</div>
					</div>
					<div class="form-group">
						<label class="hidden-xs col-xs-12 col-sm-2 col-md-offset-1 col-md-2 control-label">&nbsp;</label>
						<div id="infoRm" class="col-xs-12 col-sm-3 col-md-2">
							<input type="text" name="place" class="form-control" placeholder="<?php echo $form_pl_place ?>" maxlength="45">						
						</div>
						<div id="infoYr" class="col-xs-12 col-sm-3 col-md-2">
							<?php
							$sel_array = explode(',', $form_sel_yr);
							echo '<select name="stud_yr" class="form-control" placeholder="">';
							echo '<option value="" selected>Year (if student)</option>';
							foreach ($sel_array as $sel)
								echo '<option value="'.htmlspecialchars($sel).'">'. htmlspecialchars($sel) . "</option>\n";
								echo '</select>';
							?>
						</div>
						<div id="infoContact" class="col-xs-12 col-sm-3 col-md-2">
							<input type="text" name="contact" class="form-control" placeholder="<?php echo $form_pl_contact ?>" maxlength="45">					
						</div>
					</div>
					<div class="form-group">
						<label class="hidden-xs col-sm-2 col-md-offset-1 col-md-2 control-label">&nbsp;</label>
						<div class="col-xs-12 col-sm-10 col-md-9 controls">
							<div class="entry input-group col-sm-7 col-md-5">
								<input type="file" class="btn btn-default" name="fileToUpload[]" onchange="validate();">
								<span class="input-group-btn">
                					<button class="btn btn-default btn-add" type="button"><span class="glyphicon glyphicon-plus"></span></button>
                				</span>
                			</div>
						</div>
					</div>
					<div class="form-group">
						<label class="hidden-xs col-sm-2 col-md-offset-1 col-md-2 control-label">&nbsp;</label>
						<div class="col-xs-12 col-sm-10 col-md-9">
						<button type="submit" name="add" id="add" class="btn btn-primary active"><?php echo $submit ?></button>
						</div>
					</div>
					</form>
					<!-- Load js scripts -->
                    <script type="text/javascript" src="assets/admin.js"></script>
					<!--<script type="text/javascript">
						// Maybe try this in the future -- http://hayageek.com/docs/jquery-upload-file.php#customui
						// Dynamically add input fields for multiple file upload
						$(function() {
						    $(document).on('click', '.btn-add', function(e) {
						        e.preventDefault();						
						        var controlForm = $('.controls:first'),
						            currentEntry = $(this).parents('.entry:first'),
						            newEntry = $(currentEntry.clone()).appendTo(controlForm);						
						        newEntry.find('input').val('');
						        controlForm.find('.entry:not(:last) .btn-add')
						            .removeClass('btn-add').addClass('btn-remove')
						            .removeClass('btn-default').addClass('btn-danger')
						            .html('<span class="glyphicon glyphicon-minus"></span>');
						    }).on('click', '.btn-remove', function(e) {
						        $(this).parents('.entry:first').remove();						
								e.preventDefault();
								return false;
							});
						});
						// Validate size of files
						function validate() {
							$('input[name="fileToUpload[]"]').each(function() {
								if(this.files[0].size > 5242880){
							       alert("File is too big!");
							       this.value = "";
							    };
							});
						}
					</script>-->
				<?php
				}
				else {
					// If user is not authorised, this message will appear
					echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                   	echo "<p><a href='../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
				}
			}
			?>
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>