<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../includes/access.php");
// Get values for displaying texts on the page
require_once("../includes/texts.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../includes/headers.php");
?>
<!-- Local css to this app -->
<link rel="stylesheet" href="local.css">
<!-- For the comments -->
<script type="text/javascript" src="comments.js"></script>
<script type="text/javascript">
$(function() {
    // This makes sure the search results display on top of the modal
    $("#name").autocomplete( "option", "appendTo", ".modal" );
});
</script>
</head>
<body>
<!-- Primary Page Layout
================================================== -->
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
   	<a class="navmenu-brand" href="#"><?php echo $banner ?></a>
  	<ul class="nav navmenu-nav">
		<?php
  		// Show the sidebar items
  		include("../includes/sidebar-menu-items.php");
  		?>
  	</ul>
</div>	
<!-- This is the main navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
		<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand navbar-center" href="./"><?php echo $banner ?></a>
    </div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
        <?php
		// Control the view of menu items for the logged person		
		// Show the menu items
		include("../includes/navbar-menu-items.php");		
		?>
		</ul>
		<!-- Show user profile and logout option -->
		<ul class="nav navbar-nav navbar-right">
			<?php           
            // Show the right menu items
            include("../includes/navbar-right-menu-items.php");           
            ?>
		</ul>
	</div>
	</div>
</nav>
<div  class="container">	
	<div class="row">
		<div class="col-xs-12">
			<?php
			// Get query strings
            parse_str($_SERVER['QUERY_STRING']);
            // If user is added to access the report
			if(isset($_POST['add'])) {
				$usr_crsid = $_POST['crsid'];
				// Check for existing values in hod_crsid
                $query = "SELECT hod_crsid FROM incident WHERE id= :id LIMIT 1";
                $stmt = $db->prepare($query);
                $stmt->execute(array("id" => $id));
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    if (!empty($row['hod_crsid'])) {
                        if (strpos($row['hod_crsid'], $usr_crsid) !== false) { // Check if user already exist
                            echo "<div class='col-xs-12 col-sm-12 col-md-12' style='color:#940000;'>User already added!</div>";
                        } else {
                            // Insert values to database
                            $query2 = "UPDATE incident SET hod_crsid = CONCAT(hod_crsid,'".",".$usr_crsid."') WHERE id= :id";
                            $stmt2 = $db->prepare($query2);
                            $stmt2->execute(array("id" => $id));
                        }
                    }
                    else {
                        // Insert values to database
                        $query2 = "UPDATE incident SET hod_crsid = :hod_crsid WHERE id= :id";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("hod_crsid" => $usr_crsid,"id" => $id));
                    }
                }
           		// Fetch report subject
           		$query = "SELECT incident.subject, user_details.eadd FROM incident INNER JOIN user_details ON :hod_crsid = user_details.crsid WHERE incident.id= :id LIMIT 1";
           		$stmt = $db->prepare($query);
                $stmt->execute(array("hod_crsid" => $usr_crsid,"id" => $id));
            	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    // Email added user
                    $subj = $incident_subj.": ".$row['subject'];
                    $message = $add_not_msg.":<br><br>Subject: <i>".$row['subject']."</i><br><br>".$rep_incident_msg2."<br><a href='http://".$_SERVER['HTTP_HOST'].$dir_path."incident/report.php?id=".$id."'>View report</a>";
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    if (empty($appset_emailfrom)) { $headers .= "From:" . $default_fr_email . "\r\n"; } else { $headers .= "From:" . $appset_emailfrom . "\r\n"; }
                    mail($row['eadd'],$subj,$message,$headers);
            	}
			}
            // If comment has been submitted
            if(isset($_POST['submit'])) {
                if (!empty($_POST['hp_comment'])) {
                    $query = "UPDATE incident SET hp_comment = :hp_comment WHERE id= :id";
                    $stmt = $db->prepare($query);
                    $stmt->execute(array("hp_comment" => $_POST['hp_comment'],"id" => $id));
                    echo "<div class='col-xs-12 col-sm-12 col-md-12' style='color:#940000;'>Comment submitted!</div>";
                }
            }
            // If close report is submitted
            if(isset($_GET['close'])) {
                $query = "UPDATE incident SET status = 0 WHERE id= :id";
                $stmt = $db->prepare($query);
                $stmt->execute(array("id" => $id));
            }
            // If edit report is submitted
            if(isset($_POST['editSub'])) {
                // Upload new attachment if there's any
                $upload_err = 0;
                if(isset($_FILES["fileToUpload"]) && $_FILES['fileToUpload']['tmp_name'][0] != '') {
                    $attachment = array();                    
                    $target_dir = "../".$upload_path;
                    $fileCount = count($_FILES["fileToUpload"]["name"]);
                    for($i=0; $i < $fileCount; $i++) {
                        // rename attachment file, removing special characters in the file name
                        $img_name = "incident-".time()."-".preg_replace("/[^\w\-\.]/", '', basename($_FILES["fileToUpload"]["name"][$i]));
                        $target_file = $target_dir . $img_name;
                        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i],$target_file)) { $attachment[] = $img_name; } 
                            else { $upload_err = 1; }                       
                    } $attachment = implode(",", $attachment);
                } else { $attachment = ""; }
                // Get existing attachment name and append new one if applicable
                $query = "SELECT attachment FROM incident WHERE id=:id";
                $stmt = $db->prepare($query);
                $stmt->execute(array("id"=>$id));
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { $att_arr = $row['attachment']; }
                if (empty($att_arr)) {
                    if (!empty($attachment)) { $att_arr = $attachment; }
                } else { if (!empty($attachment)) {$att_arr .= ",".$attachment;} }
                // Remove attachment file name from array and the actual file/s
                if (!empty($_POST['delFile'])) {
                    $att_arr = array_diff(explode(",", $att_arr),$_POST['delFile']);
                    $att_arr = implode(",", $att_arr);
                    foreach ($_POST['delFile'] as $df) { unlink("../".$upload_path.$df); }
                }
                // Update the table
                $query = "UPDATE incident SET cat_id=:cat_id, subject=:subject, details=:details, attachment=:attachment, date_modified=:date_modified WHERE id=:id";
                $stmt = $db->prepare($query);
                $stmt->execute(array("cat_id"=>$_POST['editCat'],"subject"=>$_POST['editSubj'],"details"=>$_POST['editDetails'],"attachment"=>$att_arr,"date_modified"=>date('Y-m-d H:i:s'),"id" => $id));
                echo "<div class='col-xs-12 col-sm-12 col-md-12' style='color:#940000;'>";
                if ($upload_err == 1) { echo "There was an error with the file upload!"; } else { echo "Report updated!"; }
                echo "</div>";
            }
            // Show report
            // Query incident table to display to page
            $query = "SELECT * FROM incident INNER JOIN incident_categories ON incident.cat_id = incident_categories.id WHERE incident.id=:id LIMIT 1";
            $stmt = $db->prepare($query);
			$stmt->execute(array("id" => $id));
            if ($stmt->rowCount() < 1) { die("<div class='col-xs-12 col-sm-12 col-md-12'>This report is not on the database!<br><a href='../'>Dashboard</a></div>"); }
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {                          	
            	if (strpos($row['hod_crsid'], $crsid) !== false || $incident_manager == "true" || $incident_admin == "true") {
            		?>
            		<div class="reportItems">
            			<div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-12 col-sm-2 col-md-2 repTitle"><?php echo $form_incident ?></div>
            				<div class="col-xs-12 col-sm-10 col-md-10 repValue">
                                <?php 
                                echo $rep_no.": <span class='text-muted'>".$id."</span> ".$category.": <span class='text-muted'>".$row['category']."</span>";
                                if ($incident_admin == "true" && $row['status'] == 1) {
                                    echo "&nbsp;&nbsp;[<a href='#' data-toggle='modal' data-target='#editModal'>edit</a>]";
                                } 
                                ?>
                            </div>
            			</div>
            			<div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-12 col-sm-2 col-md-2 repTitle"><?php echo $subject ?></div>
            				<div class="col-xs-12 col-sm-10 col-md-10 repValue"><?php echo $row['subject'] ?></div>
            			</div>
            			<div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-12 col-sm-2 col-md-2 repTitle"><?php echo $rep_by ?></div>
            				<div class="col-xs-12 col-sm-10 col-md-10 repValue">
                                <?php                                  
                                $query2 ="SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
                                $stmt2 = $db->prepare($query2);
                                $stmt2->execute(array("crsid" => $row['rep_crsid']));
                                while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                    echo $row2['name']." <span style='font-size: 12px;' class='text-muted'> on ". date('d-m-Y H:i:s', strtotime($row['created']))."</span>";
                                }
                                ?>
                            </div>
            			</div>
            			<?php
                        if (!empty($row['hod_crsid']) || $incident_admin == "true") {
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-12 col-sm-2 col-md-2 repTitle"><?php echo $rep_subto ?></div>
            				<div class="col-xs-12 col-sm-10 col-md-10 repValue">
            					<?php 
            					if (!empty($row['hod_crsid'])) { // List all participants
                                    $hod_crsid_arr = explode(",", $row['hod_crsid']);
                                    foreach ($hod_crsid_arr as $hod_crsid) {
                                        $query2 ="SELECT name FROM user_details WHERE crsid = :crsid LIMIT 1";
                                        $stmt2 = $db->prepare($query2);
                                        $stmt2->execute(array("crsid" => $hod_crsid));
                                        while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                            $hodname = $row2['name'];
                                            // Show delete option
                                            if ($incident_admin == "true") { 
                                                $hodname .= " <a class='delhod' href='#' data-id='".$id."' data-crsid='".$hod_crsid."'><i class='far fa-times-circle'></i></a>"; 
                                            } 
                                            $hod_names_arr[] = $hodname;  
                                        }
                                    }
                                    $hod_names = implode(", ",$hod_names_arr);
                                    // Display names
                                    if (!empty($hod_names)) { echo $hod_names." "; }
                                }
            					// Show add user link
                                if ($incident_admin == "true" && $row['status'] == 1) {
            						echo "[<a href='#' data-toggle='modal' data-target='#addModal'>add</a>]";
            					} else { echo "&nbsp;"; }
            					?>
            				</div>
            			</div>
            			<?php
                        }
                        if (!empty($row['per_crsid']) || !empty($row['place']) || !empty($row['stud_yr']) || !empty($row['contact'])) {
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-12 col-sm-2 col-md-2 repTitle"><?php echo $rep_info ?></div>               					
            				<div class="col-xs-12 col-sm-10 col-md-10 repValue">
            					<?php
            					if (!empty($row['per_crsid'])) {
            						$query2 ="SELECT * FROM user_details WHERE crsid = :crsid LIMIT 1";
                                    $stmt2 = $db->prepare($query2);
                                    $stmt2->execute(array("crsid" => $row['per_crsid']));
                                    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                        echo $rep_name.": <span class='text-muted'>".$row2['name']."</span>";
                                        echo " ".$rep_crsid.": <span class='text-muted'>".$row['per_crsid']."</span>";
                                        echo " ".$rep_email.": <span class='text-muted'>".$row2['eadd']."</span> ";
                                    }
            					}
                                if (!empty($row['place'])) { echo $rep_place.": <span class='text-muted'>".$row['place']."</span> "; }
            					if (!empty($row['stud_yr'])) { echo $year.": <span class='text-muted'>".$row['stud_yr']."</span> "; }
                                if (!empty($row['contact'])) { echo $contact.": <span class='text-muted'>".$row['contact']; }
            					?>
            				</div>
            			</div>
                        <?php } ?>
            			<div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-12 col-sm-12 col-md-2 repTitle"><?php echo $rep_details ?></div>
            				<div class="col-xs-12 col-sm-12 col-md-10 repValue"><?php echo $row['details'] ?></div>
            			</div>
						<?php
                        if (!empty($row['hp_comment']) || $incident_admin) {
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 repRow">
            				<div class="col-xs-12 col-sm-12 col-md-2 repTitle"><?php echo $rep_hp_comment ?></div>
            				<div class="col-xs-12 col-sm-12 col-md-10 repValue">
                                <?php 
                                if (!empty($row['hp_comment'])) {
                                    echo $row['hp_comment'];
                                }
                                elseif ($incident_admin == "true" && $row['status'] == 1) {
                                    echo "[<a href='#' data-toggle='modal' data-target='#commentModal'>comment</a>]";
                                } else { echo "&nbsp;"; }
                                ?>
                            </div>
            			</div>
                        <?php
                        }
                        // Show attachment if any
                        if (!empty($row['attachment'])) { ?>
                            <div class="col-xs-12 col-sm-12 col-md-12 repRow">
                                <div class="col-xs-12 col-sm-2 col-md-2 repTitle"><?php echo $header_attach ?></div>
                                <div class="col-xs-12 col-sm-10 col-md-10 repValue">
                                    <?php
                                    $att_arr = explode(",", $row['attachment']);
                                    foreach ($att_arr as $att) {
                                        // Define iconbased on type
                                        $att_icon = "alt";
                                        if (preg_match('/.jpg|.jpeg|.gif|.png/i', $att)) { $att_icon = "image"; }
                                        if (preg_match('/.doc|.docx/i', $att)) { $att_icon = "word"; }
                                        if (preg_match('/.xls|.xlsx/i', $att)) { $att_icon = "excel"; }
                                        if (preg_match('/.pdf/i', $att)) { $att_icon = "pdf"; }
                                        echo "<a href='../".$upload_path.$att."' target='_blank'><i class='far fa-file-".$att_icon."'></i> ".substr($att,20)."</a>&nbsp;&nbsp;";
                                    }
                                    ?>      
                                </div>
                            </div>
                        <?php } ?>
            		</div>
                    <!-- Start of pagination -->
                    <?php
                    if ($incident_manager == "true" || $incident_admin == "true") { // If user is manager/admin
                        echo "<div id='page_nav' class='col-xs-12 col-sm-12 col-md-12'>";
                        $b_id = $id;
                        $query2 = "SELECT id FROM incident WHERE id < :b_id ORDER BY id DESC LIMIT 1";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("b_id" => $b_id));
                        if ($stmt2->rowCount() > 0) {
                            while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                echo "<a href='./report.php?id=".$row2['id']."'><i class='fa fa-backward' aria-hidden='true'></i></a>";
                            }
                        }
                        else {
                            echo "<i class='fa fa-backward' aria-hidden='true'></i>";
                        }
                        echo "<a href='admin/'><i class='fa fa-home' aria-hidden='true'></i></a>";
                        $f_id = $id;
                        $query2 = "SELECT id FROM incident WHERE id > :f_id ORDER BY id ASC LIMIT 1";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("f_id" => $f_id));
                        if ($stmt2->rowCount() > 0) {
                            while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                echo "<a href='./report.php?id=".$row2['id']."'><i class='fa fa-forward' aria-hidden='true'></i></a>";
                            }
                        }
                        else {
                            echo "<i class='fa fa-forward' aria-hidden='true'></i>";
                        }
                        echo "</div>";
                    }
                    elseif ($incident_part_rows > 0) { // If user is not manager/admin but included in a report
                        echo "<div id='page_nav' class='col-xs-12 col-sm-12 col-md-12'>";
                        $b_id = $id;
                        $query2 = "SELECT id FROM incident WHERE LOCATE('".$crsid."',hod_crsid) > 0 AND id < :b_id ORDER BY id DESC LIMIT 1";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("b_id" => $b_id));
                        if ($stmt2->rowCount() > 0) {
                            while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                echo "<a href='./report.php?id=".$row2['id']."'><i class='fa fa-backward' aria-hidden='true'></i></a>";
                            }
                        }
                        else {
                            echo "<i class='fa fa-backward' aria-hidden='true'></i>";
                        }
                        echo "<a href='view-rep.php'><i class='fa fa-home' aria-hidden='true'></i></a>";
                        $f_id = $id;
                        $query2 = "SELECT id FROM incident WHERE LOCATE('".$crsid."',hod_crsid) > 0 AND id > :f_id ORDER BY id ASC LIMIT 1";
                        $stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("f_id" => $f_id));
                        if ($stmt2->rowCount() > 0) {
                            while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                echo "<a href='./report.php?id=".$row2['id']."'><i class='fa fa-forward' aria-hidden='true'></i></a>";
                            }
                        }
                        else {
                            echo "<i class='fa fa-forward' aria-hidden='true'></i>";
                        }
                        echo "</div>";
                    }
                    ?>
                    <!-- End of pagination -->
            		<!-- Start add modal -->
            		<div id="addModal" class="modal fade" role="dialog">
  						<div class="modal-dialog modal-sm">
						<!-- Add modal content-->
							<div class="modal-content">
  								<div class="modal-body">
                                <h4>Add person</h4>
  								<form class="form-inline" method="post" action="<?php $_PHP_SELF; ?>" role="form">                
                   					<div class="input-group">
                   					    <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
                   					    <input type="text" class="form-control" name="name" id="name" placeholder="Search for people">
                   					    <input type="hidden" name="crsid" id="crsid" class="form-control" value="<?php echo $crsid ?>">    
                   					</div>                                  					
  								</div>
  								<div class="modal-footer">
  								 	<button type="submit" name="add" id="add" class="btn btn-default btn-sm">Add</button>
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  								</div>
  								</form>
							</div>
  						</div>
					</div>
					<!-- End add modal -->
                    <!-- Start HoD comment modal -->
                    <div id="commentModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                        <!-- Comment modal content-->
                            <div class="modal-content">
                                <div class="modal-body">
                                <h4>Enter comment</h4>
                                <form class="form-horizontal" method="post" action="<?php $_PHP_SELF; ?>" role="form">                
                                    <div class="form-group">
                                        <div class="col-xs-12"><textarea name="hp_comment" class="form-control" rows="4"></textarea></div>  
                                    </div>                                                      
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="submit" id="submit" class="btn btn-default btn-sm">Submit</button>
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End HoD comment modal -->
                    <!-- Start edit report modal -->
                    <div id="editModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                        <!-- Edit modal content-->
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php $_PHP_SELF; ?>" enctype="multipart/form-data" role="form">
                                <div class="modal-body">
                                    <h4>Edit report</h4>                                                    
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-9 col-md-9" style="margin-bottom: 5px;">
                                            <span class="editMdlTitle">Category:</span><br>
                                            <select class="form-control input-sm" name="editCat">
                                                <?php
                                                $query2 = "SELECT id, category FROM incident_categories";
                                                $stmt2 = $db->prepare($query2);
                                                $stmt2->execute();
                                                while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($row['category'] == $row2['category']) {
                                                        echo "<option value='".$row2['id']."' selected>".$row2['category']."</option>";
                                                    } else { echo "<option value='".$row2['id']."'>".$row2['category']."</option>"; }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-xs-12" style="margin-bottom: 5px;">
                                            <span class="editMdlTitle">Subject:</span><br>
                                            <input type="text" class="form-control input-sm" name="editSubj" value="<?php echo $row['subject'] ?>">
                                        </div>
                                        <div class="col-xs-12" style="margin-bottom: 5px;">
                                            <span class="editMdlTitle">Details:</span><br>
                                            <textarea name="editDetails" class="form-control input-sm" rows="4"><?php echo $row['details'] ?></textarea>
                                        </div>
                                        <!-- Attachments section -->
                                        <div id="attCol" class="col-xs-12 col-sm-7 col-md-7">
                                            <span class="editMdlTitle"><?php echo $header_attach ?>:</span>
                                            <?php
                                            if (!empty($row['attachment'])) {
                                                foreach ($att_arr as $att) {
                                                    echo "<div class='input-group' style='margin-bottom: 5px;'>";
                                                    echo "<span style='display:none;'><input type='checkbox' name='delFile[]' value='".$att."'></span>";
                                                    echo "<input type='text' class='form-control input-sm' value='".substr($att,20)."' readonly>";
                                                    echo "<span class='input-group-btn'><button class='btn btn-danger btn-sm btn-remove' type='button'><span class='glyphicon glyphicon-minus'></span></button></span>";
                                                    echo "</div>";
                                                }
                                            }
                                            ?>
                                            <div class="controls">
                                                <div class="entry input-group col-md-5">
                                                    <input type="file" class="btn btn-default input-sm" name="fileToUpload[]" onchange="validate();">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default btn-sm btn-add" type="button"><span class="glyphicon glyphicon-plus"></span></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                                      
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" name="editSub" id="editSub" class="btn btn-default btn-sm" onclick="getElementById('editSub').innerHTML='<i>Processing, please wait...</i>'">Submit</button>
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>                              
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End edit report modal -->					
					<div class="col-xs-12 col-sm-12 col-md-12 comments">
						<?php
						//Query table if there are comments
						$query2 = "SELECT name, date_ins, com_text FROM incident_comments INNER JOIN user_details ON incident_comments.crsid = user_details.crsid WHERE page_id= :id ORDER BY date_ins ASC, hour_ins ASC";
						$stmt2 = $db->prepare($query2);
                        $stmt2->execute(array("id" => $id));
						$nbrCom = $stmt2->rowCount();
						echo "<h4>Comments:</h4>";
						if ($nbrCom == 0) { // if 0 comment
							echo '<i>No comments at the moment!</i><br /><br />';
						}
						else {
							while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                echo '<b>'.$row2['name'].'</b> <small class="text-muted">Posted on '.date('d-m-Y', strtotime($row2['date_ins'])).'</small>';
                                echo '<br />';
                                echo nl2br($row2['com_text']);
                                echo '<hr></hr>';
							}
						}
						?>
						<!-- the comment posted will be displayed here -->
						<div id="newComment"></div>
  						<?php
                        if ($row['status'] == 1) { ?>
                            <!-- comment form -->
  						    <form data-toggle="validator" action="#" method="post" role="form">
                            <!-- hidden fields to pass the variables. The page_id corresponds to the id of the page -->
						  	<!-- This is useful if you add the comment system in a large number of pages so that only comments for that particular page shows up -->
						  	<input type="hidden" id="page_id" value="<?php echo $id ?>">
                            <!--Value of crsid is from hidden input "id=crsid" in Add Modal
                            <input type="hidden" id="crsid" value="<?php echo $crsid ?>">-->
						  	<div class="form-group">
						  		<label class="control-label">Post comment:</label>
						  		<div>
						  		<textarea name="com_text" id="com_text" class="form-control" rows="3" style="height: auto;" placeholder="Type your comment here...." required></textarea>
						  		</div>
						  	</div>
						  	<div class="form-group">
						  		<button type="submit" name="submitCom" id="submitCom" class="submitComment btn btn-default btn-sm">Send</button>
                                <?php
                                if ($incident_admin == "true") {
                                echo "&nbsp;<i class='fa fa-ellipsis-v'></i>&nbsp;&nbsp;<a type='button' class='btn btn-default btn-sm' href='../export/apps/incident.php?id=".$id."' target='_blank'>Print</a>";
                                echo "&nbsp;<a type='button' class='btn btn-warning btn-sm' href='./report.php?id=".$id."&close' onclick='return confirm(\"Are you sure you want to close this report?\")'>Close report</a>";
                                } ?>
                            </div>
						    </form> 
                        <?php
                        } else { 
                            echo "<button class='btn btn-danger btn-sm btn-static'>Closed</button>&nbsp;&nbsp;<i class='fa fa-ellipsis-v'></i>&nbsp;&nbsp;";
                            echo "<a type='button' class='btn btn-default btn-sm' href='../export/apps/incident.php?id=".$id."' target='_blank'>Print</a>";
                        }
                        ?>
					</div>
                    <!-- Load js scripts -->
                    <script type="text/javascript" src="assets/admin.js"></script>	
				<?php
            	}
            	else {
            		// If user is not authorised, this message will appear
					echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
               		echo "<p><a href='../' class='btn btn-primary active' role='button'>Dashboard</a></p>";
            	}
            }
			?>
		</div>
	</div>
	<br /><br />       
</div>
</body>
</html>