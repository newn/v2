// File comments.js
$(function() {
	$(".submitComment").click(function() {
		var crsid = $('#crsid').val();
		var com_text = $('#com_text').val(); // get text from comment text field
		var page_id = $('#page_id').val(); // get the page id from the hidden page_id field
		var dataFields = {'crsid': crsid, 'com_text': com_text, 'page_id': page_id}; // prepare datas string
		if(com_text=='') { // text not empty
			if (com_text=='') {
				$('#formGroupText').attr('class', 'form-group has-error'); // this is a Bootstrap CSS, I add has-error to the class so the field will turn red 
				$('#formGroupText span').text('This field can\'t be empty'); // I add a help text in the <span> node
			} else {
				$('#formGroupText').attr('class', 'form-group has-success'); // this is a Bootstrap CSS, I add has-success to the class so the field will turn green 
				$('#formGroupText span').text(''); // remove help text in the <span> node
			}
		} else { // if everything valid
			$('#newComment').html('<img src="loader.gif" /> Processing...'); // loader image apprears in the <div id="newComment"></div>
			$.ajax({
				type: "POST",
				url: "comments.php", // call the php file ajax/tuto_blogcommentajax.php to insert new datas in the database
				data: dataFields, // send dataFields var
				timeout: 3000,
				success: function(dataBack){ // if success
					$('#newComment').html(dataBack); // return new datas and insert in the <div id="newComment"></div>
					$('#com_text').val(''); // clear the com_text field // I don't clear the name and email field if the guy wants to post another comment
					},
				error: function() { // if error
					$('#newComment').text('Problem!'); // display "Problem!" in the <div id="newComment"></div>
				}
			});
		}
		return false;
	});
});