<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");

require_once("../../ajaxcrud/preheader.php"); // <-- this include file MUST go first before any HTML/output
#the code for the class
include ("../../ajaxcrud/ajaxCRUD.class.php"); // <-- this include file MUST go first before any HTML/output

?>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Basic Page Needs
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<title><?php echo $banner ?></title>
<meta name="description" content="">
<meta name="author" content="">
<?php 
// Get the headers
include("../../includes/headers.php");
?>
</head>
<?php

#this one line of code is how you implement the class
########################################################
##
$tbl_incident = new ajaxCRUD("Item", "incident", "id", "");
##
########################################################
// Detect if mobile device, hide some column
if(!isset($_GET['screen'])) {
/* This code will be executed if screen resolution has not been detected.*/
    echo "<script language='JavaScript'>
    <!-- 
    document.location=\"$PHP_SELF?screen=done&w=\"+screen.width;
    //-->
    </script>";
}
else {    
    if(($_GET['w']<=480)) {
        $tbl_incident->omitField("cat_id");
        $tbl_incident->omitField("rep_crsid");
        $tbl_incident->omitField("created");
    }
}
#i can define a relationship to another table
$tbl_incident->defineRelationship("rep_crsid", "user_details", "crsid", "name");
$tbl_incident->defineRelationship("cat_id","incident_categories","id","category","category",0,"WHERE deleted='0'");
#the table fields have prefixes; i want to give the heading titles something more meaningful
$tbl_incident->displayAs("id", $rep_no);
$tbl_incident->displayAs("cat_id", $category);
$tbl_incident->displayAs("rep_crsid", $rep_by);
$tbl_incident->displayAs("subject", $subject);
$tbl_incident->displayAs("status", "Status");
$tbl_incident->displayAs("details", $form_details);
$tbl_incident->displayAs("created", $header_date);
#i could omit a field if I wanted
$tbl_incident->omitField("per_crsid");
$tbl_incident->omitField("place");
$tbl_incident->omitField("stud_yr");
$tbl_incident->omitField("contact");
$tbl_incident->omitField("details");
$tbl_incident->omitField("hp_comment");
$tbl_incident->omitField("hod_crsid");
$tbl_incident->omitField("attachment");
$tbl_incident->omitField("date_modified");
#i could disallow editing for certain, individual fields
$tbl_incident->disallowEdit('id');
$tbl_incident->disallowEdit('cat_id');
$tbl_incident->disallowEdit('rep_crsid');
$tbl_incident->disallowEdit('subject');
$tbl_incident->disallowEdit('status');
$tbl_incident->disallowEdit('created');
#i can order my table by whatever i want
$tbl_incident->addOrderBy("ORDER BY status DESC, created DESC");
#i can disallow adding rows to the table
$tbl_incident->disallowAdd();
#set the number of rows to display (per page)
$tbl_incident->setLimit($appset_actrowlimit);
#set a filter box at the top of the table
$tbl_incident->addAjaxFilterBox('subject');
$tbl_incident->addAjaxFilterBox('details');
$tbl_incident->addAjaxFilterBox('cat_id');
#i can format the data in cells however I want with formatFieldWithFunction
#this is arguably one of the most important (visual) functions
$tbl_incident->formatFieldWithFunction('id', 'makeLink');
$tbl_incident->formatFieldWithFunction('status', 'showIcon');
$tbl_incident->formatFieldWithFunction('created', 'formatDate');
$tbl_incident->deleteText = "delete";
if ($incident_admin !== "true") {
    #i can disallow deleting of rows from the table
    $tbl_incident->disallowDelete();
}
?>

<body>
<!-- Primary Page Layout
================================================== -->   
<!-- This is the sidebar -->
<div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <a class="navmenu-brand" href="../"><?php echo $banner ?></a>
    <ul class="nav navmenu-nav">
        <?php
        // Show the sidebar items
        include("../../includes/sidebar-menu-items.php");
        ?>
    </ul>
</div>
<!-- This is main the navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand navbar-center" href="../"><?php echo $banner ?></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <?php
        // Control the view of menu items for the logged user       
        // Show the menu items
        include("../../includes/navbar-menu-items.php");        
        ?>
        </ul>
        <!-- Show user profile and logout option -->
        <ul class="nav navbar-nav navbar-right">
            <?php           
            // Show the right menu items
            include("../../includes/navbar-right-menu-items.php");           
            ?>
        </ul>
    </div>
    </div>
</nav>
<div  class="container">    
    <div class="row">
        <div class="col-xs-12">
            <?php
            // my self-defined functions used for formatFieldWithFunction
            function makeLink($val) {
                if (empty($val)) return "";
                    return "<a href='../report.php?id=$val'>$val</a>";
            }
            function showIcon($val) {
                if ($val == 1) { return "<span style='color:#FFA544;'><i class='fas fa-envelope-open-text'></i></span>"; }
                if ($val == 0) { return "<span style='color:#D9534F;'><i class='fas fa-minus-circle'></i></span>"; }
            }   
            function formatDate($val) { return date("d-m-Y H:i:s", strtotime($val)); }               
            // actually show the table if user has access
            if ($incident_manager == "true" || $incident_admin == "true") {                    
                $tbl_incident->showTable();
                # Output number of rows returned
                echo "<button type='button' class='btn btn-static'>Total rows: ";
                $tbl_incident->insertRowsReturned();
                echo "</button>";
                echo "&nbsp;<a type='button' class='btn btn-default' data-toggle='modal' data-target='#exportModal'>Export to file</a>";
                echo "<br /><br />";
                // Export modal
                include ("../../export/expmodal.php");
            }
            else {
                // If user is not authorised, this message will appear
                echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
                echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
            }   
            ?>  
        </div>
    </div>
    <br /><br />       
</div>
</body>
</html>