<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
// Define user's identity and access
require_once("../../includes/access.php");
// Get values for displaying texts on the page
require_once("../../includes/texts.php");
// Limit access to key admins only
if ($incident_admin == "true") {
	// Delete participant from report
	if (isset($_GET['delhod'])) {
		$query = "SELECT hod_crsid FROM incident WHERE id=:id LIMIT 1";
        $stmt = $db->prepare($query);
        $stmt->execute(array("id"=>$_GET['id']));
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        	$hod_arr = explode(",", $row['hod_crsid']);
            if (in_array($_GET['crsid'], $hod_arr)) { // Check if crsid exist in the array
            	$hod_arr = array_diff($hod_arr, array($_GET['crsid'])); // Remove the crsid
            	$hod_str = implode(",", $hod_arr); // Convert back to string
            	// Update the table
            	$query2 = "UPDATE incident SET hod_crsid=:hod_crsid WHERE id=:id";
            	$stmt2 = $db->prepare($query2);
            	$stmt2->execute(array("hod_crsid"=>$hod_str,"id"=>$_GET['id']));
            }	
        }
        echo $_GET['id'];
	}
}
else {
    echo "<h4><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;&nbsp;".$unauthorised."</h4><br><p>".$unauthorised_msg."</p><br>";
    echo "<p><a href='../../' class='btn btn-primary active' role='button'>".$site_home."</a></p>";
}
?>