// Delete participant from report
$('.delhod').click( function(){
	var id = $(this).attr('data-id');
	var crsid = $(this).attr('data-crsid');
	$.ajax({
        type: 'POST',
        url: 'admin/queries.php?delhod&id='+id+'&crsid='+crsid,
        beforeSend: function(){
            return confirm("Are you sure you want to remove this participant?");
        },
        success: function(id) {
            //Reload page;
			$(location).attr('href', './report.php?id='+id);
        },
        error:function(err){
            alert("error"+JSON.stringify(err));
        }
    });
});
// Mark file for deletion and fade out from page
$("#attCol").on('click', '.btn-remove', function() {
    $(this).closest('div').find('[type=checkbox]').prop('checked', true);
    $(this).closest('div').fadeOut("slow");
});
// Maybe try this in the future -- http://hayageek.com/docs/jquery-upload-file.php#customui
// Dynamically add input fields for multiple file upload
$(document).on('click', '.btn-add', function(e) {
    e.preventDefault();                     
    var controlForm = $('.controls:first'),
        currentEntry = $(this).parents('.entry:first'),
        newEntry = $(currentEntry.clone()).appendTo(controlForm);                       
    newEntry.find('input').val('');
    controlForm.find('.entry:not(:last) .btn-add')
        .removeClass('btn-add').addClass('btn-remove')
        .removeClass('btn-default').addClass('btn-danger')
        .html('<span class="glyphicon glyphicon-minus"></span>');
}).on('click', '.btn-remove', function(e) {
    $(this).parents('.entry:first').remove();                       
    e.preventDefault();
    return false;
});
// FUNCTIONS
// Validate size of files
function validate() {
    $('input[name="fileToUpload[]"]').each(function() {
        if(this.files[0].size > 5242880){
           alert("File is too big!");
           this.value = "";
        };
    });
}