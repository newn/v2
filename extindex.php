<!-- This is specific to Newnham College -->
<div class="col-xs-12 col-sm-12 col-md-12"><h5><span style="color: #404040;">More Apps:</span></h5></div>
<?php
if($other_manager == "true" || $other_admin == "true") {
?>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="../ofms/shifts/" target="_blank"><i class="fa fa-calendar"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="../ofms/shifts/" target="_blank">Shifts</a>
        </div>
    </div>
</div>
<?php 
} 
if($other_manager == "true" || $other_admin == "true") {
?>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="../ofms/callouts/"><i class="fa fa-phone"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="../ofms/callouts/">Callouts</a>
        </div>
    </div>
</div>
<?php
}
if($other_admin == "true") {
?>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="http://192.168.210.50:3000" target="_blank"><i class="fa fa-desktop"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="http://192.168.210.50:3000" target="_blank">Signage</a>
        </div>
    </div>
</div>
<?php
}
if($logged_usr_assoc == "student" || $other_manager == "true" || $other_admin == "true") {
?>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="../ofms/srb/" target="_blank"><i class="fas fa-calendar-alt"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="../ofms/srb/" target="_blank">Rooms</a>
        </div>
    </div>
</div>
<?php
}
if($logged_usr_assoc == "staff") {
?>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="../smfd/"><i class="fas fa-utensils"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="../smfd/">Dinners</a>
        </div>
    </div>
</div>
<?php
}
if($logged_usr_assoc == "student" || $other_manager == "true" || $other_admin == "true") {
?>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="../ofms/gym/"><i class="fa fa-link"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="../ofms/gym/">Gym</a>
        </div>
    </div>
</div>
<?php
}
if($other_manager == "true" || $other_admin == "true") {
?>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="../ofms/accident/"><i class="fa fa-medkit"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="../ofms/accident/">Accident</a>
        </div>
    </div>
</div>
<?php
}
if($logged_usr_assoc == "staff") {
?>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="../ofms/parking/book"><i class="fab fa-product-hunt"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="../ofms/parking/book">Carpark</a>
        </div>
    </div>
</div>
<?php
}
if($other_manager == "true" || $other_admin == "true") {
?>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="https://www.camsis.cam.ac.uk/"><i class="fas fa-users"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="https://www.camsis.cam.ac.uk/">CamSIS</a>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="http://saltosvr:8100/index.html#!/login"><i class="fas fa-id-card-alt"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="http://saltosvr:8100/index.html#!/login">Salto</a>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="https://app.casc.cam.ac.uk/rms_live"><i class="fas fa-users"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="https://app.casc.cam.ac.uk/rms_live">CASC RMS</a>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="https://www.cam.ac.uk/email-and-phone-search"><i class="far fa-address-book"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="https://www.cam.ac.uk/email-and-phone-search">Lookup</a>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="https://www.newn.cam.ac.uk/"><i class="fas fa-globe-americas"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="https://www.newn.cam.ac.uk/">Newnham</a>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="https://modgovintranet.newn.cam.ac.uk/uuCoverPage.aspx?bcr=1&TPID=34176&$LO$=1"><i class="fas fa-users"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="https://modgovintranet.newn.cam.ac.uk/uuCoverPage.aspx?bcr=1&TPID=34176&$LO$=1">Intranet</a>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 block">
    <div class="tile app">
        <div class="col-xs-3 col-sm-3 col-md-3 icon-sm"><a href="https://map.cam.ac.uk/"><i class="fas fa-map-marked-alt"></i></a></div>
        <div class="col-xs-9 col-sm-9 col-md-9 title"><a href="https://map.cam.ac.uk/">Uni Map</a>
        </div>
    </div>
</div>
<?php
}
?>