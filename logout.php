<?php
header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
header ("Pragma: no-cache");
/*
// Include config file
require_once("includes/conf.php");
// Redirect to ucam webauth logout directive
if ($univ_id == "cam") {
	echo '<meta http-equiv="refresh" content="0;url=/logout.html" />';
}
*/
?>
<html>
<body>
	<p align="center" style="margin-top: 20px;"><b>In order to completely logoff, please close your browser.</b></p>
	<p align="center">Click the close button on the top-right corner of your browser, see sample below:<br><br><img src="includes/logout.png" border="1"></p>
</body>
</html>